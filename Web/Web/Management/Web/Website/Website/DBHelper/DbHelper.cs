﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Umbraco.Core.Persistence;

namespace Website.DBHelper
{
    public static class DbHelper
    {
        public static Database CurrentDb()
        {
            if (HttpContext.Current.Items["CurrentDb"] == null)
            {
                var retval =
                    new Database(ConfigurationManager.ConnectionStrings["CustomDatabaseConnection"].ConnectionString,
                        ConfigurationManager.ConnectionStrings["CustomDatabaseConnection"].ProviderName);
                HttpContext.Current.Items["CurrentDb"] = retval;

                return retval;
            }

            return (Database)HttpContext.Current.Items["CurrentDb"];
        }
    }
}