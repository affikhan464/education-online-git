﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Persistence;
using Umbraco.Web.Mvc;
using Website.Models;
using SendGrid;
using System.IO;
using Newtonsoft.Json;

namespace Website.Controllers
{
    public class HomeController : SurfaceController
    {
   
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public string ContactUs(string Body, string UserEmail, string UserName)
        {
            try
            {
                var model = new Mail()
                {
                    Body = Body,
                    UserEmail = UserEmail,
                    UserName = UserName,
                    IsTeacher = false
                };
                MessageUs(model);

                return JsonConvert.SerializeObject(BaseModel.Succeeded(" Thanks for contacting us, we will be in touch with you soon."));
            }
            catch (System.Exception ex)
            {               
                return JsonConvert.SerializeObject(BaseModel.Failed("Message not sent due to some unknown reason, Try again later, Thanks"));

            }

        }
        public string SubjectExpertContactUs(string Expertise, string UserEmail, string UserName, string Mobile, string Subject, string School)
        {
            try
            {
                var model = new Mail()
                {
                    Body = Expertise,
                    UserEmail = UserEmail,
                    UserName = UserName,
                    Subject = Subject,
                    Mobile = Mobile,
                    School = School,
                    IsTeacher = true
                };
                SubjectExpertMessageUs(model);
                var resp=JsonConvert.SerializeObject(BaseModel.Succeeded(" Thanks for showing your interest, we will be in touch with you soon."));
                return resp;
            }
            catch (System.Exception ex)
            {               
               return JsonConvert.SerializeObject(BaseModel.Failed("Message not sent due to some unknown reason, Try again later, Thanks"));

            }

        }
        public bool MessageUs(Mail model)
        {
            var configuration = ConfigurationManager.AppSettings;
            var mail = new Mail();
            var LoginLink = configuration["AppLink"];
            var FromEmail = configuration["FromEmail"];
            var ToAdminEmail = configuration["ToAdminEmail"];
            var weblink = configuration["SiteLink"];
            


            string html = System.IO.File.ReadAllText(Server.MapPath("~")+"/EmailTemplates/ContactUs.html", Encoding.UTF8);
            html = html.Replace("##Body##", model.Body);
            html = html.Replace("##Name##", model.UserName);
            html = html.Replace("##OtherDetails##", string.Empty);
            html = html.Replace("##UserType##", "");
            html = html.Replace("##UserEmail##", model.UserEmail);
            html = html.Replace("##EnvirontmentUrl##", weblink);
            html = html.Replace("##LoginLink##", LoginLink);

            var htmlWithLogo = GenerateBody(html);

            mail.FromEMailAddress = FromEmail;
            mail.ToEMailAddress=ToAdminEmail;
            mail.Subject = model.UserName + " Just contact us from website";
            mail.AlternativeView = htmlWithLogo;
            SendMail(mail);

            return true;


        }
        public bool SubjectExpertMessageUs(Mail model)
        {
            var configuration = ConfigurationManager.AppSettings;
            var mail = new Mail();
            var LoginLink = configuration["AppLink"];
            var FromEmail = configuration["FromEmail"];
            var ToAdminEmail = configuration["ToAdminEmail"];
            var weblink = configuration["SiteLink"];
            string html = System.IO.File.ReadAllText(Server.MapPath("~")+"/EmailTemplates/SubjectExpertContactUs.html", Encoding.UTF8);
            html = html.Replace("##Body##", model.Body);
            html = html.Replace("##OtherDetails##", "His Current School/College/University: <b>" + model.School + "</b>,<br> Mobile Number: <b>" + model.Mobile + "</b>,<br> Interested in Subject: <b>" + model.Subject+ "</b>");
            html = html.Replace("##Name##", model.UserName);
            html = html.Replace("##UserType##", "Subject Expert ");
            html = html.Replace("##UserEmail##", model.UserEmail);
            html = html.Replace("##EnvirontmentUrl##", weblink);
            html = html.Replace("##LoginLink##", LoginLink);

            var htmlWithLogo = GenerateBody(html);

            mail.FromEMailAddress = FromEmail;
            mail.ToEMailAddress=ToAdminEmail;
            mail.Subject = model.Subject + " Subject Expert " + model.UserName + " just shown interest in Ilum.";
            mail.AlternativeView = htmlWithLogo;
            SendMail(mail);

            return true;


        }
        private AlternateView GenerateBody(string html)
        {
            var configuration = ConfigurationManager.AppSettings;
            var filePath = Server.MapPath("~") + "/images/logo.png";
            LinkedResource res = new LinkedResource(filePath);
            res.ContentId = Guid.NewGuid().ToString();
            var logo = @"<img width='240' src='https://app.theilum.com/images/logo.png'/>";
            html = html.Replace("##LogoUrl##", logo);

            var avHtml = AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html);
            avHtml.LinkedResources.Add(res);
            return avHtml;
        }
        public bool SendMail(Mail model)
        {
            var configuration = ConfigurationManager.AppSettings;

            Environment.SetEnvironmentVariable("SENDGRID_KEY", "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");
            var apiKey = Environment.GetEnvironmentVariable("SENDGRID_KEY");
            var FromEmail = configuration["FromEmail"];
            
            var subject = model.Subject;
            var stream = model.AlternativeView.ContentStream;
            var htmlBody = string.Empty;
            using (var reader = new StreamReader(stream))
            {
                htmlBody = reader.ReadToEnd();
            }

            var myMessage = new SendGridMessage();
            myMessage.AddTo(model.ToEMailAddress);
            myMessage.From = new MailAddress(FromEmail, "Class Online");
            myMessage.Subject = subject;
            myMessage.Html = htmlBody;

            var transportWeb = new SendGrid.Web(apiKey);
            transportWeb.DeliverAsync(myMessage);

            return true;


        }
    }
}