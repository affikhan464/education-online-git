﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class Subject
    {
       
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortNumber { get; set; }
        public int ClassId { get; set; }
        public string UserId { get; set; }
        public int? IconId { get; set; }
        public string Detail { get; set; }
    }
}