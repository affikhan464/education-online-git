﻿
$(document).ready(function () {
    emptyAllDDL("#SelectSubject");
    loadEducationLevels("#SelectSubject");
});

$(document).on("click", ".small-card", function () {
    var studyType = this.dataset.studytype;
    var step = $(this).parents(".tab-pane").attr("id");
    $("#" + step + " .small-card").removeClass("selected");
    if (step == "ModeOfStudy") {
        $("[name=StudyType]").val(studyType);
    } if (step == "Teacher") {
        var card = $(this);
        card.find('input[type=radio]').prop("checked", true);
        card.addClass("selected");
    }

    $(this).addClass("selected");
    var tabPanId = $(this).parents(".tab-pane").attr("id");
    $(".nav-link[href='#" + tabPanId + "']").removeClass("error");
});

$(document).on("click", ".enrollment .btnNext", function () {
    var nextstep = this.dataset.nextstep;
    if (nextstep == "SelectSubject") {
        //first validate previous step
        if (validateModeOfStudyStep()) {
            $(".nav-link[href]").removeClass("active");
            $(".nav-link[href='#" + nextstep + "']").addClass("active");
            $(".tab-pane").removeClass("active");
            $("#" + nextstep + ".tab-pane").addClass("active");

        }
    }
    if (nextstep == "Teacher") {
        //first validate previous step
        if (validateModeOfStudyStep()) {
            if (validateSubjectStep()) {
                loadTeachersCardViewBySubjectId(".teacherRenderer", $("[name=SubjectId]").val());
                $(".nav-link[href='#SelectSubject']").removeClass("error");
                $(".nav-link[href]").removeClass("active");
                $(".nav-link[href='#" + nextstep + "']").addClass("active");
                $(".tab-pane").removeClass("active");
                $("#" + nextstep + ".tab-pane").addClass("active");

            }
        }
    }
    if (nextstep == "Batches") {
        //first validate previous step
        if (validateModeOfStudyStep()) {
            if (validateSubjectStep()) {
                if (validateTeacherStep()) {
                    loadBatchByTeacherAndSubjectId($("[name=BatchTeacherId]:checked").val(), $("[name=SubjectId]").val(), ".batchRenderer");
                    $(".nav-link[href='#Teacher']").removeClass("error");
                    $(".nav-link[href]").removeClass("active");
                    $(".nav-link[href='#" + nextstep + "']").addClass("active");
                    $(".tab-pane").removeClass("active");
                    $("#" + nextstep + ".tab-pane").addClass("active");
                }
            }
        }
    }

});

function validateModeOfStudyStep() {
    if (!$("[name=StudyType]").val().length) {
        $(".nav-link[href='#ModeOfStudy']").addClass("error");
        smallSwal("Select your mode of study, Book or Distance learning.", "", "error");
        return false;
    }
    else {
        return true;
    }

}
function validateSubjectStep() {
    if ($("[name=SubjectId]").val() == "0") {
        $(".nav-link[href='#SelectSubject']").addClass("error");
        smallSwal("Select subject please.", "", "error");
        return false;
    }
    else {
        return true;
    }

}
function validateTeacherStep() {
    if (!$("[name=BatchTeacherId]:checked").length) {
        $(".nav-link[href='#Teacher']").addClass("error");
        smallSwal("Select teacher please.", "", "error");
        return false;
    }
    else {

        return true;

    }
}

$(document).on('click', '#enrollForm #enrollSaveBtn', function () {
    var $form = $('#enrollForm');
    $form.off("submit");
    $form.submit(function (data, event) {
        $("#enrollSaveBtn i").removeClass("ti-arrow-right");
        $("#enrollSaveBtn i").addClass("fas fa-spinner fa-spin");
        var isFormValid = $form.valid();
        if (isFormValid) {
            $.post($(this).attr('action'), $(this).serialize(), function (response) {
                //can do here
            }, 'json').done(function (data) {
                if (data.success) {
                    smallSwal({
                        type: 'success',
                        title: data.message
                    });

                    setTimeout(function () {
                        location.href = location.href;
                    }, 1000)
                } else {
                    swal("Error!!", data.message, "error")
                    $("#enrollSaveBtn i").addClass("ti-arrow-right");
                    $("#enrollSaveBtn i").removeClass("fas fa-spinner fa-spin");
                }

            });
            return false;
        } else {
            $("#enrollSaveBtn i").addClass("ti-arrow-right");
            $("#enrollSaveBtn i").removeClass("fas fa-spinner fa-spin");
        }

    });
    if (validateModeOfStudyStep()) {
        if (validateSubjectStep()) {
            if (validateTeacherStep()) {
                $form.submit();
            }
        }
    }

});