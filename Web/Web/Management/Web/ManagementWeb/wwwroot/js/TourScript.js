﻿/*! TourScript.js
 
 */

$(document).ready(function () {
    isTourNeeded = true;

        takeTour();
    
});
$(document).on("click", ".tour-tour [data-role=end]", function () {

    $.ajax({
        url: '/Home/EndTour',
        type: "Post",
        success: function (response) {
            isTourNeeded = response;
        },
        error: function (error) {
            returnErrorState(error);
        }
    });

});
function takeTour() {
    if (isTourNeeded) {
        $.ajax({
            url: '/Home/IsTourTaken',
            type: "Get",
            success: function (response) {
                isTourNeeded = response;

                startTour();
            },
            error: function (error) {
                returnErrorState(error);
            }
        });
    }
}

function startTour() {
    if (isTourNeeded) {

        isResearcher = $(".LoggedUserRole").val() == "Researcher";
        isStudent = $(".LoggedUserRole").val() == "Student";

        if ($("#RequestChapterAllocationModal.in").length > 0 && (isResearcher || isStudent)) {
            takeAddChapterRequestTour();
        }
        else if ($("#RequestSubjectAllocationModal.in").length > 0 && (isResearcher || isStudent)) {
            takeAddSubjectRequestTour();
        }
        else if ($(".AllocatedSubjectsPage").length > 0 && (isResearcher || isStudent)) {
            takeAllocatedSubjectsPageTour();
        }
        else if ($(".AllocatedChaptersPage").length > 0 && (isResearcher || isStudent)) {
            takeAllocatedChaptersPageTour();
        }
        else if ($(".RequestedSubjectsPage").length > 0 && (isResearcher || isStudent)) {
            takeRequestedSubjectsPageTour();
        }
        else if ($(".RequestedChaptersPage").length > 0 && (isResearcher || isStudent)) {
            takeRequestedChaptersPageTour();
        }

        else if ($(".NoRequestedSubjectsPage").length > 0 && (isResearcher || isStudent)) {
            takeNoRequestedSubjectsPageTour();
        }
        else if ($(".NoRequestedChaptersPage").length > 0 && (isResearcher || isStudent)) {
            takeNoRequestedChaptersPageTour();
        }

        else if ($(".opened-attribute").length > 0 && (isResearcher || isStudent)) {
            takeViewAttributesPageTour();
        }
        else if ($(".DashboadPage").length > 0 && !$(".modal.in").length > 0 && (isResearcher || isStudent)) {
            takeHomePageTour();
        }
        else {
            $(".popover.tour-tour ").remove();
        }
    }
}

//Chapter Related Tours
function takeAddChapterRequestTour() {
    isAddChapterRequestTourRequired = true;

    if (isAddChapterRequestTourRequired) {
        tour = new Tour({
            steps: [
                {
                    element: "#RequestChapterAllocationModal .EducationLevelDDL ",
                    content: "<ul><li>First select Discipline for your chapter.</li> <li>Press Next for next step to perform.</li><ul>",
                    placement: 'left'
                },
                {
                    element: "#RequestChapterAllocationModal .BoardLevelDDL",
                    content: "<ul><li>Now select Board/Body for your chapter.</li> <li>Press Next for next step to perform.</li><ul>",
                    placement: 'right'
                },
                {
                    element: "#RequestChapterAllocationModal .ClassDDL",
                    content: "<ul><li>Now select Class for your chapter.</li> <li>Press Next for next step to perform.</li><ul>",
                    placement: 'left'
                },
                {
                    element: "#RequestChapterAllocationModal .SubjectDDL",
                    content: "<ul><li>Now select Subject for a chapter.</li> <li>Press Next for next step to perform.</li><ul>",
                    placement: 'right'
                },
                {
                    element: "#RequestChapterAllocationModal .ChapterDDL",
                    content: "<ul><li>Now select a chapter.</li> <li>Press Next for next step to perform.</li><ul>",
                    placement: 'left'
                },
                {
                    element: "#RequestChapterAllocationModal button.btn-success",
                    content: "<ul><li>Press Add Request Button to finally add request.</li> <li>Press End Tour button to exit this tour.</li><ul>",
                    placement: 'auto'
                }
            ]
        });
        tour.init();
        tour.restart();
    }
}

function takeRequestedChaptersPageTour() {
    isRequestedChaptersPageRequired = true;

    if (isRequestedChaptersPageRequired) {
        tour = new Tour({
            steps: [
                {
                    element: ".RequestedChaptersPage #AllChapterRequests table tbody tr:first-child",
                    content: "<ul><li>These are Chapters Requested by you.</li> <li>Press Next.</li><ul>",
                    placement: 'bottom'
                },
                {
                    element: ".RequestedChaptersPage .requestChapterAllocationOpenBtn",
                    content: "<ul><li>You can add more Chapter requests from here.</li> <li>Press Next.</li><ul>",
                    placement: 'auto'
                },
                {
                    element: ".RequestedChaptersPage #AllChapterRequests table tbody tr:first-child .deleteAllocatedChapterRequestBtn",
                    content: "<ul><li>You can remove Chapter request from here.</li> <li>Press End Tour.</li><ul>",
                    placement: 'auto'
                }
            ]
        });
        tour.init();
        tour.restart();
    }
}

function takeAllocatedChaptersPageTour() {
    isAllocatedChaptersPageRequired = true;

    if (isAllocatedChaptersPageRequired) {
        tour = new Tour({
            steps: [
                {
                    element: ".AllocatedChaptersPage table tbody tr:first-child",
                    content: "<ul><li>These are Chapters allocated to you</li> <li>Press Next.</li><ul>",
                    placement: 'bottom'
                },
                {
                    element: ".AllocatedChaptersPage table tbody tr:first-child .fa-info-circle",
                    content: "<ul><li>You can start Reading Chapter using this icon.</li> <li>Press Next.</li><ul>",
                    placement: 'left'
                },
                {
                    element: ".AllocatedChaptersPage table tbody tr:first-child .viewbutton",
                    content: "<ul><li>You can view chapter/section/sub-section of this Chapter using this icon.</li> <li>Press Next.</li><ul>",
                    placement: 'left'
                },
                {
                    element: ".AllocatedChaptersPage table tbody .badge-dark",
                    content: "<ul><li>This Chapter request is still pending, wait for Administrator to approve it.</li> <li>Press Next.</li><ul>",
                    placement: 'left'
                },
                {
                    element: ".AllocatedChaptersPage table tbody .re-requestChapterAllocation",
                    content: "<ul><li>You can request this Chapter again using this icon.</li> <li>Press Next.</li><ul>",
                    placement: 'left'
                },
                {
                    element: ".sidebar .showChapterRequestsBtn",
                    content: "<ul><li>You can request for more Chapters by clicking here.</li><ul>",
                    placement: 'auto'
                }
            ]
        });
        tour.init();
        tour.restart();
    }
}

function takeNoRequestedChaptersPageTour() {
    isNoRequestedChaptersPageRequired = true;

    if (isNoRequestedChaptersPageRequired) {
        tour = new Tour({
            steps: [
                {
                    element: " .requestChapterAllocationOpenBtn",
                    content: "<ul><li>If you want to request for a Chapter then click here.</li><ul>",
                    placement: 'auto'
                }
            ]
        });
        tour.init();
        tour.restart();
    }
}

//Subject Related Tours
function takeAddSubjectRequestTour() {
    isSubjectRequestTourRequired = true;

    if (isSubjectRequestTourRequired) {
        tour = new Tour({
            steps: [
                {
                    element: "#RequestSubjectAllocationModal .EducationLevelDDL ",
                    content: "<ul><li>First select Discipline for your Subject.</li> <li>Press Next for next step to perform.</li><ul>",
                    placement: 'left'
                },
                {
                    element: "#RequestSubjectAllocationModal .BoardLevelDDL",
                    content: "<ul><li>Now select Board/Body for your Subject.</li> <li>Press Next for next step to perform.</li><ul>",
                    placement: 'right'
                },
                {
                    element: "#RequestSubjectAllocationModal .ClassDDL",
                    content: "<ul><li>Now select Class for your Subject.</li> <li>Press Next for next step to perform.</li><ul>",
                    placement: 'left'
                },
                {
                    element: "#RequestSubjectAllocationModal .SubjectDDL",
                    content: "<ul><li>Now select Subject.</li><li>Press Next for next step to perform.</li><ul>",
                    placement: 'right'
                },
                {
                    element: "#RequestSubjectAllocationModal button.btn-success",
                    content: "<ul><li>Press Add Request Button to finally add request.</li> <li>Press End Tour button to exit this tour.</li><ul>",
                    placement: 'auto'
                }
            ]
        });
        tour.init();
        tour.restart();
    }
}
function takeRequestedSubjectsPageTour() {
    isRequestedSubjectsPageRequired = true;

    if (isRequestedSubjectsPageRequired) {
        tour = new Tour({
            steps: [
                {
                    element: ".RequestedSubjectsPage #AllRequests table tbody tr:first-child",
                    content: "<ul><li>These are Subjects Requested by you</li> <li>Press Next.</li><ul>",
                    placement: 'bottom'
                },
                {
                    element: ".RequestedSubjectsPage .requestSubjectAllocationOpenBtn",
                    content: "<ul><li>You can add more Subject requests from here.</li> <li>Press Next.</li><ul>",
                    placement: 'auto'
                },
                {
                    element: ".RequestedSubjectsPage #AllRequests table tbody tr:first-child .deleteAllocatedSubjectRequestBtn",
                    content: "<ul><li>You can remove Subject request from here.</li> <li>Press End Tour.</li><ul>",
                    placement: 'left'
                }
            ]
        });
        tour.init();
        tour.restart();
    }
}

function takeAllocatedSubjectsPageTour() {
    isAllocatedSubjectsPageRequired = true;

    if (isAllocatedSubjectsPageRequired) {
        tour = new Tour({
            steps: [
                {
                    element: ".AllocatedSubjectsPage table tbody tr:first-child",
                    content: "<ul><li>These are Subjects allocated to you</li> <li>Press Next.</li><ul>",
                    placement: 'bottom'
                },
                {
                    element: ".AllocatedSubjectsPage table tbody tr:first-child .fa-info-circle",
                    content: "<ul><li>You can start Reading Subject using this icon.</li> <li>Press Next.</li><ul>",
                    placement: 'left'
                },
                {
                    element: ".AllocatedSubjectsPage table tbody tr:first-child .viewbutton",
                    content: "<ul><li>You can view chapter/section/sub-section of this Subject using this icon.</li> <li>Press Next.</li><ul>",
                    placement: 'left'
                },
                {
                    element: ".AllocatedSubjectsPage table tbody .badge-dark",
                    content: "<ul><li>This subject request is still pending, wait for Administrator to approve it.</li> <li>Press Next.</li><ul>",
                    placement: 'left'
                },
                {
                    element: ".AllocatedSubjectsPage table tbody .re-requestSubjectAllocation",
                    content: "<ul><li>You can request this Subject again using this icon.</li> <li>Press Next.</li><ul>",
                    placement: 'left'
                },
                {
                    element: ".sidebar .showRequestsBtn",
                    content: "<ul><li>You can request for more subjects by clicking here.</li><ul>",
                    placement: 'auto'
                }
            ]
        });
        tour.init();
        tour.restart();
    }
}

function takeNoRequestedSubjectsPageTour() {
    isNoRequestedSubjectsPageRequired = true;

    if (isSubjectRequestTourRequired) {
        tour = new Tour({
            steps: [
                {
                    element: " .requestSubjectAllocationOpenBtn ",
                    content: "<ul><li>If you want to request for a Subject then click here.</li><ul>",
                    placement: 'auto'
                }
            ]
        });
        tour.init();
        tour.restart();
    }
}

//ViewAttribute
function takeViewAttributesPageTour() {
    isViewAttributeRequired = true;

    if (isViewAttributeRequired) {
        tour = new Tour({
            steps: [
                {
                    element: ".opened-attribute .page-header .fa-info-circle",
                    content: "<ul><li>You can start Reading using this icon </li> <li>Press Next.</li><ul>",
                    placement: 'left'
                },
                {
                    element: ".opened-attribute .all-attributes .attributesRow:first-child",
                    content: "<ul><li>These are " + $(".opened-attribute .all-attributes .attributesRow:first-child").data().attributetype + " of the opened " + $(".opened-attribute").data().attributetype + ".</li> <li>Press Next.</li><ul>",
                    placement: 'auto'
                },
                {
                    element: ".opened-attribute .all-attributes .attributesRow:first-child .fa-info-circle",
                    content: "<ul><li>You can start reading this " + $(".opened-attribute .all-attributes .attributesRow:first-child").data().attributetype + " using this icon.</li> <li>Press Next.</li><ul>",
                    placement: 'left'
                },
                {
                    element: ".opened-attribute .all-attributes .attributesRow:first-child .viewbutton",
                    content: "<ul><li>Press this icon to view this document.</li> <li>Press Next.</li><ul>",
                    placement: 'left'
                }
            ]
        });
        tour.init();
        tour.restart();
    }
}
function takeHomePageTour() {
    isHomePageTourRequired = true;

    if (isHomePageTourRequired) {
        var steps = [
            {
                element: "#page-wrapper .myAllocatedSubjects ",
                content: "<ul><li>Clicking here will take you to Subjects Allocated to you.</li> <li>Press Next.</li><ul>",
                placement: 'auto'
            },
            {
                element: "#page-wrapper .myAllocatedChapters ",
                content: "<ul><li>Clicking here will take you to Chapters Allocated to you.</li> <li>Press Next.</li><ul>",
                placement: 'auto'
            }, {
                element: "#page-wrapper .showRequestsBtn ",
                content: "<ul><li>Clicking here will take you to list of Subjects Requsted by you.</li> <li>Press Next.</li><ul>",
                placement: 'auto'
            },
            {
                element: "#page-wrapper .requestSubjectAllocationOpenBtn",
                content: "<ul><li>Clicking here will take you to add Request for Subject allocation.</li> <li>Press Next.</li><ul>",
                placement: 'auto'
            },
            {
                element: "#page-wrapper .showChapterRequestsBtn ",
                content: "<ul><li>Clicking here will take you to list of Chapters Requsted by you for allocation.</li> <li>Press Next.</li><ul>",
                placement: 'auto'
            },
            {
                element: "#page-wrapper .requestChapterAllocationOpenBtn",
                content: "<ul><li>Clicking here will take you to add request for chapter allocation.</li> <li>Press Next.</li><ul>",
                placement: 'auto'
            }, {
                element: "#page-wrapper .myBatches",
                content: "<ul><li>Clicking here will take you to batches in which you are enrolled.</li> <li>Press Next.</li><ul>",
                placement: 'auto'
            }, {
                element: "#page-wrapper .newEnrollmenet ",
                content: "<ul><li>Click here to enroll in one more batch.</li> <li>Press Next.</li><ul>",
                placement: 'auto'
            },
            {
                element: ".sidebar .myAllocatedSubjects ",
                content: "<ul><li>On clicking here will also take you to Subjects Allocated to you.</li> <li>Press Next.</li><ul>",
                placement: 'auto'
            },
            {
                element: ".sidebar .myAllocatedChapters ",
                content: "<ul><li>On clicking here will also take you to Chapters Allocated to you.</li> <li>Press Next.</li><ul>",
                placement: 'auto'
            },
            {
                element: ".sidebar .showChapterRequestsBtn ",
                content: "<ul><li>Clicking here will take you to list of Chapters Requsted by you for allocation.</li> <li>Press Next.</li><ul>",
                placement: 'auto'
            },
            {
                element: ".sidebar .showRequestsBtn ",
                content: "<ul><li>Clicking here will also take you to list of Subjects Requsted by you.</li><ul>",
                placement: 'auto'
            }
        ];

        //if ($("#page-wrapper .showChapterRequestsBtn").length > 0) {
        //    var step1 = ;
        //    var step2 = ;
        //    steps.push(step1);
        //    steps.push(step2);
        //}
        //else {

        //    var step1 = ;
        //    var step2 =;
        //    steps.push(step1);
        //    steps.push(step2);

        //}


        tour = new Tour({
            steps
        });
        tour.init();
        tour.restart();
    }
}
