﻿/*! ChapterAndSubjectAllocationScriptNotAdmin.js
 
 */
    $(document).on("click", "#RequestChapterAllocationModal #requestChapterAllocationBtn", function () {

        var chapterId = $("#RequestChapterAllocationModal .ChapterDDL .dd-selected-value").val();
        if (chapterId != 0 && chapterId != undefined) {
            requestChapterAllocation(chapterId);
        } else {
            swal("Select Subject", "<strong>Please Select Subject!</strong>", "error");
        }
    });

    $(document).on("click", ".requestChapterAllocationOpenBtn", function () {

        var btn = $(this);

        $("#RequestChapterAllocationModal").modal();
        loadEducationLevels("#RequestChapterAllocationModal");
        emptyAllDDL("#RequestChapterAllocationModal");

    });
    
    $(document).on("click", ".re-requestChapterAllocation", function () {
        var chapterId = this.dataset.chapterid;
        requestChapterAllocation(chapterId);
    });

    function requestChapterAllocation(chapterId) {
        $.ajax({
            url: '/Book/RequestAllocateChapter',
    type: "POST",
    data: { chapterId: parseInt(chapterId) },
    success: function (response) {
        if (response.success) {
            smallSwal({
                type: 'success',
                title: response.message
            });
            showChapterRequests();

        }
        else {
            swal("Error!", response.message, "error")

        }

    },
    error: function (error) {
        returnErrorState(error);
    }

});
}




/* Subject Allocation Scripts */


$(document).on("click", "#RequestSubjectAllocationModal #requestSubjectAllocationBtn", function () {

    var subjectId = $("#RequestSubjectAllocationModal .SubjectDDL .dd-selected-value").val();
    if (subjectId != 0 && subjectId != undefined) {
        requestSubjectAllocation(subjectId);
    } else {
        swal("Select Subject", "<strong>Please Select Subject!</strong>", "error");
    }
});

$(document).on("click", ".re-requestSubjectAllocation", function () {
    var subjectId = this.dataset.subjectid;
    requestSubjectAllocation(subjectId);
});

$(document).on("click", ".requestSubjectAllocationOpenBtn", function () {

    var btn = $(this);

    $("#RequestSubjectAllocationModal").modal();
    loadEducationLevels("#RequestSubjectAllocationModal");
    emptyAllDDL("#RequestSubjectAllocationModal");

});

function requestSubjectAllocation(subjectId) {
    $.ajax({
        url: '/Book/RequestAllocateSubject',
        type: "POST",
        data: { subjectId: parseInt(subjectId) },
        success: function (response) {
            if (response.success) {
                smallSwal({
                    type: 'success',
                    title: response.message
                });
                // swal("Done!", response.message, "success");
                showRequests();

            }
            else {
                swal("Error!", response.message, "error")

            }

        },
        error: function (error) {
            returnErrorState(error);
        }

    });
}

