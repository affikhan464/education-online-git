﻿/*!
Teacher.js
*/
$(document).on("click", ".teacherBtn", function () {
    loadTeacher();
});

$(document).on("click", ".addTeacherBtn", function () {
    $("#AddTeacher form").attr("action", "/Teacher/Register");
    $("#AddTeacher form textarea").val("");
    $("#AddTeacher form input:not([type=radio])").val("");
    $("#AddTeacher form #saveBtn").html("<i class='fas fa-user-plus'></i> Add");
    $("#AddTeacher .modal-header .type").text("Add new");
    $("#AddTeacher").modal();
    validateTeacherForm();
    appendclickOfAddTeacher();
});

$(document).on("click", ".inviteTeacherBtn", function () {

    $("#TeacherInviteModal").modal();
});

$(document).on("click", "#inviteTeacherForm #saveBtn", function () {


    var isValid = $("#inviteTeacherForm").valid();
});
$(document).on("click", "#addTeacherForm #saveBtn", function () {
    var isValid = $("#addTeacherForm").valid();
    if (isValid) {
        $("#addTeacherForm").submit();
    }
});
$(document).ready(function () {
    validateTeacherForm();
})

function validateTeacherForm() {
    /**
* Custom validator for contains at least one lower-case letter
*/
    $.validator.addMethod("atLeastOneLowercaseLetter", function (value, element) {
        return this.optional(element) || /[a-z]+/.test(value);
    }, "Must have at least one lowercase letter");

    /**
     * Custom validator for contains at least one upper-case letter.
     */
    $.validator.addMethod("atLeastOneUppercaseLetter", function (value, element) {
        return this.optional(element) || /[A-Z]+/.test(value);
    }, "Must have at least one uppercase letter");

    /**
     * Custom validator for contains at least one number.
     */
    $.validator.addMethod("atLeastOneNumber", function (value, element) {
        return this.optional(element) || /[0-9]+/.test(value);
    }, "Must have at least one number");

    /**
     * Custom validator for contains at least one symbol.
     */
    $.validator.addMethod("atLeastOneSymbol", function (value, element) {
        return this.optional(element) || /[!@@#$%^&*()]+/.test(value);
    }, "Must have at least one symbol");
    $.validator.addMethod("validCNIC", function (value, element) {
        return this.optional(element) || /^[0-9+]{5}-[0-9+]{7}-[0-9]{1}$/.test(value);
    });
    $('#addTeacherForm').validate({
        rules: {
            Password: {
                required: true,
                atLeastOneLowercaseLetter: true,
                atLeastOneUppercaseLetter: true,
                atLeastOneSymbol: true,
                atLeastOneNumber: true,
                minlength: 8,
            },
            CNIC: {
                required: true,
                minlength: 15,
                maxlength: 15,
                validCNIC: true

            }
        },
        messages: {
            CNIC: "Enter your valid CNIC number e.g. 12345-1234567-1."
             

        }

    })


}

function loadTeacher() {
    $.ajax({
        url: '/Teacher/Teachers',
        type: "Get",

        success: function (view) {
            $("#page-wrapper").html(view);
            applyTooltip();
            if (typeof takeTour === "function")
                takeTour();


        },
        error: function (error) {
            returnErrorState(error);
        }


    })
}


$(document).on("click", ".teacherViewbutton", function () {
    var userid = this.dataset.id;
    loadUserDetailById(userid);

});
function appendclickOfAddTeacher() {

    var $form = $('#addTeacherForm');
    $form.off("submit");
    $form.submit(function (data, event) {


        var isFormValid = $('#addTeacherForm').valid();

        if (isFormValid) {

            var btnHtml = $('#addTeacherForm #saveBtn').html();
            $('#addTeacherForm #saveBtn').addClass("disabledBtn");

            $('#addTeacherForm #saveBtn').html('<i class="fas fa-spinner fa-spin"></i>');
            $.post($(this).attr('action'), $(this).serialize(), function (response) {
                //can do here
            }, 'json').done(function (data) {
                if (data.success) {
                    smallSwal("Done!!", data.message, "success");
                    if ($("#detailPage").length > 0) {
                        var userid = document.getElementById("detailPage").dataset.userid;
                        loadUserDetailById(userid);
                        $('#addTeacherForm #saveBtn').removeClass("disabledBtn");
                        $('#addTeacherForm #saveBtn').html(btnHtml);
                    }
                    else {
                        loadTeacher();
                        $('#addTeacherForm #saveBtn').removeClass("disabledBtn");
                        $('#addTeacherForm #saveBtn').html(btnHtml);
                    }
                } else {
                    swal("Error!!", data.message, "error");
                    $('#addTeacherForm #saveBtn').removeClass("disabledBtn");
                    $('#addTeacherForm #saveBtn').html(btnHtml);

                }

            });
            return false;
        }

    });
}
$(document).on("click", ".teacherEditButton", function () {
    var userId = this.dataset.id;
    $.ajax({
        url: '/Teacher/Edit',
        type: "POST",
        data: { userId: userId },
        success: function (view) {
            $("#AddTeacher .form").html(view);
            $("#AddTeacher .modal-header .type").text("Edit");
            $("#AddTeacher form #saveBtn").text("Update");
            $("#AddTeacher").modal();
            validateTeacherForm();
            appendclickOfAddTeacher();
            setInputMaskt();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
});



$(document).on("click", ".teacherDeleteButton", function () {
    var userId = this.dataset.id;
    swal({
        title: "Are you sure?",
        text: "Are you sure to delete this teacher?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                $.ajax({
                    url: '/Teacher/Delete',
                    type: "POST",
                    data: { userId: userId },
                    success: function (response) {
                        if (response.success) {

                            swal("Deleted!", response.message, "success")
                            loadTeacher();
                        }
                        else {
                            swal("Error!", response.message, "error")

                        }

                    },
                    error: function (error) {
                        returnErrorState(error);
                    }

                });


            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });

});
