﻿

$(document).on("click", ".addBatchBtn", function () {
    emptyAllDDL("#addBatchModal");
    loadEducationLevels("#addBatchModal");
    $("#addBatchModal").modal();
});
$(document).on("click", ".selectableCard", function () {
    var card = $(this);
    var formId = card.parents(".tab-pane").attr('id');
    if (!formId) {
         formId = card.parents("form").attr('id');
    }
    $("#" + formId+ " .selectableCard").removeClass("selected");
    card.find('input[type=radio]').prop("checked", true);
    card.addClass("selected");
});
$(document).on("click", ".assignBatchTeacherButton", function () {
    var batchId = this.dataset.id;
    $.ajax({
        url: '/Batch/GetSelectableTeachers',
        type: "Get",
        success: function (view) {

            $("#assignBatchTeacherModal .modal-body .form").html(view);
            $("#assignBatchTeacherModal .modal-body .form [name=Id]").val(batchId);
            $("#assignBatchTeacherModal").modal();
        },
        error: function (error) {
            returnErrorState(error);
        }

    });
});
$(document).on("click", ".schedule-live-class", function () {
    var batchId = this.dataset.id;
    $("#schedule-live-class").modal();
    
});
$(document).on("click", ".editBatchButton", function () {
    var id = $(this).data("id");
    $.ajax({
        url: '/Batch/Edit',
        type: "Get",
        data: {
            id: id,
        },
        success: function (view) {
            $("#editBatchModal .modal-body .form").html(view);
            emptyAllDDL("#editBatchModal");
            loadEducationLevels("#editBatchModal");
            $("#editBatchModal").modal();
            editBatch();
        },
        error: function (error) {
            returnErrorState(error);
        }

    });

});

$(document).on("click", ".deleteBatchButton", function () {

    var id = this.dataset.id;
    swal({
        title: 'Are you sure?',
        html: 'Type <b>"delete"</b>, to erase this batch.',
        input: 'text',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Delete',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                if (text != 'delete') {
                    swal.showValidationError('Type correct spelling of delete.');
                    resolve();
                } else {
                    $.ajax({
                        url: '/Batch/Delete',
                        type: "POST",
                        data: { id: $(this).data("id") },
                        success: function (response) {
                            if (response.success) {
                                swal("Deleted!!", response.message, "success");
                                loadBatch();
                            }
                            else {
                                sweetAlert("Failed", response.message, "error");
                            }
                        },
                        error: function (error) {
                            returnErrorState(error);
                        }
                    });
                }
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });


});
function loadBatch() {
    $.ajax({
        url: '/Batch/GetList',
        type: "POST",
        success: function (view) {
            $(".all-batches").html(view);
        },
        error: function (error) {
            returnErrorState(error);
        }
    });
}
function applyNewBatchSlick(){
    if ($('.index-carousal-slick').length) {
        $('.index-carousal-slick').slick({
            dots: false,
            autoplay: false,
            infinite: true,
            speed: 700,
            autoplaySpeed: 3000,
            slidesToShow: 2,
            slidesToScroll: 2,
            arrows: true,

            focusOnSelect: true,
            pauseOnHover: true,
            swipeToSlide: true,
            draggable: true,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: false,
                        autoplay: true,
                        arrows: true,
                        pauseOnHover: false,
                        swipeToSlide: true,

                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        })
        $(".index-carousal-slick [data-toggle=rating]").addRating({
            max: 5,
            half: false,
            selectedRatings: 5
        });
        var cardsLength = $(".slick-track .slick-card").length;
        if (cardsLength == 1) {
            $(".slick-track").css("transform", "translate3d(0, 0, 0)");
        }
    }
}
function loadNewBatches() {
    $.ajax({
        url: '/Batch/GetNewBatches',
        type: "POST",
        success: function (newBatchesView) {

            $(".newBatches").html(newBatchesView);
            if ($(".slick-card").length) {
                $(".newbatch-alert").removeClass("hidden");
            }
          
                applyNewBatchSlick();
        },
        error: function (error) {
            returnErrorState(error);
        }
    });
}
function loadReferredBatches() {
    $.ajax({
        url: '/Batch/GetReferredBatches',
        type: "POST",
        success: function (referredBatches) {

            $(".referredBatches").html(referredBatches);
          
            applyNewBatchSlick();
        },
        error: function (error) {
            returnErrorState(error);
        }
    });
}
function loadBatchByTeacherAndSubjectId(userid,subjectId, renderer) {
    $.ajax({
        url: '/Batch/GetBatchByTeacherId',
        type: "Get",
        data: { id: userid, subjectId:subjectId },
        success: function (view) {
            $(renderer).html(view);
            applyTooltip();
            if (typeof takeTour === "function")
                takeTour();
        },
        error: function (error) {
            returnErrorState(error);
        }

    });
}
function editBatch() {
    var $form = $('#editBatchForm');
    $form.off("submit");
    $form.submit(function (data, event) {

        if (Date.parse($("[name=StartDate]").val()) >= Date.parse($("[name=EndDate]").val())) {
            swal("Select Valid Date!!", "Please select a valid start and end date.", "error");
            return false;

        }

        var isFormValid = $form.valid();
        if (isFormValid) {

            $.post($(this).attr('action'), $(this).serialize(), function (response) {
                //can do here
            }, 'json').done(function (data) {
                if (data.success) {
                    //swal("Sent!!", data.message, "success");
                    smallSwal({
                        type: 'success',
                        title: data.message
                    });

                    setTimeout(function () {
                        location.href = location.href;
                    }, 1000);
                } else {
                    swal("Error!!", data.message, "error")

                }

            });
            return false;
        }
    });
}

$(document).on('click', '#addBatchForm #saveBtn', function () {
    var $form = $('#addBatchForm');
    $form.off("submit");
    $form.submit(function (data, event) {

        var isFormValid = $form.valid();
        if (isFormValid) {
            $.post($(this).attr('action'), $(this).serialize(), function (response) {
                //can do here
            }, 'json').done(function (data) {
                if (data.success) {
                    smallSwal({
                        type: 'success',
                        title: "Batch Announced Successfully!"
                    });

                    setTimeout(function () {
                        location.href = location.href;
                    }, 1000)
                } else {
                    swal("Error!!", data.message, "error")

                }

            });
            return false;
        }

    });
    if (Number($("#addBatchForm [name=SubjectId]").val()) != 0
        && Date.parse($("[name=StartDate]").val()) < Date.parse($("[name=EndDate]").val())) {
        var isFormValid = $form.valid();
        if (isFormValid) { $form.submit(); }
    } else {
        if ($("#addBatchForm [name=SubjectId]").val() == 0) {
            swal("Select Subject!!", "Please select Subject.", "error");

        } else if (Date.parse($("[name=StartDate]").val()) >= Date.parse($("[name=EndDate]").val())) {
            swal("Select Valid Date!!", "Please select a valid start and end date.", "error");

        }
    }

});
$(document).on('click', '#assignBatchTeacherForm .saveBtn', function () {
    var $form = $('#assignBatchTeacherForm');
    $form.off("submit");
    $form.submit(function (data, event) {

        var isFormValid = $form.valid();
        if (isFormValid) {
            $.post($(this).attr('action'), $(this).serialize(), function (response) {
                //can do here
            }, 'json').done(function (data) {
                if (data.success) {
                    smallSwal({
                        type: 'success',
                        title: "Teacher Assigned Successfully!"
                    });
                    setTimeout(function () {
                        location.href = location.href;
                    }, 1000)
                } else {
                    swal("Error!!", data.message, "error");
                }

            });
            return false;
        }

    });

    if ($("#assignBatchTeacherForm [name=BatchTeacherId]:checked").length) {
        $form.submit();
    } else {
        swal("Select Teacher!!", "Please select at least one teacher.", "error");
    }
});


function setBatchSchedule(startDate, endDate, title, labelCategory, session_url,username,password) {
    debugger;
    var model = {
        Title: title,
        StartDateTime: moment(startDate, "YYYYMMDD").format(),
        EndDateTime: moment(endDate, "YYYYMMDD").format(),
        BatchId: location.pathname.split('/')[location.pathname.split('/').length - 1],
        LabelCategory: labelCategory,
        EventUrl: session_url,
        UserName: username,
        Password: password
    };
    var done = {id:0, success:false};
    $.ajax({
        url: '/Batch/SetSchedule',
        type: "POST",
        async: false,
        data: { model: model },
        success: function (response) {
            if (response.success) {
                smallSwal("Done!!", response.message, "success");
                done.success = response.success;
                done.id = response.data.id;
            }
            else {
                sweetAlert("Failed!!", response.message, "error");
                done.success = response.success;
            }
        },
        error: function (error) {
            returnErrorState(error);
        }
    });
    return done;
}
function updateBatchSchedule(title, id, eventUrl,username,password) {
    debugger;
    var model = {
        Id: id,
        Title: title,
        BatchId: location.pathname.split('/')[location.pathname.split('/').length - 1],
        EventUrl: eventUrl,
        UserName: username,
        Password: password
    };
    var isDone = false;
    $.ajax({
        url: '/Batch/UpdateSchedule',
        type: "POST",
        async: false,
        data: { model: model },
        success: function (response) {
            if (response.success) {
                smallSwal("Done!!", response.message, "success");
                isDone = response.success;
            }
            else {
                sweetAlert("Failed!!", response.message, "error");
                isDone = response.success;
            }
        },
        error: function (error) {
            returnErrorState(error);
        }
    });
    return isDone;
}

function deleteBatchSchedule(id) {
   
    var isDone = false;
    $.ajax({
        url: '/Batch/DeleteSchedule',
        type: "POST",
        async: false,
        data: { id: id },
        success: function (response) {
            if (response.success) {
                smallSwal("Done!!", response.message, "success");
                isDone = response.success;
            }
            else {
                sweetAlert("Failed!!", response.message, "error");
                isDone = response.success;
            }
        },
        error: function (error) {
            returnErrorState(error);
        }
    });
    return isDone;
}