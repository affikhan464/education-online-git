﻿function initializeDatePicker() {
    $('.datetimepicker').datetimepicker({
        format: 'd-m-Y h:i a',
        timepicker: true,
        autoclose: true,
        step: 5,
        value: new Date() 
    });
  
}