﻿/*!
Student.js
*/
$(document).on("click", ".studentBtn", function () {
    loadStudent();
});

$(document).on("click", ".addStudentBtn", function () {
    $("#AddStudent form").attr("action","/Student/Register");
    $("#AddStudent form textarea").val("");
    $("#AddStudent form input:not([type=radio])").val("");
    $("#AddStudent form #saveBtn").html("<i class='fas fa-user-plus'></i> Add");
    $("#AddStudent .modal-header .type").text("Add new");
    $("#City").select2({
        placeholder: "Select your City"
    });
    $("#Region").select2({
        placeholder: "Select your Region/State"
    })
        .on('select2:select', function (e) {
            var countryCode = e.params.data.id;
            loadCities(countryCode);
        });
    function formatState(state) {
        if (!state.id) {
            return state.text;
        }
        var $state = $(
            '<span><img src="' + state.element.dataset.flag + '" class="img-flag" /> ' + state.text + '</span>'
        );
        return $state;
    };
    $("#Country").select2({
        placeholder: "Select your Country",
        templateResult: formatState
    }).on('select2:select', function (e) {
        var countryCode = e.params.data.id;
        loadRegions(countryCode);
    });

    $("#AddStudent").modal();
   
});

$(document).on("click", ".inviteStudentBtn", function () {

    $("#StudentInviteModal").modal();
});

$(document).on("click", "#inviteStudentForm #saveBtn", function () {

  
    var isValid = $("#inviteStudentForm").valid();
});
$(document).on("click", "#addStudentForm #saveBtn", function () {
  
    var isValid = $("#addStudentForm").valid();
    if (isValid) {
        $("#addStudentForm").submit();
    }
});
 $(document).ready(function () {
     validateStudentForm();
     appendclickOfAddStudent();

})

function validateStudentForm() {
    /**
* Custom validator for contains at least one lower-case letter
*/
    $.validator.addMethod("atLeastOneLowercaseLetter", function (value, element) {
        return this.optional(element) || /[a-z]+/.test(value);
    }, "Must have at least one lowercase letter");
    $.validator.addMethod("validCNIC", function (value, element) {
        return this.optional(element) || /^[0-9+]{5}-[0-9+]{7}-[0-9]{1}$/.test(value);
    });
    /**
     * Custom validator for contains at least one upper-case letter.
     */
    $.validator.addMethod("atLeastOneUppercaseLetter", function (value, element) {
        return this.optional(element) || /[A-Z]+/.test(value);
    }, "Must have at least one uppercase letter");

    /**
     * Custom validator for contains at least one number.
     */
    $.validator.addMethod("atLeastOneNumber", function (value, element) {
        return this.optional(element) || /[0-9]+/.test(value);
    }, "Must have at least one number");

    /**
     * Custom validator for contains at least one symbol.
     */
    $.validator.addMethod("atLeastOneSymbol", function (value, element) {
        return this.optional(element) || /[!@@#$%^&*()]+/.test(value);
    }, "Must have at least one symbol");

    $('#addStudentForm').validate({
        rules: {
            Password: {
                required: true,
                atLeastOneLowercaseLetter: true,
                atLeastOneUppercaseLetter: true,
                atLeastOneSymbol: true,
                atLeastOneNumber: true,
                minlength: 8,
            },
            CNIC: {
                required: true,
                minlength: 15,
                maxlength: 15,
                validCNIC: true

            }
        },
        messages: {
            CNIC: "Enter your valid CNIC number e.g. 12345-1234567-1."

        }

    })


}
function loadStudent(){
    $.ajax({
        url: '/Student/Students',
        type: "Get",

        success: function (view) {
            $("#page-wrapper").html(view);
            applyTooltip();
            if (typeof takeTour === "function")
                takeTour();
        },
        error: function (error) {
            returnErrorState(error);
        }


    })
}
$(document).on("click", ".studentViewbutton", function () {
    var userid = this.dataset.id;
    loadUserDetailById(userid);

});
function appendclickOfAddStudent() {

    var $form = $('#addStudentForm');
    $form.off("submit");
            $form.submit(function (data, event) {
            
                
                var isFormValid = $('#addStudentForm').valid();

                if (isFormValid) {
    var btnHtml = $('#addStudentForm #saveBtn').html();
                $('#addStudentForm #saveBtn').addClass("disabledBtn");
               
                $('#addStudentForm #saveBtn').html('<i class="fas fa-spinner fa-spin"></i>');

                $.post($(this).attr('action'), $(this).serialize(), function (response) {
                    //can do here
                }, 'json').done(function (data) {
                    if (data.success) {
                       smallSwal("Done!!", data.message, "success");
                        if ($("#detailPage").length > 0) {
                            var userid = document.getElementById("detailPage").dataset.userid;

                            loadUserDetailById(userid);
                            $('#addStudentForm #saveBtn').removeClass("disabledBtn");
                            $('#addStudentForm #saveBtn').html(btnHtml);
                        }
                        else {
                            loadStudent();
                            $('#addStudentForm #saveBtn').removeClass("disabledBtn");
                            $('#addStudentForm #saveBtn').html(btnHtml);
                            if ($("#AddStudent.modal.show").length) {
                                $("#AddStudent.modal.show").modal("hide");
                            }
                        }
                    } else {
                        swal("Error!!", data.message, "error");
                        $('#addStudentForm #saveBtn').removeClass("disabledBtn");
                        $('#addStudentForm #saveBtn').html(btnHtml);
                    }

                });
                return false;
            }

                });
        }
$(document).on("click", ".studentEditButton", function () {
    var userId = this.dataset.id;
    $.ajax({
        url: '/Student/Edit',
        type: "POST",
        data: { userId: userId },
        success: function (view) {
            $("#AddStudent .form").html(view);
            $("#AddStudent .modal-header .type").text("Edit");
            $("#AddStudent form #saveBtn").text("Update");
            $("#AddStudent").modal();
            validateStudentForm();
            appendclickOfAddStudent();
            setInputMaskt();


            $("#City").select2({
                placeholder: "Select your City"
            });
            $("#Region").select2({
                placeholder: "Select your Region/State"
            })
                .on('select2:select', function (e) {
                    var countryCode = e.params.data.id;
                    loadCities(countryCode);
                });
            function formatState(state) {
                if (!state.id) {
                    return state.text;
                }
                var $state = $(
                    '<span><img src="' + state.element.dataset.flag + '" class="img-flag" /> ' + state.text + '</span>'
                );
                return $state;
            };
            $("#Country").select2({
                placeholder: "Select your Country",
                templateResult: formatState
            }).on('select2:select', function (e) {
                var countryCode = e.params.data.id;
                loadRegions(countryCode);
            });
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
});



  $(document).on("click", ".studentDeleteButton", function () {
      var userId = this.dataset.id;
               swal({
                   title: "Are you sure?",
                   text: "Are you sure to delete this student?",
                   type: "info",
                   showCancelButton: true,
                   showLoaderOnConfirm: true,
                   preConfirm: (text) => {
                       return new Promise((resolve) => {

                           $.ajax({
                               url: '/Student/Delete',
                               type: "POST",
                               data: { userId: userId },
                               success: function (response) {
                                   if (response.success) {

                                       swal("Deleted!", response.message, "success")
                                       loadStudent();
                                   }
                                   else {
                                       swal("Error!", response.message, "error")

                                   }

                               },
                               error: function (error) {
                                   returnErrorState(error);
                               }

                           });


                       })
                   },
                   allowOutsideClick: () => !swal.isLoading()
               });

        });
  $(document).on("click", ".studentBlockbutton", function () {


               var userId = this.dataset.id;
            $.ajax({
                url: '/Student/Block',
                type: "POST",
                data: { userId: userId },
                success: function (response) {
                    if (response.success) {
                       smallSwal("Done!!", response.message, "success")
                        loadStudent();
                    }
                    else {
                        swal("Error!", response.message, "error")

                    }

                },
                error: function (error) {
                    returnErrorState(error);
                }



            });



});


function loadRegions(countryCode) {
    $.ajax({
        url: "/Geo/GetRegions",
        method: 'GET',
        data: { countryCode: countryCode },
        success: function (data) {
            $("#City").html('');
            $("#City").select2({
                placeholder: "Select your City"
            });
            $("#Region").html('');
            $("#Region").append(`<option> Select your Region/State </option>`)
            for (var i = 0; i < data.length; i++) {
                var country = data[i];
                $("#Region").append(`<option  value="${country.id}"> ${country.name} </option>`)
            }

            $("#Region").select2("destroy").select2({
                placeholder: "Select your Region/State"
            })
                .on('select2:select', function (e) {
                    var countryCode = e.params.data.id;
                    loadCities(countryCode);
                });;

        }
    });
}
function loadCities(code) {
    $.ajax({
        url: "/Geo/GetCities",
        method: 'GET',
        data: { regionCode: code },
        success: function (data) {
            $("#City").html('');

            $("#City").append(`<option>Select your City</option>`)
            for (var i = 0; i < data.length; i++) {
                var country = data[i];
                $("#City").append(`<option  value="${country.id}"> ${country.name} </option>`)
            }

            $("#City").select2("destroy").select2({
                placeholder: "Select your City"
            })
                .on('select2:select', function (e) {
                    var code = e.params.data.id;
                    $("#CityId").val(code)
                });

        }
    });
}

    $(function () {
        $('.one .btn').on('click', function () {
            $(this).parents('.one').animate({
                top: '-100%'
            }, 500);

            $(this).parents('.one').siblings('.two').
                animate({
                    top: '0px'
                }, 500);
        });

    $('.two .close').on('click', function () {
        $(this).parent().animate({
            top: '0'
        }, 500);

    $(this).parent().siblings('.one').animate({
        top: '0px'
}, 500);
});
    });