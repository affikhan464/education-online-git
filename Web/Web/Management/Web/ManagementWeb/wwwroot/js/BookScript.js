﻿/*! BookScript.js
 
 */
$(document).ready(function () {
    
    if ($("#document-form #Detail.tmceeditor").length) {

        TinyMVCGlobalOptions.selector = "#document-form #Detail.tmceeditor";

        tinymce.init(TinyMVCGlobalOptions);
    }
    TinyMCENewArrivalOptions = {

        // General options
        selector: "",
        mode: "exact",
        theme: "modern",
        height: "300px",
        width: "100%",
        verify_html: false,
        browser_spellcheck: true,
        contextmenu: true,
        plugins: "powerpaste emoticons pagebreak,table,insertdatetime,preview,image ,imagetools,media,searchreplace,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,template,wordcount, lists, advlist, autosave,textcolor colorpicker,code spellchecker",
        font_formats: 'Montserrat=Montserrat,sans-serif;Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n;Comfortaa=Comfortaa, cursive;Raleway=Raleway, sans-serif;Roboto=Roboto, sans-serif;Nunito=Nunito, sans-serif;',
        fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',

        toolbar: "emoticons styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | link image | forecolor backcolor code | fontselect fontsizeselect spellchecker",


        content_css: "/css/style.css",

        style_formats: [
            { title: 'Line height 0.5', selector: 'p,div,h1,h2,h3,h4,h5,h6', styles: { lineHeight: '0.5' } },
            { title: 'Line height 1', selector: 'p,div,h1,h2,h3,h4,h5,h6', styles: { lineHeight: '1' } },
            { title: 'Line height 1.5', selector: 'p,div,h1,h2,h3,h4,h5,h6', styles: { lineHeight: '1.5' } },
            { title: 'Line height 2', selector: 'p,div,h1,h2,h3,h4,h5,h6', styles: { lineHeight: '2' } },
            { title: 'Line height 2.5', selector: 'p,div,h1,h2,h3,h4,h5,h6', styles: { lineHeight: '2.5' } },
            { title: 'Line height 3', selector: 'p,div,h1,h2,h3,h4,h5,h6', styles: { lineHeight: '3' } },
            { title: 'Line height 3.5', selector: 'p,div,h1,h2,h3,h4,h5,h6', styles: { lineHeight: '3.5' } },

        ]

    };
});






let CommentaryId = "";
let CommentaryAttributeType = "";

$(document).on("click", ".loadDocumentBtn", function () {
    $('#load-document-menu').metisMenu();
    $("#LoadDocumentModal").modal();
});
function loadDocumentForNavigation(attributetype, attributeid, wordid, navigationid) {

    $.ajax({
        url: '/Book/Load' + attributetype + 'DetailView',
        type: "POST",

        data: {
            id: attributeid
        },
        success: function (view) {
            $("#Document_" + attributeid).remove();
            $(".question-detail").append(view);

            if ($(".navigation-confirmation-box").css("display") == "block") {
                activateNaviationAppliedPowertip();
            }
            applyPopover();

            if (wordid != 0) {
                $("#Document_" + attributeid + " .backToWord").attr("data-wordid", wordid);
                $("#" + navigationid + "").addClass("blink_me");
                setInterval(function () {
                    $("#" + navigationid + "").removeClass("blink_me");
                }, 8000);

                $(".backToNavigationWord").attr("data-wordid", wordid);
                $(".backToNavigationWord").fadeIn();
                $(".backToNavigationWord").removeClass("hidden");
                $(".ring-container").removeClass("hidden");
            }


            if ($(".navigation-confirmation-box").css("display") == "block") {
                activateNaviationAppliedPowertip();
            }

            if (wordid == 0) {
                scrollTo("#Document_" + attributeid);
            } else {
                scrollTo("#" + navigationid);
            }

            if ($("#LoadDocumentModal.show").length > 0) {
                $("#LoadDocumentModal .close").click();
            }
            applyPopover();

        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}
$(document).on("click", ".open-attribute", function () {
    var attributetype = this.dataset.attributetype;
    var attributeid = this.dataset.attributeid;
    loadDocumentForNavigation(attributetype, attributeid, 0, 0);
});

function saveDocumentForm() {
    var $form = $('#document-form');
    $form.off("submit");
    $form.submit(function (data, evecomnt) {

        $.post($(this).attr('action'), $(this).serialize(), function (response) {

        }, 'json').done(function (data) {

            if (data.success) {
                //swal("Successfull!!", data.message, "success");
                smallSwal({
                    type: 'success',
                    title: data.message
                });
                setTimeout(function () {
                    window.location.href = window.location.href;

                }, 1000)
            } else {
                swal("Error!!", data.message, "error");
            }

        }).fail(function (error) {
            returnErrorState(error);
        });;
        return false;
    });


};
var applier;
var navApplier;
window.onload = function () {
    rangy.init();
    appendSelectionApplier();
    appendNavigationApplier();
};
function appendSelectionApplier() {
    applier = rangy.createClassApplier("selectedRangy", {
        elementTagName: "span",
        elementProperties: {
            id: "selectedRangy"
        }, elementAttributes: {

        }
    });
    $('[data-toggle="tooltip"]').powerTip();
}
function appendNavigationApplier(guid) {
    navApplier = rangy.createClassApplier("navigationApplied", {
        elementTagName: "span",
        elementProperties: {
            id: guid
        }, elementAttributes: {
            "data-navigationid": guid
        }
    });
    $('[data-toggle="tooltip"]').powerTip();
}


//Commentary
function appendCommentaryToWord(CommentaryId, CommentaryAttributeType, WordId) {

    var SelectedText = $("#selectedRangy").text();
    var anchorTag = "<a href='#' id='" + WordId + "' data-commentaryid='" + CommentaryId + "' data-attributetype='" + CommentaryAttributeType + "' data-wordid='" + WordId + "' class='questionAttached'> " + SelectedText + "</a>";

    $("#selectedRangy").replaceWith(anchorTag);

    //$("#" + WordId).attr('data-content', "<i data-wordid='" + WordId + "' data-commentaryid='" + CommentaryId + "' data-attributeType= '" + CommentaryAttributeType + "' data-toggle='tooltip' title='See commentary against this word' class='fas fa-question showQ'></i> <i   data-wordid='" + WordId + "' data-questionid='" + QuestionId + "' data-attributeType= '" + QuestionAttributeType + "' data-toggle='tooltip' title='Add more question' class='fas fa-plus  addMoreQ'></i>");
}
function removeRangySelected() {
    var text = $("#selectedRangy").text();
    $("#selectedRangy").replaceWith(text);
}

function openAddingNavigationModel() {


    var selectedWord = rangy.getSelection();

    if (selectedWord.toString().length > 0 || $(".selectedWord").val().trim().length > 0) {


        showNavigationBox();
        activateNaviationAppliedPowertip();

    } else {
        swal("", "Select Some Text First!", "error")

    }


}
function showNavigationBox() {
    $(".navigation-confirmation-box").fadeIn();
    $(".navigation-confirmation-box").css("top", "20px");

}
function closeNavigationBox() {
    $(".navigation-confirmation-box").fadeOut();
    $(".navigation-confirmation-box").css("top", "-100px");
}
function navigationAppliedPowerTip() {
    $('.navigationApplied').powerTip({
        openEvents: ['mouseover'],
        closeEvents: [],
        mouseOnToPopup: true
    });
    $('.navigationApplied').data('powertip', function () {

        var item = this.dataset;

        if (LoggedUserRole == "Student" || LoggedUserRole == "Teacher")
            return "";
        else
            return "<i data-navigationid= '" + item.navigationid + "'  data-toggle='tooltip' title='Apply navigation to this destination.' class='fas fa-check applyExistingNavigationBtn m-1 ml-3 mr-3 '></i>";
    });
}


function openAddingCommentaryModel() {
    $('#addCommentaryForm')[0].reset();
    $("#addCommentaryForm .existingCommentariesSection .selectCommentary").removeClass("selected");
    $(".newCommentarySection").show();
   
    //removeRangySelected();

    //appendSelectionApplier();
    //applier.toggleSelection();

    loadAvailableCommentaries();
    //$(".selectedWord").remove();
    $("#addCommentaryForm [name=WordId]").val(0);
    var selectedWord = rangy.getSelection();

    if (selectedWord.toString().length > 0 || $(".selectedWord").val().trim().length > 0) {
        $("#SelectedWord").val(selectedWord.toString());
        $("#appendCommentary").modal();
        if (tinymce.get("CommentaryDetail") !== null)
            tinymce.get("CommentaryDetail").remove();

        TinyMVCGlobalOptions.selector = "#addCommentaryForm .tmceeditor";

        tinymce.init(TinyMVCGlobalOptions);
        $('#addCommentaryForm .mce-tinymce.mce-container').css("display", "block");
    } else {
        swal("", "Select Some Text First!", "error")

    }
    addCommentaryForm();

}
function guid() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}


function appendCommentaryAttributeToWord(data) {

    var wordId = data.data.wordId;
    var isWordExist = $("#" + wordId).length > 0;
    debugger
    var commentaryIds = data.data.commentaryIds == undefined ? "" : data.data.commentaryIds;

    if (isWordExist) { 

        $("#" + wordId).attr("data-commentaryid", commentaryIds);

        //loadCommentaries(wordId, commentaryIds, "#addCommentaryForm .existingCommentariesSection", "#appendCommentary");
    } else {

        appendCommentaryToWord(commentaryIds, data.data.attributeType, data.data.wordId);
    }


    applyPopover();
    saveHtml(wordId);


}
function appendCommentaryAttributeToWordAfterRemoveCommentary(data) {

    var wordId = data.data.wordId;
    var isWordExist = $("#" + wordId).length > 0;
    debugger
     var commentaryIds = data.data.ids == undefined ? "" : data.data.ids;

    if (isWordExist) { 

        $("#" + wordId).attr("data-commentaryid", commentaryIds);

        //loadCommentaries(wordId, commentaryIds, "#addCommentaryForm .existingCommentariesSection", "#appendCommentary");
    } else {

        appendCommentaryToWord(commentaryIds, data.data.attributeType, data.data.wordId);
    }


    applyPopover();
    saveHtml(wordId);


}


function addCommentaryForm() {

    var $form = $('#addCommentaryForm');
    $form.off("submit");
    $form.submit(function (data, event) {
        if ($(".selectedWord").length > 0) {
            $("#addCommentaryForm  [name=SelectedWord]").val($(".selectedWord").val())
        } else
            $("#addCommentaryForm [name=SelectedWord]").val($("#selectedRangy").text());

        var selectedWord = rangy.getSelection();
        //if (selectedWord.toString().length > 0 || $(".selectedWord").length > 0) {
        if (!$(".existingCommentariesSection .selected").length > 0) {

            $("#addCommentaryForm  [name=SelectedWord]").val($(".selectedWord").val())
            $.post($(this).attr('action'), $(this).serialize(), function (response) {

            }, 'json').done(function (data) {

                if (data.success) {
                    //smallSwal("Successfull!!", data.message, "success");
                    smallSwal({
                        type: 'success',
                        title: data.message
                    });
                    appendCommentaryAttributeToWord(data);
                } else {
                    swal("Error!!", data.message, "error")

                }

            }).fail(function (error) {
                returnErrorState(error);
            });
        } else {

            var elements = $(".existingCommentariesSection .selected");
            var ids = "";
            elements.each(function (i, element) {
                ids += element.dataset.commentaryid + ",";
            })

            var model = {
                Ids: ids,
                SelectedWord: $("#selectedRangy").text(),
                WordId: $("#addCommentaryForm [name=WordId]").val()
            }

            $.post("/Commentary/AddAvailableCommentary", model, function (response) {

            }, 'json').done(function (data) {

                if (data.success) {
                    //smallSwal("Successfull!!", data.message, "success");
                    smallSwal({
                        type: 'success',
                        title: data.message
                    });
                    appendCommentaryAttributeToWord(data);
                } else {
                    swal("Error!!", data.message, "error")

                }

            }).fail(function (error) {
                returnErrorState(error);
            });

        }
        //} else {
        //    swal("Text Unselected", "Re-select the text first!", "error")

        //}
        return false;
    });



};


$(document).on("click", ".newArrivalAddBtn",function () {

    $('#addNewArrivalForm')[0].reset();
   
    if (tinymce.get("NewArrivalText") !== null)
        tinymce.get("NewArrivalText").remove();

    TinyMCENewArrivalOptions.selector = "#addNewArrivalModal [name=NewArrivalText].tmceeditor";

    tinymce.init(TinyMCENewArrivalOptions);

    addNewArrivalForm();
    $("#addNewArrivalModal").modal();
   

});
$(document).on("click", ".newArrivalUpdateBtn", function () {
    var id = $(this).data("id");
    var type = $(this).data("attributetype");
    $.ajax({
        url: '/Attribute/Edit' + type + 'NewArrival',
        type: "Get",

        data: {
            id: id,
        },
        success: function (view) {

            $("#updateNewArrivalModal .UpdateNewArrivalFormView").html(view);


            if (tinymce.get("NewArrivalText") !== null)
                tinymce.get("NewArrivalText").remove();

            TinyMCENewArrivalOptions.selector = "#updateNewArrivalModal [name=NewArrivalText].tmceeditor";

            tinymce.init(TinyMCENewArrivalOptions);
            
            updateNewArrivalForm();
            $("#updateNewArrivalModal").modal();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
  
});
function updateNewArrivalForm() {

    var $form = $('#updateNewArrivalForm');
    $form.off("submit");
    $form.submit(function (data, event) {

        $.post($(this).attr('action'), $(this).serialize(), function (response) {

        }, 'json').done(function (data) {

            if (data.success) {

                smallSwal({
                    type: 'success',
                    title: data.message
                });
                $(".newArrivalBtn").text("Update New Arrivals");
                $(".newArrivalBtn").removeClass("newArrivalAddBtn");
                $(".newArrivalBtn").addClass("newArrivalUpdateBtn");

            } else {
                swal("Error!!", data.message, "error")

            }

        }).fail(function (error) {
            returnErrorState(error);
        });

        return false;
    });



};
function addNewArrivalForm() {

    var $form = $('#addNewArrivalForm');
    $form.off("submit");
    $form.submit(function (data, event) {
      
            $.post($(this).attr('action'), $(this).serialize(), function (response) {

            }, 'json').done(function (data) {

                if (data.success) {
                  
                    smallSwal({
                        type: 'success',
                        title: data.message
                    });
                    $(".newArrivalBtn").text("Update New Arrivals");
                    $(".newArrivalBtn").removeClass("newArrivalAddBtn");
                    $(".newArrivalBtn").addClass("newArrivalUpdateBtn");

                } else {
                    swal("Error!!", data.message, "error")

                }

            }).fail(function (error) {
                returnErrorState(error);
            });
         
        return false;
    });



};
function html2text(html) {
    $(".tempDiv").remove();
    $("body").append("<div class='tempDiv'></div>");
    $(".tempDiv").html(html);
    $(".tempDiv").html();

    return $(".tempDiv").html();
}
function saveHtml(wordid) {

    var sourceMainPage = $("#" + wordid).parents("[data-savetype]");
    var detail = sourceMainPage.html();
    var attributeType = sourceMainPage.attr("data-savetype");
    var attributeId = sourceMainPage.attr("data-id");


    var model = {
        Detail: detail,
        Id: attributeId
    }
    $.ajax({
        url: '/Book/Save' + attributeType,
        type: "POST",
        data: {
            model: model
        },
        success: function () {
            //smallSwal({
            //    type: 'success',
            //    title: "Saved"
            //});
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}


function loadAvailableCommentaries(wordId, attributeId, attributeType) {

    wordId = wordId == undefined ? 0 : wordId;
    attributeId = attributeId == undefined ? $(".Page").length > 0 ? $(".Page").data().attributeid : $(".opened-attribute").data().attributeid : attributeId;
    attributeType = attributeType == undefined ? $(".Page").length > 0 ? $(".Page").data().attributetype : $(".opened-attribute").data().attributetype : attributeType;



    getAvailableCommentaries(wordId, attributeId, attributeType, "#addCommentaryForm .existingCommentariesSection", "#appendCommentary");

}
function getAvailableCommentaries(wordId, attributeId, attributeType, viewSection, modal) {
    $.ajax({
        url: '/Commentary/GetAvailableCommentaries',
        type: "POST",

        data: {
            attributeId: attributeId, attributeType: attributeType, wordId: wordId
        },
        success: function (view) {

            $(viewSection).html(view);
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}

$(document).on("click", ".selectCommentary", function (event) {
    var btn = $(this);
    if (!btn.hasClass("selected")) {
        btn.addClass("selected");
        $(".newCommentarySection").hide();
    } else {
        btn.removeClass("selected");
        var selectCommentariesLength = $("#addCommentaryForm .existingCommentariesSection .selected").length;
        if (selectCommentariesLength > 0) {
            $(".newCommentarySection").hide();
        } else {

            $(".newCommentarySection").show();

        }
    }
});



$(document).on("click", ".showCommentary", function (event) {
    var btn = this;
    var commentaryids = $(btn).data().commentaryid;
    var wordid = $(btn).data().wordid;

    loadCommentaries(wordid, commentaryids, "#commentaryListModal .existingCommentariesSection", "#commentaryListModal");
});
$(document).on("click", ".viewCommentaryBtn", function (event) {


    var btn = this;
    var wordid = $(btn).data().wordid;
    var commentaryid = $(btn).data().commentaryid;
    var isCommentaryExist = $("#Commentary_" + commentaryid).length > 0;
    if (isCommentaryExist) {
        $("#Commentary_" + commentaryid).remove();
        loadCommentaryDetail(commentaryid, wordid);

    } else
        loadCommentaryDetail(commentaryid, wordid);


});
$(document).on("click", ".deleteCommentaryBtn", function (event) {

    var btn = this;
    var commentaryid = $(btn).data().commentaryid;
    var attributeId = $("#appendCommentary #AttributeId").val();
    var attributeType = $("#appendCommentary #AttributeType").val();
    
    swal({
        title: "Are you sure?",
        text: "Are you sure to delete this commentary from application completely?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                $.ajax({
                    url: '/Commentary/Delete',
                    type: "POST",
                    data: {
                        id: commentaryid
                    },
                    success: function (data) {
                        if (data.success) {
                            smallSwal({
                                type: 'success',
                                title: data.message
                            });
                            var wordid = $("#WordId").val();
                            loadAvailableCommentaries(wordid, attributeId, attributeType);

                        } else {
                            swal("Error!!", data.message, "error");

                        }
                    },
                    error: function (error) {
                        returnErrorState(error);
                    }

                })

            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });

});


$(document).on('hidden.bs.modal', function (event) {
    if ($('.modal:visible').length) {
        $('body').addClass('modal-open');
    }
});


$(document).on("click", ".addMoreCommentary", function (event) {
    $('#addCommentaryForm')[0].reset();
    $("#commentaryListModal .close").click();
    $("#appendCommentary .existingCommentariesSection").show();
    $("#appendCommentary .newCommentarySection").show();
    var btn = this;
    var wordid = $(btn).data().wordid;
    var attributeId = $(btn).data().attributeid;
    var attributeType = $(btn).data().attributetype;
    var selectedText = $("#" + $(btn).data().wordid).text();
    $("#appendCommentary [name=WordId]").val(wordid);
    $(".selectedWord").remove();
    $("<input class='selectedWord' hidden value='" + selectedText + "' />").appendTo("body");

    loadAvailableCommentaries(wordid, attributeId, attributeType);
    $("#appendCommentary").modal();

    if (tinymce.get("CommentaryDetail") !== null)
        tinymce.get("CommentaryDetail").remove();

    TinyMVCGlobalOptions.selector = "#commentaryListModal .tmceeditor";

    tinymce.init(TinyMVCGlobalOptions);
    $('#commentaryListModal .mce-tinymce.mce-container').css("display", "block");
    addCommentaryForm();
});


function editCommentaryForm() {

    var $form = $('#editCommentaryForm');
    $form.off("submit");
    $form.submit(function (data, event) {


        $.post($(this).attr('action'), $(this).serialize(), function (response) {

        }, 'json').done(function (data) {

            if (data.success) {
                //swal("Updated!!", data.message, "success");
                smallSwal({
                    type: 'success',
                    title: data.message
                });

            } else {
                swal("Error!!", data.message, "error")

            }

        });

        return false;
    });



};

$(document).on("click", ".emailAllocatedMember", function (event) {


    var btn = this;
    var attributeid = $(btn).data().attributeid;
    var attributetype = $(btn).data().attributetype;

    //open modal on success

    $.ajax({
        url: '/Book/EmailAllocatedMember',
        type: "POST",

        data: {
            attributeType: attributetype, attributeId: attributeid
        },
        success: function (responce) {
            if (responce.success) {
                smallSwal({
                    type: 'success',
                    title: responce.message
                });
            }
            else {
                smallSwal({
                    type: 'error',
                    title: responce.message
                });
            }
        },
        error: function (error) {
            returnErrorState(error);
        }

    })

});

$(document).on("click", ".editCommentaryBtn", function (event) {


    var btn = this;
    var commentaryId = $(btn).data().commentaryid;

    //open modal on success

    $.ajax({
        url: '/Commentary/Edit',
        type: "POST",

        data: {
            id: commentaryId
        },
        success: function (view) {
            $("#editCommentaryModal .commentaryEditors").html(view);


            $("#editCommentaryModal").modal();

            if (tinymce.get("EditCommentaryDetail") !== null)
                tinymce.get("EditCommentaryDetail").remove();

            TinyMVCGlobalOptions.selector = "#editCommentaryModal .tmceeditor";

            tinymce.init(TinyMVCGlobalOptions);
            $('#editCommentaryModal .mce-tinymce.mce-container').css("display", "block");
            editCommentaryForm();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })

});


$(document).on("click", ".removeCommentaryBtn", function (event) {

    var btn = this;
    var commentaryid = $(btn).data().commentaryid;
    var wordid = $(btn).data().wordid;

    $.ajax({
        url: '/Commentary/Remove',
        type: "POST",

        data: {
            id: commentaryid, wordId: wordid
        },
        success: function (data) {
            if (data.success) {
                //swal("Removed!!", data.message, "success");
                smallSwal({
                    type: 'success',
                    title: data.message
                });
                appendCommentaryAttributeToWordAfterRemoveCommentary(data);
                loadCommentaries(wordid, commentaryid, "#commentaryListModal .existingCommentariesSection ", "#commentaryListModal");
            } else {
                swal("Error!!", data.message, "error");

            }
        },
        error: function (error) {
            returnErrorState(error);
        }

    })

});



$(document).on("click", ".closeCommentaryBtn", function (event) {

    var btn = this;

    var commentaryid = $(btn).data().commentaryid;
    var wordid = $("#Commentary_" + commentaryid + " .backToWord").data().wordid;
    $("#Commentary_" + commentaryid).remove();

    scrollTo("#" + wordid);
    $("#" + wordid + "").addClass("blink_me");
    setInterval(function () {
        $("#" + wordid + "").removeClass("blink_me");
    }, 8000);
});
$(document).on("click", ".closeDocumentBtn", function (event) {

    var btn = this;

    var documentid = $(btn).data().documentid;
    //var wordid = $("#Document_" + documentid + " .backToWord").data().wordid;
    $("#Document_" + documentid).remove();

    //scrollTo("#" + wordid);
    //$("#" + wordid + "").addClass("blink_me");
    //setInterval(function () {
    //    $("#" + wordid + "").removeClass("blink_me");
    //}, 8000);
});
function loadCommentaryDetail(commentaryId, wordid) {
    $.ajax({
        url: '/Commentary/Get',
        type: "POST",

        data: {
            id: commentaryId
        },
        success: function (view) {

            $(".question-detail").append(view);
            $("#commentaryListModal .close").click()
            $("#Commentary_" + commentaryId + " .backToWord").attr("data-wordid", wordid);
            scrollTo("#Commentary_" + commentaryId);
            applyTooltip();
            applyPopover();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}
function loadCommentaryForNavigation(commentaryId, wordid, navigationid, attributType) {
    $.ajax({
        url: '/Commentary/Get',
        type: "POST",

        data: {
            id: commentaryId
        },
        success: function (view) {
            $("#Commentary_" + commentaryId).remove();
            $(".question-detail").append(view);
            $("#Commentary_" + commentaryId + " .backToWord").attr("data-wordid", wordid);

            if ($(".navigation-confirmation-box").css("display") == "block") {
                activateNaviationAppliedPowertip();
            }

            $("#" + navigationid + "").addClass("blink_me");
            setInterval(function () {
                $("#" + navigationid + "").removeClass("blink_me");
            }, 8000);
            $(".backToNavigationWord").attr("data-wordid", wordid);
            $(".backToNavigationWord").fadeIn();
            $(".backToNavigationWord").removeClass("hidden");
            $(".ring-container").removeClass("hidden");
            scrollTo("#" + navigationid);
            applyPopover();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}
$(document).on("click", ".printPageBtn", function () {
    var printContent = document.getElementById("Page");
    var WinPrint = window.open('', '', 'width=900,height=650');
    WinPrint.document.write(printContent.innerHTML);
    WinPrint.document.close();
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close();
});

$(document).on("click", ".printQuestionBtn", function () {
    var css = '<link rel="stylesheet" href="/css/print.css" />';
    var printContentID = $(this).parents("[id*=Question_]").attr("id");
    var printContent = document.getElementById(printContentID);

    var WinPrint = window.open('', '', 'width=900,height=650');
    WinPrint.document.head.innerHTML = css;
    WinPrint.document.head.append(css);
    WinPrint.document.write(printContent.innerHTML);

    WinPrint.document.close();
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close();
});
var currentZoom = 1.0;

$('#ZoomIn').click(
    function () {
        $('.content-page').animate({ 'zoom': currentZoom += .1 }, 'slow');
        $('.questionAttached').powerTip('destroy');
        applyPopover();
        applyTooltip();
    })
$('#ZoomOut').click(
    function () {
        $('.content-page').animate({ 'zoom': currentZoom -= .1 }, 'slow');
        $('.questionAttached').powerTip('destroy');
        applyPopover();
        applyTooltip();
    })
$('#ZoomReset').click(
    function () {
        currentZoom = 1.0
        $('.content-page').animate({ 'zoom': 1 }, 'slow');
        $('.questionAttached').powerTip('destroy');
        applyPopover();
        applyTooltip();
    })


function loadCommentaries(wordid, commentariesIds, viewSection, modal) {
    commentariesIds = commentariesIds == undefined || commentariesIds == "undefined" ? "" : commentariesIds;
    $.ajax({
        url: '/Commentary/GetCommentaries',
        type: "POST",

        data: {
            ids: commentariesIds, wordId: wordid
        },
        success: function (view) {

            $(viewSection).html("");
            $(viewSection).html(view);
            //open modal on success
            $(modal).modal();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}


function deleteDetails(id, type) {

    $.ajax({
        url: "/Book/DeleteDetailFor" + type,
        type: "POST",
        data: { id: id },
        success: function (response) {
            if (response.success) {

                //swal("Done!", response.message, "success");
                smallSwal({
                    type: 'success',
                    title: response.message
                });
                setTimeout(function () {
                    window.location.href = window.location.href;

                }, 1000)

            }
            else {
                swal("Error!", response.message, "error")

            }

        },
        error: function (error) {
            returnErrorState(error);
        }

    })

}
$(document).on("click", ".deleteDetailBtn", function () {
    var id = this.dataset.id;
    var type = this.dataset.type;
    swal({
        title: 'Are you sure?',
        html: 'Type <b>"delete"</b>, to erase detail!',
        input: 'text',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Submit',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                if (text != 'delete') {
                    swal.showValidationError(
                        'Type correct spelling.'
                    )
                    resolve();
                } else {
                    deleteDetails(id, type);

                }


            })
        },
        allowOutsideClick: () => !swal.isLoading()
    })

});

function positionAddBtn() {
    //var bottomOfPage = $(".main-page").offset().top+ $(".main-page").innerHeight()-160;
    if ($(".main-page").length > 0) {
        var topOfPage = $(".main-page").offset().top - 100;
        var chapterDetailPosition = $(".main-page").offset();
        if ($(this).scrollTop() < topOfPage) {
            $('.addQuestionToWord').css("position", "absolute");
            $('.addQuestionToWord').css("left", 5);
            $(".addQuestionToWord").css("top", 100);

            $('.backToNavigationWord').css("position", "absolute");
            $('.backToNavigationWord').css("left", 5);
            $(".backToNavigationWord").css("top", 150);

            $('.ringring').css("position", "absolute");
            $('.ringring').css("left", - 3);
            $(".ringring").css("top", 143);


            $('.navigation-confirmation-box').css("position", "fixed");
            $('.navigation-confirmation-box').css("left", (Number(chapterDetailPosition.left) + Number($(".main-page").css("padding-left").replace(/[^-\d\.]/g, ''))));
            $(".navigation-confirmation-box").css("top", 20);
            $('.navigation-confirmation-box').css("width", $(".main-page").width() - 5);
        }
        //else if ($(this).scrollTop() > bottomOfPage) {
        //    $('.addQuestionToWord ').fadeOut();
        //}
        else {
            $('.addQuestionToWord').fadeIn();
            $('.addQuestionToWord').css("position", "fixed");
            $(".addQuestionToWord").css("left", chapterDetailPosition.left + 5);
            $(".addQuestionToWord").css("top", 100);

            $('.backToNavigationWord ').css("position", "fixed");
            $(".backToNavigationWord").css("left", chapterDetailPosition.left + 5);
            $(".backToNavigationWord").css("top", 150);

            $('.ringring ').css("position", "fixed");
            $(".ringring").css("left", chapterDetailPosition.left - 3);
            $(".ringring").css("top", 142);


            $('.navigation-confirmation-box').css("position", "fixed");
            $(".navigation-confirmation-box").css("top", 20);
            $('.navigation-confirmation-box').css("width", $(".main-page").width() - 5);
            $('.navigation-confirmation-box').css("left", (Number(chapterDetailPosition.left) + Number($(".main-page").css("padding-left").replace(/[^-\d\.]/g, ''))));
        }
    }
    else if ($(".adminPage").length > 0) {
        var topOfPage = $("#page-wrapper").offset().top;
        var chapterDetailPosition = $(".adminPage").offset();
        if ($(this).scrollTop() < topOfPage) {
            $('.addQuestionToWord').css("position", "absolute");
            $('.addQuestionToWord').css("left", -45);
            $(".addQuestionToWord").css("top", 100);

            $('.backToNavigationWord').css("position", "absolute");
            $('.backToNavigationWord').css("left", -45);
            $(".backToNavigationWord").css("top", 150);

            $('.ringring').css("position", "absolute");
            $('.ringring').css("left", - 3);
            $(".ringring").css("top", 143);


            $('.navigation-confirmation-box').css("position", "fixed");
            $('.navigation-confirmation-box').css("left", (Number(chapterDetailPosition.left) + Number($(".adminPage").css("padding-left").replace(/[^-\d\.]/g, ''))));
            $(".navigation-confirmation-box").css("top", 20);
            $('.navigation-confirmation-box').css("width", $(".adminPage").width() - 5);
        }
        //else if ($(this).scrollTop() > bottomOfPage) {
        //    $('.addQuestionToWord ').fadeOut();
        //}
        else {
            var chapterDetailPosition = $("#page-wrapper").offset();
            $('.addQuestionToWord').fadeIn();
            $('.addQuestionToWord').css("position", "fixed");
            $(".addQuestionToWord").css("left", chapterDetailPosition.left - 45);
            $(".addQuestionToWord").css("top", 100);

            $('.backToNavigationWord ').css("position", "fixed");
            $(".backToNavigationWord").css("left", chapterDetailPosition.left - 45);
            $(".backToNavigationWord").css("top", 150);

            $('.ringring ').css("position", "fixed");
            $(".ringring").css("left", chapterDetailPosition.left - 3);
            $(".ringring").css("top", 142);


            $('.navigation-confirmation-box').css("position", "fixed");
            $(".navigation-confirmation-box").css("top", 20);
            $('.navigation-confirmation-box').css("width", $("#page-wrapper").width() - 5);
            $('.navigation-confirmation-box').css("left", (Number(chapterDetailPosition.left) + Number($("#page-wrapper").css("padding-left").replace(/[^-\d\.]/g, ''))));
        }
    }
}

$(window).resize(function () {
    //positionAddQuestionToWordBtn();
    positionAddBtn();
});

$("body").scroll(function () {
    positionAddBtn();
    //positionAddQuestionToWordBtn();
});


function appDifficultyTooltip() {
    var icons = $("#QuestionDifficultyLevel .material-icons");
    icons.each(function (index, iconElement) {
        var difficulty = index == 0 ? "Easy" : index == 1 ? "Moderate" : index == 2 ? "Difficult" : "";
        $(iconElement).attr({ "data-toggle": "tooltip", "title": difficulty })
    });
    applyTooltip();
}
$(document).on("click", ".navigation-confirmation-box .closeBtn", function () {
    closeNavigationBox();
});


//$(document).on("click", '.questionAttached',function() {
//    applyTooltip();
//})
function closePopOver() {

}
$(document).on("click", ".closePopOver", function () {
    var wordid = this.dataset.wordid;
    $("#" + wordid).popover('hide')
});
function applyHoverPopover(commentaryid, wordid) {
    var title = "Commentary <i class='fas fa-times float-right closePopOver' data-wordid='" + wordid + "'></i><i class='fas fa-eye float-right  mr-2    viewCommentaryBtn' data-commentaryid='" + commentaryid + "'   data-wordid='" + wordid + "'></i> ";


    $.ajax({
        url: "/Commentary/GetCommentary",
        type: "Get",
        data: { id: commentaryid },
        success: function (commentary) {
            debugger;

            commentaryDetail = commentary.commentaryDetail.substring(0, 250);

            var content = "<p> " + commentaryDetail + "... <a class=' custom-btn btn-blue oval-rounded mb-3 viewCommentaryBtn' data-commentaryid='" + commentaryid + "' data-wordid='" + wordid + "'>Load More</a> </p>";
            $("#" + wordid).popover({
                trigger: 'hover',
                title: title,
                delay: { "show": 500, "hide": 100 },
                html: true,
                content: content
            })
            //    .on("mouseenter", function () {
            //    var _this = this;
            //    $(this).popover("show");
            //    $(this).siblings(".popover").on("mouseleave", function () {
            //        $(_this).popover('hide');
            //    });
            //}).on("mouseleave", function () {
            //    var _this = this;
            //    setTimeout(function () {
            //        if (!$(".popover:hover").length) {
            //            $(_this).popover("hide")
            //        }
            //    }, 100);
            //});
            $("#" + wordid).popover('show')


        },
        error: function (error) {
            returnErrorState(error);
        }

    });

    return false;
}
function getCommentaryContent(commentaryid) {

}
function linkWithSolution(wordid) {
    $("#" + wordid).addClass('goToNavigation');
}

function linkWithQuestion(wordid) {
    $("#" + wordid).addClass('viewQuestionBtn');
}

function applyPopover() {
    //$('.questionAttached').popover({
    //    trigger: 'focus',
    //    placement: 'auto',
    //    animation: true,
    //    html: true
    //});
    $('.questionAttached').powerTip({
        openEvents: ['mouseenter'],
        smartPlacement: true,
        closeEvents: [],
        mouseOnToPopup: true,
        html: true
    });
    $('.questionAttached').data('powertip', function () {

        var item = this.dataset;
        if (LoggedUserRole == "Student" || LoggedUserRole == "Teacher") {
            var str = "";
            var questions = 0;
            var commentaries = 0;
            var navigations = 0;
            var resultantValue = 0;
            if (item.questionid != undefined && item.questionid != "") {
                questions = $("#" + item.wordid).data().questionid.toString().split(',').length
            }
            if (item.commentaryid != undefined && item.commentaryid != "") {
                commentaries = $("#" + item.wordid).data().commentaryid.toString().split(',').length

            }
            if (item.navigationid != undefined && item.navigationid != "") {
                navigations = 1;

            }
            resultantValue = questions + commentaries + navigations;
            if (resultantValue > 1 && questions > 0) {
                str += "<i data-wordid='" + item.wordid + "' data-questionid='" + item.questionid + "' data-attributeType= '" + item.attributetype + "' data-toggle='tooltip' title='View question against this word.' class='fas fa-question showQ m-1 ml-3 mr-3 '></i>"
            }
            else if (questions > 0) {
                linkWithQuestion(item.wordid);
                return "Click here to open to question."
            }
            if (resultantValue > 1 && commentaries > 0) {
                str += "<i class='fas fa-copyright showCommentary m-1 ml-3 mr-3' data-wordid='" + item.wordid + "' data-commentaryid='" + item.commentaryid + "' data-toggle='tooltip' title='View Commentaries against this word.'></i>";

            } else if (commentaries > 0) {
                return applyHoverPopover(item.commentaryid, item.wordid, "Commentary");
            }
            if (resultantValue > 1 && navigations == 1) {
                str += "<i data-wordid='" + item.wordid + "' data-questionid='" + item.questionid + "' data-destinationtype='" + item.destinationtype + "' data-destinationid='" + item.destinationid + "' data-navigationid= '" + item.navigationid + "'  data-toggle='tooltip' title='Navigate to solution' class='fas fa-long-arrow-alt-right goToNavigation m-1 ml-3 mr-3 '></i>";

            } else if (navigations == 1) {
                linkWithSolution(item.wordid);
                return "Click here to navigate to solution."
            }
            return str;
        } else {


            if (item.navigationid != undefined && item.navigationid != "") {
                return "<i data-wordid='" + item.wordid + "' data-questionid='" + item.questionid + "' data-attributeType= '" + item.attributetype + "' data-toggle='tooltip' title='View question against this word.' class='fas fa-question showQ m-1 ml-3 mr-3 '></i><i class='fas fa-copyright showCommentary m-1 ml-3 mr-3' data-wordid='" + item.wordid + "' data-commentaryid='" + item.commentaryid + "' data-toggle='tooltip' title='View Commentaries against this word.'></i><i data-wordid='" + item.wordid + "' data-questionid='" + item.questionid + "' data-destinationtype='" + item.destinationtype + "' data-destinationid='" + item.destinationid + "' data-navigationid= '" + item.navigationid + "'  data-toggle='tooltip' title='Navigate to solution' class='fas fa-long-arrow-alt-right goToNavigation m-1 ml-3 mr-3 '></i><i data-wordid='" + item.wordid + "' data-questionid='" + item.questionid + "' data-navigationid= '" + item.navigationid + "'  data-toggle='tooltip' title='Remove navigation to solution' class='ilum ilum-remove-navigation removeNavigation m-1 ml-3 mr-3 '></i>";

            }
            else {
                return "<i data-wordid='" + item.wordid + "' data-questionid='" + item.questionid + "' data-attributeType= '" + item.attributetype + "' data-toggle='tooltip' title='View question against this word.' class='fas fa-question showQ m-1 ml-3 mr-3 '></i><i class='fas fa-copyright showCommentary m-1 ml-3 mr-3' data-wordid='" + item.wordid + "' data-commentaryid='" + item.commentaryid + "' data-toggle='tooltip' title='View Commentaries against this word.'></i><i data-wordid='" + item.wordid + "' data-questionid='" + item.questionid + "' data-navigationid= '" + item.navigationid + "'  data-toggle='tooltip' title='Add navigation to this word/sentence' class='ilum ilum-add-navigation  addNavigationToExitingWord m-1 ml-3 mr-3 '></i>";

            }
        }
    });

}



$(document).on("click", ".addQuestionToWord", function () {
    $('[data-toggle=tooltip]').powerTip();

    removeRangySelected();

    appendSelectionApplier();
    applier.toggleSelection();

    $(".selectedWord").remove();
    var selectedWord = rangy.getSelection();
    $("<input class='selectedWord' hidden='hidden' value='" + selectedWord + "' />").appendTo("body");
});

$(document).on("click", ".removeNavigation", function () {
    var btn = this.dataset;
    var wordId = btn.wordid;

    $("#" + wordId).attr("data-navigationid", "").attr("data-destinationtype", "").attr("data-destinationid", "").attr("href", "");
    saveHtml(wordId);
    smallSwal({
        type: 'success',
        title: "Removed"
    });
    //swal("Removed", "Navigation to solution has been removed.", "success");
});

$(document).on("click", ".addNavigationToExitingWord", function () {

    var btn = this.dataset;
    var wordId = btn.wordid;

    showNavigationBox();

    $(".navigation-confirmation-box .ModalWordId").remove();
    $(".navigation-confirmation-box").append("<input hidden='hidden' value='" + wordId + "' class='ModalWordId' />");
    activateNaviationAppliedPowertip();

});
function activateNaviationAppliedPowertip() {
    navigationAppliedPowerTip();
    $(".navigationApplied").addClass("active");
}
function deactivateNaviationAppliedPowertip() {
    $.powerTip.destroy(".navigationApplied");
    $(".navigationApplied").removeClass("active");
}
$(document).on("click", ".applyExistingNavigationBtn", function () {

    var nav = this.dataset;
    var id = nav.navigationid;

    if ($(".navigation-confirmation-box .ModalWordId").length > 0) {

        var wordId = $(".navigation-confirmation-box .ModalWordId").val();
        $("#" + wordId).attr("data-navigationid", id);
        $(".navigation-confirmation-box .ModalWordId").remove();
        saveAndCloseModal(id, wordId);
        deactivateNaviationAppliedPowertip();
    } else {
        if ($('.selectedRangy').length > 0) {
            var wordSelected = $('.selectedRangy').text();
            $.ajax({
                url: "/Book/AddWord",
                type: "POST",
                data: { wordText: wordSelected },
                success: function (word) {
                    appendNavigationToWord(id, word.id);
                    saveAndCloseModal(id, word.id);
                    deactivateNaviationAppliedPowertip();
                },
                error: function (error) {
                    returnErrorState(error);
                }

            });
        }
    }
});
//Navigation
function appendNavigationToWord(NavigationId, WordId) {

    var SelectedText = $(".selectedRangy").text();
    var anchorTag = "<a href='#" + NavigationId + "' id='" + WordId + "' data-navigationid='" + NavigationId + "' data-wordid='" + WordId + "' class='questionAttached'> " + SelectedText + "</a>";

    $(".selectedRangy").replaceWith(anchorTag);
}
$(document).on("click", ".navigation-confirmation-box .saveBtn", function () {


    var selectedWord = rangy.getSelection();

    if (selectedWord.toString().length > 0) {
        var id = guid();
        appendNavigationApplier(id);
        navApplier.toggleSelection();
        if ($(".navigation-confirmation-box .ModalWordId").length > 0) {

            var wordId = $(".navigation-confirmation-box .ModalWordId").val();
            $("#" + wordId).attr("data-navigationid", id);
            $(".navigation-confirmation-box .ModalWordId").remove();
            saveAndCloseModal(id, wordId);
            deactivateNaviationAppliedPowertip();


        } else {


            var wordSelected = $('.selectedRangy').text();
            $.ajax({
                url: "/Book/AddWord",
                type: "POST",
                data: { wordText: wordSelected },
                success: function (word) {
                    appendNavigationToWord(id, word.id);
                    saveAndCloseModal(id, word.id);
                    deactivateNaviationAppliedPowertip();
                },
                error: function (error) {
                    returnErrorState(error);
                }

            });

        }

    } else {
        swal("", "Select Some Destination Text First!", "error")

    }




});
function saveAndCloseModal(destinationWordId, wordid) {
    saveNavigationHtml(destinationWordId, wordid);

    ////var newHtml = $('#addNavigationModal #LoadedNavigationDocument').html();
    ////$('.Page').html(newHtml);
    applyPopover();
    //swal("Applied", "Navigation Applied", "success");
    smallSwal({
        type: 'success',
        title: "Navigation Applied!"
    });
    setTimeout(function () {
        closeNavigationBox();
    }, 1000);
}
function saveNavigationHtml(destinationWordId, wordid) {

    deactivateNaviationAppliedPowertip();


    var sourceMainPage = $("#" + wordid).parents("[data-savetype]");

    var attributeType = sourceMainPage.attr("data-savetype");
    var attributeId = sourceMainPage.attr("data-id");


    var destinationPageType = $("#" + destinationWordId).parents("[data-savetype]").attr("data-savetype");
    var destinationPageId = $("#" + destinationWordId).parents("[data-savetype]").attr("data-id");

    $("#" + wordid).attr("data-destinationtype", destinationPageType).attr("data-destinationid", destinationPageId);

    var destinationPageDetail = $("#" + destinationWordId).parents("[data-savetype]").html();
    var detail = sourceMainPage.html();
    var model = {
        Detail: detail,
        Id: attributeId,
    }
    var desModel = {
        Detail: destinationPageDetail,
        Id: destinationPageId,
    }
    if (destinationPageType == attributeType && attributeId == destinationPageId) {
        save(model, attributeType);
    } else {
        save(model, attributeType);
        save(desModel, destinationPageType);

    }

}

function save(model, attributeType) {
    deactivateNaviationAppliedPowertip();
    $.ajax({
        url: '/Book/Save' + attributeType,
        type: "POST",
        data: {
            model: model
        },
        success: function () {

        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}




$(document).on("click", ".goToNavigation ", function () {
    var btn = this.dataset;
    var navigationid = btn.navigationid;
    var wordid = btn.wordid;

    var destinationtype = btn.destinationtype;
    var destinationid = btn.destinationid;
    if ($(".main-page [data-savetype=" + destinationtype + "][data-id=" + destinationid + "]").length > 0) {
        scrollTo("#" + navigationid + "");
        $("#" + navigationid + "").addClass("blink_me");
        setInterval(function () {
            $("#" + navigationid + "").removeClass("blink_me");
        }, 8000);
        $(".backToNavigationWord").attr("data-wordid", wordid);
        $(".backToNavigationWord").fadeIn();
        $(".backToNavigationWord").removeClass("hidden");
        $(".ring-container").removeClass("hidden");
    }
    else if ((destinationtype == "Question" || destinationtype == "Answer") && !$("#" + navigationid).length > 0) {
        loadQuestionForNavigation(destinationid, wordid, navigationid, destinationtype);

    }
    else if (destinationtype == "Commentary") {
        loadCommentaryForNavigation(destinationid, wordid, navigationid, destinationtype);

    }
    else if (destinationtype == "Subject" || destinationtype == "Chapter" || destinationtype == "Section" || destinationtype == "SubSection") {
        loadDocumentForNavigation(destinationtype, destinationid, wordid, navigationid)
    }
    else {
        scrollTo("#" + navigationid + "");
        $("#" + navigationid + "").addClass("blink_me");
        setInterval(function () {
            $("#" + navigationid + "").removeClass("blink_me");
        }, 8000);
        $(".backToNavigationWord").attr("data-wordid", wordid);
        $(".backToNavigationWord").fadeIn();
        $(".backToNavigationWord").removeClass("hidden");
        $(".ring-container").removeClass("hidden");
    }


});
$(document).on("click", ".backToNavigationWord ", function () {
    var btn = this.dataset;
    var wordid = btn.wordid;

    scrollTo("#" + wordid + "");
    $(".backToNavigationWord").addClass("hidden");
    $(".ring-container").addClass("hidden");
    $("#" + wordid + "").addClass("blink_me");
    $(".backToNavigationWord").attr("data-wordid", "");
    setInterval(function () {
        $("#" + wordid + "").removeClass("blink_me");
    }, 8000);
});

$(document).on("click", ".backToWord ", function () {
    var btn = this.dataset;
    var wordid = btn.wordid;

    scrollTo("#" + wordid + "");
    $("#" + wordid + "").addClass("blink_me");
    setInterval(function () {
        $("#" + wordid + "").removeClass("blink_me");
    }, 8000);
});
function resizeTable() {
    $("#page-wrapper .table-body").css("max-height", $(window).innerHeight() - $("header").innerHeight() - 280)
}
$(window).resize(function () {
    if ($(window).innerWidth() > 767) {
        resizeTable();
    }

});
$(document).ready(function () {
    resizeTable();
    LoggedUserRole = $(".LoggedUserRole").val();
    positionAddBtn();
    //$('.addQuestionToWord').popover({
    //    trigger: 'focus',navigationApplied
    //    html: true,
    //    content: "<div><i class='fas fa-question m-1 powertip' data-toggle='tooltip' title='Add Question' onclick='openAddingQuestionModel()'></i><i class='fas fa-copyright powertip m-1'  data-toggle='tooltip' title='Add Commentary'  onclick='openAddingCommentaryModel()'></i></div>"
    //});
    $('.addQuestionToWord').powerTip({
        openEvents: ['click'],
        closeEvents: [],
        mouseOnToPopup: true
    });
    $('.addQuestionToWord').data('powertip', function () {
        $('[data-toggle=tooltip]').powerTip();
        return "<div><i class='fas fa-question m-1 ml-3 mr-3 ' data-toggle='tooltip' title='Add Question' onclick='openAddingQuestionModel()'></i><i class='fas fa-copyright powertip m-1 ml-3 mr-3'  data-toggle='tooltip' title='Add Commentary'  onclick='openAddingCommentaryModel()'></i><i class='ilum ilum-add-navigation powertip m-1 ml-3 mr-3'  data-toggle='tooltip' title='Add Solution Navigation'  onclick='openAddingNavigationModel()'></i></div>";
    });


    applyPopover();


    $('#QuestionDifficultyLevel').addRating({
        max: 3,
        half: false,
        fieldId: "QuestionDifficultyLevelId",
        fieldName: "QuestionDifficultyLevelId",
        selectedRatings: 1
    });
    appDifficultyTooltip();
    $("#appendQuestion #QustionTypeId").ddslick({
        showSelectedHTML: false
    });

    saveDocumentForm();
    addQuestionForm();
    addCommentaryForm();
    if ($(".Page").length > 0) {
        var imgs = $(".Page img");
        imgs.each(function (i, element) {

            if ($(element).attr("src").indexOf("ConvertedHtmls") == -1 && $(element).attr("src").indexOf("UploadedArtifacts") == -1) {
                $(element).attr("src", "/ConvertedHtmls/" + $(element).attr("src"))
            }

        });
        $("a[href*='http://www.sautinsof']").remove();

    }
    if ($("#fine-uploader-manual-trigger").length > 0) {
        attachUploader();
    }
    applyTooltip();



    $(document).on("click", ".question-box i.fa-chevron-left", function (d, e) {
        $(this.parentElement).css("height", 80);
        $(this.parentElement).css("width", 120);
        $(this).removeClass("fa-chevron-left").addClass("fa-times");
        $(".question-box .btns").fadeIn("slow")
    })
    $(document).on("click", ".question-box i.fa-times", function (d, e) {
        $(this.parentElement).css("height", 34);
        $(this.parentElement).css("width", 40);
        $(this).removeClass("fa-times").addClass("fa-chevron-left");
        $(".question-box .btns").hide()

    })
    $(".moveTo-question").click(function () {
        $(".moveTo-question-input").slideToggle();
    });


});


