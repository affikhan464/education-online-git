﻿/*! Question.js
 
 */
QuestionId = "";
QuestionAttributeType = "";

//Question
function appendQuestionToWord(QuestionId, QuestionAttributeType, WordId) {

    var SelectedText = $("#selectedRangy").text();
    var anchorTag = "<a href='#' id='" + WordId + "' data-questionid='" + QuestionId + "' data-attributetype='" + QuestionAttributeType + "' data-wordid='" + WordId + "' class='questionAttached'> " + SelectedText + "</a>";

    $("#selectedRangy").replaceWith(anchorTag);

    //$("#" + WordId).attr('data-content', "<i data-wordid='" + WordId + "' data-questionid='" + QuestionId + "' data-attributeType= '" + QuestionAttributeType + "' data-toggle='tooltip' title='See question against this word' class='fas fa-question showQ'></i> <i   data-wordid='" + WordId + "' data-questionid='" + QuestionId + "' data-attributeType= '" + QuestionAttributeType + "' data-toggle='tooltip' title='Add more question' class='fas fa-plus  addMoreQ'></i>");
}

function openAddingQuestionModel() {
    $('#addQuestionForm')[0].reset();
    $("#addQuestionForm .existingQuestionsSection  .selectQuestion").removeClass("selected");
    $(".newQuestionSection").show();
    //removeRangySelected();

    //appendSelectionApplier();
    //applier.toggleSelection();
    $(".QuestionDifficultyLevel").html("");
    $("#addQuestionForm .QuestionDifficultyLevel").addRating({
        selectedRatings: 1,
        max: 3,
        fieldId: "QuestionDifficultyLevelId",
        fieldName: "QuestionDifficultyLevelId",
    });

    loadAvailableQuestions();
    //$(".selectedWord").remove();
    $("#WordId").val(0);
    //$(".existingQuestionsSection").hide();
    var selectedWord = rangy.getSelection();

    if (selectedWord.toString().length > 0 || $(".selectedWord").val().trim().length > 0) {
        $("#SelectedWord").val(selectedWord.toString());

        $("#appendQuestion #QustionTypeId").ddslick({
            showSelectedHTML: false
        });

        $(".Yearpicker").datepicker({
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years"
        });
        $("#appendQuestion").modal();



        if (tinymce.get("AnswerDetail") !== null)
            tinymce.get("AnswerDetail").remove();

        if (tinymce.get("QuestionDetail") !== null)
            tinymce.get("QuestionDetail").remove();

        TinyMVCGlobalOptions.selector = "#addQuestionForm .tmceeditor";

        tinymce.init(TinyMVCGlobalOptions);
        $('#addQuestionForm .mce-tinymce.mce-container').css("display", "block");

        addQuestionForm();

    } else {
        swal("", "Select Some Text First!", "error")

    }
    addQuestionForm();
}
function appendAttributeToWord(data) {

    var wordId = data.data.wordId;
    var isWordExist = $("#" + wordId).length > 0;
    var questionIds = data.data.questionIds == undefined ? "" : data.data.questionIds;

    if (isWordExist) {

        $("#" + wordId).attr("data-questionid", questionIds);

    } else {

        appendQuestionToWord(questionIds, data.data.attributeType, data.data.wordId);
    }


    applyPopover();
    saveHtml(wordId);


}
function appendAttributeToWordAftarRemoveQuestion(data) {

    var wordId = data.data.wordId;
    var isWordExist = $("#" + wordId).length > 0;
    var questionIds = data.data.ids == undefined ? "" : data.data.ids;

    if (isWordExist) {

        $("#" + wordId).attr("data-questionid", questionIds);

    } else {

        appendQuestionToWord(questionIds, data.data.attributeType, data.data.wordId);
    }


    applyPopover();
    saveHtml(wordId);


}
//Add Question Form Submission
function addQuestionForm() {

    var $form = $('#addQuestionForm');
    $form.off("submit");
    $form.submit(function (data, event) {

        var saveBtn = $("#addQuestionForm #saveBtn");
        smallSwal("Please Wait...!", '', 'info');
        saveBtn.addClass("disableClick");

        if ($(".selectedWord").length > 0) {
            $("#addQuestionForm [name=SelectedWord]").val($(".selectedWord").val())
        }
        else
            $("#addQuestionForm [name=SelectedWord]").val($("#selectedRangy").text());
        var selectedWord = rangy.getSelection();
        //if (selectedWord.toString().length > 0 || $(".selectedWord").length > 0) {
        if (!$(".existingQuestionsSection  .selectQuestion.selected").length > 0) {

            $("#addQuestionForm [name=SelectedWord]").val($(".selectedWord").val())
            $.post($(this).attr('action'), $(this).serialize(), function (response) {

            }, 'json').done(function (data) {

                if (data.success) {
                    smallSwal("Successfull!!", data.message, "success");
                    saveBtn.removeClass("disableClick");
                    appendAttributeToWord(data);
                } else {
                    swal("Error!!", data.message, "error");
                    saveBtn.removeClass("disableClick");
                }

            });
        } else {

            var elements = $(".existingQuestionsSection  .selectQuestion.selected");
            var ids = "";
            elements.each(function (i, element) {
                ids += element.dataset.questionid + ",";
            })

            var model = {
                Ids: ids,
                SelectedWord: $("#selectedRangy").text(),
                WordId: $("#addQuestionForm [name=WordId]").val()
            }

            $.post("/Question/AddAvailableQuestion", model, function (response) {

            }, 'json').done(function (data) {

                if (data.success) {
                    smallSwal("Successfull!!", data.message, "success");
                    saveBtn.removeClass("disableClick");
                    appendAttributeToWord(data);
                } else {
                    saveBtn.removeClass("disableClick");
                    swal("Error!!", data.message, "error")

                }

            });

        }
        //} else {
        //    swal("Text Unselected", "Re-select the text first!", "error")

        //}
        return false;
    });



};

function loadAvailableQuestions(wordId, attributeId, attributeType) {

    wordId = wordId == undefined ? 0 : wordId;
    attributeId = attributeId == undefined ? $(".Page").length > 0 ? $(".Page").data().attributeid : $(".opened-attribute").data().attributeid : attributeId;
    attributeType = attributeType == undefined ? $(".Page").length > 0 ? $(".Page").data().attributetype : $(".opened-attribute").data().attributetype : attributeType;


    getAvailableQuestions(wordId, attributeId, attributeType, "#addQuestionForm .existingQuestionsSection", "#appendQuestion");

}
function getAvailableQuestions(wordId, attributeId, attributeType, viewSection, modal) {
    $.ajax({
        url: '/Question/GetAvailableQuestions',
        type: "POST",

        data: {
            attributeId: attributeId, attributeType: attributeType, wordId: wordId
        },
        success: function (view) {

            $(viewSection).html(view);
            $(viewSection + " #WordId").val(wordId);
            $(viewSection + " #WordId").attr("value", wordId);
            var questionDeficulties = $(viewSection + " [name=questionDificulty]");
            questionDeficulties.each(function (i, e) {

                var level = e.dataset.rating;
                $(e).addRating({ selectedRatings: level, max: 3 });
            });
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}

$(document).on("click", ".selectQuestion", function (event) {
    var btn = $(this);
    if (!btn.hasClass("selected")) {
        btn.addClass("selected");
        $(".newQuestionSection").hide();
    } else {
        btn.removeClass("selected");
        var selectQuestionsLength = $("#addQuestionForm .existingQuestionsSection  .selectQuestion.selected").length;
        if (selectQuestionsLength > 0) {
            $(".newQuestionSection").hide();
        } else {

            $(".newQuestionSection").show();

        }
    }
});

$(document).on("click", ".showQ", function (event) {
    var btn = this;
    var questionids = $(btn).data().questionid;
    var wordid = $(btn).data().wordid;

    loadQuestions(wordid, questionids, "#questionListModal .existingQuestionsSection", "#questionListModal");
});
$(document).on("click", ".viewQuestionBtn", function (event) {
    var btn = this;
    var wordid = $(btn).data().wordid;
    var questionid = $(btn).data().questionid;
    var isQuestionExist = $("#Question_" + questionid).length > 0;
    if (isQuestionExist) {
        $("#Question_" + questionid).remove();
        loadQuestionDetail(questionid, wordid);
    } else
        loadQuestionDetail(questionid, wordid);

    applyTooltip();
    applyPopover();

});



$(document).on("click", ".addMoreQ", function (event) {
    $('#addQuestionForm')[0].reset();
    $("#questionListModal .close").click();
    $("#appendQuestion .existingQuestionsSection").show();
    $("#addQuestionForm .existingQuestionsSection  .selectQuestion").removeClass("selected");
    $("#appendQuestion .newQuestionSection").show();
    var btn = this;
    var wordid = $(btn).data().wordid;
    var attributeId = $(btn).data().attributeid;
    var attributeType = $(btn).data().attributetype;
    var selectedText = $("#" + $(btn).data().wordid).text();
    $("#appendQuestion [name=WordId]").val(wordid);
    $(".selectedWord").remove();
    $("<input class='selectedWord' hidden value='" + selectedText + "' />").appendTo("body");
    //loadQuestions($(btn).data().wordid,$(btn).data().questionid, "#addQuestionForm .existingQuestionsSection", "#appendQuestion");
    //open modal on success

    loadAvailableQuestions(wordid, attributeId, attributeType);


    $("#appendQuestion #QustionTypeId").ddslick({
        showSelectedHTML: false
    });

    $(".QuestionDifficultyLevel").html("");
    $("#appendQuestion .QuestionDifficultyLevel").addRating({
        selectedRatings: 1,
        max: 3,
        fieldId: "QuestionDifficultyLevelId",
        fieldName: "QuestionDifficultyLevelId",
    });

    $(".Yearpicker").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });
    $("#appendQuestion").modal();



    if (tinymce.get("AnswerDetail") !== null)
        tinymce.get("AnswerDetail").remove();

    if (tinymce.get("QuestionDetail") !== null)
        tinymce.get("QuestionDetail").remove();

    TinyMVCGlobalOptions.selector = "#addQuestionForm .tmceeditor";


    tinymce.init(TinyMVCGlobalOptions);
    $('#addQuestionForm .mce-tinymce.mce-container').css("display", "block");
    addQuestionForm();
});

function editQuestionForm() {

    var $form = $('#editQuestionForm');
    $form.off("submit");
    $form.submit(function (data, event) {

        var saveBtn = $("#editQuestionForm #saveBtn");
        smallSwal("Please Wait...!", '', 'info');
        saveBtn.addClass("disableClick");

        $.post($(this).attr('action'), $(this).serialize(), function (response) {

        }, 'json').done(function (data) {

            if (data.success) {
                swal("Updated!!", data.message, "success");
                saveBtn.removeClass("disableClick");

            } else {
                swal("Error!!", data.message, "error")
                saveBtn.removeClass("disableClick");

            }

        });

        return false;
    });



};

$(document).on("click", ".editQuestionBtn", function (event) {


    var btn = this;
    var questionId = $(btn).data().questionid;

    //open modal on success

    $.ajax({
        url: '/Question/Edit',
        type: "POST",

        data: {
            id: questionId
        },
        success: function (view) {
            $("#editQuestionModal .questionEditors").html(view);
            $(".QuestionDifficultyLevel").html("");
            $("#editQuestionModal .QuestionDifficultyLevel").addRating({
                max: 3,
                half: false,
                fieldName: "QuestionDifficultyLevelId",
                selectedRatings: $(".QuestionDifficultyLevelId").val()
            });
            $("#editQuestionModal").modal();


            if (tinymce.get("EditAnswerDetail") !== null)
                tinymce.get("EditAnswerDetail").remove()

            if (tinymce.get("EditQuestionDetail") !== null)
                tinymce.get("EditQuestionDetail").remove()

            $("#editQuestionModal #QustionTypeId").ddslick({
                showSelectedHTML: false,

                //onSelected: function (selectedData) {
                //
                //    $("#editQuestionModal [name=QustionTypeId]").val(selectedData.selectedData.value);
                //}
            });

            TinyMVCGlobalOptions.selector = "#editQuestionModal .tmceeditor";
            tinymce.init(TinyMVCGlobalOptions);
            $('#editQuestionModal .mce-tinymce.mce-container').css("display", "block");
            editQuestionForm();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })

});
$(document).on("click", ".removeQuestionBtn", function (event) {

    var btn = this;
    var questionid = $(btn).data().questionid;
    var wordid = $(btn).data().wordid;

    $.ajax({
        url: '/Question/Remove',
        type: "POST",

        data: {
            id: questionid, wordId: wordid
        },
        success: function (data) {
            if (data.success) {
                smallSwal({
                    type: 'success',
                    title: data.message
                });

                appendAttributeToWordAftarRemoveQuestion(data);
                loadQuestions(wordid, questionid, "#questionListModal .existingQuestionsSection ", "#questionListModal");

            } else {
                swal("Error!!", data.message, "error");

            }
        },
        error: function (error) {
            returnErrorState(error);
        }

    })

});
$(document).on("click", ".deleteQuestionBtn", function (event) {

    var btn = this;
    var questionid = $(btn).data().questionid;
    var attributeId = $("#appendQuestion #AttributeId").val();
    var attributeType = $("#appendQuestion #AttributeType").val();


    swal({
        title: "Are you sure?",
        text: "Are you sure to delete this question from application completely?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {


                $.ajax({
                    url: '/Question/Delete',
                    type: "POST",

                    data: {
                        id: questionid
                    },
                    success: function (data) {
                        if (data.success) {
                            smallSwal({
                                type: 'success',
                                title: data.message
                            });
                            var wordid = $("#WordId").val();
                            loadAvailableQuestions(wordid, attributeId, attributeType);

                        } else {
                            swal("Error!!", data.message, "error");

                        }
                    },
                    error: function (error) {
                        returnErrorState(error);
                    }

                })

            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });

});
$(document).on("click", ".closeQuestionBtn", function (event) {

    var btn = this;

    var questionid = $(btn).data().questionid;
    var wordid = $(btn).data().wordid;
    $("#Question_" + questionid).remove();

    scrollTo("#" + wordid);
    $("#" + wordid).addClass("blink_me");
    setInterval(function () {
        $("#" + wordid).removeClass("blink_me");
    }, 4000);
});


$(document).on("click", "a.questionAttached", function (event) {
    applyTooltip();
    // This will prevent the default action of the anchor
    event.preventDefault();

    // Failing the above, you could use this, however the above is recommended
    return false;

});
function loadQuestionDetail(questionId, wordid) {
    $.ajax({
        url: '/Question/Get',
        type: "POST",

        data: {
            id: questionId
        },
        success: function (view) {

            $(".question-detail").append(view);
            $("#questionListModal .close").click()
            $("#Question_" + questionId + " .backToWord").attr("data-wordid", wordid);
            scrollTo("#Question_" + questionId);

            if ($(".navigation-confirmation-box").css("display") == "block") {
                activateNaviationAppliedPowertip();
            }
            applyPopover();
            var questionDeficulties = $("#Question_" + questionId + " [name=questionDificulty]");
            questionDeficulties.each(function (i, e) {

                var level = e.dataset.rating;
                $(e).addRating({ selectedRatings: level, max: 3 });
            });
            appDifficultyTooltip();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}
function loadQuestionForNavigation(questionId, wordid, navigationid, attributType) {
    $.ajax({
        url: '/Question/Get',
        type: "POST",

        data: {
            id: questionId
        },
        success: function (view) {
            $("#Question_" + questionId).remove();
            $(".question-detail").append(view);
            $("#Question_" + questionId + " .backToWord").attr("data-wordid", wordid);

            if ($(".navigation-confirmation-box").css("display") == "block") {
                activateNaviationAppliedPowertip();
            }
            if (attributType == "Answer") {
                $("#Answer_" + questionId).css("display", "block");
            }
            $("#" + navigationid + "").addClass("blink_me");
            setInterval(function () {
                $("#" + navigationid + "").removeClass("blink_me");
            }, 4000);
            $(".backToNavigationWord").attr("data-wordid", wordid);
            $(".backToNavigationWord").fadeIn();
            $(".backToNavigationWord").removeClass("hidden");
            $(".ring-container").removeClass("hidden");
            scrollTo("#" + navigationid);
            applyPopover();

        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}
function loadQuestions(wordid, questionsIds, viewSection, modal) {
    $.ajax({
        url: '/Question/GetQuestions',
        type: "POST",

        data: {
            ids: questionsIds, wordId: wordid
        },
        success: function (view) {

            $(viewSection).html("");
            $(viewSection).html(view);
            //open modal on success
            $(modal).modal();
            var questionDeficulties = $(viewSection + " [name=questionDificulty]");
            questionDeficulties.each(function (i, e) {

                var level = e.dataset.rating;
                $(e).addRating({ max: 3,selectedRatings: level });
            });
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}