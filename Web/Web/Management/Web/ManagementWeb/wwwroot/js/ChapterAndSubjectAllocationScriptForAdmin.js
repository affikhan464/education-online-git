﻿/*! ChapterAndSubjectAllocationScriptForAdmin.js*/
$(document).on("click", ".allocateChapterBtn", function () {

    var chapterid = this.dataset.chapterid;
    var userid = this.dataset.userid;
    $.ajax({
        url: '/Book/AllocateChapterRequest',

        type: "POST",
        data: { userId: userid, chapterId: chapterid },
        success: function (response) {
            if (response.success) {
                // swal("Allocated!", response.message, "success");
                smallSwal({
                    type: 'success',
                    title: response.message
                });
                if ($("#AllChapterRequests").length > 0) {
                    showChapterRequests();

                }

                else if ($("#AssignedSubjects.in").length > 0) {
                    showAllocatedSubjectByUserId(userid);
                }
                else if ($("#detailPage").length > 0) {
                    loadUserDetailById(userid);
                }
            }
            else {
                swal("Error!", response.message, "error")

            }

        },
        error: function (error) {
            returnErrorState(error);
        }

    });
});
$(document).on("click", ".allocatedChapterBtn", function () {

    var btn = $(this);
    if (btn.parents("#AssignedSubjects").length > 0) {
        $("#AssignedSubjects .close").click();
        $("#AllocatedChapterModal").attr("data-userid", btn.data().userid);
    }
    $("#AllocatedChapterModal").modal();
    emptyAllDDL("#AllocatedChapterModal");

});
$(document).on("click", "#allocateChapterBtn", function () {

    var chapterId = $("#AllocatedChapterModal .ChapterDDL .dd-selected-value").val();
    if ($("#detailPage").length > 0) {
        userid = $("#detailPage").data().userid;

    }
    else {
        userid = $("#AllocatedChapterModal").attr("data-userid");
    }
    if (chapterId != 0 && chapterId != undefined) {


        $.ajax({
            /**/
            url: '/Book/AllocateChapter',
            /**/
            type: "POST",
            data: { userId: userid, chapterId: parseInt(chapterId) },
            success: function (response) {
                if (response.success) {
                    //swal("Done!", response.message, "success");
                    smallSwal({
                        type: 'success',
                        title: response.message
                    });
                    if ($("#detailPage").length > 0) {
                        loadUserDetailById(userid);
                    }


                }
                else {
                    swal("Error!", response.message, "error")

                }

            },
            error: function (error) {
                returnErrorState(error);
            }

        })
    } else {
        swal("Select Chapter", "<strong>Please Select Chapter!</strong>", "error");
    }
})
/*Subject Allocation Scripts*/
$(document).on("click", ".allocateSubjectBtn", function () {

    var subjectid = this.dataset.subjectid;
    var userid = this.dataset.userid;
    $.ajax({
        url: '/Book/AllocateSubjectRequest',

        type: "POST",
        data: { userId: userid, subjectId: subjectid },
        success: function (response) {
            if (response.success) {
                // swal("Allocated!", response.message, "success");
                smallSwal({
                    type: 'success',
                    title: response.message
                });
                if ($("#AllRequests").length > 0) {
                    showRequests();
                }

                else if ($("#AssignedSubjects.in").length > 0) {
                    showAllocatedSubjectByUserId(userid);
                }
                else if ($("#detailPage").length > 0) {
                    loadUserDetailById(userid);
                }
            }
            else {
                swal("Error!", response.message, "error")

            }

        },
        error: function (error) {
            returnErrorState(error);
        }

    });
});
$(document).on("click", ".allocatedSubjectBtn", function () {
    var btn = $(this);
    if (btn.parents("#AssignedSubjects").length > 0) {
        $("#AssignedSubjects .close").click();
        $("#AllocatedSubjectModal").attr("data-userid", btn.data().userid);
    }
    $("#AllocatedSubjectModal").modal();
    emptyAllDDL("#AllocatedSubjectModal");
});
$(document).on("click", "#allocateBtn", function () {

    var subjectId = $("#AllocatedSubjectModal .SubjectDDL .dd-selected-value").val();
    if ($("#detailPage").length > 0) {
        userid = $("#detailPage").data().userid;

    }
    else {
        userid = $("#AllocatedSubjectModal").attr("data-userid");
    }
    if (subjectId != 0 && subjectId != undefined) {

        $.ajax({
            url: '/Book/AllocateSubject',
            type: "POST",
            data: { userId: userid, subjectId: parseInt(subjectId) },
            success: function (response) {
                if (response.success) {
                    //swal("Done!", response.message, "success");

                    smallSwal({
                        type: 'success',
                        title: response.message
                    });

                    if ($("#detailPage").length > 0) {
                        loadUserDetailById(userid);
                    }


                }
                else {
                    swal("Error!", response.message, "error")

                }

            },
            error: function (error) {
                returnErrorState(error);
            }

        })
    } else {
        swal("Select Subject", "<strong>Please Select Subject!</strong>", "error");
    }
})
$(document).on("click", ".assignedSubjectsBtn", function () {

    var userid = this.dataset.userid;
    showAllocatedSubjectByUserId(userid);
});

/*Batches Allocation Scripts*/
$(document).on("click", ".allocateBatchBtn", function () {

    var batchid = this.dataset.batchid;
    var userid = this.dataset.userid;
    $.ajax({
        url: '/Batch/AllocateBatchRequest',
        type: "POST",
        data: { userId: userid, batchId: batchid },
        success: function (response) {
            if (response.success) {
                // swal("Allocated!", response.message, "success");
                smallSwal({
                    type: 'success',
                    title: response.message
                });
                if ($("#AllBatchRequests").length > 0) {
                    showRequests();
                }

                else if ($("#AssignedBatches.in").length > 0) {
                    showAllocatedBatchByUserId(userid);
                }
                else if ($("#detailPage").length > 0) {
                    loadUserDetailById(userid);
                }
            }
            else {
                swal("Error!", response.message, "error")

            }

        },
        error: function (error) {
            returnErrorState(error);
        }

    });
});
$(document).on("click", ".allocatedBatchBtn", function () {
    var btn = $(this);
    if (btn.parents("#AssignedBatches").length > 0) {
        $("#AssignedBatches .close").click();
        $("#AllocatedBatchModal").attr("data-userid", btn.data().userid);
    }
    $("#AllocatedBatchModal").modal();
    emptyAllDDL("#AllocatedBatchModal");
});
$(document).on("click", "#allocateBatchBtn", function () {

    var batchId = $("#AllocatedBatchModal .BatchDDL .dd-selected-value").val();
    if ($("#detailPage").length > 0) {
        userid = $("#detailPage").data().userid;

    }
    else {
        userid = $("#AllocatedBatchModal").attr("data-userid");
    }
    if (batchId != 0 && batchId != undefined) {

        $.ajax({
            url: '/Book/AllocateBatch',
            type: "POST",
            data: { userId: userid, batchId: parseInt(batchId) },
            success: function (response) {
                if (response.success) {
                    //swal("Done!", response.message, "success");

                    smallSwal({
                        type: 'success',
                        title: response.message
                    });

                    if ($("#detailPage").length > 0) {
                        loadUserDetailById(userid);
                    }


                }
                else {
                    swal("Error!", response.message, "error")

                }

            },
            error: function (error) {
                returnErrorState(error);
            }

        })
    } else {
        swal("Select Batch", "<strong>Please Select Batch!</strong>", "error");
    }
})
$(document).on("click", ".assignedBatchesBtn", function () {

    var userid = this.dataset.userid;
    showAllocatedBatchByUserId(userid);
});

$(document).on("click", ".unassignBatchTeacherButton", function () {
    var userid = $(this).data("userid");
    var callfrom = $(this).data("callfrom");
    var $form = $(this).parents("form");
    swal({
        title: 'Are you sure?',
        html: 'Type <b>"remove"</b>, to unassign teacher from this batch.',
        input: 'text',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Unassign',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                if (text != 'remove') {
                    swal.showValidationError('Type correct spelling of remove.');
                    resolve();
                } else {
                    $form.off("submit");
                    $form.submit(function (data, event) {
                        $.post($(this).attr('action'), $(this).serialize(), function (response) {
                        }, 'json').done(function (data) {
                            if (data.success) {
                                smallSwal({
                                    type: 'success',
                                    title: data.message
                                });
                                if (callfrom=="assignedBatches") {
                                    showAllocatedBatchByUserId(userid);
                                } else {
                                    setTimeout(function () {
                                        location.href = location.href;
                                    }, 1000);
                                }
                            } else {
                                swal("Error!!", data.message, "error");
                            }
                        });
                        return false;
                    });
                    $form.submit();
                }
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
});