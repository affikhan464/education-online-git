﻿/*! ChapterAndSubjectAllocationScript.js
 
 */

$(document).on("click", ".deallocateChapterBtn", function () {
    var userid = this.dataset.userid;
    if ($("#detailPage").length > 0) {
        userid = $("#detailPage").data().userid;

    }


    var chapterId = this.dataset.chapterid;
    swal({
        title: "Are you sure?",
        text: "Are You Sure to deallocate this chapter?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                $.ajax({
                    url: '/Book/DeallocateChapter',
                    type: "POST",
                    data: { userId: userid, chapterId: parseInt(chapterId) },
                    success: function (response) {
                        if (response.success) {
                            //swal("Deallocated!", response.message, "success");
                            smallSwal({
                                type: 'success',
                                title: response.message
                            });
                            if ($("#detailPage").length > 0) {
                                loadUserDetailById(userid);
                            }
                            if ($("#AllChapterRequests").length > 0) {
                                showChapterRequests();
                            }
                            else if ($("#AssignedSubjects.in").length > 0) {
                                showAllocatedSubjectByUserId(userid);
                            }
                        }
                        else {
                            swal("Error!", response.message, "error")

                        }

                    },
                    error: function (error) {
                        returnErrorState(error);
                    }

                })

            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });



});


$(document).on("click", ".deleteAllocatedChapterRequestBtn", function () {
    var userid = this.dataset.userid;
    if ($("#detailPage").length > 0) {
        userid = $("#detailPage").data().userid;

    }


    var chapterId = this.dataset.chapterid;
    swal({
        title: "Are you sure?",
        text: "Are You Sure to delete this reqeust?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                $.ajax({
                    url: '/Book/DeleteAllocatedChapterRequest',
                    type: "POST",
                    data: { userId: userid, chapterId: parseInt(chapterId) },
                    success: function (response) {
                        if (response.success) {
                            //swal("Deallocated!", response.message, "success");
                            smallSwal({
                                type: 'success',
                                title: response.message
                            });
                            if ($("#detailPage").length > 0) {
                                loadUserDetailById(userid);
                            }
                            if ($("#AllChapterRequests").length > 0) {
                                showChapterRequests();
                            }
                            else if ($("#AssignedSubjects.in").length > 0) {
                                showAllocatedSubjectByUserId(userid);
                            }
                        }
                        else {
                            swal("Error!", response.message, "error")

                        }

                    },
                    error: function (error) {
                        returnErrorState(error);
                    }

                })

            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });



});


$(document).on("click", ".showChapterRequestsBtn", function () {

    showChapterRequests();

});
function showChapterRequests() {
    $.ajax({
        url: '/Book/ChapterAllocationRequests',
        type: "Get",
        success: function (view) {
            $("#page-wrapper").html(view);
            applyTooltip();
            if (typeof takeTour === "function")
                takeTour();
        },
        error: function (error) {
            returnErrorState(error);
        }

    });
}

$(document).on("click", ".myAllocatedChapters", function () {
    myAllocatedChapters()

});

function myAllocatedChapters() {
    $.ajax({
        url: '/Book/MyAllocatedChapters',
        type: "Get",
        success: function (view) {
            $("#page-wrapper").html(view);
            applyTooltip();
            if (typeof takeTour === "function")
                takeTour();
        },
        error: function (error) {
            returnErrorState(error);
        }

    });
}


/* Subject Allocation */


$(document).on("click", ".deallocateBtn", function () {
    var userid = this.dataset.userid;
    if ($("#detailPage").length > 0) {
        userid = $("#detailPage").data().userid;

    }


    var subjectId = this.dataset.subjectid;
    swal({
        title: "Are you sure?",
        text: "Are You Sure to deallocate this subject?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {


                $.ajax({
                    url: '/Book/DeallocateSubject',
                    type: "POST",
                    data: { userId: userid, subjectId: parseInt(subjectId) },
                    success: function (response) {
                        if (response.success) {
                            //swal("Deallocated!", response.message, "success");
                            smallSwal({
                                type: 'success',
                                title: response.message
                            });
                            if ($("#detailPage").length > 0) {
                                loadUserDetailById(userid);
                            }
                            if ($("#AllChapterRequests").length > 0) {
                                showChapterRequests();
                            }
                            else if ($("#AssignedSubjects.in").length > 0) {
                                showAllocatedSubjectByUserId(userid);
                            }
                        }
                        else {
                            swal("Error!", response.message, "error")

                        }

                    },
                    error: function (error) {
                        returnErrorState(error);
                    }

                })

            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });



});

$(document).on("click", ".unassignBatchBtn", function () {
    var userid = this.dataset.userid;
    if ($("#detailPage").length > 0) {
        userid = $("#detailPage").data().userid;

    }


    var batchId = this.dataset.id;
    swal({
        title: "Are you sure?",
        text: "Are You Sure to deallocate this subject?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {


                $.ajax({
                    url: '/Batch/DeallocateSubject',
                    type: "POST",
                    data: { userId: userid, batchId: parseInt(batchId) },
                    success: function (response) {
                        if (response.success) {
                            //swal("Deallocated!", response.message, "success");
                            smallSwal({
                                type: 'success',
                                title: response.message
                            });
                            if ($("#detailPage").length > 0) {
                                loadUserDetailById(userid);
                            }
                            if ($("#AllChapterRequests").length > 0) {
                                showChapterRequests();
                            }
                            else if ($("#AssignedSubjects.in").length > 0) {
                                showAllocatedSubjectByUserId(userid);
                            }
                        }
                        else {
                            swal("Error!", response.message, "error")

                        }

                    },
                    error: function (error) {
                        returnErrorState(error);
                    }

                })

            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });



});

$(document).on("click", ".deleteAllocatedSubjectRequestBtn", function () {
    var userid = this.dataset.userid;
    if ($("#detailPage").length > 0) {
        userid = $("#detailPage").data().userid;

    }


    var subjectId = this.dataset.subjectid;
    swal({
        title: "Are you sure?",
        text: "Are You Sure to delete this reqeust?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                $.ajax({
                    url: '/Book/DeleteAllocatedSubjectRequest',
                    type: "POST",
                    data: { userId: userid, subjectId: parseInt(subjectId) },
                    success: function (response) {
                        if (response.success) {
                            //swal("Deallocated!", response.message, "success");
                            smallSwal({
                                type: 'success',
                                title: response.message
                            });
                            if ($("#detailPage").length > 0) {
                                loadUserDetailById(userid);
                            }
                            if ($("#AllRequests").length > 0) {
                                showRequests();
                            }
                            else if ($("#AssignedSubjects.in").length > 0) {
                                showAllocatedSubjectByUserId(userid);
                            }
                        }
                        else {
                            swal("Error!", response.message, "error")

                        }

                    },
                    error: function (error) {
                        returnErrorState(error);
                    }

                })

            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });



});

function showAllocatedSubjectByUserId(userid) {
    $.ajax({
        url: '/Student/AllocatedSubjectsByUserId',
        type: "POST",
        data: { id: userid },
        success: function (view) {
            $("#AssignedSubjects .AssignedSubjectsSection").html(view);
            $("#AssignedSubjects .allocatedSubjectBtn").attr("data-userid", userid);
            $("#AssignedSubjects .allocatedChapterBtn").attr("data-userid", userid);

            $("#AssignedSubjects").attr("data-userid", userid);
            $("#AssignedChapters").attr("data-userid", userid);

            if (!$("#AssignedSubjects.in").length > 0) {
                $("#AssignedSubjects").modal();
            }
            applyTooltip();

        },
        error: function (error, textStatus, errorThrown) {
            returnErrorState(error);
        }

    })
}

$(document).on("click", ".showRequestsBtn", function () {
    showRequests()

});

function showRequests() {
    $.ajax({
        url: '/Book/SubjectAllocationRequests',
        type: "Get",
        success: function (view) {
            $("#page-wrapper").html(view);
            applyTooltip();
            if (typeof takeTour === "function")
                takeTour();
        },
        error: function (error) {
            returnErrorState(error);
        }

    });
}


$(document).on("click", ".myAllocatedSubjects", function () {
    myAllocatedSubjects()

});

function myAllocatedSubjects() {
    $.ajax({
        url: '/Book/MyAllocatedSubjects',
        type: "Get",
        success: function (view) {
            $("#page-wrapper").html(view);
            applyTooltip();
            if (typeof takeTour === "function")
                takeTour();
        },
        error: function (error) {
            returnErrorState(error);
        }

    });
}

function showAllocatedBatchByUserId(userid) {
    $.ajax({
        url: '/Teacher/AllocatedBatchesByUserId',
        type: "POST",
        data: { id: userid },
        success: function (view) {
            $("#AssignedBatches .AssignedBatchesSection").html(view);
            $("#AssignedBatches .allocatedBatchBtn").attr("data-userid", userid);
            $("#AssignedBatches .allocatedChapterBtn").attr("data-userid", userid);

            $("#AssignedBatches").attr("data-userid", userid);

            if (!$("#AssignedBatches.in").length > 0) {
                $("#AssignedBatches").modal();
            }
            applyTooltip();

        },
        error: function (error, textStatus, errorThrown) {
            returnErrorState(error);
        }

    })
}
