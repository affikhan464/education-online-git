﻿/*! Attribute_CRUD.js
 
 */
var imageUploader;
function attachUploader() {

    imageUploader = new qq.FineUploader({
        element: document.getElementById('fine-uploader-manual-trigger'),
        template: 'qq-template-manual-trigger',
        autoUpload: true,
        request: {
            endpoint: '/Upload/UploadFile'
        },

        thumbnails: {
            placeholders: {
                waitingPath: '/images/waiting-generic.png',
                notAvailablePath: '/images/not_available-generic.png'
            }
        },

        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'png'],
            itemLimit: 1

        },
        deleteFile: {
            enabled: true, // defaults to false

        },
        callbacks: {
            onDelete: function (id) {
                this.setDeleteFileParams({ filename: this.getName(id) }, id);
            },
            onSubmitDelete: function (id) {

            },
            onComplete: function (a, fileName, response) {

                if (response.success) {
                    $(".uploaderDeleteBtn").attr("data-filename", response.fileName);
                    $(".uploaderDeleteBtn").css("display", "inline-block");
                    $("#monoGramModal form").attr("data-monogramFileName", response.fileName);
                    $("#monoGramModal form #saveMonoGram").removeClass("disabledBtn");
                    //swal("Done!", response.message, "success");

                    //need to send ajax for adding artifact;


                }
                else {
                    swal("Error!", response.message, "error")


                }

            }
        },
    })

    attachUploaderClick();
}
function attachUploaderClick() {
    qq($("#trigger-upload")).attach("click", function () {
        imageUploader.uploadStoredFiles();
    });

}



$(document).on("click", ".addbutton", function () {
    var attribute = $(this);

    var attributeText = attribute.data().attributetype;
    if (attribute.data().attributetype == "BoardLevel") {
        attributeText = attribute.data().modaltext;
    }
    $("#addTypeModal .attribute-type").text(attributeText);
    $("#addTypeModal form [name=name]").val("");
    $("#addTypeModal form [name=name]").attr("placeholder", "Enter a " + attributeText + " Name");

    if (attribute.data().attributetype == "EducationLevel" || attribute.data().attributetype == "BoardLevel" || attribute.data().attributetype == "Class" || attribute.data().attributetype == "Subject") {
        $("#addTypeModal form [name=sortNumber]").hide();
        $("#addTypeModal form [name=sortNumber]").parent().hide();
    } else {
        $("#addTypeModal form [name=sortNumber]").show();
        $("#addTypeModal form [name=sortNumber]").parent().show();
        $("#addTypeModal form [name=sortNumber]").val("");
        $("#addTypeModal form [name=sortNumber]").attr("placeholder", "Enter a " + attribute.data().attributetype + " Number");
    }

    $("#addTypeModal form").attr("data-attributeid", attribute.data().attributeid);
    $("#addTypeModal form").attr("data-parentattributetype", attribute.data().parentattributetype);
    $("#addTypeModal form").attr("data-attributetable", attribute.data().attributetype);
    loadAttributes(attribute.data().attributetype, attribute.data().attributeid);
    $("#addTypeModal").modal();
});
$(document).on("click", ".editbutton", function () {
    var attribute = $(this);
    var attributeText = attribute.data().attributetype;
    if (attribute.data().attributetype == "BoardLevel") {
        attributeText = attribute.data().modaltext;
    }

    $("#editTypeModal .attribute-type").text(attributeText);
    $("#editTypeModal form [name=name]").val(attribute.data().modaltext);
    $("#editTypeModal form [name=sortNumber]").val(attribute.data().sortnumber);


    $("#editTypeModal form [name=name]").attr("placeholder", "Enter a " + attributeText + " Name");

    if (attribute.data().attributetype == "EducationLevel" || attribute.data().attributetype == "BoardLevel" || attribute.data().attributetype == "Class" || attribute.data().attributetype == "Subject") {
        $("#editTypeModal form [name=sortNumber]").hide();
        $("#editTypeModal form [name=sortNumber]").parent().hide();
    } else {
        $("#editTypeModal form [name=sortNumber]").show();
        $("#editTypeModal form [name=sortNumber]").parent().show();
        $("#editTypeModal form [name=sortNumber]").attr("placeholder", "Enter a " + attribute.data().attributetype + " Number");
    }

    $("#editTypeModal form").attr("data-attributeid", attribute.data().attributeid);
    $("#editTypeModal form").attr("data-modalcall", attribute.data().modalcall);

    $("#editTypeModal form").attr("data-parentid", attribute.data().parentid);
    $("#editTypeModal form").attr("data-attributetable", attribute.data().attributetype);
    $("#editTypeModal").modal();
});
$(document).on("click", ".deletebutton", function () {
    var attribute = $(this);
    var text = attribute.data().modaltext;
    var parentId = attribute.data().parentid;
    swal({
        title: "Are you sure?",
        text: "Are You Sure to delete " + text + "",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                $.ajax({
                    url: '/Attribute/Delete' + attribute.data().attributetype,
                    type: "POST",

                    data: { id: attribute.data().attributeid },
                    success: function (response) {
                        if (response.success) {

                            swal(
                                "Successful",

                                response.message, "success"
                            );
                            var LoggedUserRole = $("#LoggedUserRole").val();
                            if (LoggedUserRole == "Admin") {
                                loadDisciplineMenu();
                            }
                            var callType = attribute.data().modalcall;
                            if (callType == "true" || callType) {
                                loadAttributes(attribute.data().attributetype, parentId);
                            }
                            reloadOpenedAttribute();
                        }
                        else {
                            sweetAlert("Failed", response.message, "error");
                        }

                    },
                    error: function (error) {
                        returnErrorState(error);
                    }
                });


            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
});

$(document).on("click", ".uploaderDeleteBtn", function () {
    var filename = this.dataset.filename;
    $.ajax({
        /**/
        url: '/Upload/DeleteFile',
        /**/
        type: "POST",
        data: { fileName: filename },
        success: function (response) {
            if (response.success) {

                imageUploader.reset()
                //swal("Done!", response.message, "success");
                smallSwal({
                    type: 'success',
                    title: response.message
                });
                $("#monoGramModal form #saveMonoGram").addClass("disabledBtn");
                attachUploaderClick();

            }
            else {
                swal("Error!", response.message, "error")

            }

        },
        error: function (error) {
            returnErrorState(error);
        }

    })
});

$(document).ready(function () {
    var attributeHash = location.hash;
    if (attributeHash) {
        var attribute = attributeHash.replace("#", "").split("_");
        var attributeType = attribute[0];
        var attributeId = attribute[1];
        if (attribute.length>1) {
            loadAttributeView(attributeId, attributeType);
        }
    }
    $("#addTypeModal form").validate({

        rules: {

            name: "required"

        },
        messages: {
            name: "This field is required",

        }
    });
    $("#editTypeModal form").validate({

        rules: {

            name: "required"

        },
        messages: {
            name: "This field is required",

        }
    });
    attachUploader();
    $('[data-toggle="tooltip"]').tooltip();
    $("ul.in").removeClass("in");
    $("a.active").removeClass("active");
});


$(document).on("click", ".deleteAttributeMonogramBtn", function () {
    var attribute = $(this);
    swal({
        title: "Are you sure?",
        text: "Are You Sure to delete this picture?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                if (attribute.data().attributetype == "UserProfile") {
                    functionName = "DeleteProfile";
                    controllerName = "Researcher";
                }
                if (attribute.data().attributetype == "StudentProfile") {
                    functionName = "DeleteProfile";
                    controllerName = "Student";
                }
                else {
                    functionName = "DeleteMonoGramFor" + attribute.data().attributetype;
                    controllerName = "Attribute";
                }


                $.ajax({
                    url: '/' + controllerName + '/' + functionName,
                    type: "POST",

                    data: { Id: attribute.data().attributeid },
                    success: function (response) {
                        if (response.success) {

                            swal(
                                "Deleted!!",
                                response.message, "success"
                            );
                            if (attribute.data().attributetype == "UserProfile")
                                loadUserDetailById(attribute.data().attributeid)
                            else if (attribute.data().attributetype == "StudentProfile")
                                loadUserDetailById(attribute.data().attributeid)
                            else
                                reloadOpenedAttribute();
                        }
                        else {
                            sweetAlert("Failed", response.message, "error");
                        }

                    },
                    error: function (error) {
                        returnErrorState(error);
                    }
                });


            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });


});



$(document).on("click", "#saveEditBtn", function () {

    var isFormValid = $("#editTypeModal form").valid();
    if (isFormValid) {

        var saveBtn = $(this);
        smallSwal("Please Wait...!", '', 'info');
        saveBtn.addClass("disableClick");

        var name = $("#editTypeModal form [name=name]").val();
        var id = parseInt($("#editTypeModal form").attr("data-attributeid"));
        var parentId = parseInt($("#editTypeModal form").attr("data-parentid"));
        var attributetable = $("#editTypeModal form").attr("data-attributetable");
        var modalcall = $("#editTypeModal form").attr("data-modalcall");
        var functionName = "Edit" + attributetable;
        var sortNumber = "";
        if (attributetable == "Chapter" || attributetable == "Section" || attributetable == "SubSection") {
            sortNumber = $("#editTypeModal form [name=sortNumber]").val();
        }
        var model = {
            Name: name,
            Id: id,
            ParentId: parentId,
            SortNumber: Number(sortNumber)
        };
        $.ajax({
            url: '/Attribute/' + functionName,
            type: "POST",

            data: { model: model },

            success: function (response) {
                if (response.success) {
                    //swal("Success", response.message, "success");
                    smallSwal({
                        type: 'success',
                        title: response.message
                    });
                    saveBtn.removeClass("disableClick");

                    var LoggedUserRole = $("#LoggedUserRole").val();
                    if (LoggedUserRole == "Admin") {
                        loadDisciplineMenu();
                    }
                    var callType = modalcall;
                    if (callType == "true" || callType) {
                        loadAttributes(attributetable, parentId);
                    }
                    reloadOpenedAttribute();
                } else {
                    swal("Error", response.message, "error")
                    saveBtn.removeClass("disableClick");

                }

            },
            error: function (error) {
                    saveBtn.removeClass("disableClick");
                returnErrorState(error);
            }


        })
    }
});


$(document).on("click", "#addTypeModal #saveBtn", function () {
    var isFormValid = $("#addTypeModal form").valid();

    if (isFormValid) {

        var saveBtn = $(this);
        smallSwal("Please Wait...!", '', 'info');
        saveBtn.addClass("disableClick");

        var name = $("#addTypeModal form [name=name]").val();

        var parentId = parseInt($("#addTypeModal form").attr("data-attributeid"));
        var attributetable = $("#addTypeModal form").attr("data-attributetable");
        var functionName = "Add" + attributetable;

        var sortNumber = "";
        if (attributetable == "Chapter" || attributetable == "Section" || attributetable == "SubSection") {
            sortNumber = $("#addTypeModal form [name=sortNumber]").val();
        }

        var model = {
            Name: name,
            ParentId: parentId,
            SortNumber: Number(sortNumber)
        };
        $.ajax({
            url: '/Attribute/' + functionName,
            type: "POST",

            data: { model: model },

            success: function (response) {
                if (response.success) {
                    // swal("Success", response.message, "success");
                    smallSwal({
                        type: 'success',
                        title: response.message
                    });
                    saveBtn.removeClass("disableClick");
                    var LoggedUserRole = $("#LoggedUserRole").val();
                    if (LoggedUserRole == "Admin") {
                        loadDisciplineMenu();
                    }
                    loadAttributes(attributetable, parentId);
                    reloadOpenedAttribute();
                } else {
                    swal("Error", response.message, "error")
                    saveBtn.removeClass("disableClick");

                }

            },
            error: function (error) {
                returnErrorState(error);
                saveBtn.removeClass("disableClick");

            }


        })
    }
});
$(document).on("click", ".addMonoGramBtn", function () {
    var attribute = $(this);
    if (imageUploader) {
        imageUploader.reset();
    }
    attachUploaderClick();


    $("#monoGramModal form").attr("data-attributeid", attribute.data().attributeid);
    $("#monoGramModal form").attr("data-parentattributetype", attribute.data().parentattributetype);
    $("#monoGramModal form").attr("data-attributetable", attribute.data().attributetype);
    //loadAttributes(attribute.data().attributetype, attribute.data().attributeid);
    $("#monoGramModal").modal();
});
$(document).on("click", "#saveMonoGram", function () {
    var monogramFileName = $("#monoGramModal form").attr("data-monogramFileName");
    var attributeid = parseInt($("#monoGramModal form").attr("data-attributeid"));
    var attributetable = $("#monoGramModal form").attr("data-attributetable");
    var functionName = "";

    if (attributetable == "UserProfile") {
        functionName = "AddProfile";
        controllerName = "Researcher";
        attributeid = $("#monoGramModal form").attr("data-attributeid");
    }
    else {
        functionName = "UploadGramFor" + attributetable;
        controllerName = "Attribute";
    }

    var model = {

        Id: attributeid,
        FileName: monogramFileName
    };
    $.ajax({
        url: '/' + controllerName + '/' + functionName,
        type: "POST",

        data: { model: model },

        success: function (response) {
            if (response.success) {
                //swal("Success", response.message, "success");
                smallSwal({
                    type: 'success',
                    title: response.message
                });
                $("#monoGramModal form #saveMonoGram").addClass("disabledBtn");
                if (attributetable == "UserProfile") {
                    loadUserDetailById(attributeid);
                } else {
                    reloadOpenedAttribute();
                }
            } else {
                swal("Error", response.message, "error");
                $("#monoGramModal form #saveMonoGram").addClass("disabledBtn");

            }

        },
        error: function (error) {
            returnErrorState(error);
        }


    })

});
function loadAttributes(attributetype, parentId) {
    var parentId = parentId;
    $.ajax({
        url: '/Attribute/Get' + attributetype,
        type: "Get",
        data: { parentId: parseInt(parentId) },
        success: function (view) {

            $("#addTypeModal .all-attributes").html(view);

            $('[data-toggle="tooltip"]').tooltip();
            if ($(".LoggedUserRole").val() == "Admin" || $(".LoggedUserRole").val() == "Researcher") {
                $(".sortable").sortable();
            }

        },
        error: function (error) {
            returnErrorState(error);
        }
    });
}


$(document).on("click", ".viewbutton", function () {
    NProgress.start();
    var attribute = $(this);
    var attributeId = attribute.data().attributeid;
    var attributeType = attribute.data().attributetype;
    location.hash = attributeType + "_" + attributeId;

    loadAttributeView(attributeId, attributeType);
});
function loadAttributeView(attributeId, attributeType) {

    var functionName = "View" + attributeType;
    $.ajax({
        url: '/Attribute/' + functionName,
        type: "Get",
        data: { id: parseInt(attributeId) },
        success: function (view) {
            $(".addQuestionToWord").removeClass("d-none");
            $("#page-wrapper").html(view);
            applyTooltip();
            if ($("[href='#ExamQuestion']").length > 0) {
                loadExamQuestions(attributeType, attributeId, "#ExamQuestion .all-attributes");
            }
            if ($("[href='#PowerSlides']").length > 0) {
                loadPowerSlides(attributeType, attributeId);
            }
            if ($(".LoggedUserRole").val() == "Admin" || $(".LoggedUserRole").val() == "Researcher") {
                $(".sortable").sortable();
            }
            NProgress.done();
            if (typeof takeTour === "function")
                takeTour();
        },
        error: function (error) {
            NProgress.done();
            returnErrorState(error);
        }
    });
}

$(document).on("click", ".setdemochapterbutton", function () {
    NProgress.start();
    var attribute = $(this);
    var chapterId = attribute.data().attributeid;
    var subjectId = attribute.data().parentid;
    $.ajax({
        url: '/Attribute/SetDemoChapter',
        type: "Post",
        data: { subjectId: Number(subjectId), chapterId: Number(chapterId) },
        success: function (response) {
            if (response.success) {
                swal(
                    "Successful",

                    response.message, "success"
                );

                reloadOpenedAttribute();
            } else {
                sweetAlert("Oops !! ", response.message, "error");

            }
            NProgress.done();
        },
        error: function (error) {
            NProgress.done();
            returnErrorState(error);
        }
    });
});

function reloadOpenedAttribute() {
    if ($(".opened-attribute").length > 0) {

        var attributeid = $(".opened-attribute").data().attributeid;
        var attributeType = $(".opened-attribute").data().attributetype;


        var functionName = "View" + attributeType;
        $.ajax({
            url: '/Attribute/' + functionName,
            type: "Get",
            data: { id: parseInt(attributeid) },
            success: function (view) {

                $("#page-wrapper").html(view);
                applyTooltip();
                if ($(".LoggedUserRole").val() == "Admin" || $(".LoggedUserRole").val() == "Researcher") {
                    $(".sortable").sortable();
                }
            },
            error: function (error) {
                returnErrorState(error);
            }
        });
    }
}

$(document).on("sortstop", ".sortable", function (event, ui) {
    var parentid = ui.item[0].dataset.parentid;
    var attributeid = ui.item[0].dataset.attributeid;
    var attributetype = ui.item[0].dataset.attributetype;
    var sortNumber = parseInt($(ui.item).index() + 1);
    var functionName = "Sort" + attributetype;
    $.ajax({
        url: '/Attribute/' + functionName,
        type: "Post",
        data: { id: parseInt(attributeid), parentId: parseInt(parentid), sortNumber: parseInt(sortNumber) },
        success: function (view) {

        },
        error: function (error) {
            returnErrorState(error);
        }
    });
});
