$(function() {

    //Morris.Area({
    //    element: 'morris-area-chart',
    //    data: [{
    //        period: '2010 Q1',
    //        registration: 20
    //    }, {
    //        period: '2010 Q2',
    //        registration: 30
    //    }, {
    //        period: '2010 Q3',
    //        registration: 10
    //    }, {
    //        period: '2010 Q4',
    //        registration: 80
    //    }, {
    //        period: '2011 Q1',
    //        registration: 290
    //    }, {
    //        period: '2011 Q2',
    //        registration: 300
    //    }, {
    //        period: '2011 Q3',
    //        registration: 260
    //    }, {
    //        period: '2011 Q4',
    //        registration: 210
    //    }, {
    //        period: '2012 Q1',
    //        registration: 203
    //    }, {
    //        period: '2013 Q1',
    //        registration: 400
    //    }],
    //    xkey: 'period',
    //    ykeys: ['registration'],
    //    labels: ['Student Registered'],
    //    pointSize: 2,
    //    hideHover: 'auto',
    //    resize: true
    //});

    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Registered Student",
            value: Number( $("#registeredStudents").val())
        }, {
                label: "Unregistered Student",
                value: Number($("#unregisteredStudents").val())
        }],
        resize: true
    });

    //Morris.Bar({
    //    element: 'morris-bar-chart',
    //    data: [{
    //        y: '2006',
    //        a: 100,
    //        b: 90
    //    }, {
    //        y: '2007',
    //        a: 75,
    //        b: 65
    //    }, {
    //        y: '2008',
    //        a: 50,
    //        b: 40
    //    }, {
    //        y: '2009',
    //        a: 75,
    //        b: 65
    //    }, {
    //        y: '2010',
    //        a: 50,
    //        b: 40
    //    }, {
    //        y: '2011',
    //        a: 75,
    //        b: 65
    //    }, {
    //        y: '2012',
    //        a: 100,
    //        b: 90
    //    }],
    //    xkey: 'y',
    //    ykeys: ['a', 'b'],
    //    labels: ['Series A', 'Series B'],
    //    hideHover: 'auto',
    //    resize: true
    //});
    
});
