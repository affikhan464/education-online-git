﻿/*! ExamQuestion.js
 
 */
ExamQuestionId = "";
ExamQuestionAttributeType = "";

//ExamQuestion

function openAddingExamQuestionModel() {


    $('#addExamQuestionForm')[0].reset();
    $("#addExamQuestionModal .QuestionDifficultyLevel").html("");
    $("#addExamQuestionModal .QuestionDifficultyLevel").addRating({
        max: 3,
        selectedRatings: 1,
        fieldId: "QuestionDifficultyLevelId",
        fieldName: "QuestionDifficultyLevelId",
    });

    $("#addExamQuestionModal #QustionTypeId").ddslick("destroy");
    $("#addExamQuestionModal [name=QustionTypeId]").ddslick({
        showSelectedHTML: false,

        onSelected: function (selectedData) {

            var questionTypeId = selectedData.selectedData.value;
            $("#addExamQuestionModal [data-questiontypeid]").addClass("hidden");
            $("#addExamQuestionModal [data-questiontypeid=" + questionTypeId + "]").removeClass("hidden");
            $("#addExamQuestionModal [data-questiontypeid*=" + questionTypeId + "].AddChoiceBtnRow").removeClass("hidden");
        }
    });

    $(".Yearpicker").datepicker({
        format: "yyyy",
        viewMode: "years",
        minViewMode: "years"
    });

    if (tinymce.get("AnswerDetail") !== null) {
        tinymce.get("AnswerDetail").remove();
        tinymce.execCommand('mceRemoveControl', true, 'AnswerDetail');
    }
    if (tinymce.get("ShortAnswerDetail") !== null) {
        tinymce.get("ShortAnswerDetail").remove();
        tinymce.execCommand('mceRemoveControl', true, 'ShortAnswerDetail');
    }
    if (tinymce.get("QuestionDetail") !== null) {
        tinymce.get("QuestionDetail").remove();
        tinymce.execCommand('mceRemoveControl', true, 'QuestionDetail');
    }
    TinyMVCGlobalOptions.selector = "#addExamQuestionModal .tmceeditor";

    tinymce.init(TinyMVCGlobalOptions);
    $('#addExamQuestionModal .mce-tinymce.mce-container').css("display", "block");
    $("#addExamQuestionModal").modal({
        backdrop: true
    });

    $(".adviceBtn[data-modalid]").removeAttr("data-modalid");
    $(".adviceModal").remove();
    //Only four choices initially
    var choicesLength = $("#addExamQuestionModal [data-questiontype=MCQ][name=AnswerType] .Choice").length;
    i = 4;
    while (i < choicesLength) {

        $("#addExamQuestionModal [data-questiontype=MCQ][name=AnswerType] .Choice:last").remove();
        i++;
    }
    addExamQuestionForm();

}

//Add Question Form Submission
function addExamQuestionForm() {
    var attributeType = $('#addExamQuestionForm [name=AttributeType]').val();
    var attributeId = $('#addExamQuestionForm [name=AttributeId]').val();

    var $form = $('#addExamQuestionForm');
    $form.off("submit");
    $form.submit(function (data, event) {
        var saveBtn = $("#addExamQuestionForm #saveBtn");
        smallSwal("Please Wait...!", '', 'info');
        saveBtn.addClass("disableClick");

        //after that you'll be able to use jquery selector to get value from TinyMCE
        tinyMCE.triggerSave();

        var row = $("#addExamQuestionForm [name=AnswerType]:not(.hidden) ");
        var questionType = row.data("questiontype");

        var choices = [];
        var choicesAdded = row.find(".choice-input").toArray();
        debugger
        var isSomeTextAreaEmpty = choicesAdded.some((a) => {
            debugger
            var elmnt = $.parseHTML(a.value);
            return $(elmnt).text().trim() == "" && $(elmnt).has("img").length == 0;
        })
        if (isSomeTextAreaEmpty) {
            swal("Some choices do not have any text please add some!!", data.message, "error");
            saveBtn.removeClass("disableClick");
            return false;
        }
        choicesAdded.forEach(function (e, i) {
            var modalid = $(e).nextAll(".adviceBtn").attr("data-modalid")
            var choice = {
                Detail: e.value,
                Advice: {
                    Detail: $(`#${modalid} .tmceeditor`).val()
                }
            }
            choices.push(choice);
        });

        //for MTQs and MCQs
        var mTQCorrectQuestionChoices = [];
        var correctAnswerChoices = row.find("[name=QuestionChoices]:checked")
        correctAnswerChoices.each(function (i, e) {
            mTQCorrectQuestionChoices.push($(e).parent().siblings(".tmceeditor").val());
        });
        var formFields = $("#addExamQuestionForm").serializeArray();

        var unindexed_array = $("#addExamQuestionForm").serializeArray();
        var model = {};

        $.map(unindexed_array, function (n, i) {
            model[n['name']] = n['value'];
        });
        debugger;
        model.QuestionChoices = choices;
        if ((questionType == "MCQ" || questionType == "MTQ") && (mTQCorrectQuestionChoices.length == 0 || mTQCorrectQuestionChoices.some((a) => a == undefined))) {
            if (mTQCorrectQuestionChoices.length == 0) {
                smallSwal("Select Correct Choice Please!!", data.message, "error");

            } else
                smallSwal("Some choices selected are undefined, refresh your page or contact developer!!", data.message, "error");           
            saveBtn.removeClass("disableClick");
            return false;
        }
        model.MTQCorrectQuestionChoices = mTQCorrectQuestionChoices;
        if (questionType == "FillBlank") {
            var modalid = row.find(".adviceBtn").attr("data-modalid");
            model.SingleQuestionChoiceAdvice = $(`#${modalid} .tmceeditor`).val();
        }
        if ((mTQCorrectQuestionChoices.length > 0 && questionType == "MCQ") || questionType == "LongQuestion" || questionType == "ShortQuestion" || (mTQCorrectQuestionChoices.length > 0 && questionType == "MTQ") || (model.CorrectQuestionChoice.trim() != "" && questionType == "FillBlank")) {
            $.post($(this).attr('action'), model, function (response) {

            }, 'json').done(function (data) {
                attributeId = $(".opened-attribute").attr("data-attributeid");
                attributeType = $(".opened-attribute").attr("data-attributetype");
                if (data.success) {
                    smallSwal("Successfull!!", data.message, "success");
                    saveBtn.removeClass("disableClick");

                    loadExamQuestions(attributeType, attributeId, "#ExamQuestion .all-attributes");
                } else {
                    swal("Error!!", data.message, "error")
                    saveBtn.removeClass("disableClick");
                }

            });

        } else {
            swal("Choose/Enter correct answer", "Please choose/enter the correct answer.", "error");
            saveBtn.removeClass("disableClick");
        }
        return false;
    });



};

$(document).on("click", ".viewExamQuestionBtn", function (event) {
    var btn = this;

    var questionid = $(btn).data().questionid;
    var isQuestionExist = $("#ExamQuestion_" + questionid).length > 0;
    if (isQuestionExist)
        $("#ExamQuestion_" + questionid).remove();

    loadExamQuestionDetail(questionid);

    applyTooltip();
    applyPopover();

});

function editExamQuestionForm() {
    var attributeType = $('#editExamQuestionForm [name=AttributeType]').val();
    var attributeId = $('#editExamQuestionForm [name=AttributeId]').val();

    var $form = $('#editExamQuestionForm');
    $form.off("submit");
    $form.submit(function (data, event) {
        var saveBtn = $("#editExamQuestionForm #saveBtn");
        smallSwal("Please Wait...!", '', 'info');
        saveBtn.addClass("disableClick");


        //after that you'll be able to use jquery selector to get value from TinyMCE
        tinyMCE.triggerSave();

        var row = $("#editExamQuestionForm [name=AnswerType]:not(.hidden) ");
        var questionType = row.data("questiontype");

        var choices = [];
        var choicesAdded = row.find(".choice-input").toArray();
        debugger
        var isSomeTextAreaEmpty = choicesAdded.some((a) => {
            debugger
            var elmnt = $.parseHTML(a.value);
            return $(elmnt).text().trim() == "" && $(elmnt).has("img").length == 0;
        })
        if (isSomeTextAreaEmpty) {
            swal("Some choices do not have any text please add some!!", data.message, "error");
            saveBtn.removeClass("disableClick");
            return false;
        }
        choicesAdded.forEach(function (e, i) {
            debugger
            var modalid = $(e).nextAll(".adviceBtn").attr("data-modalid")
            var choice = {
                Id: $(e).data("id"),
                Detail: e.value,
                Advice: {
                    Detail: $(`#${modalid} .tmceeditor`).val()
                }
            }
            choices.push(choice);
        });

        //for MTQs and MCQs
        var mTQCorrectQuestionChoices = [];
        var correctAnswerChoices = row.find("[name=QuestionChoices]:checked")
        correctAnswerChoices.each(function (i, e) {
            mTQCorrectQuestionChoices.push($(e).parent().siblings(".tmceeditor").val());
        });
        var formFields = $("#editExamQuestionForm").serializeArray();

        var unindexed_array = $("#editExamQuestionForm").serializeArray();
        var model = {};

        $.map(unindexed_array, function (n, i) {
            model[n['name']] = n['value'];
        });

        debugger;
        model.QuestionChoices = choices;
        if ((questionType == "MCQ" || questionType == "MTQ") && (mTQCorrectQuestionChoices.length == 0 || mTQCorrectQuestionChoices.some((a) => a == undefined))) {

            if (mTQCorrectQuestionChoices.length == 0) {
                smallSwal("Select Correct Choice Please!!", data.message, "error");

            } else
                smallSwal("Some choices selected are undefined, refresh your page or contact developer!!", data.message, "error");

            saveBtn.removeClass("disableClick");
            return false;
        }
        model.MTQCorrectQuestionChoices = mTQCorrectQuestionChoices;
        if (questionType == "FillBlank") {
            var modalid = row.find(".adviceBtn").attr("data-modalid");
            model.SingleQuestionChoiceAdvice = $(`#${modalid} .tmceeditor`).val();
        }
        if ((mTQCorrectQuestionChoices.length > 0 && questionType == "MCQ") || questionType == "LongQuestion" || questionType == "ShortQuestion" || (mTQCorrectQuestionChoices.length > 0 && questionType == "MTQ") || (model.CorrectQuestionChoice.trim() != "" && questionType == "FillBlank")) {
            $.post($(this).attr('action'), model, function (response) {

            }, 'json').done(function (data) {

                if (data.success) {
                    saveBtn.removeClass("disableClick");
                    smallSwal("Successfull!!", data.message, "success");
                    attributeId = $(".opened-attribute").attr("data-attributeid");
                    attributeType = $(".opened-attribute").attr("data-attributetype");
                    loadExamQuestions(attributeType, attributeId, "#ExamQuestion .all-attributes");

                } else {
                    swal("Error!!", data.message, "error")
                    saveBtn.removeClass("disableClick");
                }

            });

        } else {
            saveBtn.removeClass("disableClick");
            swal("Choose/Enter correct answer", "Please choose/enter the correct answer.", "error");
        }
        return false;
    });
};

$(document).on("click", ".editExamQuestionBtn", function (event) {


    var btn = this;
    var questionId = $(btn).data().questionid;

    //open modal on success

    $.ajax({
        url: '/Question/EditExamQuestion',
        type: "POST",

        data: {
            id: questionId
        },
        success: function (view) {
            $("#editExamQuestionModal .questionEditors").html(view);
            $("#editExamQuestionModal .QuestionDifficultyLevel").html("");
            $("#editExamQuestionModal .QuestionDifficultyLevel").addRating({
                max: 3,
                half: false,
                fieldName: "QuestionDifficultyLevelId",
                selectedRatings: $(".QuestionDifficultyLevelId").val()
            });
            $("#editExamQuestionModal").modal();


            if (tinymce.get("AnswerDetail") !== null) {
                tinymce.get("AnswerDetail").remove();
                tinymce.execCommand('mceRemoveControl', true, 'AnswerDetail');
            }

            if (tinymce.get("ShortAnswerDetail") !== null) {
                tinymce.get("ShortAnswerDetail").remove();
                tinymce.execCommand('mceRemoveControl', true, 'ShortAnswerDetail');
            }
            if (tinymce.get("QuestionDetail") !== null) {
                tinymce.get("QuestionDetail").remove();
                tinymce.execCommand('mceRemoveControl', true, 'QuestionDetail');
            }
            $("#editExamQuestionModal [name=QustionTypeId]").ddslick("destroy");

            $("#editExamQuestionModal [name=QustionTypeId]").ddslick({
                showSelectedHTML: false,

                onSelected: function (selectedData) {

                    var questionTypeId = selectedData.selectedData.value;
                    $("#editExamQuestionModal [data-questiontypeid]").addClass("hidden");
                    $("#editExamQuestionModal [data-questiontypeid=" + questionTypeId + "]").removeClass("hidden");
                    $("#editExamQuestionModal [data-questiontypeid*=" + questionTypeId + "].AddChoiceBtnRow").removeClass("hidden");
                }
            });

            TinyMVCGlobalOptions.selector = "#editExamQuestionModal .tmceeditor";

            tinymce.init(TinyMVCGlobalOptions);
            $('#editExamQuestionModal .mce-tinymce.mce-container').css("display", "block");
            editExamQuestionForm();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })

});

$(document).on("click", ".deleteExamQuestionBtn", function (event) {

    var btn = this;
    var questionid = $(btn).data().questionid;
    var attributeId = $("#addExamQuestionModal [name=AttributeId]").val();
    var attributeType = $("#addExamQuestionModal [name=AttributeType]").val();


    swal({
        title: "Are you sure?",
        text: "Are you sure to delete this question from application completely?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {


                $.ajax({
                    url: '/Question/Delete',
                    type: "POST",

                    data: {
                        id: questionid
                    },
                    success: function (data) {
                        attributeId = $(".opened-attribute").attr("data-attributeid");
                        attributeType = $(".opened-attribute").attr("data-attributetype");
                        if (data.success) {
                            smallSwal({
                                type: 'success',
                                title: data.message
                            });
                            loadExamQuestions(attributeType, attributeId, "#ExamQuestion .all-attributes");
                        } else {
                            swal("Error!!", data.message, "error");
                            loadExamQuestions(attributeType, attributeId, "#ExamQuestion .all-attributes");

                        }
                    },
                    error: function (error) {
                        returnErrorState(error);
                    }

                })

            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });

});
$(document).on("click", ".closeQuestionBtn", function (event) {
    var btn = this;

    var questionid = $(btn).data().questionid;
    $("#ExamQuestion_" + questionid).remove();

});


function loadExamQuestionDetail(questionId) {
    debugger
    $.ajax({
        url: '/Question/GetExamQuestion',
        type: "POST",

        data: {
            id: questionId
        },
        success: function (view) {

            $(".question-detail").append(view);
            $("#ExamQuestionListModal .close").click()

            scrollTo("#Question_" + questionId);

            //applyPopover();
            var questionDeficulties = $("#Question_" + questionId + " [name=questionDificulty]");
            questionDeficulties.each(function (i, e) {

                var level = e.dataset.rating;
                $(e).addRating({ max: 3, selectedRatings: level });
            });
            appDifficultyTooltip();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}

$(document).on("click", ".AddChoiceBtn", function (event) {
    var btn = $(this);
    var modalId = btn.parents(".modal").attr("id");
    var row = $("#" + modalId + " [name=AnswerType]:not(.hidden)");
    var questionType = row.data("questiontype");
    var newCol = "";
    if (questionType == "MCQ") {
        newCol = `  <div class="col col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-2 Choice">
                        <div class="d-flex">    
                            <label>
                                <input type="radio" name="QuestionChoices" class="radio rounded colored" />
                                <span></span>
                            </label>
                            <textarea class="tmceeditor choice-input" data-id="0" placeholder="Enter choice text here" style="height:150px"></textarea>
                            <i class="fas fa-times-circle m-2 remove-choice-btn"></i>
                            <span class="badge badge-info adviceBtn mb-3 m-2"><i class="fas fa-exclamation-circle"></i> Advice</span>

                        </div>
                    </div>`;

    }
    else if (questionType == "MTQ") {
        newCol = `  <div class="col col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-2 Choice">
                        <div class="d-flex">    
                            <label>
                                <input type="checkbox" name="QuestionChoices" class="checkbox squared" />
                                <span></span>
                            </label>
                            <textarea class="tmceeditor choice-input" data-id="0" placeholder="Enter choice text here" style="height:150px"></textarea>
                            <i class="fas fa-times-circle m-2 remove-choice-btn"></i>
                            <span class="badge badge-info adviceBtn mb-3 m-2"><i class="fas fa-exclamation-circle"></i> Advice</span>
                        </div>
                    </div>`;

    }
    row.append(newCol);
    var modalId = btn.parents(".modal").attr("id");
    TinyMVCGlobalOptions.selector = `#${modalId} .tmceeditor`;

    tinymce.init(TinyMVCGlobalOptions);
});

$(document).on("click", ".remove-choice-btn", function (event) {
    var btn = $(this);
    btn.parents(".col").remove();
});

$(document).on("click", ".adviceBtn", function (event) {
    var btn = $(this);

    if (!btn.attr(`data-modalid`)) {
        var modalId = guid();

        btn.attr(`data-modalid`, `${modalId}`);

        createModal(modalId);

        if (tinymce.get("AdviceDetail") !== null)
            tinymce.execCommand('mceRemoveControl', true, 'AdviceDetail');



        TinyMVCGlobalOptions.selector = `#${modalId} [name=AdviceDetail].tmceeditor`;

        tinymce.init(TinyMVCGlobalOptions);
        $(`#${modalId}`).modal();
    }
    else {
        var modalId = btn.attr(`data-modalid`);

        if (tinymce.get("AdviceDetail") !== null)
            tinymce.execCommand('mceRemoveControl', true, 'AdviceDetail');



        TinyMVCGlobalOptions.selector = `#${modalId} [name=AdviceDetail].tmceeditor`;

        tinymce.init(TinyMVCGlobalOptions);

        $(`#${modalId}`).modal();
    }


});
$(document).on("click", ".viewAdviceBtn", function (event) {
    var btn = $(this);

    if (!btn.attr(`data-modalid`)) {
        var modalId = guid();

        btn.attr(`data-modalid`, `${modalId}`);

        viewExamQuestionAdviceModal(modalId);

        if (tinymce.get("AdviceDetail") !== null)
            tinymce.execCommand('mceRemoveControl', true, 'AdviceDetail');



        TinyMVCGlobalOptions.selector = `#${modalId} [name=AdviceDetail].tmceeditor`;

        tinymce.init(TinyMVCGlobalOptions);
        $(`#${modalId}`).modal();
    }
    else {
        var modalId = btn.attr(`data-modalid`);

        if (tinymce.get("AdviceDetail") !== null)
            tinymce.execCommand('mceRemoveControl', true, 'AdviceDetail');



        TinyMVCGlobalOptions.selector = `#${modalId} [name=AdviceDetail].tmceeditor`;

        tinymce.init(TinyMVCGlobalOptions);

        $(`#${modalId}`).modal();
    }


});
function createModal(modalId, AdviceDetail = "") {

    var modalElement = `
                           <!-- Advice Modal -->
                           <div class="modal fade adviceModal" id='${modalId}'  tabindex="-1" data-focus-on="input:first">
                               <div class="modal-dialog">
                                   <div class="modal-content">

                                       <!-- Modal Header -->
                                       <div class="modal-header">
                                           <h5 class="modal-title">Add Advice </h5>
                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       </div>

                                       <!-- Modal Body And Footer-->
                                       <div class="modal-body">
                                           <div class="row">

                                               <div class="col col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                   <div class="form-group">
                                                       <label>Detail</label>
                                                       <textarea name="AdviceDetail" class="tmceeditor" >${AdviceDetail}</textarea>

                                                   </div>
                                               </div>
                                           </div>
                                       </div>

                                       <div class="modal-footer pt-2">

                                           <button type="button" class="btn btn-danger rounded-0" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>

                                           <button type="submit" class="btn btn-success rounded-0" data-dismiss="modal"><i class="fas fa-plus"></i>  Add</button>

                                       </div>
                                   </div>
                               </div>
                           </div>`;

    $("body").append(modalElement);


}
function viewExamQuestionAdviceModal(modalId, AdviceDetail = "") {

    var modalElement = `
                           <!-- Advice Modal -->
                           <div class="modal fade adviceModal" id='${modalId}'  tabindex="-1" data-focus-on="input:first">
                               <div class="modal-dialog">
                                   <div class="modal-content">

                                       <!-- Modal Header -->
                                       <div class="modal-header">
                                           <h5 class="modal-title">Add Advice </h5>
                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                       </div>

                                       <!-- Modal Body And Footer-->
                                       <div class="modal-body">
                                           <div class="row">
                                               <div class="col col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                                   <div class="form-group">
                                                       <label>Detail</label>
                                                        <input id="${modalId}_adviceDetailInput" type="hidden" value="${AdviceDetail}">
                                                       <div id="${modalId}_adviceDetail" class="adviceModalDetail question-content page-outline p-3" ></div >
                                                   </div>
                                               </div>
                                           </div>
                                       </div>

                                       <div class="modal-footer pt-2">
                                           <button type="button" class="btn btn-danger rounded-0" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
                                       </div>
                                   </div>
                               </div>
                           </div>`;

    $("body").append(modalElement);
    $(`#${modalId}_adviceDetail`).html($(`#${modalId}_adviceDetailInput`).val());

}
function makeTimer(endTime, timerType) {

    endTime = (Date.parse(endTime) / 1000);

    var now = new Date();
    now = (Date.parse(now) / 1000);

    var timeLeft = endTime - now;

    var days = Math.floor(timeLeft / 86400);
    var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
    var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600)) / 60);
    var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

    if (hours < "10") { hours = "0" + hours; }
    if (minutes < "10") { minutes = "0" + minutes; }
    if (seconds < "10") { seconds = "0" + seconds; }

    if (timerType == "startingTimer" && seconds == "00") {
        clearTimeout(startingTimer);
        location.href = "/Test/InProgress/" + $(".opened-test").data("id");
    }
    if (timerType == "remainingTimer" && hours == "00" && minutes == "00" && seconds == "00") {
        clearTimeout(startingTimer);
        if (typeof finishTest == "function") {
            finishTest();
        }
    }
    $("#hours").html(hours);
    $("#minutes").html(minutes);
    $("#seconds").html(seconds);

}

function stopWatch() {
    startingQuestionCounter += 1;
    $(".step.show").find(".questionTimeTakenClock span").text(startingQuestionCounter);
}
let startingTimer;
