﻿/*! returnErrorState.js
 
 */
function returnErrorState(error) {

    if (error.readyState == 0)
        swal(" <h1 class='display-4 text-danger'>Network Error!  </h1>", "<strong>Please check your internet connection.</strong>", "error");
    else if (error.status == 500)
        swal(" <h1 class='display-4 text-danger'> Internal Server Error </h1>", "<strong>Application Error!</strong>", "error");
    else if (error.status == 403)
        swal("<h1 class='display-4 text-danger'> Access Denied </h1>", "<strong>You do not have access to it, Please Contact Administrator!</strong>", "error");
    else if (error.status == 404)
        swal(" <h1 class='display-4 text-danger'> Resource Not Found </h1>", "<strong>Contact Administrator!</strong>", "error");
    else if (error.status == 500)
        swal(" <h1 class='display-4 text-danger'> Internal Server Error </h1>", "<strong>Application Error!</strong>", "error");
    else
        swal(" <h1 class='display-4 text-danger'> Error </h1>", "<strong>There is some error, Contact Administrator!</strong>", "error");
} 