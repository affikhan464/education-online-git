﻿/*!
Researcher.js
*/
$(document).on("click", ".researcherBtn", function () {
    loadResearcher();
});

$(document).on("click", ".addResearcherBtn", function () {
    $("#AddResearcher form").attr("action", "/Researcher/Register");
    $("#AddResearcher form textarea").val("");
    $("#AddResearcher form input:not([type=radio])").val("");
    $("#AddResearcher form #saveBtn").html("<i class='fas fa-user-plus'></i> Add");
    $("#AddResearcher .modal-header .type").text("Add new");
    $("#AddResearcher").modal();

});

$(document).on("click", ".inviteResearcherBtn", function () {

    $("#ResearcherInviteModal").modal();
});

$(document).on("click", "#inviteResearcherForm #saveBtn", function () {


    var isValid = $("#inviteResearcherForm").valid();
});
$(document).on("click", "#addReseacherForm #saveBtn", function () {

    var isValid = $("#addReseacherForm").valid();
    if (isValid) {
        $("#addReseacherForm").submit();
    }
});
$(document).ready(function () {
    validateResearcherForm();
})

function validateResearcherForm() {
    /**
* Custom validator for contains at least one lower-case letter
*/
    $.validator.addMethod("atLeastOneLowercaseLetter", function (value, element) {
        return this.optional(element) || /[a-z]+/.test(value);
    }, "Must have at least one lowercase letter");

    /**
     * Custom validator for contains at least one upper-case letter.
     */
    $.validator.addMethod("atLeastOneUppercaseLetter", function (value, element) {
        return this.optional(element) || /[A-Z]+/.test(value);
    }, "Must have at least one uppercase letter");

    /**
     * Custom validator for contains at least one number.
     */
    $.validator.addMethod("atLeastOneNumber", function (value, element) {
        return this.optional(element) || /[0-9]+/.test(value);
    }, "Must have at least one number");

    /**
     * Custom validator for contains at least one symbol.
     */
    $.validator.addMethod("atLeastOneSymbol", function (value, element) {
        return this.optional(element) || /[!@@#$%^&*()]+/.test(value);
    }, "Must have at least one symbol");
    $.validator.addMethod("validCNIC", function (value, element) {
        return this.optional(element) || /^[0-9+]{5}-[0-9+]{7}-[0-9]{1}$/.test(value);
    });
    $('#addReseacherForm').validate({
        rules: {
            Password: {
                required: true,
                atLeastOneLowercaseLetter: true,
                atLeastOneUppercaseLetter: true,
                atLeastOneSymbol: true,
                atLeastOneNumber: true,
                minlength: 8,
            },
            CNIC: {
                required: true,
                minlength: 15,
                maxlength: 15,
                validCNIC: true

            }
        },
        messages: {
            CNIC: "Enter your valid CNIC number e.g. 12345-1234567-1."

        }

    })


}

function loadResearcher() {
    $.ajax({
        url: '/Researcher/Researchers',
        type: "Get",

        success: function (view) {
            $("#page-wrapper").html(view);
            applyTooltip();
            if (typeof takeTour === "function")
                takeTour();


        },
        error: function (error) {
            returnErrorState(error);
        }


    })
}


$(document).on("click", ".researcherViewbutton", function () {
    var userid = this.dataset.id;
    loadUserDetailById(userid);

});
function appendclickOfAddResearcher() {

    var $form = $('#addReseacherForm');
    $form.off("submit");
    $form.submit(function (data, event) {


        var isFormValid = $('#addReseacherForm').valid();

        if (isFormValid) {

            var btnHtml = $('#addReseacherForm #saveBtn').html();
            $('#addReseacherForm #saveBtn').addClass("disabledBtn");

            $('#addReseacherForm #saveBtn').html('<i class="fas fa-spinner fa-spin"></i>');
            $.post($(this).attr('action'), $(this).serialize(), function (response) {
                //can do here
            }, 'json').done(function (data) {
                if (data.success) {
                    smallSwal("Done!!", data.message, "success");
                    if ($("#detailPage").length > 0) {
                        var userid = document.getElementById("detailPage").dataset.userid;
                        loadUserDetailById(userid);
                        $('#addReseacherForm #saveBtn').removeClass("disabledBtn");
                        $('#addReseacherForm #saveBtn').html(btnHtml);
                    }
                    else {
                        loadResearcher();
                        $('#addReseacherFormc #saveBtn').removeClass("disabledBtn");
                        $('#addReseacherForm #saveBtn').html(btnHtml);
                    }
                } else {
                    swal("Error!!", data.message, "error");
                    $('#addReseacherForm #saveBtn').removeClass("disabledBtn");
                    $('#addReseacherForm #saveBtn').html(btnHtml);

                }

            });
            return false;
        }

    });
}
$(document).on("click", ".researcherEditButton", function () {
    var userId = this.dataset.id;
    $.ajax({
        url: '/Researcher/Edit',
        type: "POST",
        data: { userId: userId },
        success: function (view) {
            $("#AddResearcher .form").html(view);
            $("#AddResearcher .modal-header .type").text("Edit");
            $("#AddResearcher form #saveBtn").text("Update");
            $("#AddResearcher").modal();
            validateResearcherForm();
            appendclickOfAddResearcher();
            setInputMaskt();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
});



$(document).on("click", ".researcherDeleteButton", function () {
    var userId = this.dataset.id;
    swal({
        title: "Are you sure?",
        text: "Are you sure to delete this researcher?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                $.ajax({
                    url: '/Researcher/Delete',
                    type: "POST",
                    data: { userId: userId },
                    success: function (response) {
                        if (response.success) {

                            swal("Deleted!", response.message, "success")
                            loadResearcher();
                        }
                        else {
                            swal("Error!", response.message, "error")

                        }

                    },
                    error: function (error) {
                        returnErrorState(error);
                    }

                });


            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });

});
$(document).on("click", ".blockbutton", function () {


    var userId = this.dataset.id;
    $.ajax({
        url: '/Researcher/Block',
        type: "POST",
        data: { userId: userId },
        success: function (response) {
            if (response.success) {
                smallSwal("Done!!", response.message, "success")
                loadResearcher();
            }
            else {
                swal("Error!", response.message, "error")

            }

        },
        error: function (error) {
            returnErrorState(error);
        }



    });



});
