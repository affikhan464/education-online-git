
!function ($) {
    "use strict";

    var CalendarApp = function () {
        this.$body = $("body")
        this.$calendar = $('#calendar'),
            this.$event = ('#calendar-events div.calendar-events'),
            this.$categoryForm = $('#add-new-event form'),
            this.$extEvents = $('#calendar-events'),
            this.$modal = $('#my-event'),
            this.$saveCategoryBtn = $('.save-category'),
            this.$calendarObj = null
    };


    /* on drop */
    CalendarApp.prototype.onDrop = function (eventObj, date) {
        var $this = this;
        // retrieve the dropped element's stored Event Object
        var originalEventObject = eventObj.data('eventObject');
        var $categoryClass = eventObj.attr('data-class');
        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);
        // assign it the date that was reported
        copiedEventObject.start = date;
        if ($categoryClass)
            copiedEventObject['className'] = [$categoryClass];
        // render the event on the calendar
        $this.$calendar.fullCalendar('renderEvent', copiedEventObject, true);
        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
            // if so, remove the element from the "Draggable Events" list
            eventObj.remove();
        }
    },
        /* on admin click on event */
        CalendarApp.prototype.onAdminEventClick = function (calEvent, jsEvent, view) {
        var eventId = calEvent.id;;
        if (!calEvent.isTest) {
                var $this = this;
                var hours = calEvent.start._d.getHours().toString().length > 1 ? calEvent.start._d.getHours() : "0" + calEvent.start._d.getHours();
                var mins = calEvent.start._d.getMinutes().toString().length > 1 ? calEvent.start._d.getMinutes() : "0" + calEvent.start._d.getMinutes();
                var form = $("<form></form>");
                debugger
                form.append(`
                <div class="alert alert-info text-dark text-bold">To change time, delete this schedule time and recreate it for same day.</div>
                <div class='row justify-content-end'>
                    <div class='col-12 col-md-6'>
                        <label>Topic Name</label>
                        <input class='text-left btn btn-secondary waves-effect px-3 mr-3' name="title" type='text' value='${ calEvent.title}' />
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Class/Session URL</label>
                            <input class="text-left waves-effect"
                                   placeholder="Session Url" type="text" name="session_url" value='${calEvent.session_url}'  required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Zoom Username</label>
                            <input class="text-left waves-effect"
                                   placeholder="Session Url" type="text" name="username" value='${calEvent.username}'  required="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Zoom Password</label>
                            <input class="text-left waves-effect"
                                   placeholder="Session Url" type="text" name="password" value='${calEvent.password}'  required="">
                        </div>
                    </div>

                  <!--  <div class='col-12 col-md-6'>
                        <label>Start Time</label>
                        <input class='text-left btn btn-secondary waves-effect px-3 mr-3' name="time" type='time' value='${ hours}:${mins}' />
                    </div>-->
                    <div class='col-4 col-md-4'>   
                        <button type='submit' class='btn btn-success waves-effect'>
                            <i class='fa fa-check'></i> Save
                        </button>
                    </div>
                </div>`);
                $this.$modal.modal();
                $this.$modal.find('.delete-event').show().end().find('.save-event').hide().end().find('.modal-body').empty().prepend(form).end().find('.delete-event').unbind('click').click(function () {

                    var scheduleId = calEvent.id;
                    debugger;
                    swal({
                        title: 'Are you sure?',
                        html: 'Are you sure to delete this scheduled session?',
                        type: 'question',
                        showCancelButton: true,
                        confirmButtonText: 'Delete',
                        showLoaderOnConfirm: true,
                        preConfirm: (text) => {
                            return new Promise((resolve) => {
                                var isDone = deleteBatchSchedule(scheduleId);
                                if (isDone) {
                                    $this.$calendarObj.fullCalendar('removeEvents', function (ev) {
                                        return (ev._id == calEvent._id);
                                    });
                                    $this.$modal.modal('hide');
                                    smallSwal('Deleted!!', '', 'success');
                                }
                                resolve();
                            })
                        },
                        allowOutsideClick: () => !swal.isLoading()
                    });

                });
                $this.$modal.find('form').on('submit', function () {
                    calEvent.title = form.find("input[name=title]").val();
                    calEvent.session_url = form.find("input[name=session_url]").val();
                    calEvent.username = form.find("input[name=username]").val();
                    calEvent.password = form.find("input[name=password]").val();
                    //var hours = form.find("input[name='time']").val().split(":")[0];
                    //var mins = form.find("input[name='time']").val().split(":")[1];

                    //.start = new Date(calEvent.start._d.setHours(hours, mins));
                    var scheduleId = calEvent.id;
                    var isDone = updateBatchSchedule(calEvent.title, scheduleId, calEvent.session_url, calEvent.username, calEvent.password);
                    if (isDone) {
                        location.href = location.href;
                        $this.$calendarObj.fullCalendar('updateEvent', calEvent);
                        $this.$modal.modal('hide');
                    }
                    return false;
                });
        } else {
            GetScheduleDetails(eventId);

            $this.$calendarObj.fullCalendar('unselect');
        }
        },
        /* on teacher or student click on event */
        CalendarApp.prototype.onEventClick = function (calEvent, jsEvent, view) {
            var $this = this;
            var eventId = calEvent.id; debugger;
            GetScheduleDetails(eventId);

            $this.$calendarObj.fullCalendar('unselect');
        },
        /* on select */
        CalendarApp.prototype.onAdminSelect = function (start, end, allDay) {
            var $this = this;

            $this.$modal.modal({
                backdrop: 'static'
            });
            var isZoomValid = "";

            debugger
            var ZoomUserName = $(".page-header #ZoomUserName").val();
            var ZoomUrl = $(".page-header #ZoomUrl").val();
            var ZoomPassword = $(".page-header #ZoomPassword").val();
            if (ZoomUserName.length && ZoomUrl.length && ZoomPassword.length) {
                isZoomValid = "disabled";
            }
            var form = $("<form></form>");

            form.append(`               
                    <div class='row zoomBox p-3'>                   
                        <div class="col col-md-12">
                            <div class="form-group">
                                <label class="control-label">Session/Meeting URL</label>
                                <input class="text-left waves-effect"
                                       placeholder="Session Url" type="text" name="session_url" value="${ZoomUrl}" ${isZoomValid} required="">
                            </div>
                        </div>
                        <div class="col col-md-6">
                            <div class="form-group">
                                <label class="control-label">Zoom Username</label>
                                <input class="text-left waves-effect"
                                       placeholder="User Name" type="text" name="username" value="${ZoomUserName}" ${isZoomValid} required="">
                            </div>
                        </div>
                        <div class="col col-md-6">
                            <div class="form-group">
                                <label class="control-label">Zoom Password</label>
                                <input class="text-left waves-effect"
                                       placeholder="Password" type="text" name="password" value="${ZoomPassword}" ${isZoomValid} required="">
                            </div>
                        </div> 
                    </div>
                    <div class='row p-3'>  
                        <div class='col-md-12'>
                            <div class='form-group'>
                                <label class='control-label'>Topic Name</label>
                                <input class='text-left waves-effect' placeholder='Insert Topic Name' type='text' name='title' required />
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class='form-group'>
                                <label class='control-label'>Start Time</label>
                                <input class='text-left waves-effect' type='time' name='time' required='true' />
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class='form-group'>
                                <label class='control-label'>End Time</label>
                                <input class='text-left waves-effect' type='time' name='endtime' required='true' />
                            </div>
                        </div>
                    </div>
             `);

            $this.$modal.find('.delete-event').hide().end().find('.save-event').show().end().find('.modal-body').empty().prepend(form).end().find('.save-event').unbind('click').click(function () {
                var isValid = form.valid();
                if (moment($("[name=time]").val(), "hh:mm") >= moment($("[name=endtime]").val(), "hh:mm")) {
                    smallSwal("Please enter valid start and end time.", '', "error");
                    return false;
                }
                if (isValid) { form.submit(); }
            });
            $this.$modal.find('form').on('submit', function () {
                debugger;
                var allDay = allDay;
                var title = form.find("input[name='title']").val();
                var session_url = form.find("input[name='session_url']").val();
                var username = form.find("input[name='username']").val();
                var password = form.find("input[name='password']").val();
                var beginning = form.find("input[name='beginning']").val();
                var ending = form.find("input[name='ending']").val();
                var categoryClass = form.find("select[name='category'] option:checked").val();
                var hours = form.find("input[name='time']").val().split(":")[0];
                var mins = form.find("input[name='time']").val().split(":")[1];

                var endHours = form.find("input[name='endtime']").val().split(":")[0];
                var endMins = form.find("input[name='endtime']").val().split(":")[1];

                var startTime = new Date(start._d.setHours(hours, mins));

                var endTime = new Date(start._d.setHours(endHours, endMins));
                if (end) {
                    endTime = new Date(end.subtract(1, 'd')._d.setHours(endHours, endMins));
                }

                var done = setBatchSchedule(startTime, endTime, title, categoryClass, session_url, username, password);
                if (done.success) {

                    $this.$calendarObj.fullCalendar('renderEvent', {
                        id: done.id,
                        title: title,
                        start: startTime,
                        session_url: session_url,
                        end: endTime,
                        allDay: false,
                        className: categoryClass,
                        username: username,
                        password: password
                    }, true);
                    $this.$modal.modal('hide');

                }

                return false;
            });
            $this.$calendarObj.fullCalendar('unselect');
        },
        /* on student or teacher select */
        CalendarApp.prototype.onSelect = function (start, end, allDay) {

        },
        CalendarApp.prototype.enableDrag = function () {
            //init events
            $(this.$event).each(function () {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };
                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });
            });
        },
        /* Initializing */
        CalendarApp.prototype.init = function () {
            this.enableDrag();
            /*  Initialize the calendar  */
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var form = '';
            var today = new Date($.now());

            var $this = this;
            $this.$calendarObj = $this.$calendar.fullCalendar({
                timezone: false,
                slotDuration: '00:15:00', /* If we want to split day time each 15minutes */
                minTime: '00:00:00',
                maxTime: '23:59:59',
                defaultView: 'month',
                handleWindowResize: true,
                theme: false,
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                timeFormat: 'hh:mm a',
                //events: defaultEvents,
                navLinks: true,
                editable: false,
                droppable: false, // this allows things to be dropped onto the calendar !!!
                eventLimit: true, // allow "more" link when too many events
                selectable: true,
                drop: function (date) { $this.onDrop($(this), date); },
                select: function (start, end, allDay) {
                    var loggedUserRole = $("#LoggedUserRole").val();
                    if (loggedUserRole == "Admin") {
                        $this.onAdminSelect(start, end, allDay);
                    }
                    else {
                        $this.onSelect(start, end, allDay);
                    }
                },
                eventClick: function (calEvent, jsEvent, view) {
                    var loggedUserRole = $("#LoggedUserRole").val();
                    if (loggedUserRole == "Admin") {

                        $this.onAdminEventClick(calEvent, jsEvent, view);
                    }
                    else {

                        $this.onEventClick(calEvent, jsEvent, view);
                    }
                }

            });

            //on new event
            this.$saveCategoryBtn.on('click', function () {
                var categoryName = $this.$categoryForm.find("input[name='category-name']").val();
                var categoryColor = $this.$categoryForm.find("select[name='category-color']").val();
                if (categoryName !== null && categoryName.length != 0) {
                    $this.$extEvents.append('<div class="calendar-events bg-' + categoryColor + '" data-class="bg-' + categoryColor + '" style="position: relative;"><i class="fa fa-move"></i>' + categoryName + '</div>')
                    $this.enableDrag();
                }

            });
        },

        //init CalendarApp
        $.CalendarApp = new CalendarApp, $.CalendarApp.Constructor = CalendarApp

}(window.jQuery),

    //initializing CalendarApp
    function ($) {
        "use strict";
        $.CalendarApp.init()
    }(window.jQuery);


function GetSchedulesByBatchId(id) {
    $.ajax({
        url: '/Batch/GetSchedulesByBatchId',
        type: "Get",
        data: { batchId: id },
        success: function (response) {
            var schedules = [];

            debugger
            for (var i = 0; i < response.data.result.length; i++) {
                schedules.push({
                    id: response.data.result[i].id,
                    title: response.data.result[i].title,
                    start: new Date(response.data.result[i].startDateTime),
                    end: new Date(response.data.result[i].endDateTime),
                    session_url: response.data.result[i].eventUrl,
                    username: response.data.result[i].userName,
                    password: response.data.result[i].password,
                    className: response.data.result[i].labelCategory,
                    isTest: response.data.result[i].isTest
                });
            }
            $.CalendarApp.$calendarObj.fullCalendar('renderEvents', schedules, true);

            if (typeof takeTour === "function")
                takeTour();
        },
        error: function (error) {
            returnErrorState(error);
        }
    });
}
function GetScheduleDetails(id) {
    $.ajax({
        url: '/Batch/GetScheduleDetails',
        type: "Get",
        data: { id: id },
        success: function (view) {
            $("#EventDetailModal .modal-content").html(view);
            $("#EventDetailModal").modal();
            if (typeof takeTour === "function")
                takeTour();
        },
        error: function (error) {
            returnErrorState(error);
        }
    });
}