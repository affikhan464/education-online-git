﻿/*! ddSlick_Scripts.js
 
 */

function emptyAllDDL(modalId) {
    loadEducationLevels(modalId);
    emptyChapterDDL(modalId);
    emptySubjectDDL(modalId);
    emptyBoardLevelDDL(modalId);
    emptyClassDDL(modalId);
}
function emptyChapterDDL(modalId) {

    $(modalId + " .ChapterDDL").ddslick('destroy');
    $(modalId + " .ChapterDDL").ddslick({
        data: [{
            text: "Select Chapter",
            value: 0,
            selected: true
        }],
        width: "100%"
    });

}
function emptyClassDDL(modalId) {
    $(modalId + " .ClassDDL").ddslick('destroy');
    $(modalId + " .ClassDDL").ddslick({
        data: [{
            text: "Select Class",
            value: 0,
            selected: true
        }],
        width: "100%"
    });

}
function emptySubjectDDL(modalId) {

    $(modalId + "  .SubjectDDL").ddslick('destroy');

    $(modalId + "  .SubjectDDL").ddslick({
        data: [{
            text: "Select Subject",
            value: 0,
            selected: true
        }],
        width: "100%"
    });

}
function emptyBoardLevelDDL(modalId) {
    $(modalId + " .BoardLevelDDL").ddslick('destroy');
    $(modalId + " .BoardLevelDDL").ddslick({
        data: [{
            text: "Select Board/Body",
            value: 0,
            selected: true
        }],
        width: "100%"
    });



}
function loadEducationLevels(modalId) {
    var educationLvls = [{
        text: "Select Discipline",
        value: 0,
        selected: true
    }];

    $.ajax({
        url: '/Attribute/RetrieveEducationLevels',
        type: "Get",
        success: function (response) {
            for (var i = 0; i < response.length; i++) {
                var educationLvl = {
                    text: response[i].name,
                    value: response[i].id,
                    selected: false
                }
                educationLvls.push(educationLvl);
            }


            $(modalId + " .EducationLevelDDL").ddslick({
                data: educationLvls,
                width: "100%",
                onSelected: function (data) {
                    var educationId = data.selectedData.value;
                    loadBoardLevels(educationId, modalId)


                }
            });

            if (typeof takeTour === "function")
                takeTour();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}

function loadBoardLevels(educationId, modalId) {


    $.ajax({
        url: '/Attribute/RetrieveBoardLevel',
        type: "Get",
        data: { parentId: educationId },
        success: function (response) {
            var boardLevels = [{
                text: "Select Board/Body",
                value: 0,
                selected: true
            }];
            for (var i = 0; i < response.length; i++) {
                var lvl = {
                    text: response[i].name,
                    value: response[i].id,
                    selected: false
                }
                boardLevels.push(lvl);

            }
            $(modalId + " .BoardLevelDDL").ddslick('destroy');
            $(modalId + " .BoardLevelDDL").ddslick({
                data: boardLevels,
                width: "100%",
                onSelected: function (data) {
                    var boardLevelId = data.selectedData.value;
                    emptyChapterDDL();
                    loadClasses(boardLevelId, modalId);


                }
            });
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}
function loadClasses(boardLevelId, modalId) {

    $.ajax({
        url: '/Attribute/RetrieveClass',
        type: "Get",
        data: { parentId: boardLevelId },
        success: function (response) {
            var classes = [{
                text: "Select Class",
                value: 0,
                selected: true
            }];
            for (var i = 0; i < response.length; i++) {
                var lvl = {
                    text: response[i].name,
                    value: response[i].id,
                    selected: false
                }
                classes.push(lvl);

            }
            $(modalId + " .ClassDDL").ddslick('destroy');
            $(modalId + " .ClassDDL").ddslick({
                data: classes,
                width: "100%",
                onSelected: function (data) {
                    var id = data.selectedData.value;
                    loadSubjects(id, modalId);



                }
            });
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}
function loadSubjects(classId, modalId) {

    $.ajax({
        url: '/Attribute/RetrieveSubject',
        type: "Get",
        data: { parentId: classId },
        success: function (response) {
            var subjects = [{
                text: "Select Subject",
                value: 0,
                selected: true
            }];
            for (var i = 0; i < response.length; i++) {
                var lvl = {
                    text: response[i].name,
                    value: response[i].id,
                    selected: false
                }
                subjects.push(lvl);

            }
            $(modalId + " .SubjectDDL").ddslick('destroy');
            $(modalId + " .SubjectDDL").ddslick({
                data: subjects,
                width: "100%",
                onSelected: function (data) {
                    var id = data.selectedData.value;
                    loadChapters(id, modalId);


                }
            });
        },
        error: function (error) {
            returnErrorState(error);

        }

    })
}
function loadChapters(subjectId, modalId) {

    $.ajax({
        url: '/Attribute/RetrieveChapter',
        type: "Get",
        data: { parentId: subjectId },
        success: function (response) {
            var chapters = [{
                text: "Select Chapter",
                value: 0,
                selected: true
            }];
            for (var i = 0; i < response.length; i++) {
                var lvl = {
                    text: response[i].name,
                    value: response[i].id,
                    selected: false
                }
                chapters.push(lvl);

            }
            if ($(modalId + " .ChapterDDL").length > 0) {
                $(modalId + " .ChapterDDL").ddslick('destroy');
                $(modalId + " .ChapterDDL").ddslick({
                    data: chapters,
                    width: "100%"
                });
            }

            if (typeof loadChaptersToDiv == "function") {
                loadChaptersToDiv(chapters);
            }
        },
        error: function (error) {
            returnErrorState(error);
        }

    })
}
