﻿/*! PowerSlide.js
 
 */



$(document).on("click", ".addPowerSlidebutton", function () {
    $('#addPowerSlideForm')[0].reset();
    var attributeid = this.dataset.attributeid;
    var attributetype = this.dataset.attributetype;
    $('#addPowerSlideModal [name=AttributeId]').attr("value", attributeid);
    $('#addPowerSlideModal [name=AttributeType]').attr("value", attributetype);
    $("#addPowerSlideModal [name=QuestionIds]").attr("value", "");
    $("#addPowerSlideModal [name=QuestionIds]").val("");

    if (tinymce.get("Detail") !== null)
        tinymce.get("Detail").remove();

    TinyMVCGlobalOptions.selector = "#addPowerSlideModal .tmceeditor";
    initializeDatePicker();
    tinymce.init(TinyMVCGlobalOptions);
    $('#addPowerSlideModal .mce-tinymce.mce-container').css("display", "block");
    addPowerSlideSubmitForm();
    $('#addPowerSlideModal').modal();

});

$(document).on("click", ".editPowerSlideButton", function () {
    var id = this.dataset.id;
    $.ajax({
        url: '/PowerSlide/Edit',
        type: "POST",

        data: {
            id: id
        },
        success: function (view) {
            $('#editPowerSlideModal .editPowerSlideView').html(view);

            if (tinymce.get("Detail") !== null)
                tinymce.get("Detail").remove();

            TinyMVCGlobalOptions.selector = "#editPowerSlideModal .tmceeditor";

            tinymce.init(TinyMVCGlobalOptions);
            $('#editPowerSlideModal .mce-tinymce.mce-container').css("display", "block");
            editPowerSlideSubmitForm();
            $('#editPowerSlideModal').modal();
        },
        error: function (error) {
            returnErrorState(error);
        }

    })





});



$(document).on("click", ".deletePowerSlideButton", function () {
    var id = this.dataset.id;
    var attributeType = $(".opened-attribute").data().attributetype;
    var attributeId = $(".opened-attribute").data().attributeid;

    swal({
        title: 'Are you sure?',
        html: 'Type <b>"delete"</b>, to erase this Power Slide.',
        input: 'text',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Delete',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                if (text != 'delete') {
                    swal.showValidationError(
                        'Type correct spelling.'
                    )
                    resolve();
                } else {
                    deletePowerSlideDetails(id, attributeType, attributeId);

                }


            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });

})

function deletePowerSlideDetails(id, attributeType, attributeId) {

    $.ajax({
        url: "/PowerSlide/Delete",
        type: "POST",
        data: { id: id },
        success: function (response) {
            if (response.success) {


                smallSwal({
                    type: 'success',
                    title: response.message
                });
                loadPowerSlides(attributeType, attributeId);

            }
            else {
                swal("Error!", response.message, "error")
                loadPowerSlides(attributeType, attributeId);
            }

        },
        error: function (error) {
            returnErrorState(error);
        }

    })

}

//Add PowerSlide Form Submission
function addPowerSlideSubmitForm() {

    var $form = $('#addPowerSlideForm');
    $form.off("submit");
    $form.submit(function (data, event) {
        $('#addPowerSlideForm button[type=submit]').addClass("disableClick");
        $('#addPowerSlideForm button[type=submit] i').addClass("fa-spinner fa-spin");

        $.post($(this).attr('action'), $(this).serialize(), function (response) {

        }, 'json').done(function (data) {

            if (data.success) {
                smallSwal("Added!!", data.message, "success");

                var attributeid = $(".opened-attribute").data('attributeid');
                var attributetype = $(".opened-attribute").data('attributetype');

                loadPowerSlides(attributetype, attributeid);
                $('#addPowerSlideForm button[type=submit]').removeClass("disableClick");
                $('#addPowerSlideForm button[type=submit] i').removeClass("fa-spinner fa-spin");

            } else {
                swal("Error!!", data.message, "error")
                $('#addPowerSlideForm button[type=submit]').removeClass("disableClick");
                $('#addPowerSlideForm button[type=submit] i').removeClass("fa-spinner fa-spin");

            }

        });


        return false;
    });



};

//Edit PowerSlide Form Submission
function editPowerSlideSubmitForm() {

    var $form = $('#editPowerSlideForm');

    $form.off("submit");
    $form.submit(function (data, event) {
        $('#editPowerSlideForm button[type=submit] i').addClass("fa-spinner fa-spin");
        $('#editPowerSlideForm button[type=submit]').addClass("disableClick");
        $.post($(this).attr('action'), $(this).serialize(), function (response) {

        }, 'json').done(function (data) {

            if (data.success) {

                var attributeid = $(".opened-attribute").data('attributeid');
                var attributetype = $(".opened-attribute").data('attributetype');

                smallSwal("Updated!!", data.message, "success");
                loadPowerSlides(attributetype, attributeid);
                $('#editPowerSlideForm button[type=submit]').removeClass("disableClick");

                $('#editPowerSlideForm button[type=submit] i').removeClass("fa-spinner fa-spin");

            } else {
                $('#editPowerSlideForm button[type=submit]').removeClass("disableClick");
                $('#editPowerSlideForm button[type=submit] i').removeClass("fa-spinner fa-spin");
                swal("Error!!", data.message, "error")

            }

        });


        return false;
    });



};

