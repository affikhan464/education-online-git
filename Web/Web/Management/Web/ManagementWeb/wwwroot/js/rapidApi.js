﻿
totalCityCount = 0;
totalCityDoneCount = 0;
let totalCityFailedurl = [];
let notEqual = [];
var c;

function getCountriesCityCount() {
    $([
        "AR"
        , "KH"
        , "BY"
        , "AM"
        , "AW"
        , "AU"
        , "AT"
        , "AZ"
        , "BS"
        , "BH"
        , "BD"
        , "BB"
        , "BV"
        , "CM"
        , "CA"
        , "CV"
        , "KY"
        , "CF"
        , "TD"
        , "CL"
        , "CN"
        , "CX"
        , "BE"
        , "BZ"
        , "BJ"
        , "BM"
        , "BT"
        , "BO"
        , "BQ"
        , "BA"
        , "BW"
        , "BR"
        , "IO"
        , "UM"
        , "VG"
        , "VI"
        , "BN"
        , "BG"
        , "BF"
        , "BI"
        , "AF"
        , "AX"
        , "AL"
        , "DZ"
        , "AS"
        , "AD"
        , "AO"
        , "AI"
        , "AQ"
        , "AG"
        , "CC"
        , "CO"
        , "KM"
        , "CG"
        , "CD"
        , "CK"
        , "CR"
        , "HR"
        , "CU"
        , "CW"
        , "CY"
        , "CZ"
        , "DK"
        , "DJ"
        , "DM"
        , "DO"
        , "EC"
        , "EG"
        , "SV"
        , "GQ"
        , "ER"
        , "EE"
        , "ET"
        , "FK"
        , "FO"
        , "FJ"
        , "FI"
        , "FR"
        , "GF"
        , "PF"
        , "TF"
        , "GA"
        , "GM"
        , "GE"
        , "DE"
        , "GH"
        , "GI"
        , "GR"
        , "GL"
        , "GD"
        , "GP"
        , "GU"
        , "GT"
        , "GG"
        , "GN"
        , "GW"
        , "GY"
        , "HT"
        , "HM"
        , "VA"
        , "HN"
        , "HK"
        , "HU"
        , "IS"
        , "IN"
        , "ID"
        , "CI"
        , "IR"
        , "IQ"
        , "IE"
        , "IM"
        , "IL"
        , "IT"
        , "JM"
        , "JP"
        , "JE"
        , "JO"
        , "KZ"
        , "KE"
        , "KI"
        , "KW"
        , "KG"
        , "LA"
        , "LV"
        , "LB"
        , "LS"
        , "LR"
        , "LY"
        , "LI"
        , "LT"
        , "MQ"
        , "MR"
        , "MU"
        , "YT"
        , "MX"
        , "FM"
        , "MD"
        , "MC"
        , "MN"
        , "ME"
        , "LU"
        , "MO"
        , "MK"
        , "MG"
        , "MW"
        , "MY"
        , "MV"
        , "ML"
        , "MT"
        , "MH"
        , "NI"
        , "NE"
        , "NG"
        , "NU"
        , "NF"
        , "KP"
        , "MP"
        , "NO"
        , "OM"
        , "PK"
        , "MS"
        , "MA"
        , "MZ"
        , "MM"
        , "NA"
        , "NR"
        , "NP"
        , "NL"
        , "NC"
        , "NZ"
        , "PW"
        , "PS"
        , "PA"
        , "PG"
        , "PY"
        , "PE"
        , "PH"
        , "PN"
        , "PL"
        , "PT"
        , "PR"
        , "QA"
        , "XK"
        , "RE"
        , "RO"
        , "RU"
        , "RW"
        , "BL"
        , "SH"
        , "KN"
        , "LC"
        , "MF"
        , "PM"
        , "VC"
        , "WS"
        , "SM"
        , "ST"
        , "SA"
        , "SN"
        , "RS"
        , "KR"
        , "SS"
        , "ES"
        , "LK"
        , "SD"
        , "SR"
        , "SJ"
        , "SZ"
        , "SE"
        , "CH"
        , "SY"
        , "TW"
        , "TJ"
        , "TZ"
        , "TH"
        , "TL"
        , "TG"
        , "TK"
        , "TO"
        , "TT"
        , "SC"
        , "SL"
        , "SG"
        , "SX"
        , "SK"
        , "SI"
        , "SB"
        , "SO"
        , "ZA"
        , "GS"
        , "TN"
        , "TR"
        , "TM"
        , "TC"
        , "TV"
        , "UG"
        , "UA"
        , "AE"
        , "GB"
        , "US"
        , "UY"
        , "UZ"
        , "VU"
        , "VE"
        , "VN"
        , "WF"
        , "EH"
        , "YE"
        , "ZM"
        , "ZW"
    ]).each(function (index, ccc) {
        var code = ccc;
        console.log(ccc);

        var settings = {
            "async": false,
            "crossDomain": true,
            "url": `https://wft-geo-db.p.rapidapi.com/v1/geo/cities?offset=${0}&limit=1&countryIds=${code}`,
            "method": "GET",
            "headers": {
                "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
                "x-rapidapi-key": "2216949b03msh4e8e6d26dc44a6bp10a8d9jsn3677eaf1c605"
            }
        }

        $.ajax(settings).done(function (data) {
            totalCount = data.metadata.totalCount;
            if (totalCount != 144300) {
                console.log("Total: " + totalCount);
            }
            $.ajax({
                async: false,
                url: "/Geo/GetCountriesCitiesData",
                method: 'GET',
                data: { countryCode: code }
            }).done(function (res) {
                console.log(res.data);
                if (res.data < totalCount && totalCount != 144300) {
                    notEqual.push(ccc.toString());
                }
            }).fail(function () {
                totalCityFailedurl.push(settings.url);
            });
        }).fail(function () {
            totalCityFailedurl.push(settings.url);
        });;


    })
}
function addNewRegions() {
    $([
        "AR"
        , "KH"
        , "BY"
        , "AM"
        , "AW"
        , "AU"
        , "AT"
        , "AZ"
        , "BS"
        , "BH"
        , "BD"
        , "BB"
        , "BV"
        , "CM"
        , "CA"
        , "CV"
        , "KY"
        , "CF"
        , "TD"
        , "CL"
        , "CN"
        , "CX"
        , "BE"
        , "BZ"
        , "BJ"
        , "BM"
        , "BT"
        , "BO"
        , "BQ"
        , "BA"
        , "BW"
        , "BR"
        , "IO"
        , "UM"
        , "VG"
        , "VI"
        , "BN"
        , "BG"
        , "BF"
        , "BI"
        , "AF"
        , "AX"
        , "AL"
        , "DZ"
        , "AS"
        , "AD"
        , "AO"
        , "AI"
        , "AQ"
        , "AG"
        , "CC"
        , "CO"
        , "KM"
        , "CG"
        , "CD"
        , "CK"
        , "CR"
        , "HR"
        , "CU"
        , "CW"
        , "CY"
        , "CZ"
        , "DK"
        , "DJ"
        , "DM"
        , "DO"
        , "EC"
        , "EG"
        , "SV"
        , "GQ"
        , "ER"
        , "EE"
        , "ET"
        , "FK"
        , "FO"
        , "FJ"
        , "FI"
        , "FR"
        , "GF"
        , "PF"
        , "TF"
        , "GA"
        , "GM"
        , "GE"
        , "DE"
        , "GH"
        , "GI"
        , "GR"
        , "GL"
        , "GD"
        , "GP"
        , "GU"
        , "GT"
        , "GG"
        , "GN"
        , "GW"
        , "GY"
        , "HT"
        , "HM"
        , "VA"
        , "HN"
        , "HK"
        , "HU"
        , "IS"
        , "IN"
        , "ID"
        , "CI"
        , "IR"
        , "IQ"
        , "IE"
        , "IM"
        , "IL"
        , "IT"
        , "JM"
        , "JP"
        , "JE"
        , "JO"
        , "KZ"
        , "KE"
        , "KI"
        , "KW"
        , "KG"
        , "LA"
        , "LV"
        , "LB"
        , "LS"
        , "LR"
        , "LY"
        , "LI"
        , "LT"
        , "MQ"
        , "MR"
        , "MU"
        , "YT"
        , "MX"
        , "FM"
        , "MD"
        , "MC"
        , "MN"
        , "ME"
        , "LU"
        , "MO"
        , "MK"
        , "MG"
        , "MW"
        , "MY"
        , "MV"
        , "ML"
        , "MT"
        , "MH"
        , "NI"
        , "NE"
        , "NG"
        , "NU"
        , "NF"
        , "KP"
        , "MP"
        , "NO"
        , "OM"
        , "PK"
        , "MS"
        , "MA"
        , "MZ"
        , "MM"
        , "NA"
        , "NR"
        , "NP"
        , "NL"
        , "NC"
        , "NZ"
        , "PW"
        , "PS"
        , "PA"
        , "PG"
        , "PY"
        , "PE"
        , "PH"
        , "PN"
        , "PL"
        , "PT"
        , "PR"
        , "QA"
        , "XK"
        , "RE"
        , "RO"
        , "RU"
        , "RW"
        , "BL"
        , "SH"
        , "KN"
        , "LC"
        , "MF"
        , "PM"
        , "VC"
        , "WS"
        , "SM"
        , "ST"
        , "SA"
        , "SN"
        , "RS"
        , "KR"
        , "SS"
        , "ES"
        , "LK"
        , "SD"
        , "SR"
        , "SJ"
        , "SZ"
        , "SE"
        , "CH"
        , "SY"
        , "TW"
        , "TJ"
        , "TZ"
        , "TH"
        , "TL"
        , "TG"
        , "TK"
        , "TO"
        , "TT"
        , "SC"
        , "SL"
        , "SG"
        , "SX"
        , "SK"
        , "SI"
        , "SB"
        , "SO"
        , "ZA"
        , "GS"
        , "TN"
        , "TR"
        , "TM"
        , "TC"
        , "TV"
        , "UG"
        , "UA"
        , "AE"
        , "GB"
        , "US"
        , "UY"
        , "UZ"
        , "VU"
        , "VE"
        , "VN"
        , "WF"
        , "EH"
        , "YE"
        , "ZM"
        , "ZW"
    ]).each(function (index, ccc) {
        var code = ccc;
        console.log(ccc);
        var limit = 100;
        var length = 1;
        var logCounter = 1;
        for (var i = 0; i < length; i++) {

            var offset = i * limit;
            var settings = {
                "async": false,
                "crossDomain": true,
                "url": `https://wft-geo-db.p.rapidapi.com/v1/geo/countries/${code}/regions?offset=${offset}&limit=100`,
                "method": "GET",
                "headers": {
                    "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
                    "x-rapidapi-key": "2216949b03msh4e8e6d26dc44a6bp10a8d9jsn3677eaf1c605"
                }
            }

            $.ajax(settings).done(function (data) {
                totalCount = data.metadata.totalCount;
                if (logCounter == 1) {
                    console.log("Total: " + totalCount);
                    logCounter++;
                }

                length = totalCount / limit;
                totalCityCount += data.data.length;
                var innerLength = data.data.length;
                var addingData = [];
                for (var m = 0; m < innerLength; m++) {
                    var model = {
                        name: data.data[m].name,
                        countryCode: data.data[m].countryCode,
                        fipsCode: data.data[m].fipsCode,
                        isoCode: data.data[m].isoCode
                    };
                    addingData.push(model);
                }
                n = m * 20;
                $.ajax({
                    async: false,
                    url: "/Geo/AddRegion",
                    method: 'POST',
                    data: { model: addingData }
                }).done(function (res) {
                    if (res.success) {
                        console.log("done");
                        totalCityDoneCount += data.data.length;

                    } else {
                        console.log("error")
                        totalCityFailedurl.push(settings.url);

                    }
                }).fail(function () {
                    totalCityFailedurl.push(settings.url);
                });




            }).fail(function () {
                totalCityFailedurl.push(settings.url);
            });;
        }

    })
}
function storeRegions() {
    $([
        "US", "GB", "SO", "TG", "TH", "SN", "PT", "MA", "LT", "IT", "IN", "GR", "CU", "CU", "AL", "BR", "AU"
    ]).each(function (index, ccc) {
        var code = ccc;
        console.log(ccc);
        var limit = 100;
        var length = 1;
        var logCounter = 1;
        for (var i = 0; i < length; i++) {

            var offset = i * limit;
            var settings = {
                "async": false,
                "crossDomain": true,
                "url": `https://wft-geo-db.p.rapidapi.com/v1/geo/cities?offset=${offset}&limit=100&countryIds=${code} `,
                "method": "GET",
                "headers": {
                    "x-rapidapi-host": "wft-geo-db.p.rapidapi.com",
                    "x-rapidapi-key": "2216949b03msh4e8e6d26dc44a6bp10a8d9jsn3677eaf1c605"
                }
            }

            $.ajax(settings).done(function (data) {
                totalCount = data.metadata.totalCount;
                if (logCounter == 1) {
                    console.log("Total: " + totalCount);
                    logCounter++;
                }

                length = totalCount / limit;
                totalCityCount += data.data.length;
                var innerLength = data.data.length;
                var addingData = [];
                for (var m = 0; m < innerLength; m++) {
                    var model = {
                        name: data.data[m].name,
                        countryCode: data.data[m].countryCode,
                        regionfipsCode: data.data[m].regionfipsCode == undefined ? "" : data.data[m].regionfipsCode,
                        latitude: (Number(data.data[m].latitude)).toFixed(2),
                        longitude: (Number(data.data[m].longitude)).toFixed(2),
                        regionCode: data.data[m].regionCode == undefined ? "" : data.data[m].regionCode,
                    };
                    addingData.push(model);
                }
                n = m * 20;
                $.ajax({
                    async: false,
                    url: "/Geo/AddCities",
                    method: 'POST',
                    data: { model: addingData }
                }).done(function (res) {
                    if (res.success) {
                        totalCityDoneCount += data.data.length;
                        console.log("done");
                    } else {
                        console.log("error")
                        totalCityFailedurl.push(settings.url);

                    }
                }).fail(function () {
                    totalCityFailedurl.push(settings.url);
                });




            }).fail(function () {
                totalCityFailedurl.push(settings.url);
            });;
        }

    })
}