﻿/*! TestAndSet.js
 
 */

        $(document).on("click", ".addSetBtn", function () {
            $("#addSetModal").modal();
        });

        $(document).on("click", ".closeSetBtn", function () {
            $(".listOfSetQuestions").fadeOut();
        });
        $(document).on("click", ".editSetBtn", function () {
            
            var setid = this.dataset.setid;
            var title = this.dataset.title;
            $("#editSetModal").modal();
            $("#editSetForm [name=Title]").val(title);
            $("#editSetForm").attr("data-setid", setid);
        });

        $(document).on("click", ".viewSetBtn", function () {

            var setid = this.dataset.setid;
            var title = this.dataset.title;
            loadSetQuestions(setid, title)

        });
        function loadSetQuestions(setid, title) {
            var setid = setid == undefined ? $(".listOfSetQuestions").attr("data-setid") : setid;
            var title = title == undefined ? $(".listOfSetQuestions .setTitle").text() : title;

            $.ajax({
                url: '/Set/GetQuestions',
                type: "POST",

                data: {
                    setId: setid
                },
                success: function (view) {
                    $(".listOfSetQuestions").fadeIn();
                    $(".listOfSetQuestions .setTitle").text(title);
                    $(".listOfSetQuestions .all-questions").html(view);
                    $(".listOfSetQuestions").attr("data-setid", setid);
                    $(".listOfSetQuestions .addSetQuestionButton").attr("data-setid", setid);
                    $(".listOfSetQuestions .addSetQuestionButton").attr("data-title", title);

                    var questionDeficulties = $(".attributesRecord [name=questionDificulty]");
                    questionDeficulties.each(function (i, e) {

                        var level = e.dataset.rating;
                        $(e).addRating({ max: 3,selectedRatings: level });
                    });
                },
                error: function (error) {
                    returnErrorState(error);
                }

            })
        }
        $(document).on("click", "#addSetForm #saveBtn", function () {
            var attributeType = $(".opened-attribute").data().attributetype;
            var attributeId = $(".opened-attribute").data().attributeid;
            var model = {
                Title: $("#addSetForm [name=Title]").val(),
                AttributeType: attributeType,
                AttributeId: attributeId,
            };
            $.ajax({
                url: '/Set/Add',
                type: "POST",

                data: {
                    model: model
                },
                success: function (data) {

                    if (data.success) {
                        smallSwal("Done", data.message, "success");
                        loadSets(attributeType, attributeId);

                    } else {
                        smallSwal("Error", data.message, "error");
                    }


                },
                error: function (error) {
                    returnErrorState(error);
                }

            })
        });
        $(document).on("click", "#editSetForm #saveBtn", function () {
            var attributeType = $(".opened-attribute").data().attributetype;
            var attributeId = $(".opened-attribute").data().attributeid;
            var id = $("#editSetForm").data().setid;

            var model = {
                Id: id,
                Title: $("#editSetForm [name=Title]").val(),
                AttributeType: attributeType,
                AttributeId: attributeId,
            };
            $.ajax({
                url: '/Set/Update',
                type: "POST",

                data: {
                    model: model
                },
                success: function (data) {

                    if (data.success) {
                        smallSwal("Updated", data.message, "success");
                        loadSets(attributeType, attributeId);

                    } else {
                        smallSwal("Error", data.message, "error");
                    }


                },
                error: function (error) {
                    returnErrorState(error);
                }

            })
        });
        $(document).on("click", ".deleteSetBtn", function () {
            var id = this.dataset.setid;
            var attributeType = $(".opened-attribute").data().attributetype;
            var attributeId = $(".opened-attribute").data().attributeid;

            swal({
                title: 'Are you sure?',
                html: 'Type <b>"delete"</b>, to erase this set and its questions as well.',
                input: 'text',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: (text) => {
                    return new Promise((resolve) => {

                        if (text != 'delete') {
                            swal.showValidationError(
                                'Type correct spelling.'
                            )
                            resolve();
                        } else {
                            deleteSetDetails(id, attributeType, attributeId);

                        }


                    })
                },
                allowOutsideClick: () => !swal.isLoading()
            });

        })

        function deleteSetDetails(id, attributeType, attributeId) {

            $.ajax({
                url: "/Set/Delete",
                type: "POST",
                data: { id: id },
                success: function (response) {
                    if (response.success) {


                        smallSwal({
                            type: 'success',
                            title: response.message
                        });
                        loadSets(attributeType, attributeId);

                    }
                    else {
                        swal("Error!", response.message, "error")
                        loadSets(attributeType, attributeId);
                    }

                },
                error: function (error) {
                    returnErrorState(error);
                }

            })

        }

        function loadSets(attributeType, attributeId) {
            
            var model = {
                AttributeType: attributeType,
                AttributeId: attributeId,
            };
            $.ajax({
                url: '/Set/Get',
                type: "POST",

                data: {
                    model: model
                },
                success: function (view) {
                    $(".listOfSets").html(view);
                    applyTooltip();

                },
                error: function (error) {
                    returnErrorState(error);
                }

            })
        }
$(document).on("click", ".addSetQuestionButton", function () {
    $('#addSetQuestionForm')[0].reset();

            var setid = this.dataset.setid;
            var title = this.dataset.title;
            $('#addSetQuestionModal .set-title').html(title);

            $('[name=QuestionDifficultyLevel]').html("");
            $('#addSetQuestionForm [name=SetId]').attr("value", setid);
            $('#addSetQuestionForm [name=SetId]').val(setid);
            $('#addSetQuestionModal  [name=QuestionDifficultyLevel]').addRating({
                max: 3,
                half: false,
                fieldId: "QuestionDifficultyLevelId",
                fieldName: "QuestionDifficultyLevelId",
                selectedRatings: 1
            });
            appDifficultyTooltip();
            $("#addSetQuestionModal #QustionTypeId").ddslick({
                showSelectedHTML: false
            });

            $(".Yearpicker").datepicker({
                format: "yyyy",
                viewMode: "years",
                minViewMode: "years"
            });
            $("#addSetQuestionModal").modal();



            if (tinymce.get("AnswerDetail") !== null)
                tinymce.get("AnswerDetail").remove();

            if (tinymce.get("QuestionDetail") !== null)
                tinymce.get("QuestionDetail").remove();

    TinyMVCGlobalOptions.selector = "#addSetQuestionModal .tmceeditor";

            tinymce.init(TinyMVCGlobalOptions);
    $('#addSetQuestionModal .mce-tinymce.mce-container').css("display", "block");
    addSetQuestionForm();

        });

        $(document).on("click", ".editSetQuestionButton", function () {

            var id = this.dataset.id;
            $.ajax({
                url: '/Set/EditSetQuestion',
                type: "POST",

                data: {
                    id: id
                },
                success: function (view) {
                    $('#editSetQuestionModal .editSetQuestionView').html(view);

                    $('[name=QuestionDifficultyLevel]').html("");
                    $('#editSetQuestionModal [name=QuestionDifficultyLevel]').addRating({
                        max: 3,
                        half: false,
                        fieldId: "QuestionDifficultyLevelId",
                        fieldName: "QuestionDifficultyLevelId",
                        selectedRatings: $('#editSetQuestionForm .QuestionDifficultyLevelId').val()
                    });
                    appDifficultyTooltip();
                    $("#editSetQuestionModal [name=QustionTypeId]").ddslick({
                        showSelectedHTML: false
                    });
                    $(".Yearpicker").datepicker({
                        format: "yyyy",
                        viewMode: "years",
                        minViewMode: "years"
                    });

                    $("#editSetQuestionModal").modal();

                    if (tinymce.get("AnswerDetail") !== null)
                        tinymce.get("AnswerDetail").remove();

                    if (tinymce.get("QuestionDetail") !== null)
                        tinymce.get("QuestionDetail").remove();


                    TinyMVCGlobalOptions.selector = "#editSetQuestionModal .tmceeditor";

                    tinymce.init(TinyMVCGlobalOptions);
                    $('#editSetQuestionModal .mce-tinymce.mce-container').css("display", "block");
                    editSetQuestionForm();

                },
                error: function (error) {
                    returnErrorState(error);
                }

            })



        });
        $(document).on("click", ".deleteSetQuestionButton", function () {

            var id = this.dataset.id;
            swal({
                title: 'Are you sure?',
                html: 'Are you to delete this questions?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Delete',
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return new Promise((resolve) => {
                        deleteQuestionDetails(id);
                        resolve();
                    })
                },
                allowOutsideClick: () => !swal.isLoading()
            });


        });
        $(document).on("click", ".closeQuestionBtn", function (event) {

            var btn = this;

            var questionid = $(btn).data().questionid;
            $("#Question_" + questionid).remove();

        });
        function editSetQuestionForm() {


            var $form = $('#editSetQuestionForm');
            $form.off("submit");
            $form.submit(function (data, event) {

                $.post($(this).attr('action'), $(this).serialize(), function (response) {

                }, 'json').done(function (data) {

                    if (data.success) {
                        smallSwal("Updated!!", data.message, "success");
                        loadSetQuestions();

                    } else {
                        swal("Error!!", data.message, "error")

                    }

                });


                return false;
            });

        }

        function deleteQuestionDetails(id) {

            $.ajax({
                url: "/Set/RemoveQuestion",
                type: "POST",
                data: { id: id },
                success: function (response) {
                    if (response.success) {

                        smallSwal({
                            type: 'success',
                            title: response.message
                        });
                        loadSetQuestions();

                    }
                    else {
                        swal("Error!", response.message, "error")
                        loadSetQuestions();
                    }

                },
                error: function (error) {
                    returnErrorState(error);
                }

            })

        }

        //Add Question Form Submission
        function addSetQuestionForm() {


            var $form = $('#addSetQuestionForm');
            $form.off("submit");
            $form.submit(function (data, event) {

                $.post($(this).attr('action'), $(this).serialize(), function (response) {

                }, 'json').done(function (data) {

                    if (data.success) {
                        smallSwal("Added!!", data.message, "success");
                        var setid = $(".listOfSetQuestions").data('setid');;
                        var title = $("#Sets .setTitle").text();
                        loadSetQuestions(setid, title)

                    } else {
                        swal("Error!!", data.message, "error")

                    }

                });


                return false;
            });


        };

        function appDifficultyTooltip() {
            var icons = $("#QuestionDifficultyLevel .material-icons");
            icons.each(function (index, iconElement) {
                var difficulty = index == 0 ? "Easy" : index == 1 ? "Moderate" : index == 2 ? "Difficult" : "";
                $(iconElement).attr({ "data-toggle": "tooltip", "title": difficulty })
            });
            applyTooltip();
        }


        $(document).on("click", ".addTestbutton", function () {
            $('#addTestForm')[0].reset();
            var attributeid = this.dataset.attributeid;
            var attributetype = this.dataset.attributetype;
            $('#addTestModal [name=AttributeId]').attr("value", attributeid);
            $('#addTestModal [name=AttributeType]').attr("value", attributetype);
            $("#addTestModal [name=QuestionIds]").attr("value", "");
            $("#addTestModal [name=QuestionIds]").val("");
         
            loadAvailableSetsWithQuestions(attributeid, attributetype, "addTestModal");

            if (tinymce.get("Detail") !== null)
                tinymce.get("Detail").remove();

            TinyMVCGlobalOptions.selector = "#addTestModal .tmceeditor";
            initializeDatePicker();
            tinymce.init(TinyMVCGlobalOptions);
            $('#addTestModal .mce-tinymce.mce-container').css("display", "block");
            addTestSubmitForm();
            
            
        });

        $(document).on("click", ".editTestButton", function () {
            var attributeid = this.dataset.attributeid;
            var attributetype = this.dataset.attributetype;
            var id = this.dataset.id;
            $.ajax({
                url: '/Test/Edit',
                type: "POST",

                data: {
                    id: id
                },
                success: function (view) {
                    $('#editTestModal .editTestView').html(view);

                    loadAvailableSetsWithQuestions(attributeid, attributetype, "editTestModal");

                    if (tinymce.get("Detail") !== null)
                        tinymce.get("Detail").remove();

                    TinyMVCGlobalOptions.selector = "#editTestModal .tmceeditor";
                    $('.datetimepicker').datetimepicker({
                        format: 'd-m-Y h:i a',
                        timepicker: true,
                        autoclose: true,
                        step: 5,
                        value: $(".editTestForm [name=ExpiryDate]").val()
                    });

                    tinymce.init(TinyMVCGlobalOptions);
                    $('#editTestModal .mce-tinymce.mce-container').css("display", "block");
                    editTestSubmitForm();

                },
                error: function (error) {
                    returnErrorState(error);
                }

            })


           


        });

      

        $(document).on("click", ".deleteTestButton", function () {
            var id = this.dataset.id;
            var attributeType = $(".opened-attribute").data().attributetype;
            var attributeId = $(".opened-attribute").data().attributeid;

            swal({
                title: 'Are you sure?',
                html: 'Type <b>"delete"</b>, to erase this test.',
                input: 'text',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Delete',
                showLoaderOnConfirm: true,
                preConfirm: (text) => {
                    return new Promise((resolve) => {

                        if (text != 'delete') {
                            swal.showValidationError(
                                'Type correct spelling.'
                            )
                            resolve();
                        } else {
                            deleteTestDetails(id, attributeType, attributeId);

                        }


                    })
                },
                allowOutsideClick: () => !swal.isLoading()
            });

        })

        function deleteTestDetails(id, attributeType, attributeId) {

            $.ajax({
                url: "/Test/Delete",
                type: "POST",
                data: { id: id },
                success: function (response) {
                    if (response.success) {


                        smallSwal({
                            type: 'success',
                            title: response.message
                        });
                        loadTests(attributeType, attributeId);

                    }
                    else {
                        swal("Error!", response.message, "error")
                        loadTests(attributeType, attributeId);
                    }

                },
                error: function (error) {
                    returnErrorState(error);
                }

            })

        }

        //Add Test Form Submission
        function addTestSubmitForm() {

            var $form = $('#addTestForm');
            $form.off("submit");
            $form.submit(function (data, event) {
                $('#addTestForm button[type=submit]').addClass("disableClick");
                $('#addTestForm button[type=submit] i').addClass("fa-spinner fa-spin");

                $.post($(this).attr('action'), $(this).serialize(), function (response) {

                }, 'json').done(function (data) {

                    if (data.success) {
                        smallSwal("Added!!", data.message, "success");

                        var attributeid = $(".opened-attribute").data('attributeid');
                        var attributetype = $(".opened-attribute").data('attributetype');
                        
                        loadTests(attributetype, attributeid);
                        $('#addTestForm button[type=submit]').removeClass("disableClick");
                        $('#addTestForm button[type=submit] i').removeClass("fa-spinner fa-spin");

                    } else {
                        swal("Error!!", data.message, "error")
                        $('#addTestForm button[type=submit]').removeClass("disableClick");
                        $('#addTestForm button[type=submit] i').removeClass("fa-spinner fa-spin");

                    }

                });


                return false;
            });



        };

        //Edit Test Form Submission
        function editTestSubmitForm() {

            var $form = $('#editTestForm');
           
            $form.off("submit");
            $form.submit(function (data, event) {
                $('#editTestForm button[type=submit] i').addClass("fa-spinner fa-spin");
                $('#editTestForm button[type=submit]').addClass("disableClick");
                $.post($(this).attr('action'), $(this).serialize(), function (response) {

                }, 'json').done(function (data) {

                    if (data.success) {
                       
                        var attributeid = $(".opened-attribute").data('attributeid');
                        var attributetype = $(".opened-attribute").data('attributetype');

                        smallSwal("Updated!!", data.message, "success");
                        loadTests(attributetype, attributeid);
                        $('#editTestForm button[type=submit]').removeClass("disableClick");
                   
                        $('#editTestForm button[type=submit] i').removeClass("fa-spinner fa-spin");

                    } else {
                        $('#editTestForm button[type=submit]').removeClass("disableClick");
                        $('#editTestForm button[type=submit] i').removeClass("fa-spinner fa-spin");
                        swal("Error!!", data.message, "error")

                    }

                });


                return false;
            });



        };

        function loadAvailableSetsWithQuestions(attributeid, attributetype, modal) {
            $.ajax({
                url: "/Test/GetSetWithQuestions",
                type: "POST",
                data: { attributeId: attributeid, attributeType: attributetype },
                success: function (view) {
                    $('#' + modal).modal();
                    $('#' + modal + ' .listOfSetWithQuestions').html(view);

                    var questionsSelected = $("#" + modal + " [name=QuestionIds]").val().split(',');
                    if (questionsSelected != "") {
                        for (var i = 0; i < questionsSelected.length; i++) {
                            var id = questionsSelected[i];
                            $('#' + modal + ' .listOfSetWithQuestions  .question-item[data-id=' + id + ']').addClass("selected");
                        }
                    }
                    

                    appendDificulties();
                },
                error: function (error) {
                    returnErrorState(error);
                }

            })
        }
      
        


        $(document).on("click", ".listOfSetWithQuestions .selectQuestion", function (event) {

            var btn = $(this);
         

            if (!btn.parents("li").hasClass("selected")) {
              
                btn.parents("li").addClass("selected");
            } else {
              
                btn.parents("li").removeClass("selected");
            }

            var selectedQuestions = $(".listOfSetWithQuestions .question-item.selected");
            var questionids=[];
            selectedQuestions.each(function (i, element) {
               
                var questionid = $(element).data('id');
                questionids.push(questionid);
              
            });
            if ($("#addTestModal.show").length > 0) {
                $("#addTestModal [name=QuestionIds]").attr("value", questionids.join(","));
                $("#addTestModal [name=QuestionIds]").val(questionids.join(","));

            } else {
                $("#editTestModal [name=QuestionIds]").attr("value", questionids.join(","));
                $("#editTestModal [name=QuestionIds]").val(questionids.join(","));
            }
            
           
           
        });

        $(document).on("click", ".viewSetQuestionButton", function (event) {
            var btn = this;
            var id = $(btn).data('id');
          
            var isQuestionExist = $("#Question_" + id).length > 0;
            if (isQuestionExist) {
                $("#Question_" + id).remove();
                loadSetQuestionDetail(id);
            } else
                loadSetQuestionDetail(id);

            applyTooltip();
            

        });
        function loadSetQuestionDetail(questionId) {
            $.ajax({
                url: '/Question/GetSetQuestion',
                type: "POST",

                data: {
                    id: questionId
                },
                success: function (view) {

                    $(".question-detail").append(view);
                    scrollTo("#Question_" + questionId);

                    var questionDeficulties = $("#Question_" + questionId+" [name=questionDificulty]");
                    questionDeficulties.each(function (i, e) {

                        var level = e.dataset.rating;
                        $(e).addRating({ max: 3,selectedRatings: level });
                    });
                    appDifficultyTooltip();
                    if ($(".navigation-confirmation-box").css("display") == "block") {
                        activateNaviationAppliedPowertip();
                    }
                    applyPopover();
                },
                error: function (error) {
                    returnErrorState(error);
                }

            })
        }
     
    