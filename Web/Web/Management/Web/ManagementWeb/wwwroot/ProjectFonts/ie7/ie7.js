/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'ilumfonts\'">' + entity + '</span>' + html;
	}
	var icons = {
		'ilum-add-request': '&#xe907;',
		'ilum-add-navigation': '&#xe905;',
		'ilum-remove-navigation': '&#xe906;',
		'ilum-answer': '&#xe903;',
		'ilum-researcher': '&#xe901;',
		'ilum-advice-request': '&#xe902;',
		'ilum-plus': '&#xe904;',
		'ilum-add-invoice': '&#xe900;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/ilum-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
