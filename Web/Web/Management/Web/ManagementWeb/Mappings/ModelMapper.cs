﻿using AutoMapper;
using Management.Data.Models;
using Management.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Mappings
{
    public static class ModelMapper
    {
        public static MapperConfiguration Configure()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserViewModel,User >();
                cfg.CreateMap<User, UserViewModel>();
                cfg.CreateMap<AttributeViewModel,EducationLevel>();
                cfg.CreateMap<AttributeViewModel,BoardLevel>()
                    .ForMember(dest => dest.EducationLevelId, opts => opts.MapFrom(src => src.ParentId));
            });
            return config;
        }
    }
}
