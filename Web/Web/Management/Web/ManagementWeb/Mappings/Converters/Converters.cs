﻿using AutoMapper;
using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Mappings.Converters
{
    public class DateTimeConverter : ITypeConverter<string, DateTime>
    {
        public DateTime Convert(string source, DateTime destination, ResolutionContext context)
        {
            
            return DateTime.ParseExact(source, "dd/MM/yyy HH:mm", null);
        }
    }

    public class DateConverter : ITypeConverter<string, DateTime>
    {
        public DateTime Convert(string source, DateTime destination, ResolutionContext context)
        {
            return DateTime.ParseExact(source, "dd/MM/yyyy", null); 
        }
    }

   
}
