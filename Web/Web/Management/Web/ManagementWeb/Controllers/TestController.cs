﻿using AutoMapper;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Management.Filters;
using Management.Infrastructure.Communication;
using Management.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class TestController : Controller
    {
        private readonly UserManager<User> userManager;

        private readonly IUnitOfWork unitOfWork;
        private IHostingEnvironment env;
        private readonly IChapterRepository chapterRepository;
        private readonly IEducationLevelRepository educationLevelRepository;

        private readonly IQuestionRepository questionRepository;
        private readonly ICorrectAnswerRepository correctAnswerRepository;
        private readonly IAnswerRepository answerRepository;
        private readonly IEmailService emailService;
        private readonly IQuestionTypeRepository questionTypeRepository;
        private readonly ISetRepository setRepository;
        private readonly ISectionRepository sectionRepository;
        private readonly ISubSectionRepository subSectionRepository;
        private readonly ISetQuestionRepository setQuestionRepository;
        private readonly ITestRepository testRepository;
        private readonly ITestCreatedFromRepository testCreatedFromRepository;
        private readonly ITestQuestionRepository testQuestionRepository;
        private readonly IQuestionDifficultyLevelRepository questionDifficultyLevelRepository;
        private readonly IUserTestAttemptRepository userTestAttemptRepository;
        private readonly IUserQuestionAttemptRepository userQuestionAttemptRepository;
        private readonly IMTQAnsweredChoiceRepository mTQAnsweredChoiceRepository;
        private readonly IBatchTestRepository batchTestRepository;
        private readonly IUserBatchRepository userBatchRepository;
        private readonly IBatchRepository batchRepository;
        private readonly ISubjectRepository subjectRepository;
        private readonly IScheduleRepository scheduleRepository;

        public TestController(IScheduleRepository _scheduleRepository, ISubjectRepository _subjectRepository
            , IUserBatchRepository _userBatchRepository, IBatchRepository _batchRepository, IBatchTestRepository _batchTestRepository, IMTQAnsweredChoiceRepository _mTQAnsweredChoiceRepository, IAnswerRepository _answerRepository, IUserTestAttemptRepository _userTestAttemptRepository, IUserQuestionAttemptRepository _userQuestionAttemptRepository, ISubSectionRepository _subSectionRepository, ISectionRepository _sectionRepository, ITestCreatedFromRepository _testCreatedFromRepository, UserManager<User> _userManager, IChapterRepository _chapterRepository, IEducationLevelRepository _educationLevelRepository, ITestQuestionRepository _testQuestionRepository, ITestRepository _testRepository, ISetQuestionRepository _setQuestionRepository, ISetRepository _setRepository, IQuestionDifficultyLevelRepository _questionDifficultyLevelRepository, IQuestionTypeRepository _questionTypeRepository, ICorrectAnswerRepository _correctAnswerRepository, IQuestionRepository _questionRepository, IEmailService _emailService, IHostingEnvironment _env, IUnitOfWork _unitOfWork)
        {

            unitOfWork = _unitOfWork;
            userManager = _userManager;

            emailService = _emailService;

            correctAnswerRepository = _correctAnswerRepository;
            questionRepository = _questionRepository;
            setRepository = _setRepository;
            setQuestionRepository = _setQuestionRepository;
            testRepository = _testRepository;
            testQuestionRepository = _testQuestionRepository;
            userTestAttemptRepository = _userTestAttemptRepository;
            userQuestionAttemptRepository = _userQuestionAttemptRepository;
            batchTestRepository = _batchTestRepository;
            questionTypeRepository = _questionTypeRepository;
            questionDifficultyLevelRepository = _questionDifficultyLevelRepository;
            educationLevelRepository = _educationLevelRepository;
            chapterRepository = _chapterRepository;
            env = _env;
            testCreatedFromRepository = _testCreatedFromRepository;
            subSectionRepository = _subSectionRepository;
            sectionRepository = _sectionRepository;
            answerRepository = _answerRepository;
            mTQAnsweredChoiceRepository = _mTQAnsweredChoiceRepository;
            batchRepository = _batchRepository;
            userBatchRepository = _userBatchRepository;
            subjectRepository = _subjectRepository;
            scheduleRepository = _scheduleRepository;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Student,Researcher,Admin")]
        public IActionResult Demo()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> List()
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);

                var testsList = testRepository.Get().Where(a => a.TestTypeId == Convert.ToInt32(TestTypes.StudentTest) && a.UserId == loggedUser.Id).ToList();
                var tests = Mapper.Map<List<TestViewModel>>(testsList);

                return View(tests);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Student")]
        public async Task<IActionResult> StartTest(int id)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);

                var test = testRepository.Get().Include("TestQuestions").Include("TestQuestions.Question").FirstOrDefault(a => a.Id == id && a.TestTypeId == Convert.ToInt32(TestTypes.StudentTest) && a.UserId == loggedUser.Id);
                var model = Mapper.Map<TestViewModel>(test);

                return View(model);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Student")]
        public async Task<IActionResult> InProgress(int id)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var userTestAttempt = new UserTestAttempt();
                var userTestAttemptExist = userTestAttemptRepository.Get().Include(x => x.Test).FirstOrDefault(a => a.TestId == id && a.UserId == loggedUser.Id);
                if (userTestAttemptExist == null)
                {
                    userTestAttempt = new UserTestAttempt()
                    {
                        UserId = loggedUser.Id,
                        TestId = id,
                    };
                    await userTestAttemptRepository.AddAsync(userTestAttempt);
                    await unitOfWork.SaveChangesAsync();
                }
                else
                {
                    double duration = TimeSpan.Parse(userTestAttemptExist.Test.Hours + ":" + userTestAttemptExist.Test.Minutes).TotalMinutes;
                    TimeSpan span = new TimeSpan();
                    DateTime startTime = userTestAttemptExist.StartedDate;
                    DateTime endTime = userTestAttemptExist.StartedDate.AddMinutes(duration);
                    span = endTime - DateTime.Now;
                    //Inprogress
                    if (span.Minutes > 0)
                    {
                        userTestAttemptExist.LastUpdated = DateTime.Now;
                        await userTestAttemptRepository.UpdateAsync(userTestAttemptExist);
                        await unitOfWork.SaveChangesAsync();
                    }
                    //Finished
                    else
                    {
                        return Redirect("/Student/Index");
                    }

                }

                var test = await testRepository.GetInProgressTest(id, loggedUser.Id);
                //var test = testRepository.Get().Include("UserTestAttempts").Include("TestQuestions").Include("TestQuestions.Question").FirstOrDefault(a => a.Id == id && a.TestTypeId == Convert.ToInt32(TestTypes.StudentTest) && a.UserId == loggedUser.Id);

                if (test != null)
                {

                    var model = Mapper.Map<TestViewModel>(test);

                    return View(model);

                }
                else
                {
                    return new NotAllowedViewResult();
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Student")]
        public async Task<BaseModel> SetTestStartTime(int id, DateTime startDateTime)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var userTestAttemptExist = userTestAttemptRepository.Get().Include(x => x.Test).FirstOrDefault(a => a.TestId == id && a.UserId == loggedUser.Id);

                userTestAttemptExist.StartedDate = startDateTime;

                await userTestAttemptRepository.UpdateAsync(userTestAttemptExist);
                await unitOfWork.SaveChangesAsync();
                return BaseModel.Succeeded("Start Time Set Succeessfully!!");

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Student")]
        public async Task<BaseModel> Finish(int id)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var userTestAttempt = await userTestAttemptRepository.Get().Include("User").Include("Test").Include("Test").Include("Test.BatchTest").Include("Test.BatchTest.Batch").Include("Test.BatchTest.Batch.Subject").FirstOrDefaultAsync(a => a.TestId == id && a.UserId == loggedUser.Id);
                var userTestAttemptExist = userTestAttemptRepository.Get().Include(x => x.Test).FirstOrDefault(a => a.TestId == id && a.UserId == loggedUser.Id);
                if (userTestAttemptExist != null)
                {
                    userTestAttemptExist.LastUpdated = DateTime.Now;
                    userTestAttemptExist.IsFinished = true;

                    await userTestAttemptRepository.UpdateAsync(userTestAttemptExist);

                    var examTestMailModel = new ExamTestMailModel()
                    {
                        Test = userTestAttempt.Test,
                        Batch = userTestAttempt.Test.BatchTest.FirstOrDefault().Batch,
                        Student = userTestAttempt.User,
                        Teacher = loggedUser,
                        Subject = userTestAttempt.Test.BatchTest.FirstOrDefault().Batch.Subject,
                        UserTestAttempt = userTestAttempt
                    };
                    emailService.TestCompletionMailToTeacher(examTestMailModel);
                    await unitOfWork.SaveChangesAsync();
                }
                return new BaseModel() { Success = true, Message = "Test has been Finished!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Student")]
        public async Task<IActionResult> Result(int id)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var test = await testRepository.GetInProgressTest(id, loggedUser.Id);
                if (test != null)
                {
                    var model = Mapper.Map<TestViewModel>(test);
                    return View(model);
                }
                else
                {
                    return new NotAllowedViewResult();
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Student")]
        public async Task<PartialViewResult> ResultView(int id)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var test = await testRepository.GetInProgressTest(id, loggedUser.Id);
                if (test != null)
                {
                    var model = Mapper.Map<TestViewModel>(test);
                    return PartialView("_ResultView", model);
                }
                else
                {
                    return new NotAllowedViewResult();
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<BaseModel> AwardMarks(MarksAndRemarks model)
        {
            try
            {

                var loggedUser = await userManager.GetUserAsync(User);
                var userQuestionAttempt = await userQuestionAttemptRepository.Get().FirstOrDefaultAsync(a => a.TestId == model.TestId && a.UserId == model.UserId && a.QuestionId == model.QuestionId);

                var userTestAttempt = await userTestAttemptRepository.Get().Include("User").Include("Test").Include("Test").Include("Test.BatchTest").Include("Test.BatchTest.Batch").Include("Test.BatchTest.Batch.Subject").FirstOrDefaultAsync(a => a.TestId == model.TestId && a.UserId == model.UserId);
                var msg = "Marks Awarded Successfully!!";
                var isUpdating = false;
                if (userQuestionAttempt != null)
                {
                    if (userQuestionAttempt.IsMarksAwarded)
                    {
                        msg = "Updated Successfully!!";
                        isUpdating = true;
                    }
                    userQuestionAttempt.IsMarksAwarded = true;
                    userQuestionAttempt.Remarks = model.Remarks;
                    userQuestionAttempt.Marks = model.Marks;
                    await userQuestionAttemptRepository.UpdateAsync(userQuestionAttempt);
                    await unitOfWork.SaveChangesAsync();
                }
                else
                {
                    var answer = new Answer()
                    {
                        UserId = model.UserId,
                        QuestionId = model.QuestionId,

                    };
                    var newUserQuestionAttempt = new UserQuestionAttempt()
                    {
                        IsQuestionAttempted = false,
                        IsMarksAwarded = true,
                        Remarks = model.Remarks,
                        Marks = model.Marks,
                        ViewSeconds = 0,
                        TestId = model.TestId,
                        QuestionId = model.QuestionId,
                        UserId = model.UserId,
                        Answer = answer

                    };
                    await answerRepository.AddAsync(answer);
                    await userQuestionAttemptRepository.AddAsync(newUserQuestionAttempt);
                    await unitOfWork.SaveChangesAsync();

                }

                var testQuestions = testQuestionRepository.Get().Where(a => a.TestId == model.TestId);
                var evaluatedQuestions = userQuestionAttemptRepository.Get().Where(a => a.TestId == model.TestId && a.UserId == model.UserId);
                var isUnevaluatedQuestionsLeft = evaluatedQuestions.Any(a => !a.IsMarksAwarded);
                var marks = evaluatedQuestions.Sum(x => x.Marks);
                var numberOfEvaluatedQuestions = evaluatedQuestions.Count();
                var numberOfTestQuestions = testQuestions.Count();
                var isEvaluatedAllNumberOfQuestions = numberOfEvaluatedQuestions == numberOfTestQuestions;
                if (!isUnevaluatedQuestionsLeft && isEvaluatedAllNumberOfQuestions && !isUpdating)
                {
                    userTestAttempt.IsEvaluated = true;
                    userTestAttempt.EvaluationDate = DateTime.Now;
                    userTestAttempt.Marks = marks;

                    await userTestAttemptRepository.UpdateAsync(userTestAttempt);
                    await unitOfWork.SaveChangesAsync();

                    var totalMarks = await testQuestionRepository.Get().Include(a => a.Question).Where(a => a.TestId == model.TestId).SumAsync(a => string.IsNullOrEmpty(a.Question.Marks) ? 0 : Convert.ToInt32(a.Question.Marks));
                    var unattemptedQuestions = await userQuestionAttemptRepository.Get().Where(a => a.TestId == model.TestId && a.UserId == model.UserId && !a.IsQuestionAttempted).CountAsync();
                    var examTestMailModel = new ExamTestMailModel()
                    {
                        TotalMarks = totalMarks,
                        UnattemptedQuestions = unattemptedQuestions,
                        Test = userTestAttempt.Test,
                        Batch = userTestAttempt.Test.BatchTest.FirstOrDefault().Batch,
                        Student = userTestAttempt.User,
                        Teacher = loggedUser,
                        Subject = userTestAttempt.Test.BatchTest.FirstOrDefault().Batch.Subject,
                        UserTestAttempt = userTestAttempt
                    };
                    emailService.TestEvaluationCompletedMail(examTestMailModel);
                }
                else
                {
                    await unitOfWork.SaveChangesAsync();
                }

                return BaseModel.Succeeded(msg);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Teacher,Student")]
        public async Task<BaseModel> AwardMarksToMultipleQuestions(List<MarksAndRemarks> models)
        {
            try
            {
                var msg = "Marks Awarded Successfully!!";
                var userTestAttempt = await userTestAttemptRepository.Get().Include("User").Include("Test").Include("Test").Include("Test.BatchTest").Include("Test.BatchTest.Batch").Include("Test.BatchTest.Batch.Subject").Include("Test.BatchTest.Batch.BatchUsers").Include("Test.BatchTest.Batch.BatchUsers.User").FirstOrDefaultAsync(a => a.TestId == models.First().TestId && a.UserId == models.First().UserId);
                var loggedUser = await userManager.GetUserAsync(User);

                foreach (var model in models)
                {
                    var userQuestionAttempt = await userQuestionAttemptRepository.Get().FirstOrDefaultAsync(a => a.TestId == model.TestId && a.UserId == model.UserId && a.QuestionId == model.QuestionId);

                    if (userQuestionAttempt != null)
                    {
                        userQuestionAttempt.IsMarksAwarded = true;
                        userQuestionAttempt.Remarks = model.Remarks;
                        userQuestionAttempt.Marks = model.Marks;
                        await userQuestionAttemptRepository.UpdateAsync(userQuestionAttempt);
                        await unitOfWork.SaveChangesAsync();
                    }
                    else
                    {
                        var answer = new Answer()
                        {
                            UserId = model.UserId,
                            QuestionId = model.QuestionId,

                        };
                        var newUserQuestionAttempt = new UserQuestionAttempt()
                        {
                            IsQuestionAttempted = false,
                            IsMarksAwarded = true,
                            Remarks = model.Remarks,
                            Marks = model.Marks,
                            ViewSeconds = 0,
                            TestId = model.TestId,
                            QuestionId = model.QuestionId,
                            UserId = model.UserId,
                            Answer = answer

                        };
                        await answerRepository.AddAsync(answer);
                        await userQuestionAttemptRepository.AddAsync(newUserQuestionAttempt);
                        await unitOfWork.SaveChangesAsync();

                    }



                }
                var batch = userTestAttempt.Test.BatchTest.FirstOrDefault().Batch;
                var batchViewModel = Mapper.Map<BatchViewModel>(batch);

                await batchViewModel.BatchUsers.ToAsyncEnumerable().ForEachAsync(a => a.IsTeacher = userManager.IsInRoleAsync(Mapper.Map<User>(a.User), UserRoles.Teacher.ToString()).Result);
                var teacher = Mapper.Map<User>(batchViewModel.BatchUsers.FirstOrDefault(a => a.IsTeacher).User);

                var testQuestions = testQuestionRepository.Get().Where(a => a.TestId == models.First().TestId);
                var evaluatedQuestions = userQuestionAttemptRepository.Get().Where(a => a.TestId == models.First().TestId && a.UserId == models.First().UserId);
                var isUnevaluatedQuestionsLeft = evaluatedQuestions.Any(a => !a.IsMarksAwarded);
                var marks = evaluatedQuestions.Sum(x => x.Marks);
                var numberOfEvaluatedQuestions = evaluatedQuestions.Count();
                var numberOfTestQuestions = testQuestions.Count();
                var isEvaluatedAllNumberOfQuestions = numberOfEvaluatedQuestions == numberOfTestQuestions;
                if (!isUnevaluatedQuestionsLeft && isEvaluatedAllNumberOfQuestions)
                {
                    userTestAttempt.IsEvaluated = true;
                    userTestAttempt.EvaluationDate = DateTime.Now;
                    userTestAttempt.Marks = marks;

                    await userTestAttemptRepository.UpdateAsync(userTestAttempt);
                    await unitOfWork.SaveChangesAsync();

                    var totalMarks = await testQuestionRepository.Get().Include(a => a.Question).Where(a => a.TestId == models.First().TestId).SumAsync(a => string.IsNullOrEmpty(a.Question.Marks) ? 0 : Convert.ToInt32(a.Question.Marks));
                    var unattemptedQuestions = await userQuestionAttemptRepository.Get().Where(a => a.TestId == models.First().TestId && a.UserId == models.First().UserId && !a.IsQuestionAttempted).CountAsync();
                    var examTestMailModel = new ExamTestMailModel()
                    {
                        TotalMarks = totalMarks,
                        UnattemptedQuestions = unattemptedQuestions,
                        Test = userTestAttempt.Test,
                        Batch = userTestAttempt.Test.BatchTest.FirstOrDefault().Batch,
                        Student = userTestAttempt.User,
                        Teacher = teacher,
                        Subject = userTestAttempt.Test.BatchTest.FirstOrDefault().Batch.Subject,
                        UserTestAttempt = userTestAttempt
                    };
                    emailService.TestEvaluationCompletedMail(examTestMailModel);
                }

                return BaseModel.Succeeded(msg);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Student")]
        public async Task<BaseModel> UserAttemptQuestion(UserQuestionAttemptViewModel model)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var userQuestionAttempt = userQuestionAttemptRepository.Get().Where(a => a.TestId == model.TestId && a.QuestionId == model.QuestionId && a.UserId == loggedUser.Id);
                var userTestAttemptExist = userTestAttemptRepository.Get().Include(x => x.Test).FirstOrDefault(a => a.TestId == model.TestId && a.UserId == loggedUser.Id);
                double duration = TimeSpan.Parse(userTestAttemptExist.Test.Hours + ":" + userTestAttemptExist.Test.Minutes).TotalMinutes;
                TimeSpan span = new TimeSpan();
                DateTime startTime = userTestAttemptExist.StartedDate;
                DateTime endTime = userTestAttemptExist.StartedDate.AddMinutes(duration);
                span = endTime.Subtract(DateTime.Now);
                //Inprogress
                if (span.Seconds > 0)
                {

                    if (!userQuestionAttempt.Any())
                    {
                        var uQA = new UserQuestionAttempt()
                        {
                            Date = DateTime.Now,
                            QuestionId = model.QuestionId,
                            TestId = model.TestId,
                            UserId = loggedUser.Id,
                            ViewSeconds = model.ViewSeconds,
                            IsQuestionAttempted = true
                        };
                        if (model.QuestionTypeId == (int)QustionTypesEnum.MTQ)
                        {
                            var answer = new Answer();
                            answer.QuestionId = model.QuestionId;
                            answer.UserId = loggedUser.Id;
                            await answerRepository.AddAsync(answer);

                            var mTQAnswers = new List<MTQAnsweredChoice>();
                            foreach (var mtqChoiceId in model.Answer.MTQChoicesIds)
                            {
                                var mTQAnswer = new MTQAnsweredChoice()
                                {
                                    MTQChoiceId = mtqChoiceId,
                                    Answer = answer,
                                };
                                mTQAnswers.Add(mTQAnswer);
                            }

                            await mTQAnsweredChoiceRepository.AddManyAsync(mTQAnswers);
                            uQA.Answer = answer;
                        }
                        else if (model.QuestionTypeId == (int)QustionTypesEnum.MCQ)
                        {
                            var answer = new Answer()
                            {
                                MCQChoiceId = model.Answer.MCQChoiceId,
                                QuestionId = model.QuestionId,
                                UserId = loggedUser.Id
                            };
                            await answerRepository.AddAsync(answer);
                            uQA.Answer = answer;
                        }
                        else if (model.QuestionTypeId == (int)QustionTypesEnum.FillBlank)
                        {
                            var answer = new Answer()
                            {
                                Detail = model.Answer.Detail,
                                QuestionId = model.QuestionId,
                                UserId = loggedUser.Id
                            };
                            await answerRepository.AddAsync(answer);
                            uQA.Answer = answer;
                        }

                        else
                        {
                            if (model.Answer.WrittenAnswerType == (int)WrittenAnswerType.Upload)
                            {
                                var webRoot = env.WebRootPath + "/UploadedArtifacts/";
                                var fileName = model.Answer.ImageFileName.Split(',').ToList();
                                fileName.ForEach(a => a = "/UploadedArtifacts/" + a);
                                var fileNameWithUrl = string.Join(',', fileName);
                                foreach (var name in fileName)
                                {
                                    var filePath = System.IO.Path.Combine(webRoot, name);
                                    var isFileExist = System.IO.File.Exists(filePath);
                                    if (!isFileExist)
                                    {
                                        return new BaseModel() { Success = false, Message = "File number " + fileName.IndexOf(name) + " is not uploaded, try again or you can add answer image through editor, by clicking image icon and upload." };
                                    }
                                }

                                var answer = new Answer()
                                {
                                    WrittenAnswerType = (int)WrittenAnswerType.Upload,
                                    ImageFileName = model.Answer.ImageFileName,
                                    ImageFileUrl = fileNameWithUrl,
                                    QuestionId = model.QuestionId,
                                    UserId = loggedUser.Id
                                };
                                await answerRepository.AddAsync(answer);
                                uQA.Answer = answer;
                            }
                            else
                            {
                                var answer = new Answer()
                                {
                                    WrittenAnswerType = (int)WrittenAnswerType.Editor,
                                    Detail = model.Answer.Detail,
                                    QuestionId = model.QuestionId,
                                    UserId = loggedUser.Id
                                };
                                await answerRepository.AddAsync(answer);
                                uQA.Answer = answer;
                            }

                        }


                        await userQuestionAttemptRepository.AddAsync(uQA);
                        await unitOfWork.SaveChangesAsync();
                        return new BaseModel() { Success = true, Message = "Answer submited successfully!" };
                    }
                    else
                    {
                        return new BaseModel() { Success = false, Message = "Answer already submited!" };

                    }
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Answer can not be submited, Time is Over!" };
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Student")]
        public async Task<BaseModel> UserDemoAttemptQuestion(UserQuestionAttemptViewModel model)
        {
            try
            {

                return new BaseModel() { Success = true, Message = "Answer submited successfully!" };


            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Detail(int id)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var test = testRepository.Get().Include("TestQuestions").Include("TestQuestions.Question").FirstOrDefault(a => a.Id == id && a.TestTypeId == Convert.ToInt32(TestTypes.StudentTest));

                var batch = batchTestRepository.Get().Include(s => s.Batch).Include("Batch.Subject").FirstOrDefault(a => a.TestId == id).Batch;
                var isAllocated = userBatchRepository.Get().Any(x => x.BatchId == batch.Id && x.UserId == loggedUser.Id);
                if ((isAllocated || isAdmin) && test != null)
                {

                    var userTestAttempted = userTestAttemptRepository.Get().Where(a => a.TestId == id && a.IsFinished).Include(a => a.Test).Include(a => a.User).ToList();
                    ViewBag.UserTestAttempted = userTestAttempted;

                    ViewBag.Batch = Mapper.Map<BatchViewModel>(batch);
                    var model = Mapper.Map<TestViewModel>(test);

                    return View(model);
                }
                else
                {
                    return new NotAllowedViewResult();
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin, Teacher")]
        public IActionResult Create(int id)
        {
            try
            {
                var batch = batchRepository.Get().Include(a => a.Subject).FirstOrDefault(a => a.Id == id);
                ViewBag.Batch = Mapper.Map<BatchViewModel>(batch);
                return View();
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin, Teacher")]
        [HttpPost]
        public async Task<BaseModel> Create(TestViewModel model)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);

                var test = new Test()
                {
                    AddedDate = DateTime.Now,
                    TestTitle = model.TestTitle,
                    Hours = model.Hours,
                    Minutes = model.Minutes,
                    TestTypeId = Convert.ToInt32(TestTypes.StudentTest),
                    UserId = loggedUser.Id,
                    IncludeQuestion = model.IncludeQuestion,
                    WithAdvice = model.WithAdvice,
                    ExpiryDate = model.ExpiryDate,
                    QuestionDifficultyLevelId=model.QuestionDifficultyLevelId

                };
                await testRepository.AddAsync(test);

                var batchTest = new BatchTest()
                {
                    Test = test,
                    BatchId = model.BatchId
                };
                await batchTestRepository.AddAsync(batchTest);

                var testCreateFromList = new List<TestCreatedFrom>();
                foreach (var attribute in model.Attributes)
                {
                    var testCreateFrom = new TestCreatedFrom()
                    {
                        AttributeId = attribute.Id,
                        AttributeType = attribute.Type,
                        Test = test
                    };
                    testCreateFromList.Add(testCreateFrom);
                }
                await testCreatedFromRepository.AddManyAsync(testCreateFromList);
                IQueryable<Question> examQuestions;
                if (model.IncludeQuestion == IncludeQuestionType.byQuestionLevel)
                {
                    examQuestions = questionRepository.Get().Where(a => !a.IsDeleted && a.QuestionCategoryId == Convert.ToInt32(QuestionCategories.ExamQuestion) && a.QuestionDifficultyLevelId==model.QuestionDifficultyLevelId );
                }
                else
                {
                    examQuestions = questionRepository.Get().Where(a => !a.IsDeleted && a.QuestionCategoryId == Convert.ToInt32(QuestionCategories.ExamQuestion));
                }
                var testQuestions = new List<TestQuestion>();

                var chapters = model.Attributes.Where(a => a.Type == AttributeType.Chapter).ToList();


                // && model.Attributes.Select(x => x.Id).ToList().Contains(Convert.ToInt32(a.AttributeId)) && model.Attributes.Select(x => x.Type).ToList().Contains(a.AttributeType)

                var duration = TimeSpan.Parse(model.Hours + ":" + model.Minutes).TotalMinutes;
                var takeQ = 100;
                if (duration <= 30)
                {
                    takeQ = 50;
                }

                foreach (var chapter in chapters)
                {
                    var thisChapterQuestionCount = 0;
                    var chapterLeft = chapters.Count - chapters.IndexOf(chapter);

                    var chapterTake = DivideEvenly(takeQ, chapterLeft).FirstOrDefault();


                    var sections = model.Attributes.Where(a => a.Type == AttributeType.Section);

                    var chapSectionsDB = sectionRepository.Get().Where(x => x.ChapterId == chapter.Id && sections.Select(c => c.Id).Contains(Convert.ToInt32(x.Id))).Include(a => a.SubSections).ToList();

                    if (chapSectionsDB.Any())
                    {

                        foreach (var section in chapSectionsDB)
                        {
                            var thisSectionQuestionCount = 0;
                            var sectionLeft = chapSectionsDB.Count - chapSectionsDB.IndexOf(section);
                            var sectiontakeQ = DivideEvenly(chapterTake, sectionLeft).FirstOrDefault();

                            if (section.SubSections.Any() && sectiontakeQ > 0)
                            {



                                var subSections = model.Attributes.Where(a => a.Type == AttributeType.SubSection);
                                var subSectionDB = section.SubSections.Where(a => subSections.Select(c => c.Id).Contains(a.Id)).ToList();
                                foreach (var subSection in subSectionDB)
                                {
                                    var thisSubSectionQuestionCount = 0;
                                    var subsectionLeft = subSectionDB.Count - subSectionDB.IndexOf(subSection);
                                    var subsectiontakeQ = DivideEvenly(sectiontakeQ, subsectionLeft).FirstOrDefault();
                                    if (subsectiontakeQ > 0)
                                    {

                                        var subSectionQuestions = examQuestions.Where(a => Convert.ToInt32(a.AttributeId) == subSection.Id && a.AttributeType == AttributeType.SubSection).Take(subsectiontakeQ).ToList();

                                        foreach (var qs in subSectionQuestions)
                                        {
                                            thisSubSectionQuestionCount++;
                                            thisSectionQuestionCount++;
                                            thisChapterQuestionCount++;

                                            var testQ = new TestQuestion()
                                            {
                                                QuestionId = qs.Id,
                                                Test = test
                                            };
                                            testQuestions.Add(testQ);
                                        }
                                        sectiontakeQ = sectiontakeQ - thisSubSectionQuestionCount;
                                    }

                                }

                            }

                            var sectionQuestions = examQuestions.Where(a => Convert.ToInt32(a.AttributeId) == section.Id && a.AttributeType == AttributeType.Section).Take(sectiontakeQ).ToList();


                            foreach (var qs in sectionQuestions)
                            {
                                thisSectionQuestionCount++;
                                thisChapterQuestionCount++;
                                var testQ = new TestQuestion()
                                {
                                    QuestionId = qs.Id,
                                    Test = test
                                };
                                testQuestions.Add(testQ);
                            }
                            chapterTake -= thisSectionQuestionCount;

                        }


                    }
                    var chapterQuestions = examQuestions.Where(a => Convert.ToInt32(a.AttributeId) == chapter.Id && a.AttributeType == AttributeType.Chapter).Take(chapterTake).ToList();
                    foreach (var qs in chapterQuestions)
                    {
                        thisChapterQuestionCount++;
                        var testQ = new TestQuestion()
                        {
                            QuestionId = qs.Id,
                            Test = test
                        };
                        testQuestions.Add(testQ);
                    }
                    takeQ -= thisChapterQuestionCount;

                }


                await testQuestionRepository.AddManyAsync(testQuestions);
                await unitOfWork.SaveChangesAsync();

                var batch = batchRepository.Get().Include(x => x.Subject).Include("BatchUsers").Include("BatchUsers.User").FirstOrDefault(a => a.Id == model.BatchId);

                var batchViewModel = Mapper.Map<BatchViewModel>(batch);
                await batchViewModel.BatchUsers.ToAsyncEnumerable().ForEachAsync(a => a.IsTeacher = userManager.IsInRoleAsync(Mapper.Map<User>(a.User), UserRoles.Teacher.ToString()).Result);
                var teacher = Mapper.Map<User>(batchViewModel.BatchUsers.FirstOrDefault(a => a.IsTeacher).User);

                foreach (var userViewModel in batchViewModel.BatchUsers.Where(a => !a.IsTeacher).Select(a => a.User))
                {
                    var student = Mapper.Map<User>(userViewModel);

                    var examTestMailModel = new ExamTestMailModel()
                    {
                        Test = test,
                        Batch = batch,
                        Student = student,
                        Teacher = teacher,
                        Subject = batch.Subject
                    };
                    emailService.TestCreatedMail(examTestMailModel);
                }

                var scheduleEvent = new ScheduleViewModel()
                {
                    Title ="Test: "+ model.TestTitle,
                    EventUrl="",                   
                    BatchId = batch.Id,
                    EndDateTime = test.ExpiryDate,
                    StartDateTime = DateTime.Now,
                    LabelCategory = "bg-warning",
                    BatchTeacher= teacher,
                    ScheduleType = Convert.ToInt32( ScheduleTypes.Test),
                    TestId=test.Id
                };
                await SetSchedule(scheduleEvent);

                return new BaseModel()
                {
                    Success = true,
                    Message = "Test Created Successfully!",
                    Data = new { TestId = test.Id }
                };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }

        private async Task<bool> SetSchedule(ScheduleViewModel model)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var existSchedule = scheduleRepository.Get().Include("Batch").Include("Batch.Subject").Include("Batch.BatchUsers").Include("Batch.BatchUsers.User")
                    .Where(a => a.BatchId == model.BatchId
                    && a.StartDateTime == model.StartDateTime
                    && a.UserId == loggedUser.Id && a.ScheduleType == Convert.ToInt32(ScheduleTypes.Test));

                if (!existSchedule.Any())
                {
                    var Teachers = await userManager.GetUsersInRoleAsync("Teacher");
                    var batchTeacher = userBatchRepository.Get().Include("User").Include("Batch").FirstOrDefault(a => a.BatchId == model.BatchId && Teachers.Select(x => x.Id).Contains(a.UserId)).User;

                    var savemodel = Mapper.Map<Schedule>(model);
                    savemodel.UserId = loggedUser.Id;

                    savemodel.EventUrl = "";
                    
                    await scheduleRepository.AddAsync(savemodel);
                    await unitOfWork.SaveChangesAsync();
                    
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw;

            }
        }

        public static IEnumerable<int> DivideEvenly(int numerator, int denominator)
        {
            int rem;
            int div = Math.DivRem(numerator, denominator, out rem);

            for (int i = 0; i < denominator; i++)
            {
                yield return i < rem ? div + 1 : div;
            }
        }

        [Authorize(Roles = "Admin, Teacher")]
        public PartialViewResult GetChaptersListWithChilds(List<int> ids)
        {
            try
            {
                var model = chapterRepository.GetListWithChilds(ids);

                return PartialView("_AllChaptersTree", model);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin")]
        public PartialViewResult Get(TestViewModel model)
        {
            try
            {
                var listOfTests = testRepository.Get().Where(x => x.AttributeId == model.AttributeId && x.AttributeType == model.AttributeType).ToList();
                var tests = Mapper.Map<List<TestViewModel>>(listOfTests);
                return PartialView("_AllTests", tests);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin, Teacher")]
        public PartialViewResult GetBatchTests(int id)
        {
            try
            {
                var listOfTests = batchTestRepository.Get().Where(x => x.BatchId == id).Select(a => a.Test).ToList();
                var tests = Mapper.Map<List<TestViewModel>>(listOfTests);
                return PartialView("_AllExamTests", tests);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Teacher")]
        [HttpPost]
        public async Task<BaseModel> Add(TestViewModel model)
        {
            try
            {

                var test = new Test()
                {
                    TestTitle = model.TestTitle,
                    AttributeType = model.AttributeType,
                    AttributeId = model.AttributeId,
                    LastUpdated = DateTime.Now,
                    AddedDate = DateTime.Now,
                    ExpiryDate = model.ExpiryDate,
                    Detail = model.Detail,
                    TestTypeId = model.TestTypeId
                };
                await testRepository.AddAsync(test);


                if (model.QuestionIds != null)
                {
                    var testQuestions = new List<TestQuestion>();
                    foreach (var questionId in model.QuestionIds.Split(','))
                    {
                        var Qid = Convert.ToInt32(questionId);

                        var testQ = new TestQuestion()
                        {
                            QuestionId = Qid,
                            Test = test
                        };
                        testQuestions.Add(testQ);
                    }
                    await testQuestionRepository.AddManyAsync(testQuestions);
                }


                await unitOfWork.SaveChangesAsync();
                return BaseModel.Succeeded("Test Added Successfully!");


            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<BaseModel> Delete(int id)
        {
            try
            {
                var test = testRepository.Get().FirstOrDefault(a => a.Id == id);

                if (test != null)
                {
                    await testQuestionRepository.DeleteManyAsync(testQuestionRepository.Get().Where(a => a.TestId == id));
                    await testRepository.DeleteAsync(test);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseModel() { Success = true, Message = "Test Removed Successfully!" };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "No Test Exist or it is Removed, refresh your page please!!" };
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<BaseModel> DeleteBatchTest(int id, int batchId)
        {
            try
            {
                var test = testRepository.Get().FirstOrDefault(a => a.Id == id);

                if (test != null)
                {
                    var batchTest = batchTestRepository.Get().FirstOrDefault(a => a.TestId == id && a.BatchId == batchId);
                    await batchTestRepository.DeleteAsync(batchTest);

                    var userQuestionsAttemptExist = userQuestionAttemptRepository.Get().Where(a => a.TestId == id);
                    await userQuestionAttemptRepository.DeleteManyAsync(userQuestionsAttemptExist);

                    var userTestAttemptExist = userTestAttemptRepository.Get().Where(a => a.TestId == id);
                    await userTestAttemptRepository.DeleteManyAsync(userTestAttemptExist);

                    var testCreatedFrom = testCreatedFromRepository.Get().Where(a => a.TestId == id);
                    await testCreatedFromRepository.DeleteManyAsync(testCreatedFrom);

                    var scheduledTest = scheduleRepository.Get().Where(a => a.TestId == id);
                    await scheduleRepository.DeleteManyAsync(scheduledTest);

                    await testQuestionRepository.DeleteManyAsync(testQuestionRepository.Get().Where(a => a.TestId == id));
                    await testRepository.DeleteAsync(test);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseModel() { Success = true, Message = "Test Removed Successfully!" };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "No Test Exist or it is Removed, refresh your page please!!" };
                }
            }
            catch (Exception ex)
            {

                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> Update(TestViewModel model)
        {
            try
            {
                var test = testRepository.Get().FirstOrDefault(x => x.Id == model.Id);

                test.TestTitle = model.TestTitle;
                test.LastUpdated = DateTime.Now;
                test.ExpiryDate = model.ExpiryDate;
                test.Detail = model.Detail;
                test.QuestionDifficultyLevelId = model.QuestionDifficultyLevelId;

                await testRepository.UpdateAsync(test);
                var testQuestions = testQuestionRepository.Get().Where(a => a.TestId == model.Id);
                await testQuestionRepository.DeleteManyAsync(testQuestions);


                if (model.QuestionIds != null)
                {
                    var newtestQuestions = new List<TestQuestion>();
                    foreach (var questionId in model.QuestionIds.Split(','))
                    {
                        var Qid = Convert.ToInt32(questionId);

                        var testQ = new TestQuestion()
                        {
                            QuestionId = Qid,
                            TestId = test.Id
                        };
                        newtestQuestions.Add(testQ);
                    }
                    await testQuestionRepository.AddManyAsync(newtestQuestions);
                }



                await unitOfWork.SaveChangesAsync();
                return BaseModel.Succeeded("Test Updated Successfully!");


            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        public PartialViewResult GetSetWithQuestions(int attributeId, string attributeType)
        {
            var model = GetSetsWithQuestions(attributeId, attributeType);
            return PartialView("_SetsListWithQuestions", model);
        }
        private List<SetWithQuestionsViewModel> GetSetsWithQuestions(int attributeId, string attributeType)
        {
            var setWithQuestions = setRepository.GetSetWithQuestions(attributeId, attributeType);
            var model = Mapper.Map<List<SetWithQuestionsViewModel>>(setWithQuestions);
            return model;
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<PartialViewResult> Edit(int id)
        {
            var test = await testRepository.GetTestWithQuestions(id);

            var ids = string.Join(',', test.TestQuestions.Select(a => a.QuestionId.ToString()));
            var model = Mapper.Map<TestViewModel>(test);
            model.QuestionIds = ids;
            return PartialView("_EditTest", model);
        }
        [Authorize(Roles = "Admin")]
        public async Task<PartialViewResult> View(int id)
        {
            var test = await testRepository.GetTestWithQuestions(id);

            var model = Mapper.Map<TestViewModel>(test);
            var questions = test.TestQuestions.Select(a => a.Question).ToList();
            var setWithQuestionsModel = setRepository.GetSetWithQuestionsByTestId(Convert.ToInt32(test.AttributeId), test.AttributeType, id);
            var setWithQuestionsViewModel = Mapper.Map<List<SetWithQuestionsViewModel>>(setWithQuestionsModel);

            model.SetWithQuestionsViewModel = setWithQuestionsViewModel;
            return PartialView("_ViewTest", model);
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> AddQuestion(QuestionViewModel model)
        {
            try
            {

                var question = new Question()
                {
                    Date = DateTime.Now,
                    Detail = model.QuestionDetail,
                    QuestionDifficultyLevelId = model.QuestionDifficultyLevelId,
                    QustionTypeId = model.QustionTypeId,
                    QuestionTitle = model.QuestionTitle,
                    QuestionCategoryId = Convert.ToInt32(QuestionCategories.SetQuestion)
                };

                await questionRepository.AddAsync(question);


                var answer = new CorrectAnswer()
                {
                    Answer = model.AnswerDetail,
                    Qustion = question
                };
                await correctAnswerRepository.AddAsync(answer);


                var setQuestion = new SetQuestion()
                {
                    SetId = model.SetId,
                    Question = question
                };

                await setQuestionRepository.AddAsync(setQuestion);
                await unitOfWork.SaveChangesAsync();
                return BaseModel.Succeeded("Question Added Successfully!");
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> UpdateQuestion(QuestionViewModel model)
        {
            try
            {

                var question = questionRepository.Get().FirstOrDefault(a => a.Id == Convert.ToInt32(model.Id));
                question.Detail = model.QuestionDetail;
                question.LastUpdated = DateTime.Now;
                question.QuestionDifficultyLevelId = model.QuestionDifficultyLevelId;
                question.QustionTypeId = model.QustionTypeId;
                question.QuestionTitle = model.QuestionTitle;

                await questionRepository.UpdateAsync(question);

                var answer = correctAnswerRepository.Get().FirstOrDefault(a => a.QuestionId == Convert.ToInt32(model.Id));
                answer.Answer = model.AnswerDetail;

                await correctAnswerRepository.UpdateAsync(answer);

                await unitOfWork.SaveChangesAsync();

                return new BaseModel() { Success = true, Message = "Question Updated Successfully!" };

            }
            catch (Exception ex)
            {

                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> RemoveQuestion(int id)
        {
            try
            {
                var question = questionRepository.Get().Where(a => a.Id == id);

                if (question != null)
                {

                    var setQuestion = setQuestionRepository.Get().FirstOrDefault(a => a.QuestionId == id);
                    if (setQuestion != null)
                    {
                        await setQuestionRepository.DeleteAsync(setQuestion);
                    }

                    await setQuestionRepository.DeleteAsync(setQuestion);
                    await unitOfWork.SaveChangesAsync();

                    return new BaseModel() { Success = true, Message = "Question Removed Successfully!" };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "No Question exist or it is Removed, refresh your page please!!" };
                }
            }
            catch (Exception ex)
            {

                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }


    }
}