﻿using AutoMapper;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Management.Filters;
using Management.Infrastructure.Communication;
using Management.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class BatchController : Controller
    {
        private readonly UserManager<User> userManager;

        private readonly IUnitOfWork unitOfWork;
        private IHostingEnvironment env;
        private readonly IChapterRepository chapterRepository;
        private readonly IEducationLevelRepository educationLevelRepository;

        private readonly IQuestionRepository questionRepository;
        private readonly ICorrectAnswerRepository correctAnswerRepository;
        private readonly IAnswerRepository answerRepository;
        private readonly IEmailService emailService;
        private readonly IQuestionTypeRepository questionTypeRepository;
        private readonly ISetRepository setRepository;
        private readonly ISectionRepository sectionRepository;
        private readonly ISubSectionRepository subSectionRepository;
        private readonly ISetQuestionRepository setQuestionRepository;
        private readonly IBatchRepository batchRepository;
        private readonly IUserBatchRepository userBatchRepository;
        private readonly ITestCreatedFromRepository testCreatedFromRepository;
        private readonly ITestQuestionRepository testQuestionRepository;
        private readonly IQuestionDifficultyLevelRepository questionDifficultyLevelRepository;
        private readonly IUserQuestionAttemptRepository userQuestionAttemptRepository;
        private readonly IUserArtifactRepository userArtifactRepository;
        private readonly IMTQAnsweredChoiceRepository mTQAnsweredChoiceRepository;
        private readonly IUserSubjectRepository userSubjectRepository;
        private readonly IUserTestAttemptRepository userTestAttemptRepository;
        private readonly IScheduleRepository scheduleRepository;
        private readonly IBatchTestRepository batchTestRepository;
        private readonly IStudentStudyTypeRepository studentStudyTypeRepository;
        private readonly IUserBatchReferralRepository userBatchReferralRepository;
        private readonly ITestRepository testRepository;
        public BatchController(IUserBatchReferralRepository _userBatchReferralRepository, ITestRepository _testRepository, IStudentStudyTypeRepository _studentStudyTypeRepository,
            IBatchTestRepository _batchTestRepository, IUserTestAttemptRepository _userTestAttemptRepository,
            IScheduleRepository _scheduleRepository, IUserSubjectRepository _userSubjectRepository, IUserArtifactRepository _userArtifactRepository, IMTQAnsweredChoiceRepository _mTQAnsweredChoiceRepository, IAnswerRepository _answerRepository, IUserBatchRepository _userBatchRepository, IUserQuestionAttemptRepository _userQuestionAttemptRepository, ISubSectionRepository _subSectionRepository, ISectionRepository _sectionRepository, ITestCreatedFromRepository _testCreatedFromRepository, UserManager<User> _userManager, IChapterRepository _chapterRepository, IEducationLevelRepository _educationLevelRepository, ITestQuestionRepository _testQuestionRepository, IBatchRepository _batchRepository, ISetQuestionRepository _setQuestionRepository, ISetRepository _setRepository, IQuestionDifficultyLevelRepository _questionDifficultyLevelRepository, IQuestionTypeRepository _questionTypeRepository, ICorrectAnswerRepository _correctAnswerRepository, IQuestionRepository _questionRepository, IEmailService _emailService, IHostingEnvironment _env, IUnitOfWork _unitOfWork)
        {

            unitOfWork = _unitOfWork;
            userManager = _userManager;

            emailService = _emailService;
            userArtifactRepository = _userArtifactRepository;

            correctAnswerRepository = _correctAnswerRepository;
            questionRepository = _questionRepository;
            setRepository = _setRepository;
            setQuestionRepository = _setQuestionRepository;
            batchRepository = _batchRepository;
            testQuestionRepository = _testQuestionRepository;
            userBatchRepository = _userBatchRepository;
            userQuestionAttemptRepository = _userQuestionAttemptRepository;
            questionTypeRepository = _questionTypeRepository;
            questionDifficultyLevelRepository = _questionDifficultyLevelRepository;
            educationLevelRepository = _educationLevelRepository;
            chapterRepository = _chapterRepository;
            env = _env;
            testCreatedFromRepository = _testCreatedFromRepository;
            subSectionRepository = _subSectionRepository;
            sectionRepository = _sectionRepository;
            answerRepository = _answerRepository;
            mTQAnsweredChoiceRepository = _mTQAnsweredChoiceRepository;
            userSubjectRepository = _userSubjectRepository;
            scheduleRepository = _scheduleRepository;
            userTestAttemptRepository = _userTestAttemptRepository;
            batchTestRepository = _batchTestRepository;
            testRepository = _testRepository;
            userBatchReferralRepository = _userBatchReferralRepository;
            studentStudyTypeRepository = _studentStudyTypeRepository;
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Teacher, Student")]
        public IActionResult Schedule(int id)
        {
            try
            {
                var batch = batchRepository.Get().Include(x => x.Subject).Include("BatchUsers").Include("BatchUsers.User").Include("BatchUsers.User.ProfilePicture.Artifact").FirstOrDefault(a => a.Id == id);
                ViewBag.Batchs = Mapper.Map<BatchViewModel>(batch);
                return View();
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<BaseModel> SetSchedule(ScheduleViewModel model)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var existSchedule = scheduleRepository.Get().Include("Batch").Include("Batch.Subject").Include("Batch.BatchUsers").Include("Batch.BatchUsers.User")
                    .Where(a => a.BatchId == model.BatchId
                    && a.StartDateTime == model.StartDateTime
                    && a.UserId == loggedUser.Id);

                if (!existSchedule.Any())
                {
                    var Teachers = await userManager.GetUsersInRoleAsync("Teacher");
                    var batchTeacher = userBatchRepository.Get().Include("User").Include("Batch").FirstOrDefault(a => a.BatchId == model.BatchId && Teachers.Select(x => x.Id).Contains(a.UserId)).User;

                    var savemodel = Mapper.Map<Schedule>(model);
                    savemodel.UserId = loggedUser.Id;
                    savemodel.ScheduleType = Convert.ToInt32(ScheduleTypes.OnlineClass);
                    savemodel.LabelCategory = "bg-primary";

                    batchTeacher.ZoomPassword = model.Password;
                    batchTeacher.ZoomUserName = model.UserName;
                    batchTeacher.ZoomUrl = model.EventUrl;

                    await userManager.UpdateAsync(loggedUser);
                    await scheduleRepository.AddAsync(savemodel);
                    await unitOfWork.SaveChangesAsync();


                    var batchUsers = userBatchRepository.Get().Where(a => a.BatchId == model.BatchId);
                    var testOnlybatchUsers = studentStudyTypeRepository.Get().Where(a => batchUsers.Select(x => x.UserId).Contains(a.StudentId));
                    var noTestOnlybatchUsers = batchUsers.Where(a => !testOnlybatchUsers.Select(x => x.StudentId).Contains(a.UserId));

                    var mailModel = new InformAllocatedMemberModel()
                    {
                        Users = noTestOnlybatchUsers.Select(a => a.User).ToList(),
                        Schedule = existSchedule.FirstOrDefault()
                    };

                    emailService.OnlineClassScheduled(mailModel);

                    return new BaseModel { Success = true, Message = "Scheduled Successfully!!", Data = new { id = savemodel.Id } };
                }
                else
                {
                    return BaseModel.Failed("Already scheduled on this date and time.!!");
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);

            }
        }
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<BaseModel> UpdateSchedule(ScheduleViewModel model)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var alreadExist = scheduleRepository.Get()
                    .FirstOrDefault(a => a.Id == model.Id);

                if (alreadExist != null)
                {
                    alreadExist.Title = model.Title;
                    alreadExist.EventUrl = model.EventUrl;

                    var Teachers = await userManager.GetUsersInRoleAsync("Teacher");
                    var batchTeacher = userBatchRepository.Get().Include("User").Include("Batch").FirstOrDefault(a => a.BatchId == model.BatchId && Teachers.Select(x => x.Id).Contains(a.UserId)).User;

                    batchTeacher.ZoomPassword = model.Password;
                    batchTeacher.ZoomUserName = model.UserName;
                    batchTeacher.ZoomUrl = model.EventUrl;

                    await userManager.UpdateAsync(loggedUser);
                    await scheduleRepository.UpdateAsync(alreadExist);
                    await unitOfWork.SaveChangesAsync();
                    return BaseModel.Succeeded("Updated Successfully!!");
                }
                else
                {
                    return BaseModel.Failed("No Schedule found!!");
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);

            }
        }
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<BaseModel> DeleteSchedule(int id)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var alreadExist = scheduleRepository.Get()
                    .FirstOrDefault(a => a.Id == id);

                if (alreadExist != null)
                {
                    await scheduleRepository.DeleteAsync(alreadExist);
                    await unitOfWork.SaveChangesAsync();
                    return BaseModel.Succeeded("Deleted Successfully!!");
                }
                else
                {
                    return BaseModel.Failed("No Schedule found!!");
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);

            }
        }
        [Authorize(Roles = "Admin,Teacher,Student")]
        public async Task<BaseModel> GetSchedulesByBatchId(int batchId)
        {
            try
            {

                var schedule = scheduleRepository.Get().Where(a => a.BatchId == batchId).ToList();
                var model = Mapper.Map<IList<ScheduleViewModel>>(schedule).ToAsyncEnumerable();
                if (schedule.Any())
                {
                    var Teachers = await userManager.GetUsersInRoleAsync("Teacher");
                    var batchTeacher = userBatchRepository.Get().Include("User").Include("Batch").FirstOrDefault(a => a.BatchId == schedule.FirstOrDefault().BatchId && Teachers.Select(x => x.Id).Contains(a.UserId)).User;
                    await model.ForEachAsync(a => a.UserName = batchTeacher.ZoomUserName);
                    await model.ForEachAsync(a => a.Password = batchTeacher.ZoomPassword);
                    await model.ForEachAsync(a => a.IsTest = a.ScheduleType == Convert.ToInt32(ScheduleTypes.Test));
                }

                return new BaseModel()
                {
                    Data = model.ToList()
                };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);

            }
        }

        [Authorize(Roles = "Admin,Teacher,Student")]
        public async Task<PartialViewResult> GetScheduleDetails(int id)
        {
            var loggedUser = await userManager.GetUserAsync(User);
            var schedule = await scheduleRepository.Get().FirstOrDefaultAsync(a => a.Id == id);
            var model = Mapper.Map<ScheduleViewModel>(schedule);
            var isBatchUser = await userBatchRepository.Get().AnyAsync(a => a.UserId == loggedUser.Id && a.BatchId == schedule.BatchId);
            model.IsBatchUser = isBatchUser;
            return PartialView("_ScheduleDetail", model);
        }

        [Authorize(Roles = "Admin, Teacher, Student")]
        public async Task<IActionResult> List()
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var dataModel = new List<Batch>();
                if (isAdmin)
                {
                    dataModel = batchRepository.Get().Include(x => x.Subject).Include(x => x.BatchUsers).Include("BatchUsers.User").ToList();
                }
                else
                {
                    dataModel = userBatchRepository.Get().Where(a => a.UserId == loggedUser.Id && a.IsActive).Include(x => x.Batch).Include("Batch.Subject").Include("Batch.BatchUsers").Include("Batch.BatchUsers.User").AsEnumerable().Select(x => x.Batch).ToList();
                }
                var model = Mapper.Map<List<BatchViewModel>>(dataModel);
                foreach (var item in model)
                {
                    await item.BatchUsers.ToAsyncEnumerable().ForEachAsync(a => a.IsTeacher = userManager.IsInRoleAsync(Mapper.Map<User>(a.User), UserRoles.Teacher.ToString()).Result);
                }
                return View(model);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin, Teacher, Student")]
        public async Task<IActionResult> GetNewBatches()
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var dataModel = new List<Batch>();

                dataModel = batchRepository.Get().Where(a => a.StartDate > DateTime.Now).Include(x => x.Subject).Include("BatchUsers.User").ToList();

                var model = Mapper.Map<List<BatchViewModel>>(dataModel);
                foreach (var item in model)
                {
                    await item.BatchUsers.ToAsyncEnumerable().ForEachAsync(a => a.IsTeacher = userManager.IsInRoleAsync(Mapper.Map<User>(a.User), UserRoles.Teacher.ToString()).Result);
                    await item.BatchUsers.ToAsyncEnumerable()
                        .ForEachAsync(a => a.User.ProfilePictureUrl = userArtifactRepository.Get().Include(c => c.Artifact).FirstOrDefault(x => x.UserId == a.UserId) != null ? userArtifactRepository.Get().Include(c => c.Artifact).FirstOrDefault(x => x.UserId == a.UserId).Artifact.FileName : string.Empty);
                }

                return PartialView("_NewBatches", model);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Admin, Teacher, Student")]
        public async Task<IActionResult> GetReferredBatches()
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                //var dataModel = new List<Batch>();
                var refBatches = userBatchReferralRepository.Get().Where(a => a.ReferringUserEmail == loggedUser.Email).Include(x => x.Batch).Include(x => x.Batch.Subject).Include("BatchUsers.User").ToList();
                //dataModel = batchRepository.Get().Where(a => a.StartDate > DateTime.Now).Include(x => x.Subject).Include("BatchUsers.User").ToList();

                var refmodel = Mapper.Map<List<UserBatchReferralViewModel>>(refBatches);
                //var model = Mapper.Map<List<BatchViewModel>>(dataModel);
                foreach (var item in refmodel.Select(a=>a.Batch))
                {
                    await item.BatchUsers.ToAsyncEnumerable().ForEachAsync(a => a.IsTeacher = userManager.IsInRoleAsync(Mapper.Map<User>(a.User), UserRoles.Teacher.ToString()).Result);
                    await item.BatchUsers.ToAsyncEnumerable()
                        .ForEachAsync(a => a.User.ProfilePictureUrl = userArtifactRepository.Get().Include(c => c.Artifact).FirstOrDefault(x => x.UserId == a.UserId) != null ? userArtifactRepository.Get().Include(c => c.Artifact).FirstOrDefault(x => x.UserId == a.UserId).Artifact.FileName : string.Empty);
                }

                return PartialView("_ReferralBatches", refmodel);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin")]
        public PartialViewResult GetList()
        {
            try
            {
                var listOfBatches = batchRepository.Get().Include(x => x.Subject).ToList();
                var model = Mapper.Map<List<BatchViewModel>>(listOfBatches);
                return PartialView("_AllBatches", model);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Student([FromQuery(Name = "bid")] int batchId, [FromQuery(Name = "uid")] string userId)
        {
            try
            {
                var isUserInBatch = userBatchRepository.Get().Any(a => a.BatchId == batchId && a.UserId == userId);
                if (isUserInBatch)
                {
                    var student = await userManager.FindByIdAsync(userId);
                    var userArtifact = userArtifactRepository.Get().Include("Artifact").FirstOrDefault(x => x.UserId == student.Id);
                    var profilePic = userArtifact != null ? userArtifact.Artifact.FileName : "";
                    var studentStudyType = await studentStudyTypeRepository.Get().FirstOrDefaultAsync(a => a.BatchId == batchId && a.StudentId == userId);
                    var model = Mapper.Map<UserViewModel>(student);
                    model.StudentStudyTypeId = studentStudyType.StudyTypeId;

                    model.ProfilePictureUrl = profilePic;
                    var batchTests = batchTestRepository.Get().Include(aa => aa.Test).Where(a => a.BatchId == batchId).ToList();
                    ViewBag.BatchTest = batchTests;
                    var userTestAttempted = userTestAttemptRepository.Get().Include(a => a.Test).Where(a => batchTests.Select(c => c.Test.Id).Contains(a.TestId) && a.UserId == userId && a.IsFinished).ToList();
                    ViewBag.UserTestAttempted = userTestAttempted;
                    return View(model);
                }
                else
                {
                    return new NotFoundViewResult();
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Test([FromQuery(Name = "id")] int testId, [FromQuery(Name = "uid")] string userId)
        {
            try
            {

                var test = await testRepository.GetInProgressTest(testId, userId);
                if (test != null)
                {
                    var model = Mapper.Map<TestViewModel>(test);
                    return View("Result", model);
                }
                else
                {
                    return new NotAllowedViewResult();
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Teacher")]
        public async Task<PartialViewResult> ViewTest(int testId, string userId)
        {
            try
            {

                var test = await testRepository.GetInProgressTest(testId, userId);
                if (test != null)
                {
                    var model = Mapper.Map<TestViewModel>(test);
                    return PartialView("_ResultView", model);
                }
                else
                {
                    return new NotAllowedViewResult();
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Teacher, Student")]
        public async Task<IActionResult> Detail(int id)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var isAllocated = userBatchRepository.Get().Any(x => x.BatchId == id && x.UserId == loggedUser.Id);
                if (isAllocated || isAdmin)
                {
                    var batch = batchRepository.Get().Include(x => x.Subject).Include(b => b.BatchTests).Include("BatchTests.Test").Include("BatchUsers").Include("BatchUsers.User").Include("BatchUsers.User.ProfilePicture.Artifact").FirstOrDefault(a => a.Id == id);

                    var model = Mapper.Map<BatchViewModel>(batch);
                    await model.BatchUsers.ToAsyncEnumerable().ForEachAsync(a => a.IsTeacher = userManager.IsInRoleAsync(Mapper.Map<User>(a.User), UserRoles.Teacher.ToString()).Result);
                    await model.BatchUsers.ToAsyncEnumerable().Where(x => !x.IsTeacher).ForEachAsync(a => a.User.StudentStudyTypeId = studentStudyTypeRepository.Get().FirstOrDefaultAsync(c => c.BatchId == id && c.StudentId == a.UserId).Result.StudyTypeId);

                    var attemptedTests = userTestAttemptRepository.Get().Include(x => x.Test).Include(x => x.User).Where(c => c.UserId == loggedUser.Id).ToList();
                    ViewBag.AttemptedTests = attemptedTests;

                    return View(model);
                }
                else
                {
                    return new NotAllowedViewResult();
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseModel> Create(BatchViewModel model)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);

                var batch = Mapper.Map<Batch>(model);
                batch.CreatedDate = DateTime.Now;

                await batchRepository.AddAsync(batch);
                await unitOfWork.SaveChangesAsync();
                return new BaseModel()
                {
                    Success = true,
                    Message = "Batch Created Successfully!"
                };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseModel> AssignBatchTeacher(BatchViewModel model)
        {
            try
            {

                var loggedUser = await userManager.GetUserAsync(User);
                var userBatchExist = userBatchRepository.Get().FirstOrDefault(a => a.UserId == model.BatchTeacherId && a.BatchId == model.Id);
                if (userBatchExist == null)
                {

                    var userBatch = new UserBatch()
                    {
                        BatchId = model.Id,
                        IsActive = true,
                        UserId = model.BatchTeacherId
                    };
                    await userBatchRepository.AddAsync(userBatch);

                    var batch = batchRepository.Get().FirstOrDefault(a => a.Id == model.Id);
                    var isUserSubjectExist = userSubjectRepository.Get().Any(a => a.SubjectId == batch.SubjectId && a.UserId == model.BatchTeacherId);
                    if (!isUserSubjectExist)
                    {

                        var teacherSubject = new UserSubject()
                        {
                            SubjectId = (int)batch.SubjectId,
                            IsActive = true,
                            UserId = model.BatchTeacherId,
                        };

                        await userSubjectRepository.AddAsync(teacherSubject);
                    }
                    await unitOfWork.SaveChangesAsync();
                    return new BaseModel()
                    {
                        Success = true,
                        Message = "Teacher Assigned Successfully!"
                    };
                }
                else
                {
                    return new BaseModel()
                    {
                        Success = false,
                        Message = "Sorry you can not assign more then one teacher, refresh your page please!"
                    };
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<PartialViewResult> GetSelectableTeachers()
        {
            var Teachers = await userManager.GetUsersInRoleAsync("Teacher");
            var userVM = Mapper.Map<List<UserViewModel>>(Teachers.Where(a => a.IsDeleted == false));
            var userArtifacts = userArtifactRepository.Get().Where(x => userVM.Select(a => a.Id).Contains(x.UserId)).Include(a => a.Artifact);
            foreach (var user in userVM)
            {
                var userArtifact = userArtifacts.FirstOrDefault(x => x.UserId == user.Id);
                var profilePic = userArtifact != null ? userArtifact.Artifact.FileName : "";
                user.ProfilePictureUrl = profilePic;
            }
            var model = new BatchViewModel()
            {
                ListBatchUsers = userVM
            };
            return PartialView("_AssignBatchTeacherForm", model);
        }
        [Authorize(Roles = "Admin")]
        public PartialViewResult Get(int id)
        {
            try
            {
                var listOfBatches = batchRepository.Get().Where(x => x.Id == id).ToList();
                var model = Mapper.Map<List<BatchViewModel>>(listOfBatches);
                return PartialView("_AllBatches", model);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Student")]
        public PartialViewResult GetBatchByTeacherId(string id, int subjectId)
        {
            try
            {
                var listOfBatches = userBatchRepository.Get().Where(x => x.UserId == id).Select(a => a.Batch).Where(a => a.SubjectId == subjectId).ToList();
                var model = Mapper.Map<List<BatchViewModel>>(listOfBatches);
                return PartialView("~/Views/Teacher/_TeacherBatches.cshtml", model);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin")]
        public List<BatchViewModel> GetAll()
        {
            try
            {
                var listOfBatches = batchRepository.Get().ToList();
                var model = Mapper.Map<List<BatchViewModel>>(listOfBatches);
                return model.OrderByDescending(a => a.CreatedDate).ToList();
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }


        [Authorize(Roles = "Admin")]
        public async Task<BaseModel> Delete(int id)
        {
            try
            {
                var batch = batchRepository.Get().FirstOrDefault(a => a.Id == id);

                if (batch != null)
                {

                    await batchRepository.DeleteAsync(batch);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseModel() { Success = true, Message = "Batch Removed Successfully!" };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "No Batch Exist or it is Removed, refresh your page please!!" };
                }
            }
            catch (Exception ex)
            {

                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin")]
        public async Task<BaseModel> UnassignBatchTeacher(BatchViewModel model)
        {
            try
            {
                var userbatch = userBatchRepository.Get().Include(x => x.Batch).FirstOrDefault(a => a.BatchId == model.Id && a.UserId == model.BatchTeacherId);
                var userSubject = userSubjectRepository.Get().FirstOrDefault(a => a.SubjectId == userbatch.Batch.SubjectId && a.UserId == model.BatchTeacherId);

                if (userbatch != null)
                {

                    await userBatchRepository.DeleteAsync(userbatch);
                    await unitOfWork.SaveChangesAsync();

                    await userSubjectRepository.DeleteAsync(userSubject);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseModel() { Success = true, Message = "Teacher Unassigned from Batch Successfully!" };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "No Teacher Exist or it is Removed, refresh your page please!!" };
                }
            }
            catch (Exception ex)
            {

                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin")]
        public async Task<BaseModel> Update(BatchViewModel model)
        {
            try
            {
                var batch = batchRepository.Get().FirstOrDefault(x => x.Id == model.Id);

                batch.Title = model.Title;
                batch.StartDate = model.StartDate;
                batch.EndDate = model.EndDate;

                await batchRepository.UpdateAsync(batch);
                await unitOfWork.SaveChangesAsync();
                return BaseModel.Succeeded("Batch Updated Successfully!");


            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<PartialViewResult> Edit(int id)
        {
            var batch = await batchRepository.Get().FirstOrDefaultAsync(a => a.Id == id);
            var model = Mapper.Map<BatchViewModel>(batch);
            return PartialView("_EditBatch", model);
        }


    }
}