﻿using AutoMapper;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Management.Infrastructure.Communication;
using Management.Models;
using Management.ViewModels;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Management.Controllers
{
  
    public class WebsiteController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IEducationLevelRepository educationLevelRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IBoardLevelRepository boardLevelRepository;
        private readonly IClassRepository classRepository;
        private readonly ISubSectionRepository subSectionRepository;
        private readonly ISectionRepository sectionRepository;
        private readonly IUserSubjectRepository userSubjectRepository;
        private readonly ISubjectRepository subjectRepository;
        private readonly IEmailService emailService;
        private readonly IChapterRepository chapterRepository;
        private readonly IArtifactRepository artifactRepository;
        private IHostingEnvironment _env;
        private readonly ISetRepository setRepository;
        private readonly ISetQuestionRepository setQuestionRepository;
        private readonly IUserArtifactRepository userArtifactRepository;
        private readonly ISubjectDemoChapterRepository subjectDemoChapterRepository;
        public WebsiteController(ISubjectDemoChapterRepository _subjectDemoChapterRepository, IUserArtifactRepository _userArtifactRepository, ISetRepository _setRepository, IUserSubjectRepository _userSubjectRepository, ISetQuestionRepository _setQuestionRepository, IEmailService _emailService, IHostingEnvironment env, UserManager<User> _userManager, IArtifactRepository _artifactRepository, IChapterRepository _chapterRepository, ISubSectionRepository _subSectionRepository, ISectionRepository _sectionRepository, ISubjectRepository _subjectRepository, IClassRepository _classRepositoryy, IEducationLevelRepository _educationLevelRepository, IBoardLevelRepository _boardLevelRepository, IUnitOfWork _unitOfWork)
        {
            boardLevelRepository = _boardLevelRepository;
            educationLevelRepository = _educationLevelRepository;
            classRepository = _classRepositoryy;
            subjectRepository = _subjectRepository;
            sectionRepository = _sectionRepository;
            subSectionRepository = _subSectionRepository;
            userSubjectRepository = _userSubjectRepository;
            chapterRepository = _chapterRepository;
            emailService = _emailService;
            subjectDemoChapterRepository = _subjectDemoChapterRepository;
        }



        
        [EnableCors("CorsPolicy")]
        public BaseModel ContactUs(string Body, string UserEmail, string UserName)
        {
            try
            {
                var model = new Mail()
                {
                    Body = Body,
                    UserEmail = UserEmail,
                    UserName = UserName,
                    IsTeacher = false
                };
                emailService.ContactUs(model);

                return BaseModel.Succeeded(" Thanks for contacting us, we will be in touch with you soon.");
            }
            catch (System.Exception ex)
            {
                emailService.SendException("Exception thrown while contacting us.<br/><br/> " + ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);

                return BaseModel.Failed("Message not sent due to some unknown reason, Try again later, Thanks");

            }

        }
        

        public BaseModel SubjectExpertContactUs(string Expertise, string UserEmail, string UserName, string Mobile, string Subject, string School)
        {
            try
            {
                var model = new Mail()
                {
                    Body = Expertise,
                    UserEmail = UserEmail,
                    UserName = UserName,
                    Subject=Subject,
                    Mobile=Mobile,
                    School=School,
                    IsTeacher=true
                };
                emailService.SubjectExpertContactUs(model);

                return BaseModel.Succeeded(" Thanks for showing your, we will be in touch with you soon.");
            }
            catch (System.Exception ex)
            {
                emailService.SendException("Exception thrown while contacting us.<br/><br/> " + ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);

                return BaseModel.Failed("Message not sent due to some unknown reason, Try again later, Thanks");

            }

        }
        #region Retrieve Attributes

        
        public CourseViewModel RetrieveEducationLevels()
        {
            var model = educationLevelRepository.Get().ToList();

            var newmodel = new CourseViewModel()
            {
                Courses = model.ToList<object>(),
                CoursesType = AttributeType.BoardLevel

            };
            return newmodel;
        }

        
        public CourseViewModel RetrieveBoardLevel(int parentId)
        {
            List<BoardLevel> model;
            if (parentId == 0)
                model = boardLevelRepository.Get().ToList();
            else
                model = boardLevelRepository.Get().Where(a => a.EducationLevelId == parentId).ToList();

            var newmodel = new CourseViewModel()
            {
                Courses = model.ToList<object>(),
                CoursesType = AttributeType.Subject

            };
            return newmodel;

        }
        
        public CourseViewModel RetrieveClass(int parentId)
        {

            var model = classRepository.Get().Where(a => a.BoardLevelId == parentId).ToList();

            var newmodel = new CourseViewModel()
            {
                Courses = model.ToList<object>(),
                CoursesType = AttributeType.Subject

            };
            return newmodel;

        }


        
        public CourseViewModel RetrieveSubject(int parentId)
        {

            var model = subjectRepository.GetWithDemoChapter(parentId).ToList();

            var newmodel = new CourseViewModel()
            {
                Courses = model.ToList<object>(),
                CoursesType = null

            };
            return newmodel;

        }
        
        public CourseViewModel RetrieveChapter(int parentId)
        {

            var model = chapterRepository.Get().Where(a => a.SubjectId == parentId).ToList();
            var newmodel = new CourseViewModel()
            {
                Courses = model.ToList<object>(),
                CoursesType = null

            };
            return newmodel;
        }
        
        public List<Section> RetrieveSection(int parentId)
        {

            var model = sectionRepository.Get().Where(a => a.ChapterId == parentId).ToList();
            return model;
        }
        

        public List<SubSection> RetrieveSubSection(int parentId)
        {

            var model = subSectionRepository.Get().Where(a => a.SectionId == parentId).ToList();
            return model;

        }

        #endregion
        
        public AttributeViewModel LoadChapter(int id)
        {
            AttributeViewModel newmodel;
            var model = subjectDemoChapterRepository.GetWithChilds(id);
            if (model != null)
            {
                var nonDemoChapters = chapterRepository.Get().Where(a => a.SubjectId == model.SubjectId && a.Id != model.ChapterId).ToList();
                var otherNonDemoChapters = Mapper.Map<List<ChapterViewModel>>(nonDemoChapters);
                var sectionsModel = sectionRepository.Get().Where(a => a.ChapterId == model.ChapterId);
                var subject = subjectRepository.Get().FirstOrDefault(a => a.Id == model.SubjectId);
                var sections = Mapper.Map<List<AttributeViewModel>>(sectionsModel);
                newmodel = Mapper.Map<AttributeViewModel>(model.Chapter);
                newmodel.ParentName = subject.Name;
                newmodel.Childs = sections;
                newmodel.OtherNonChapters = otherNonDemoChapters;
                newmodel.IsDemoChapter = true;
                
                return newmodel;
            }
            else
            {
                newmodel = new AttributeViewModel()
                {
                    IsDemoChapter = false

                };
                return newmodel;

            }


        }
        
        public AttributeViewModel LoadSection(int id)
        {
            AttributeViewModel newmodel;
            var section = sectionRepository.Get().FirstOrDefault(x=>x.Id== id);
            var model = subjectDemoChapterRepository.GetWithChilds(section.ChapterId);
            if (model != null)
            {
                var nonDemoChapters = chapterRepository.Get().Where(a => a.SubjectId == model.SubjectId && a.Id != model.ChapterId).ToList();
                var otherNonDemoChapters = Mapper.Map<List<ChapterViewModel>>(nonDemoChapters);
                var subsectionsModel = subSectionRepository.Get().Where(a => a.SectionId == section.Id);
                var subsections = Mapper.Map<List<AttributeViewModel>>(subsectionsModel);
                newmodel = Mapper.Map<AttributeViewModel>(section);
                newmodel.ParentName = model.Chapter.Name;
                newmodel.Childs = subsections;
                newmodel.OtherNonChapters = otherNonDemoChapters;
                newmodel.IsDemoChapter = true;

                return newmodel;
            }
            else
            {
                newmodel = new AttributeViewModel()
                {
                    IsDemoChapter = false

                };
                return newmodel;

            }


        }
        
        public AttributeViewModel LoadSubSection(int id)
        {
            AttributeViewModel newmodel;
            var subsection = subSectionRepository.GetWithChilds(id);

            var model = subjectDemoChapterRepository.GetWithChilds(subsection.Section.ChapterId);
            if (model != null)
            {
                var section = sectionRepository.Get().FirstOrDefault(a => a.Id == subsection.SectionId);
                var nonDemoChapters = chapterRepository.Get().Where(a => a.SubjectId == model.SubjectId && a.Id!=model.ChapterId).ToList();
                var otherNonDemoChapters = Mapper.Map<List<ChapterViewModel>>(nonDemoChapters);
                newmodel = Mapper.Map<AttributeViewModel>(subsection);
                newmodel.ParentName = section.Name;
                newmodel.OtherNonChapters = otherNonDemoChapters;
                newmodel.IsDemoChapter = true;

                return newmodel;
            }
            else
            {
                newmodel = new AttributeViewModel()
                {
                    IsDemoChapter = false

                };
                return newmodel;

            }


        }
    }
}