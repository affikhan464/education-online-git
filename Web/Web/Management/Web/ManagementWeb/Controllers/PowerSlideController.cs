﻿using AutoMapper;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Infrastructure.Communication;
using Management.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class PowerSlideController : Controller
    {

        private readonly IUnitOfWork unitOfWork;
        private IHostingEnvironment env;
        
        private readonly IEmailService emailService;
        private readonly IPowerSlideRepository powerSlideRepository;
        public PowerSlideController(IPowerSlideRepository _powerSlideRepository, IEmailService _emailService, IHostingEnvironment _env, IUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
            emailService = _emailService;
            powerSlideRepository = _powerSlideRepository;
            env = _env;
        }

        [Authorize(Roles = "Admin,Researcher,Student,Teacher")]
        public PartialViewResult Get(PowerSlideViewModel model)
        {
            try
            {
                var listOfPowerSlides = powerSlideRepository.Get().Where(x => x.AttributeId == model.AttributeId && x.AttributeType == model.AttributeType).ToList();
                var powerSlides = Mapper.Map<List<PowerSlideViewModel>>(listOfPowerSlides);

                return PartialView("_ListOfPowerSlides", powerSlides);
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Researcher,Teacher")]
        [HttpPost]
        public async Task<BaseModel> Add(PowerSlideViewModel model)
        {
            try
            {
                var addingModel = new PowerSlide()
                {
                    Title = model.Title,
                    Detail=model.Detail,
                    AttributeType = model.AttributeType,
                    AttributeId = model.AttributeId,
                    LastUpdated = DateTime.Now,
                    Date = DateTime.Now
                };

                await powerSlideRepository.AddAsync(addingModel);
                await unitOfWork.SaveChangesAsync();
                return BaseModel.Succeeded("Power Slide Added Successfully!");
                
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseModel> Delete(int id)
        {
            try
            {
                var powerSlide = powerSlideRepository.Get().FirstOrDefault(a => a.Id == id);
                if (powerSlide != null)
                {                    
                    await powerSlideRepository.DeleteAsync(powerSlide);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseModel() { Success = true, Message = "Power Slide Removed Successfully!" };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "No Power Slide Exist or it is Removed, refresh your page please!!" };
                }
            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseModel> Update(PowerSlideViewModel model)
        {
            try
            {
                var powerSlide = powerSlideRepository.Get().FirstOrDefault(a => a.Id == Convert.ToInt32(model.Id));

                if (powerSlide != null)
                {
                    powerSlide.Title = model.Title;
                    powerSlide.Detail = model.Detail;
                    powerSlide.LastUpdated = DateTime.Now;

                    await powerSlideRepository.UpdateAsync(powerSlide);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseModel() { Success = true, Message = "Power Slide Updated Successfully!" };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "This Power Slide is not exist or may be deleted, kindly refesh your page!" };
                }

            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }

        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public PartialViewResult Edit(int id)
        {
            var powerSlide =  powerSlideRepository.Get().FirstOrDefault(x=>x.Id==id);
            var model = Mapper.Map<PowerSlideViewModel>(powerSlide);
            return PartialView("_EditPowerSlide", model);
        }

        //[Authorize(Roles = "Admin,Researcher,Student")]
        //public async Task<IActionResult> Detail(int id)
        //{
        //    try
        //    {
        //        if (id != 0)
        //        {
        //            var powerSlide = powerSlideRepository.Get().FirstOrDefault(a=>a.Id== id);
        //            if (powerSlide != null)
        //            {
        //                ViewBag.AttributeType = AttributeType.Subject;
        //                var loggedUser = await userManager.GetUserAsync(User);
        //                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
        //                var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == id && a.UserId == loggedUser.Id && a.IsActive);
        //                if (isAttributeAllocated || isAdmin)
        //                {
        //                    ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

        //                    var childAttributes = powerSlide.Chapters;
        //                    var childAttributesModel = new List<AttributeViewModel>();
        //                    foreach (var child in childAttributes)
        //                    {
        //                        var childAttributeModel = new AttributeViewModel()
        //                        {
        //                            Id = child.Id,
        //                            Name = child.Name,
        //                            Type = AttributeType.Chapter,
        //                            TypeText = AttributeType.Chapter
        //                        };
        //                        childAttributesModel.Add(childAttributeModel);
        //                    }

        //                    ViewBag.Attributes = childAttributesModel;

        //                    //ViewBag.Subject = subject;
        //                    ViewBag.QuestionTypes = questionTypeRepository.Get().ToList();
        //                    ViewBag.QuestionDifficultyLevels = questionDifficultyLevelRepository.Get().ToList();

        //                    if (!childAttributesModel.Any())
        //                    {
        //                        var otherSections = subjectRepository.Get().Where(x => x.ClassId == powerSlide.ClassId && x.Id != powerSlide.Id);
        //                        var otherSectionsModel = new List<AttributeViewModel>();
        //                        foreach (var child in otherSections)
        //                        {
        //                            var childAttributeModel = new AttributeViewModel()
        //                            {
        //                                Id = child.Id,
        //                                Name = child.Name,
        //                                Type = AttributeType.Subject,
        //                                TypeText = AttributeType.SubjectText,
        //                            };
        //                            otherSectionsModel.Add(childAttributeModel);
        //                        }
        //                        ViewBag.OtherSectionAttributes = otherSectionsModel;
        //                    }

        //                    var attribute = new AttributeViewModel()
        //                    {
        //                        Id = powerSlide.Id,
        //                        Name = powerSlide.Name,
        //                        Type = AttributeType.Subject,
        //                        Detail = powerSlide.Detail,
        //                        TypeText = AttributeType.SubjectText,
        //                        ParentId = powerSlide.ClassId,
        //                        ParentType = AttributeType.Class
        //                    };
        //                    return View("Detail", attribute);
        //                }
        //                else
        //                {
        //                    return new NotAllowedViewResult();

        //                }
        //            }
        //            else
        //            {
        //                return new NotFoundViewResult();
        //            }

        //        }
        //        else
        //        {
        //            return new NotFoundViewResult();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
        //        return new NotFoundViewResult("Views/Error/Error.cshtml");
        //    }
        //}
    }

}