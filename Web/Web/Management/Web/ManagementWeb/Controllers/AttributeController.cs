﻿using AutoMapper;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Infrastructure.Communication;
using Management.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{

    public class AttributeController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IEducationLevelRepository educationLevelRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IBoardLevelRepository boardLevelRepository;
        private readonly IClassRepository classRepository;
        private readonly ISubSectionRepository subSectionRepository;
        private readonly ISectionRepository sectionRepository;
        private readonly IUserSubjectRepository userSubjectRepository;
        private readonly ISubjectRepository subjectRepository;
        private readonly IEmailService emailService;
        private readonly IChapterRepository chapterRepository;
        private readonly IArtifactRepository artifactRepository;
        private IHostingEnvironment _env;
        private readonly ISetRepository setRepository;
        private readonly ISetQuestionRepository setQuestionRepository;
        private readonly IUserArtifactRepository userArtifactRepository;
        private readonly ISubjectDemoChapterRepository subjectDemoChapterRepository;
        public AttributeController(ISubjectDemoChapterRepository _subjectDemoChapterRepository, IUserArtifactRepository _userArtifactRepository, ISetRepository _setRepository, IUserSubjectRepository _userSubjectRepository, ISetQuestionRepository _setQuestionRepository, IEmailService _emailService, IHostingEnvironment env, UserManager<User> _userManager, IArtifactRepository _artifactRepository, IChapterRepository _chapterRepository, ISubSectionRepository _subSectionRepository, ISectionRepository _sectionRepository, ISubjectRepository _subjectRepository, IClassRepository _classRepositoryy, IEducationLevelRepository _educationLevelRepository, IBoardLevelRepository _boardLevelRepository, IUnitOfWork _unitOfWork)
        {
            boardLevelRepository = _boardLevelRepository;
            educationLevelRepository = _educationLevelRepository;
            classRepository = _classRepositoryy;
            subjectRepository = _subjectRepository;
            unitOfWork = _unitOfWork;
            sectionRepository = _sectionRepository;
            subSectionRepository = _subSectionRepository;
            userSubjectRepository = _userSubjectRepository;
            chapterRepository = _chapterRepository;
            emailService = _emailService;
            setRepository = _setRepository;
            setQuestionRepository = _setQuestionRepository;
            artifactRepository = _artifactRepository;
            _env = env;
            userManager = _userManager;
            userArtifactRepository = _userArtifactRepository;
            subjectDemoChapterRepository = _subjectDemoChapterRepository;

        }
        #region EducationLevel

        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> AddEducationLevel(AttributeViewModel model)
        {
            var loggedUser = await userManager.GetUserAsync(User);
            var educationLevel = new EducationLevel()
            {
                Name = model.Name,
                UserId = loggedUser.Id
            };
            await educationLevelRepository.AddAsync(educationLevel);
            await unitOfWork.SaveChangesAsync();


            return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };

        }

        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> EditEducationLevel(AttributeViewModel model)
        {

            var existingEducationLevel = educationLevelRepository.Get()
                                      .FirstOrDefault(x => x.Id == model.Id);

            if (existingEducationLevel != null)
            {

                existingEducationLevel.Name = model.Name;
                await educationLevelRepository.UpdateAsync(existingEducationLevel);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Updated Successfully!!" };


            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteEducationLevel(int id)
        {

            var existingEducationLevel = educationLevelRepository.Get()
                                       .FirstOrDefault(x => x.Id == id);

            if (existingEducationLevel != null)
            {
                await educationLevelRepository.DeleteAsync(existingEducationLevel);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted  Successfully!!" };
            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> SortEducationLevel(int id, int parentId, int sortNumber)
        {

            var existingModels = RetrieveEducationLevels();


            var counter = 1;
            foreach (var item in existingModels.Where(x => x.Id != id))
            {
                if (counter != sortNumber)
                {
                    item.SortNumber = counter;
                }
                else
                {
                    counter++;
                    item.SortNumber = counter;

                }
                counter++;
            }
            existingModels.FirstOrDefault(a => a.Id == id).SortNumber = sortNumber;
            await educationLevelRepository.UpdateManyAsync(existingModels);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true };

        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> UploadGramForEducationLevel(AttributeViewModel model)
        {
            try
            {
                var webRoot = _env.WebRootPath + "/UploadedArtifacts";
                var filePath = System.IO.Path.Combine(webRoot, model.FileName);

                var artifact = new Artifact()
                {
                    URL = filePath,
                    FileName = model.FileName
                };
                var existing = educationLevelRepository.Get()
                                          .FirstOrDefault(x => x.Id == model.Id);
                if (!string.IsNullOrEmpty(existing.IconId.ToString()))
                {
                    var existingArtifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                    var isFileExist = System.IO.File.Exists(existingArtifact.URL);
                    if (isFileExist)
                    {
                        System.IO.File.Delete(existingArtifact.URL);

                    }
                }
                //var artifactNewId =  artifactRepository.AddAsync(artifact);
                existing.Artifact = artifact;

                await educationLevelRepository.UpdateAsync(existing);
                await unitOfWork.SaveChangesAsync();
                return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }

        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteMonoGramForEducationLevel(int Id)
        {

            try
            {
                var existing = educationLevelRepository.Get()
                                           .FirstOrDefault(x => x.Id == Id);
                if (!string.IsNullOrEmpty(existing.IconId.ToString()))
                {
                    var artifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                    var isFileExist = System.IO.File.Exists(artifact.URL);
                    if (isFileExist)
                    {
                        System.IO.File.Delete(artifact.URL);
                    }
                    existing.IconId = null;

                }
                await educationLevelRepository.UpdateAsync(existing);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }

            // the anonymous object in the result below will be convert to json and set back to the browser

        }

        #endregion

        #region BoardLevel
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> AddBoardLevel(AttributeViewModel model)
        {

            var loggedUser = await userManager.GetUserAsync(User);
            var boardLevel = new BoardLevel()
            {
                Name = model.Name,
                EducationLevelId = model.ParentId,
                UserId = loggedUser.Id

            };
            await boardLevelRepository.AddAsync(boardLevel);

            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> EditBoardLevel(AttributeViewModel model)
        {

            var existingBoardLevel = boardLevelRepository.Get()
                                      .FirstOrDefault(x => x.Id == model.Id);

            if (existingBoardLevel != null)
            {

                existingBoardLevel.Name = model.Name;

                await boardLevelRepository.UpdateAsync(existingBoardLevel);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Updated Successfully!!" };


            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteBoardLevel(int id)
        {

            var existingBoardLevel = boardLevelRepository.Get()
                                      .FirstOrDefault(x => x.Id == id);

            if (existingBoardLevel != null)
            {
                await boardLevelRepository.DeleteAsync(existingBoardLevel);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };
            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseViewModel> SortBoardLevel(int id, int parentId, int sortNumber)
        {

            var existingModels = RetrieveBoardLevel(parentId).OrderBy(x => x.SortNumber);


            var counter = 1;
            foreach (var item in existingModels.Where(x => x.Id != id))
            {
                if (counter != sortNumber)
                {
                    item.SortNumber = counter;
                }
                else
                {
                    counter++;
                    item.SortNumber = counter;

                }
                counter++;
            }
            existingModels.FirstOrDefault(a => a.Id == id).SortNumber = sortNumber;
            await boardLevelRepository.UpdateManyAsync(existingModels);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true };
        }

        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> UploadGramForBoardLevel(AttributeViewModel model)
        {

            var webRoot = _env.WebRootPath + "/UploadedArtifacts";
            var filePath = System.IO.Path.Combine(webRoot, model.FileName);

            var artifact = new Artifact()
            {
                URL = filePath,
                FileName = model.FileName
            };
            var existing = boardLevelRepository.Get()
                                      .FirstOrDefault(x => x.Id == model.Id);
            if (!string.IsNullOrEmpty(existing.IconId.ToString()))
            {
                var existingArtifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                var isFileExist = System.IO.File.Exists(existingArtifact.URL);
                if (isFileExist)
                {
                    System.IO.File.Delete(existingArtifact.URL);

                }
            }
            //var artifactNewId =  artifactRepository.AddAsync(artifact);
            existing.Artifact = artifact;

            await boardLevelRepository.UpdateAsync(existing);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" }; ;
        }

        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteMonoGramForBoardLevel(int Id)
        {

            try
            {
                var existing = boardLevelRepository.Get()
                                           .FirstOrDefault(x => x.Id == Id);
                if (!string.IsNullOrEmpty(existing.IconId.ToString()))
                {
                    var artifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                    var isFileExist = System.IO.File.Exists(artifact.URL);
                    if (isFileExist)
                    {
                        System.IO.File.Delete(artifact.URL);
                    }
                    existing.IconId = null;

                }
                await boardLevelRepository.UpdateAsync(existing);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }

            // the anonymous object in the result below will be convert to json and set back to the browser

        }
        #endregion

        #region Class
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> AddClass(AttributeViewModel model)
        {

            var loggedUser = await userManager.GetUserAsync(User);
            var Class = new Class()
            {
                Name = model.Name,
                BoardLevelId = model.ParentId,
                UserId = loggedUser.Id
            };
            await classRepository.AddAsync(Class);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };

        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> EditClass(AttributeViewModel model)
        {

            var existingClass = classRepository.Get()
                                      .FirstOrDefault(x => x.Id == model.Id);

            if (existingClass != null)
            {

                existingClass.Name = model.Name;

                await classRepository.UpdateAsync(existingClass);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Updated Successfully!!" };

            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }

        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteClass(int id)
        {

            var existingClass = classRepository.Get()
                                      .FirstOrDefault(x => x.Id == id);

            if (existingClass != null)
            {
                await classRepository.DeleteAsync(existingClass);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };
            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> SortClass(int id, int parentId, int sortNumber)
        {

            var existingModels = RetrieveClass(parentId);

            var counter = 1;
            foreach (var item in existingModels.Where(x => x.Id != id))
            {
                if (counter != sortNumber)
                {
                    item.SortNumber = counter;
                }
                else
                {
                    counter++;
                    item.SortNumber = counter;

                }
                counter++;
            }
            existingModels.FirstOrDefault(a => a.Id == id).SortNumber = sortNumber;
            await classRepository.UpdateManyAsync(existingModels);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true };

        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> UploadGramForClass(AttributeViewModel model)
        {


            var webRoot = _env.WebRootPath + "/UploadedArtifacts";
            var filePath = System.IO.Path.Combine(webRoot, model.FileName);

            var artifact = new Artifact()
            {
                URL = filePath,
                FileName = model.FileName
            };
            var existing = classRepository.Get()
                                      .FirstOrDefault(x => x.Id == model.Id);
            if (!string.IsNullOrEmpty(existing.IconId.ToString()))
            {
                var existingArtifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                var isFileExist = System.IO.File.Exists(existingArtifact.URL);
                if (isFileExist)
                {
                    System.IO.File.Delete(existingArtifact.URL);

                }
            }
            //var artifactNewId =  artifactRepository.AddAsync(artifact);
            existing.Artifact = artifact;

            await classRepository.UpdateAsync(existing);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" }; ;
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteMonoGramForClass(int Id)
        {

            try
            {
                var existing = classRepository.Get()
                                           .FirstOrDefault(x => x.Id == Id);
                if (!string.IsNullOrEmpty(existing.IconId.ToString()))
                {
                    var artifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                    var isFileExist = System.IO.File.Exists(artifact.URL);
                    if (isFileExist)
                    {
                        System.IO.File.Delete(artifact.URL);
                    }
                    existing.IconId = null;

                }
                await classRepository.UpdateAsync(existing);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }

            // the anonymous object in the result below will be convert to json and set back to the browser

        }
        #endregion

        #region Subject
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> AddSubject(AttributeViewModel model)
        {

            var loggedUser = await userManager.GetUserAsync(User);
            var Subject = new Subject()
            {
                Name = model.Name,
                ClassId = model.ParentId,
                UserId = loggedUser.Id
            };
            await subjectRepository.AddAsync(Subject);
            await unitOfWork.SaveChangesAsync();


            return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };


        }

        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> AddSubjectNewArrival(AttributeViewModel model)
        {

            var subject = subjectRepository.Get().FirstOrDefault(x => x.Id == model.Id);
            subject.NewArrivalText = model.NewArrivalText;

            await subjectRepository.UpdateAsync(subject);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };


        }

        [Authorize(Roles = "Admin,Researcher")]
        [HttpGet]

        public PartialViewResult EditSubjectNewArrival(int id)
        {

            var subject = subjectRepository.Get().FirstOrDefault(x => x.Id == id);
            var model = new AttributeViewModel()
            {
                Id = subject.Id,
                NewArrivalText = subject.NewArrivalText,
                Type = AttributeType.Subject,
            };

            return PartialView("Common/_NewArrivalFormUpdate", model);


        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> EditSubject(AttributeViewModel model)
        {

            var existingSubject = subjectRepository.Get()
                                      .FirstOrDefault(x => x.Id == model.Id);

            if (existingSubject != null)
            {

                existingSubject.Name = model.Name;

                await subjectRepository.UpdateAsync(existingSubject);
                await unitOfWork.SaveChangesAsync();


                return new BaseViewModel() { Success = true, Message = "Updated Successfully!!" };

            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteSubject(int id)
        {

            var existingSubject = subjectRepository.Get()
                                      .FirstOrDefault(x => x.Id == id);

            if (existingSubject != null)
            {
                await subjectRepository.DeleteAsync(existingSubject);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };
            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }

        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> SortSubject(int id, int parentId, int sortNumber)
        {

            var existingModels = RetrieveSubject(parentId);

            var counter = 1;
            foreach (var item in existingModels.Where(x => x.Id != id))
            {
                if (counter != sortNumber)
                {
                    item.SortNumber = counter;
                }
                else
                {
                    counter++;
                    item.SortNumber = counter;

                }
                counter++;
            }
            existingModels.FirstOrDefault(a => a.Id == id).SortNumber = sortNumber;
            await subjectRepository.UpdateManyAsync(existingModels);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true };

        }

        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> UploadGramForSubject(AttributeViewModel model)
        {

            var webRoot = _env.WebRootPath + "/UploadedArtifacts";
            var filePath = System.IO.Path.Combine(webRoot, model.FileName);

            var artifact = new Artifact()
            {
                URL = filePath,
                FileName = model.FileName
            };
            var existing = subjectRepository.Get()
                                      .FirstOrDefault(x => x.Id == model.Id);
            if (!string.IsNullOrEmpty(existing.IconId.ToString()))
            {
                var existingArtifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                var isFileExist = System.IO.File.Exists(existingArtifact.URL);
                if (isFileExist)
                {
                    System.IO.File.Delete(existingArtifact.URL);

                }
            }
            //var artifactNewId =  artifactRepository.AddAsync(artifact);
            existing.Artifact = artifact;

            await subjectRepository.UpdateAsync(existing);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };

        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteMonoGramForSubject(int Id)
        {

            try
            {
                var existing = subjectRepository.Get()
                                           .FirstOrDefault(x => x.Id == Id);
                if (!string.IsNullOrEmpty(existing.IconId.ToString()))
                {
                    var artifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                    var isFileExist = System.IO.File.Exists(artifact.URL);
                    if (isFileExist)
                    {
                        System.IO.File.Delete(artifact.URL);
                    }
                    existing.IconId = null;

                }
                await subjectRepository.UpdateAsync(existing);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }

            // the anonymous object in the result below will be convert to json and set back to the browser

        }
        #endregion

        #region Section
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> AddSection(AttributeViewModel model)
        {

            var loggedUser = await userManager.GetUserAsync(User);
            var Section = new Section()
            {
                Name = model.Name,
                ChapterId = model.ParentId,
                UserId = loggedUser.Id,
                SortNumber = model.SortNumber
            };
            await sectionRepository.AddAsync(Section);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };

        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> EditSection(AttributeViewModel model)
        {

            var existingSection = sectionRepository.Get()
                                      .FirstOrDefault(x => x.Id == model.Id);

            if (existingSection != null)
            {
                existingSection.Name = model.Name;
                existingSection.SortNumber = model.SortNumber;
                await sectionRepository.UpdateAsync(existingSection);

                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Updated Successfully!!" };

            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }

        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteSection(int id)
        {

            var existingSection = sectionRepository.Get()
                                      .FirstOrDefault(x => x.Id == id);

            if (existingSection != null)
            {

                await sectionRepository.DeleteAsync(existingSection);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };
            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> SortSection(int id, int parentId, int sortNumber)
        {

            var existingModels = RetrieveSection(parentId);

            var counter = 1;
            foreach (var item in existingModels.Where(x => x.Id != id))
            {
                if (counter != sortNumber)
                {
                    item.SortNumber = counter;
                }
                else
                {
                    counter++;
                    item.SortNumber = counter;

                }
                counter++;
            }
            existingModels.FirstOrDefault(a => a.Id == id).SortNumber = sortNumber;
            await sectionRepository.UpdateManyAsync(existingModels);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true };

        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> UploadGramForSection(AttributeViewModel model)
        {


            var webRoot = _env.WebRootPath + "/UploadedArtifacts";
            var filePath = System.IO.Path.Combine(webRoot, model.FileName);

            var artifact = new Artifact()
            {
                URL = filePath,
                FileName = model.FileName
            };
            var existing = sectionRepository.Get()
                                      .FirstOrDefault(x => x.Id == model.Id);
            if (!string.IsNullOrEmpty(existing.IconId.ToString()))
            {
                var existingArtifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                var isFileExist = System.IO.File.Exists(existingArtifact.URL);
                if (isFileExist)
                {
                    System.IO.File.Delete(existingArtifact.URL);

                }
            }
            //var artifactNewId =  artifactRepository.AddAsync(artifact);
            existing.Artifact = artifact;

            await sectionRepository.UpdateAsync(existing);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteMonoGramForSection(int Id)
        {

            try
            {
                var existing = sectionRepository.Get()
                                           .FirstOrDefault(x => x.Id == Id);
                if (!string.IsNullOrEmpty(existing.IconId.ToString()))
                {
                    var artifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                    var isFileExist = System.IO.File.Exists(artifact.URL);
                    if (isFileExist)
                    {
                        System.IO.File.Delete(artifact.URL);
                    }
                    existing.IconId = null;

                }
                await sectionRepository.UpdateAsync(existing);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }

            // the anonymous object in the result below will be convert to json and set back to the browser

        }

        #endregion

        #region Chapter
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> AddChapter(AttributeViewModel model)
        {
            try
            {


                var loggedUser = await userManager.GetUserAsync(User);
                var Chapter = new Chapter()
                {
                    Name = model.Name,
                    SubjectId = model.ParentId,
                    UserId = loggedUser.Id,
                    SortNumber = model.SortNumber
                };
                await chapterRepository.AddAsync(Chapter);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };


            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };

            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> EditChapter(AttributeViewModel model)
        {

            var existingChapter = chapterRepository.Get()
                                      .FirstOrDefault(x => x.Id == model.Id);

            if (existingChapter != null)
            {

                existingChapter.Name = model.Name;
                existingChapter.SortNumber = model.SortNumber;

                await chapterRepository.UpdateAsync(existingChapter);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Updated Successfully!!" };

            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }


        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteChapter(int id)
        {

            var existingChapter = chapterRepository.Get()
                                      .FirstOrDefault(x => x.Id == id);

            if (existingChapter != null)
            {
                await chapterRepository.DeleteAsync(existingChapter);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };
            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }

        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> SortChapter(int id, int parentId, int sortNumber)
        {

            var existingModels = RetrieveChapter(parentId);

            var counter = 1;
            foreach (var item in existingModels.Where(x => x.Id != id))
            {
                if (counter != sortNumber)
                {
                    item.SortNumber = counter;
                }
                else
                {
                    counter++;
                    item.SortNumber = counter;

                }
                counter++;
            }
            existingModels.FirstOrDefault(a => a.Id == id).SortNumber = sortNumber;
            await chapterRepository.UpdateManyAsync(existingModels);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true };

        }

        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> UploadGramForChapter(AttributeViewModel model)
        {
            try
            {


                var webRoot = _env.WebRootPath + "/UploadedArtifacts";
                var filePath = System.IO.Path.Combine(webRoot, model.FileName);

                var artifact = new Artifact()
                {
                    URL = filePath,
                    FileName = model.FileName
                };
                var existing = chapterRepository.Get()
                                          .FirstOrDefault(x => x.Id == model.Id);
                if (!string.IsNullOrEmpty(existing.IconId.ToString()))
                {
                    var existingArtifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                    var isFileExist = System.IO.File.Exists(existingArtifact.URL);
                    if (isFileExist)
                    {
                        System.IO.File.Delete(existingArtifact.URL);

                    }
                }
                //var artifactNewId =  artifactRepository.AddAsync(artifact);
                existing.Artifact = artifact;

                await chapterRepository.UpdateAsync(existing);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };

            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteMonoGramForChapter(int Id)
        {

            try
            {
                var existing = chapterRepository.Get()
                                           .FirstOrDefault(x => x.Id == Id);
                if (!string.IsNullOrEmpty(existing.IconId.ToString()))
                {
                    var artifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                    var isFileExist = System.IO.File.Exists(artifact.URL);
                    if (isFileExist)
                    {
                        System.IO.File.Delete(artifact.URL);
                    }
                    existing.IconId = null;

                }
                await chapterRepository.UpdateAsync(existing);
                await unitOfWork.SaveChangesAsync();
                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }

            // the anonymous object in the result below will be convert to json and set back to the browser

        }
        #endregion

        #region SubSection
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> AddSubSection(AttributeViewModel model)
        {

            var loggedUser = await userManager.GetUserAsync(User);
            var SubSection = new SubSection()
            {
                Name = model.Name,
                SectionId = model.ParentId,
                UserId = loggedUser.Id,
                SortNumber = model.SortNumber
            };
            await subSectionRepository.AddAsync(SubSection);

            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> EditSubSection(AttributeViewModel model)
        {

            var existingSubSection = subSectionRepository.Get()
                                      .FirstOrDefault(x => x.Id == model.Id);

            if (existingSubSection != null)
            {

                existingSubSection.Name = model.Name;
                existingSubSection.SortNumber = model.SortNumber;
                await subSectionRepository.UpdateAsync(existingSubSection);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Updated Successfully!!" };

            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }

        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteSubSection(int id)
        {

            var existingSubSection = subSectionRepository.Get()
                                      .FirstOrDefault(x => x.Id == id);

            if (existingSubSection != null)
            {

                await subSectionRepository.DeleteAsync(existingSubSection);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };
            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Opps something went wrong!!" };
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> SortSubSection(int id, int parentId, int sortNumber)
        {

            var existingModels = RetrieveSubSection(parentId);

            var counter = 1;
            foreach (var item in existingModels.Where(x => x.Id != id))
            {
                if (counter != sortNumber)
                {
                    item.SortNumber = counter;
                }
                else
                {
                    counter++;
                    item.SortNumber = counter;

                }
                counter++;
            }
            existingModels.FirstOrDefault(a => a.Id == id).SortNumber = sortNumber;
            await subSectionRepository.UpdateManyAsync(existingModels);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true };

        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> UploadGramForSubSection(AttributeViewModel model)
        {

            try
            {


                var webRoot = _env.WebRootPath + "/UploadedArtifacts";
                var filePath = System.IO.Path.Combine(webRoot, model.FileName);

                var artifact = new Artifact()
                {
                    URL = filePath,
                    FileName = model.FileName
                };
                var existing = subSectionRepository.Get()
                                          .FirstOrDefault(x => x.Id == model.Id);
                if (!string.IsNullOrEmpty(existing.IconId.ToString()))
                {
                    var existingArtifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                    var isFileExist = System.IO.File.Exists(existingArtifact.URL);
                    if (isFileExist)
                    {
                        System.IO.File.Delete(existingArtifact.URL);

                    }
                }
                //var artifactNewId =  artifactRepository.AddAsync(artifact);
                existing.Artifact = artifact;

                await subSectionRepository.UpdateAsync(existing);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };

            }
        }

        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteMonoGramForSubSection(int Id)
        {

            try
            {
                var existing = subSectionRepository.Get()
                                           .FirstOrDefault(x => x.Id == Id);
                if (!string.IsNullOrEmpty(existing.IconId.ToString()))
                {
                    var artifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.IconId);
                    var isFileExist = System.IO.File.Exists(artifact.URL);
                    if (isFileExist)
                    {
                        System.IO.File.Delete(artifact.URL);
                    }
                    existing.IconId = null;

                }
                await subSectionRepository.UpdateAsync(existing);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }

            // the anonymous object in the result below will be convert to json and set back to the browser

        }
        #endregion


        #region Get Attributes

        [Authorize(Roles = "Admin,Researcher")]
        [HttpGet]
        public PartialViewResult GetEducationLevel(int parentId)
        {

            var educationLevel = RetrieveEducationLevels();
            //var model = Mapper.Map<List<AttributeViewModel>>(boardLevels);
            var model = new List<AttributeViewModel>();
            foreach (var board in educationLevel)
            {
                var attribute = new AttributeViewModel()
                {
                    Id = board.Id,
                    Name = board.Name,
                    Type = AttributeType.EducationLevel,
                    ChildText = board.Name,
                    ChildType = AttributeType.BoardLevel,
                    SortNumber = board.SortNumber
                };
                model.Add(attribute);
            }
            return PartialView("Common/_AllAttribute", model);
        }

        [Authorize(Roles = "Admin,Researcher")]
        [HttpGet]
        public PartialViewResult GetBoardLevel(int parentId)
        {

            var boardLevels = RetrieveBoardLevel(parentId);
            //var model = Mapper.Map<List<AttributeViewModel>>(boardLevels);
            var model = new List<AttributeViewModel>();
            foreach (var board in boardLevels)
            {
                var attribute = new AttributeViewModel()
                {
                    Id = board.Id,
                    Name = board.Name,
                    ParentId = board.EducationLevelId,
                    ParentType = AttributeType.EducationLevel,
                    Type = AttributeType.BoardLevel,
                    ChildText = AttributeType.ClassText,
                    ChildType = AttributeType.Class,
                    SortNumber = board.SortNumber
                };
                model.Add(attribute);
            }
            return PartialView("Common/_AllAttribute", model);
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpGet]
        public PartialViewResult GetClass(int parentId)
        {
            var Classes = RetrieveClass(parentId);
            //var model = Mapper.Map<List<AttributeViewModel>>(boardLevels);
            var model = new List<AttributeViewModel>();
            foreach (var board in Classes)
            {
                var attribute = new AttributeViewModel()
                {
                    Id = board.Id,
                    Name = board.Name,
                    ParentId = board.BoardLevelId,
                    ParentType = AttributeType.BoardLevel,
                    Type = AttributeType.Class,
                    ChildText = AttributeType.SubjectText,
                    ChildType = AttributeType.Subject,
                    SortNumber = board.SortNumber
                };
                model.Add(attribute);
            }
            return PartialView("Common/_AllAttribute", model);
        }

        [Authorize(Roles = "Admin,Researcher")]
        [HttpGet]
        public PartialViewResult GetSubject(int parentId)
        {

            var subjects = RetrieveSubject(parentId).ToList();
            //var model = Mapper.Map<List<AttributeViewModel>>(boardLevels);
            var model = new List<AttributeViewModel>();
            foreach (var board in subjects)
            {
                var attribute = new AttributeViewModel()
                {
                    Id = board.Id,
                    Name = board.Name,
                    ParentId = board.ClassId,
                    ParentType = AttributeType.Class,
                    Type = AttributeType.Subject,
                    ChildText = AttributeType.ChapterText,
                    ChildType = AttributeType.Chapter,
                    SortNumber = board.SortNumber
                };
                model.Add(attribute);
            }
            return PartialView("Common/_AllAttribute", model);
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpGet]
        public PartialViewResult GetChapter(int parentId)
        {

            var chapters = RetrieveChapter(parentId);
            //var model = Mapper.Map<List<AttributeViewModel>>(boardLevels);
            var model = new List<AttributeViewModel>();
            foreach (var board in chapters)
            {
                var attribute = new AttributeViewModel()
                {
                    Id = board.Id,
                    Name = board.Name,
                    ParentId = board.SubjectId,
                    ParentType = AttributeType.Subject,
                    Type = AttributeType.Chapter,
                    ChildText = AttributeType.SectionText,
                    ChildType = AttributeType.Section,
                    SortNumber = board.SortNumber
                };
                model.Add(attribute);
            }
            return PartialView("Common/_AllAttribute", model);
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpGet]
        public PartialViewResult GetSection(int parentId)
        {

            var section = RetrieveSection(parentId);
            //var model = Mapper.Map<List<AttributeViewModel>>(boardLevels);
            var model = new List<AttributeViewModel>();
            foreach (var board in section)
            {
                var attribute = new AttributeViewModel()
                {
                    Id = board.Id,
                    Name = board.Name,
                    ParentId = board.ChapterId,
                    ParentType = AttributeType.Chapter,
                    Type = AttributeType.Section,
                    ChildText = AttributeType.SubSectionText,
                    ChildType = AttributeType.SubSection,
                    SortNumber = board.SortNumber
                };
                model.Add(attribute);
            }
            return PartialView("Common/_AllAttribute", model);
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpGet]
        public PartialViewResult GetSubSection(int parentId)
        {

            var subSections = RetrieveSubSection(parentId);
            //var model = Mapper.Map<List<AttributeViewModel>>(boardLevels);
            var model = new List<AttributeViewModel>();
            foreach (var board in subSections)
            {
                var attribute = new AttributeViewModel()
                {
                    Id = board.Id,
                    Name = board.Name,
                    ParentId = board.SectionId,
                    ParentType = AttributeType.Section,
                    Type = AttributeType.SubSection,
                    SortNumber = board.SortNumber
                };
                model.Add(attribute);
            }
            return PartialView("Common/_AllAttribute", model);
        }

        #endregion

        #region Retrieve Attributes
        [HttpGet]
        [Authorize(Roles = "Admin,Researcher,Student,Teacher")]
        public List<EducationLevel> RetrieveEducationLevels()
        {
            var model = educationLevelRepository.Get().ToList();
            return model;
        }

        [HttpGet]
        [Authorize(Roles = "Admin,Researcher,Student,Teacher")]
        public List<BoardLevel> RetrieveBoardLevel(int parentId)
        {

            var model = boardLevelRepository.Get().Where(a => a.EducationLevelId == parentId).ToList();

            return model;

        }

        [Authorize(Roles = "Admin,Researcher,Student,Teacher")]
        public List<Class> RetrieveClass(int parentId)
        {

            var model = classRepository.Get().Where(a => a.BoardLevelId == parentId).ToList();

            return model;

        }


        [Authorize(Roles = "Admin,Researcher,Student,Teacher")]
        public List<Subject> RetrieveSubject(int parentId)
        {

            var model = subjectRepository.Get().Where(a => a.ClassId == parentId).ToList();
            return model;

        }
        [Authorize(Roles = "Admin,Researcher,Student,Teacher")]
        public List<Chapter> RetrieveChapter(int parentId)
        {

            var model = chapterRepository.Get().Where(a => a.SubjectId == parentId).ToList();
            return model;

        }

        [Authorize(Roles = "Admin,Researcher,Student,Teacher")]
        public List<Chapter> RetrieveChaptersListWithChilds(List<int> ids)
        {

            var model = chapterRepository.GetListWithChilds(ids);
            return model;

        }
        [Authorize(Roles = "Admin,Researcher,Student,Teacher")]
        public List<Section> RetrieveSection(int parentId)
        {

            var model = sectionRepository.Get().Where(a => a.ChapterId == parentId).ToList();
            return model;
        }

        [Authorize(Roles = "Admin,Researcher,Student,Teacher")]
        public List<SubSection> RetrieveSubSection(int parentId)
        {

            var model = subSectionRepository.Get().Where(a => a.SectionId == parentId).ToList();
            return model;

        }

        #endregion

        #region View Attributes


        [Authorize(Roles = "Admin,Researcher,Student, Teacher")]
        [HttpGet]
        public PartialViewResult ViewEducationLevel(int id)
        {


            var repo = educationLevelRepository.GetWithChilds(id);

            //var model = Mapper.Map<List<AttributeViewModel>>(boardLevels);
            var model = new AttributeViewModel();
            if (repo != null)
            {
                model.Name = repo.Name;
                model.Id = repo.Id;
                model.Type = AttributeType.EducationLevel;
                model.ParentType = "";
                model.ChildText = repo.Name;
                model.ChildType = AttributeType.BoardLevel;
                model.IconUrl = repo.Artifact == null ? string.Empty : "/UploadedArtifacts/" + repo.Artifact.FileName;

                var childs = new List<AttributeViewModel>();
                foreach (var item in repo.BoardLevels)
                {
                    var attribute = new AttributeViewModel()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        ParentId = id,
                        ParentType = AttributeType.EducationLevel,
                        Type = AttributeType.BoardLevel,
                        ChildText = AttributeType.ClassText,
                        ChildType = AttributeType.Class,
                        SortNumber = item.SortNumber

                    };
                    childs.Add(attribute);
                }
                model.Childs = childs;
            }
            return PartialView("Common/_ViewAttribute", model);
        }


        [Authorize(Roles = "Admin,Researcher,Student, Teacher")]
        [HttpGet]
        public PartialViewResult ViewBoardLevel(int id)
        {
            var repo = boardLevelRepository.GetWithChilds(id);
            var model = new AttributeViewModel();
            if (repo != null)
            {


                //var model = Mapper.Map<List<AttributeViewModel>>(boardLevels);

                model.Name = repo.Name;
                model.Id = repo.Id;
                model.ParentId = repo.EducationLevelId;
                model.ParentType = AttributeType.EducationLevel;
                model.Type = AttributeType.BoardLevel;
                model.ChildText = AttributeType.ClassText;
                model.ChildType = AttributeType.Class;
                model.IconUrl = repo.Artifact == null ? string.Empty : "/UploadedArtifacts/" + repo.Artifact.FileName;

                var childs = new List<AttributeViewModel>();
                //var model = Mapper.Map<List<AttributeViewModel>>(boardLevels);

                foreach (var item in repo.Classes)
                {
                    var attribute = new AttributeViewModel()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        ParentId = item.BoardLevelId,
                        ParentType = AttributeType.BoardLevel,
                        Type = AttributeType.Class,
                        ChildText = AttributeType.SubjectText,
                        ChildType = AttributeType.Subject,
                        SortNumber = item.SortNumber
                    };
                    childs.Add(attribute);
                }
                model.Childs = childs;
            }
            return PartialView("Common/_ViewAttribute", model);
        }

        [Authorize(Roles = "Admin,Researcher,Student, Teacher")]
        [HttpGet]
        public async Task<PartialViewResult> ViewClass(int id)
        {
            var loggedUser = await userManager.GetUserAsync(User);
            var isResearcher = await userManager.IsInRoleAsync(loggedUser, UserRoles.Researcher.ToString());
            var isStudent = await userManager.IsInRoleAsync(loggedUser, UserRoles.Student.ToString());
            if (isResearcher || isStudent)
            {

                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(loggedUser.Id, false);
                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);
                ViewBag.Subjects = subjectsVModel;
            }
            var repo = classRepository.GetWithChilds(id);

            var model = new AttributeViewModel();
            if (repo != null)
            {
                model.Name = repo.Name;
                model.Id = repo.Id;
                model.ParentId = repo.BoardLevelId;
                model.ParentType = AttributeType.BoardLevel;
                model.Type = AttributeType.Class;
                model.ChildText = AttributeType.SubjectText;
                model.ChildType = AttributeType.Subject;
                model.IconUrl = repo.Artifact == null ? string.Empty : "/UploadedArtifacts/" + repo.Artifact.FileName;

                var childs = new List<AttributeViewModel>();

                foreach (var item in repo.Subjects)
                {
                    var attribute = new AttributeViewModel()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        ParentId = item.ClassId,
                        ParentType = AttributeType.Class,
                        Type = AttributeType.Subject,
                        ChildText = AttributeType.ChapterText,
                        ChildType = AttributeType.Chapter,
                        SortNumber = item.SortNumber
                    };
                    childs.Add(attribute);
                }
                model.Childs = childs;
            }

            return PartialView("Common/_ViewAttribute", model);
        }


        [Authorize(Roles = "Admin,Researcher,Student, Teacher")]
        [HttpGet]
        public PartialViewResult ViewSubject(int id)
        {
            ViewBag.Sets = setRepository.Get().Where(x => x.AttributeId == id).ToList();
            var repo = subjectRepository.GetWithChilds(id);

            var model = new AttributeViewModel();
            if (repo != null)
            {
                model.Name = repo.Name;
                model.Id = repo.Id;
                model.Detail = repo.Detail;
                model.ParentId = repo.ClassId;
                model.ParentType = AttributeType.Class;
                model.Type = AttributeType.Subject;
                model.ChildText = AttributeType.ChapterText;
                model.ChildType = AttributeType.Chapter;
                model.NewArrivalText = repo.NewArrivalText;
                model.IconUrl = repo.Artifact == null ? string.Empty : "/UploadedArtifacts/" + repo.Artifact.FileName;
                var demoChapter = subjectDemoChapterRepository.Get().FirstOrDefault(a => a.SubjectId == id);

                var childs = new List<AttributeViewModel>();

                foreach (var item in repo.Chapters)
                {
                    var attribute = new AttributeViewModel()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        ParentId = item.SubjectId,
                        Detail = item.Detail,
                        ParentType = AttributeType.Subject,
                        Type = AttributeType.Chapter,
                        ChildText = AttributeType.SectionText,
                        ChildType = AttributeType.Section,
                        SortNumber = item.SortNumber
                    };
                    childs.Add(attribute);
                }
                if (demoChapter != null)
                {
                    childs.FirstOrDefault(a => a.Id == demoChapter.ChapterId).IsDemoChapter = true;
                }
                model.Childs = childs;
            }
            return PartialView("Common/_ViewAttribute", model);
        }

        [Authorize(Roles = "Admin,Researcher,Student, Teacher")]
        [HttpGet]
        public async Task<PartialViewResult> ViewChapter(int id)
        {
            ViewBag.Sets = setRepository.Get().Where(x => x.AttributeId == id).ToList();
            var repo = chapterRepository.GetWithChilds(id);

            var model = new AttributeViewModel();
            if (repo != null)
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var isParentAllocated = true;
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());

                if (!isAdmin)
                {
                    var userId = loggedUser.Id;
                    isParentAllocated = userSubjectRepository.Get().Any(x => x.SubjectId == repo.SubjectId && x.UserId == userId && x.IsActive);
                }

                model.Name = repo.Name;
                model.Id = repo.Id;
                model.Detail = repo.Detail;
                model.ParentId = repo.SubjectId;
                model.IsParentAllocated = isParentAllocated;
                model.ParentType = AttributeType.Subject;
                model.Type = AttributeType.Chapter;
                model.ChildText = AttributeType.SectionText;
                model.ChildType = AttributeType.Section;
                model.IconUrl = repo.Artifact == null ? string.Empty : "/UploadedArtifacts/" + repo.Artifact.FileName;

                var childs = new List<AttributeViewModel>();

                foreach (var item in repo.Sections)
                {
                    var attribute = new AttributeViewModel()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Detail = item.Detail,
                        ParentId = item.ChapterId,
                        ParentType = AttributeType.Chapter,
                        Type = AttributeType.Section,
                        ChildText = AttributeType.SectionText,
                        ChildType = AttributeType.SubSection,
                        SortNumber = item.SortNumber
                    };
                    childs.Add(attribute);
                }
                model.Childs = childs;
            }
            return PartialView("Common/_ViewAttribute", model);
        }

        [Authorize(Roles = "Admin,Researcher,Student, Teacher")]
        [HttpGet]
        public PartialViewResult ViewSection(int id)
        {
            ViewBag.Sets = setRepository.Get().Where(x => x.AttributeId == id).ToList();
            var repo = sectionRepository.GetWithChilds(id);

            var model = new AttributeViewModel();
            if (repo != null)
            {
                model.Name = repo.Name;
                model.Id = repo.Id;
                model.Detail = repo.Detail;
                model.ParentId = repo.ChapterId;
                model.ParentType = AttributeType.Chapter;
                model.Type = AttributeType.Section;
                model.ChildText = AttributeType.SubSectionText;
                model.ChildType = AttributeType.SubSection;
                model.IconUrl = repo.Artifact == null ? string.Empty : "/UploadedArtifacts/" + repo.Artifact.FileName;

                var childs = new List<AttributeViewModel>();

                foreach (var item in repo.SubSections)
                {
                    var attribute = new AttributeViewModel()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Detail = item.Detail,
                        ParentId = item.SectionId,
                        ParentType = AttributeType.Section,
                        Type = AttributeType.SubSection,
                        SortNumber = item.SortNumber
                    };
                    childs.Add(attribute);
                }
                model.Childs = childs;
            }
            return PartialView("Common/_ViewAttribute", model);
        }

        [Authorize(Roles = "Admin,Researcher,Student, Teacher")]
        [HttpGet]
        public PartialViewResult ViewSubSection(int id)
        {

            ViewBag.Sets = setRepository.Get().Where(x => x.AttributeId == id).ToList();
            var repo = subSectionRepository.Get().FirstOrDefault(x => x.Id == id);

            var model = new AttributeViewModel();
            if (repo != null)
            {
                model.Name = repo.Name;
                model.Id = repo.Id;
                model.Detail = repo.Detail;
                model.ParentId = repo.SectionId;
                model.ParentType = AttributeType.Section;
                model.Type = AttributeType.SubSection;
            }
            return PartialView("Common/_ViewAttribute", model);
        }

        #endregion

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseViewModel> SetDemoChapter(int subjectId, int chapterId)
        {
            try
            {
                var existing = subjectDemoChapterRepository.Get()
                                           .FirstOrDefault(x => x.SubjectId == subjectId);
                if (existing != null)
                {

                    existing.ChapterId = chapterId;
                    await subjectDemoChapterRepository.UpdateAsync(existing);
                    await unitOfWork.SaveChangesAsync();

                    return new BaseViewModel() { Success = true, Message = "Demo chapter updated successfully!!" };
                }
                else
                {
                    var subDemoChapter = new SubjectDemoChapter()
                    {
                        ChapterId = chapterId,
                        SubjectId = subjectId
                    };
                    await subjectDemoChapterRepository.AddAsync(subDemoChapter);
                    await unitOfWork.SaveChangesAsync();

                    return new BaseViewModel() { Success = true, Message = "New demo chapter selected successfully!!" };
                }


            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }



        }

        [Authorize]
        [HttpPost]
        public async Task<BaseViewModel> DeleteMonoGramForUserProfile(string Id)
        {

            try
            {
                var existing = userArtifactRepository.Get()
                                           .FirstOrDefault(x => x.UserId == Id);
                if (!string.IsNullOrEmpty(existing.ArtifactId.ToString()))
                {
                    var artifact = artifactRepository.Get().FirstOrDefault(x => x.Id == existing.ArtifactId);
                    var isFileExist = System.IO.File.Exists(artifact.URL);
                    if (isFileExist)
                    {
                        System.IO.File.Delete(artifact.URL);
                    }
                    await artifactRepository.DeleteAsync(artifact);
                    await userArtifactRepository.DeleteAsync(existing);

                }
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }

            // the anonymous object in the result below will be convert to json and set back to the browser

        }
    }
}