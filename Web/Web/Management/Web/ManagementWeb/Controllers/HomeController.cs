﻿using AutoMapper;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Infrastructure.Communication;
using Management.Infrastructure.Security.Identity;
using Management.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class HomeController : Controller
    {
        private readonly IEducationLevelRepository educationLevelRepository;
        private readonly IUserSubjectRequestRepository userSubjectRequestRepository;
        private readonly ISubjectRepository subjectRepository;
        private readonly UserManager userManager;
        private readonly IChapterRepository chapterRepository;
        private IMemoryCache cache;
        private readonly IUserChapterRequestRepository userChapterRequestRepository;
        private readonly IUserChapterRepository userChapterRepository;
        private readonly ITourRepository tourRepository;
        private readonly IBatchRepository batchRepository;
        private readonly IArtifactRepository artifactRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IUserSubjectRepository userSubjectRepository;
        private readonly IUserArtifactRepository userArtifactRepository;
        private readonly IEmailService emailService;

        public HomeController(IEmailService _emailService, IUserArtifactRepository _userArtifactRepository, IUserSubjectRepository _userSubjectRepository, IArtifactRepository _artifactRepository, IBatchRepository _batchRepository,IUnitOfWork _unitOfWork,ITourRepository _tourRepository,IMemoryCache memoryCache, IUserChapterRepository _userChapterRepository, IUserChapterRequestRepository _userChapterRequestRepository, IChapterRepository _chapterRepository, ISubjectRepository _subjectRepository,IUserSubjectRequestRepository _userSubjectRequestRepository, UserManager _userManager, IEducationLevelRepository _educationLevelRepository)
        {
            educationLevelRepository = _educationLevelRepository;
            subjectRepository = _subjectRepository;
            userManager = _userManager;
            userSubjectRequestRepository = _userSubjectRequestRepository;
            userChapterRequestRepository = _userChapterRequestRepository;
            userChapterRepository = _userChapterRepository;
            chapterRepository = _chapterRepository;
            cache = memoryCache;
            tourRepository = _tourRepository;
            unitOfWork = _unitOfWork;
            batchRepository = _batchRepository;
            artifactRepository = _artifactRepository;
            userSubjectRepository = _userSubjectRepository;
            userArtifactRepository = _userArtifactRepository;
            emailService = _emailService;
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Index()
        {
            var loggedUser = await userManager.GetUserAsync(User);
            
            var userRoles = await userManager.GetRolesAsync(loggedUser);
            if (userRoles.Contains(UserRoles.Admin.ToString()))
            {
                var researchers = userManager.GetUsersInRoleAsync(UserRoles.Researcher.ToString());
                var students = userManager.GetUsersInRoleAsync(UserRoles.Student.ToString());
                var teachers = userManager.GetUsersInRoleAsync(UserRoles.Teacher.ToString());
                var disciplines = educationLevelRepository.GetEducationLevelWithBoardLevels();
                  
                ViewBag.EducationLevel = disciplines;
                ViewBag.ReseacherCount = researchers.Result.Where(a => !a.IsDeleted).Count();
                ViewBag.StudentCount = students.Result.Where(a => !a.IsDeleted).Count();
                ViewBag.TeachersCount = teachers.Result.Where(a=>!a.IsDeleted).Count();
                ViewBag.BatchCount = batchRepository.Get().Count();


                ViewBag.RegisteredStudentCount = students.Result.Where(a => a.EmailConfirmed).Count();
                ViewBag.UnregisteredStudentCount = students.Result.Where(a=> !a.EmailConfirmed).Count();
                ViewBag.SubjectAllocationRequestCount = subjectRepository.ListOfSubjectsRequested(string.Empty).Count; 
                ViewBag.ChapterAllocationRequestCount = chapterRepository.ListOfChaptersRequested(string.Empty).Count;

                return View();
            }
            
            else if (userRoles.Contains(UserRoles.Researcher.ToString()))
            {
                return RedirectToAction("Index", "Researcher");
            }
            else if (userRoles.Contains(UserRoles.Student.ToString()))
            {
                return RedirectToAction("Index", "Student");
            }
            else 
            {
                return null;
            }
        }
        [Authorize(Roles = "Admin, Researcher")]
        [HttpGet]
        public PartialViewResult LoadDisciplineMenu()
        {
            ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();
            return PartialView("Common/_DisciplineMenuNew");
        }
        public IActionResult Editor()
        {
            //SautinSoft.UseOffice u = new SautinSoft.UseOffice();

            ////Path to any local file
            //string inputFilePath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"C:\Users\Aftab Ahmed\Desktop\Docx to html C#\C#\sample.docx"));
            ////Path to output resulted file
            //string outputFilePath = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"C:\Users\Aftab Ahmed\Desktop\Docx to html C#\C#\sampleNew.html"));

            ////Prepare UseOffice .Net, loads MS Word in memory
            //int ret = u.InitWord();

            ////Return values:
            ////0 - Loading successfully
            ////1 - Can't load MS Word® library in memory 

            //if (ret == 1)
            //    return;

            ////Converting
            //ret = u.ConvertFile(inputFilePath, outputFilePath, SautinSoft.UseOffice.eDirection.DOCX_to_HTML);
            //string html = File.ReadAllText(outputFilePath);

            //HtmlDocument doc = new HtmlDocument();
            //doc.Load("file.htm");

            //HtmlNode bodyNode = doc.DocumentNode.SelectSingleNode("/html/body");

            //if (bodyNode != null)
            //{
            //    // do something
            //}
            ////Release MS Word from memory
            //u.CloseWord();

            ////0 - Converting successfully
            ////1 - Can't open input file. Check that you are using full local path to input file, URL and relative path are not supported
            ////2 - Can't create output file. Please check that you have permissions to write by this path or probably this path already used by another application
            ////3 - Converting failed, please contact with our Support Team
            ////4 - MS Office isn't installed. The component requires that any of these versions of MS Office should be installed: 2000, XP, 2003, 2007 or 2010
            //if (ret == 0)
            //{
            //    //Show produced file
            //    System.Diagnostics.Process.Start(outputFilePath);
            //}
            return View();
        }

        [HttpPost]
        public async Task<bool> EndTour()
        {
            try
            {
                var userId = userManager.GetUserId(User);
                var IsTourTaken = tourRepository.Get().FirstOrDefault(a => a.UserId == userId) == null ? false : tourRepository.Get().FirstOrDefault(a => a.UserId == userId).IsTourTaken;
                if (!IsTourTaken)
                {
                    var tour = new Tour()
                    {
                        IsTourTaken=true,
                        UserId=userId
                    };
                    await tourRepository.AddAsync(tour);
                    await unitOfWork.SaveChangesAsync();
                    IsTourTaken = true;
                }
                return !IsTourTaken;
            }
            catch (Exception ex)
            {
                return false;
            }
           
        }
        [HttpGet]
        public bool IsTourTaken()
        {
            try
            {
                var userId = userManager.GetUserId(User);
                var IsTourTaken = tourRepository.Get().FirstOrDefault(a => a.UserId == userId)==null?false: tourRepository.Get().FirstOrDefault(a => a.UserId == userId).IsTourTaken;

                return !IsTourTaken;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        [HttpGet]
        public async Task<PartialViewResult> GetUser(string userId)
        {
            try
            {
                var user = await userManager.FindByIdAsync(userId);
                var role = await userManager.GetRolesAsync(user);
                var userArtifact = userArtifactRepository.Get().FirstOrDefault(x => x.UserId == userId);
                var profilePic = userArtifact != null ? artifactRepository.Get().FirstOrDefault(a => a.Id == userArtifact.ArtifactId).FileName : "";

                var student = Mapper.Map<UserViewModel>(user);
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(userId, isAdmin);
                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);

                student.AllocatedSubjects = subjectsVModel;

                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(user.Id, isAdmin);
                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);

                student.AllocatedChapters = chaptersVModel;

                student.ProfilePictureUrl = profilePic;
                var userRole = role.FirstOrDefault();
                student.Role = role.Contains(UserRoles.Admin.ToString()) ? UserRoles.Admin
                    : role.Contains(UserRoles.Teacher.ToString()) ? UserRoles.Teacher
                    : role.Contains(UserRoles.Researcher.ToString()) ? UserRoles.Researcher
                    : role.Contains(UserRoles.Student.ToString()) ? UserRoles.Student
                    : UserRoles.Student;
                return PartialView("_UserDetail", student);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }
        [HttpGet]
        public async Task<PartialViewResult> GetUserByEmail(string email)
        {
            try
            {
                var user = await userManager.FindByEmailAsync(email);
                var role = await userManager.GetRolesAsync(user);
                var userArtifact = userArtifactRepository.Get().FirstOrDefault(x => x.UserId == user.Id);
                var profilePic = userArtifact != null ? artifactRepository.Get().FirstOrDefault(a => a.Id == userArtifact.ArtifactId).FileName : "";

                var student = Mapper.Map<UserViewModel>(user);
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(loggedUser.Id, isAdmin);
                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(loggedUser.Id, isAdmin);
                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);
                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);

                student.AllocatedSubjects = subjectsVModel;
                student.AllocatedChapters = chaptersVModel;
                student.ProfilePictureUrl = profilePic;
                student.Role = role.Contains(UserRoles.Admin.ToString()) ? UserRoles.Admin
                    : role.Contains(UserRoles.Teacher.ToString()) ? UserRoles.Teacher
                    : role.Contains(UserRoles.Researcher.ToString()) ? UserRoles.Researcher
                    : role.Contains(UserRoles.Student.ToString()) ? UserRoles.Student
                    : UserRoles.Student;
                return PartialView("_UserDetail", student);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

    }
}
