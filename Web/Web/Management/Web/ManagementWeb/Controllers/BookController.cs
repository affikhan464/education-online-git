﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.ViewModels;
using Management.Data.Models;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using HtmlAgilityPack;
using Management.Data.Interfaces.UnitOfWork;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Management.Infrastructure.Communication;
using Microsoft.AspNetCore.Identity;
using AutoMapper;
using Management.Data.Models.CustomModels;
using Microsoft.AspNetCore.Http;
using Management.Filters;
using Microsoft.Extensions.Caching.Memory;

namespace Management.Controllers
{
    public class BookController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly ISubjectRepository subjectRepository;
        private readonly IChapterRepository chapterRepository;
        private readonly IWordRepository wordRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly ICorrectAnswerRepository correctAnswerRepository;
        private readonly IUserSubjectRequestRepository userSubjectRequestRepository;
        private readonly IUserSubjectRepository userSubjectRepository;
        private readonly IQuestionRepository questionRepository;
        private IHostingEnvironment env;
        private readonly ICommentaryRepository commentaryRepository;
        private readonly ISectionRepository sectionRepository;
        private readonly ISubSectionRepository subSectionRepository;
        private readonly IQuestionTypeRepository questionTypeRepository;
        private readonly IQuestionDifficultyLevelRepository questionDifficultyLevelRepository;
        private readonly IEducationLevelRepository educationLevelRepository;
        private readonly IEmailService emailService;
        private IMemoryCache cache;

        private readonly IUserChapterRepository userChapterRepository;
        private readonly IUserChapterRequestRepository userChapterRequestRepository;
        public BookController(IMemoryCache memoryCache,IUserChapterRepository _userChapterRepository, IUserChapterRequestRepository _userChapterRequestRepository, IEducationLevelRepository _educationLevelRepository, IQuestionRepository _questionRepository, ICommentaryRepository _commentaryRepository, ICorrectAnswerRepository _correctAnswerRepository, IWordRepository _wordRepository, IUserSubjectRequestRepository _userSubjectRequestRepository, UserManager<User> _userManager, IUserSubjectRepository _userSubjectRepository, IQuestionDifficultyLevelRepository _questionDifficultyLevelRepository, IQuestionTypeRepository _questionTypeRepository, IEmailService _emailService, IHostingEnvironment _env, ISubjectRepository _subjectRepository, IChapterRepository _chapterRepository, ISectionRepository _sectionRepository, ISubSectionRepository _subSectionRepository, IUnitOfWork _unitOfWork)
        {
            userManager = _userManager;
            userChapterRequestRepository = _userChapterRequestRepository;
            subjectRepository = _subjectRepository;
            userSubjectRequestRepository = _userSubjectRequestRepository;
            userSubjectRepository = _userSubjectRepository;
            chapterRepository = _chapterRepository;
            unitOfWork = _unitOfWork;
            commentaryRepository = _commentaryRepository;
            wordRepository = _wordRepository;
            educationLevelRepository = _educationLevelRepository;
            correctAnswerRepository = _correctAnswerRepository;
            sectionRepository = _sectionRepository;
            emailService = _emailService;
            subSectionRepository = _subSectionRepository;
            questionRepository = _questionRepository;
            questionTypeRepository = _questionTypeRepository;
            questionDifficultyLevelRepository = _questionDifficultyLevelRepository;
            env = _env;
            userChapterRepository = _userChapterRepository;
            cache = memoryCache;
        }
        [Authorize]
        public IActionResult Index()
        {
            return View("Layouts/_BookNew");
        }
      
        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseViewModel> DeleteDetailForSubject(int id)
        {
            var existingModel = subjectRepository.Get().FirstOrDefault(a => a.Id == id);
            existingModel.Detail = string.Empty;
            await subjectRepository.UpdateAsync(existingModel);
            await unitOfWork.SaveChangesAsync();

            return new BaseViewModel() { Success = true, Message = "Removed Detail Successfully!!" };
        }

        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public BaseViewModel LoadSubjectDetail(int id)
        {
            var existingModel = subjectRepository.Get().FirstOrDefault(a => a.Id == id);

            return new BaseViewModel() { Data = existingModel };
        }
        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        public async Task<PartialViewResult> LoadSubjectDetailView(int id)
        {
            var existingModel = subjectRepository.Get().FirstOrDefault(a => a.Id == id);
            var loggedUser = await userManager.GetUserAsync(User);
            var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
            var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == id && a.UserId == loggedUser.Id && a.IsActive);
            if (isAttributeAllocated || isAdmin)
            {
                var attribute = new AttributeViewModel()
                {
                    Id = existingModel.Id,
                    Name = existingModel.Name,
                    Type = AttributeType.Subject,
                    Detail = existingModel.Detail,
                    TypeText = AttributeType.SubjectText
                };
                return PartialView("_DescriptionWrapper", attribute);
            }
            else
            {
                return new NotAllowedViewResult();

            }

        }
        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<IActionResult> EditSubject(int id)
        {

            var subject = subjectRepository.Get().FirstOrDefault(a => a.Id == id);
            if (subject != null)
            {
                ViewBag.AttributeType = AttributeType.Subject;
                ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == id && a.UserId == loggedUser.Id && a.IsActive);
                if (isAttributeAllocated || isAdmin)
                {
                    var childAttributes = subject.Chapters;
                    var childAttributesModel = new List<AttributeViewModel>();
                    foreach (var child in childAttributes)
                    {
                        var childAttributeModel = new AttributeViewModel()
                        {
                            Id = child.Id,
                            Name = child.Name,
                            Type = AttributeType.Chapter,
                            TypeText = AttributeType.Chapter
                        };
                        childAttributesModel.Add(childAttributeModel);
                    }

                    ViewBag.Attributes = childAttributesModel;

                    //ViewBag.Subject = subject;
                    ViewBag.QuestionTypes = questionTypeRepository.Get().ToList();
                    ViewBag.QuestionDifficultyLevels = questionDifficultyLevelRepository.Get().ToList();


                    var attribute = new AttributeViewModel()
                    {
                        Id = subject.Id,
                        Detail = subject.Detail,
                        Name = subject.Name,
                        Type = AttributeType.Subject,
                        TypeText = AttributeType.SubjectText,
                        ParentId = subject.ClassId,
                        ParentType = AttributeType.Class
                    };
                    return View("EditDetail", attribute);
                }
                else
                {
                    return new NotAllowedViewResult();

                }

            }
            else
            {
                return new NotFoundViewResult();
            }
        }

        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        public async Task<IActionResult> Subject(int id)
        {
            try
            {
                if (id != 0)
                {
                    var subject = subjectRepository.GetWithChapters(id);
                    if (subject != null)
                    {
                        ViewBag.AttributeType = AttributeType.Subject;
                        var loggedUser = await userManager.GetUserAsync(User);
                        var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                        var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == id && a.UserId == loggedUser.Id && a.IsActive);
                        if (isAttributeAllocated || isAdmin)
                        {
                            ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

                            var childAttributes = subject.Chapters;
                            var childAttributesModel = new List<AttributeViewModel>();
                            foreach (var child in childAttributes)
                            {
                                   var childAttributeModel = new AttributeViewModel()
                                   {
                                       Id = child.Id,
                                       Name = child.Name,
                                       Type = AttributeType.Chapter,
                                       TypeText = AttributeType.Chapter
                                   };
                                childAttributesModel.Add(childAttributeModel);
                            }
                          
                            ViewBag.Attributes = childAttributesModel;

                            //ViewBag.Subject = subject;
                            ViewBag.QuestionTypes = questionTypeRepository.Get().ToList();
                            ViewBag.QuestionDifficultyLevels = questionDifficultyLevelRepository.Get().ToList();

                            if (!childAttributesModel.Any())
                            {
                                var otherSections = subjectRepository.Get().Where(x => x.ClassId == subject.ClassId && x.Id != subject.Id);
                                var otherSectionsModel = new List<AttributeViewModel>();
                                foreach (var child in otherSections)
                                {
                                    var childAttributeModel = new AttributeViewModel()
                                    {
                                        Id = child.Id,
                                        Name = child.Name,
                                        Type = AttributeType.Subject,
                                        TypeText = AttributeType.SubjectText,
                                    };
                                    otherSectionsModel.Add(childAttributeModel);
                                }
                                ViewBag.OtherSectionAttributes = otherSectionsModel;
                            }

                            var attribute = new AttributeViewModel()
                            {
                                Id = subject.Id,
                                Name = subject.Name,
                                Type = AttributeType.Subject,
                                Detail = subject.Detail,
                                TypeText = AttributeType.SubjectText,
                                ParentId = subject.ClassId,
                                ParentType=AttributeType.Class
                            };
                            return View("Detail", attribute);
                        }
                        else
                        {
                            return new NotAllowedViewResult();

                        }
                    }
                    else
                    {
                        return new NotFoundViewResult();
                    }

                }
                else
                {
                    return new NotFoundViewResult();
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new NotFoundViewResult("Views/Error/Error.cshtml");
            }
        }


        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseViewModel> DeleteDetailForChapter(int id)
        {
            var existingModel = chapterRepository.Get().FirstOrDefault(a => a.Id == id);

            var loggedUser = await userManager.GetUserAsync(User);
            var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
            var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.SubjectId && a.UserId == loggedUser.Id && a.IsActive);
            var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == existingModel.Id && a.UserId == loggedUser.Id && a.IsActive);
            if (isAttributeAllocated || isAdmin || isChapterAllocated)
            {
                existingModel.Detail = string.Empty;
                await chapterRepository.UpdateAsync(existingModel);
                await unitOfWork.SaveChangesAsync();
                return new BaseViewModel() { Success = true, Message = "Removed Detail Successfully!!" };
            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Access Denied!!" };

            }
        }
        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseViewModel> LoadChapterDetail(int id)
        {
            var existingModel = chapterRepository.Get().FirstOrDefault(a => a.Id == id);
            var loggedUser = await userManager.GetUserAsync(User);
            var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
            var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.SubjectId && a.UserId == loggedUser.Id && a.IsActive);
            var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == existingModel.Id && a.UserId == loggedUser.Id && a.IsActive);
            if (isAttributeAllocated || isAdmin || isChapterAllocated)
            {


                return new BaseViewModel() { Data = existingModel };
            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "Access Denied!!" };

            }
        }

        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        public async Task<PartialViewResult> LoadChapterDetailView(int id)
        {
            var existingModel = chapterRepository.Get().FirstOrDefault(a => a.Id == id);
            var loggedUser = await userManager.GetUserAsync(User);
            var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
            var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.SubjectId && a.UserId == loggedUser.Id && a.IsActive);
            var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == existingModel.Id && a.UserId == loggedUser.Id && a.IsActive);
            if (isAttributeAllocated || isAdmin || isChapterAllocated)
            {


                var attribute = new AttributeViewModel()
                {
                    Id = existingModel.Id,
                    Name = existingModel.Name,
                    Type = AttributeType.Chapter,
                    Detail = existingModel.Detail,
                    TypeText = AttributeType.ChapterText
                };
                return PartialView("_DescriptionWrapper", attribute);
            }
            else
            {
                return new NotAllowedViewResult();

            }
        }
        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<IActionResult> EditChapter(int id)
        {
            var chapter = chapterRepository.GetWithChilds(id);
            var subjectId = chapter.SubjectId;
            if (chapter != null)
            {
                ViewBag.AttributeType = AttributeType.Chapter;
                ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == subjectId && a.UserId == loggedUser.Id && a.IsActive);
                var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == chapter.Id && a.UserId == loggedUser.Id && a.IsActive);
                if (isAttributeAllocated || isAdmin || isChapterAllocated)
                {
                    //var Subject = subjectRepository.GetWithChilds(subjectId);
                    //ViewBag.Subject = Subject;
                    ViewBag.QuestionTypes = questionTypeRepository.Get().ToList();
                    ViewBag.QuestionDifficultyLevels = questionDifficultyLevelRepository.Get().ToList();

                    var childAttributes = chapter.Sections;
                    var childAttributesModel = new List<AttributeViewModel>();
                    foreach (var child in childAttributes)
                    {
                        var childAttributeModel = new AttributeViewModel()
                        {
                            Id = child.Id,
                            Name = child.Name,
                            Type = AttributeType.Section,
                            TypeText = AttributeType.SectionText
                        };
                        childAttributesModel.Add(childAttributeModel);
                    }

                    ViewBag.Attributes = childAttributesModel;
                    var attribute = new AttributeViewModel()
                    {
                        Id = chapter.Id,
                        Detail = chapter.Detail,
                        Name = chapter.Name,
                        Type = AttributeType.Chapter,
                        TypeText = AttributeType.ChapterText,
                        ParentId = chapter.SubjectId,
                        ParentType = AttributeType.Subject
                    };
                    return View("EditDetail", attribute);
                }
                else
                {
                    return new NotAllowedViewResult();

                }
            }
            else
            {
                return new NotFoundViewResult();
            }
        }
        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        public async Task<IActionResult> Chapter(int id)
        {
            if (id != 0)
            {
                var chapter = chapterRepository.GetWithSection(id);
                var subjectId = chapter.SubjectId;
                if (chapter != null)
                {
                    ViewBag.AttributeType = AttributeType.Chapter;
                    var loggedUser = await userManager.GetUserAsync(User);
                    var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                    var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == subjectId && a.UserId == loggedUser.Id && a.IsActive);
                    var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == chapter.Id && a.UserId == loggedUser.Id && a.IsActive);
                    if (isAttributeAllocated || isAdmin || isChapterAllocated)
                    {
                        ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

                        //var Subject = subjectRepository.GetWithChilds(subjectId);
                        //ViewBag.Subject = Subject;
                        var childAttributes = chapter.Sections;
                        var childAttributesModel = new List<AttributeViewModel>();
                        foreach (var child in childAttributes)
                        {
                            var childAttributeModel = new AttributeViewModel()
                            {
                                Id = child.Id,
                                Name = child.Name,
                                Type = AttributeType.Section,
                                TypeText = AttributeType.SectionText
                            };
                            childAttributesModel.Add(childAttributeModel);
                        }

                        ViewBag.Attributes = childAttributesModel;

                        ViewBag.QuestionTypes = questionTypeRepository.Get().ToList();
                        ViewBag.QuestionDifficultyLevels = questionDifficultyLevelRepository.Get().ToList();


                        if (!childAttributesModel.Any())
                        {
                            var otherSections = chapterRepository.Get().Where(x => x.SubjectId == chapter.SubjectId && x.Id != chapter.Id);
                            var otherSectionsModel = new List<AttributeViewModel>();
                            foreach (var child in otherSections)
                            {
                                var childAttributeModel = new AttributeViewModel()
                                {
                                    Id = child.Id,
                                    Name = child.Name,
                                    Type = AttributeType.Chapter,
                                    TypeText = AttributeType.ChapterText,
                                };
                                otherSectionsModel.Add(childAttributeModel);
                            }
                            ViewBag.OtherSectionAttributes = otherSectionsModel;
                        }

                        var attribute = new AttributeViewModel()
                        {
                            Id = chapter.Id,
                            Name = chapter.Name,
                            Detail = chapter.Detail,
                            Type = AttributeType.Chapter,
                            TypeText = AttributeType.ChapterText,
                            ParentId= chapter.SubjectId,
                            ParentType = AttributeType.Subject

                        };
                        return View("Detail", attribute);
                    }
                    else
                    {
                        return new NotAllowedViewResult();
                    }
                }
                else
                {
                    return new NotFoundViewResult();
                }

            }
            else
            {
                return new NotFoundViewResult();
            }
        }


        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseViewModel> DeleteDetailForSection(int id)
        {
            var existingModel = sectionRepository.GetWithChilds(id);
            var loggedUser = await userManager.GetUserAsync(User);
            var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());

            var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == existingModel.Chapter.Id && a.UserId == loggedUser.Id && a.IsActive);

            var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.Chapter.SubjectId && a.UserId == loggedUser.Id && a.IsActive);
            if (isAttributeAllocated || isAdmin || isChapterAllocated)
            {
                existingModel.Detail = string.Empty;
                await sectionRepository.UpdateAsync(existingModel);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Removed Detail Successfully!!" };
            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "You have no Access." };

            }

        }
        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseViewModel> LoadSectionDetail(int id)
        {

            var existingModel = sectionRepository.GetWithChilds(id);
            var loggedUser = await userManager.GetUserAsync(User);
            var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());

            var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == existingModel.Chapter.Id && a.UserId == loggedUser.Id && a.IsActive);

            var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.Chapter.SubjectId && a.UserId == loggedUser.Id && a.IsActive);
            if (isAttributeAllocated || isAdmin || isChapterAllocated)
            {
                return new BaseViewModel() { Data = existingModel };
            }
            else
            {
                return new BaseViewModel() { Success = false, Message = "You have no Access." };

            }
        }

        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        public async Task<PartialViewResult> LoadSectionDetailView(int id)
        {
            var existingModel = sectionRepository.GetWithChilds(id);
            var loggedUser = await userManager.GetUserAsync(User);
            var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());

            var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == existingModel.Chapter.Id && a.UserId == loggedUser.Id && a.IsActive);

            var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.Chapter.SubjectId && a.UserId == loggedUser.Id && a.IsActive);
            if (isAttributeAllocated || isAdmin || isChapterAllocated)
            {

                var attribute = new AttributeViewModel()
                {
                    Id = existingModel.Id,
                    Name = existingModel.Name,
                    Type = AttributeType.Section,
                    Detail = existingModel.Detail,
                    TypeText = AttributeType.SectionText
                };
                return PartialView("_DescriptionWrapper", attribute);
            }
            else
            {
                return new NotAllowedViewResult();

            }
        }
        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<IActionResult> EditSection(int id)
        {

            var section = sectionRepository.GetWithChilds(id);
            if (section != null)
            {
                ViewBag.AttributeType = AttributeType.Section;
                ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

                var subjectId = section.Chapter.SubjectId;

                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == subjectId && a.UserId == loggedUser.Id && a.IsActive);
                var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == section.Chapter.Id && a.UserId == loggedUser.Id && a.IsActive);
                if (isAttributeAllocated || isAdmin || isChapterAllocated)
                {


                    var Subject = subjectRepository.GetWithChilds(subjectId);
                    //ViewBag.Subject = Subject;
                    ViewBag.QuestionTypes = questionTypeRepository.Get().ToList();
                    ViewBag.QuestionDifficultyLevels = questionDifficultyLevelRepository.Get().ToList();
                    var childAttributes = section.SubSections;
                    var childAttributesModel = new List<AttributeViewModel>();
                    foreach (var child in childAttributes)
                    {
                        var childAttributeModel = new AttributeViewModel()
                        {
                            Id = child.Id,
                            Name = child.Name,
                            Type = AttributeType.SubSection,
                            TypeText = AttributeType.SubSectionText
                        };
                        childAttributesModel.Add(childAttributeModel);
                    }

                    ViewBag.Attributes = childAttributesModel;
                    var attribute = new AttributeViewModel()
                    {
                        Id = section.Id,
                        Name = section.Name,
                        Detail = section.Detail,
                        Type = AttributeType.Section,
                        TypeText = AttributeType.SectionText,
                        ParentId = section.ChapterId,
                        ParentType = AttributeType.Chapter
                    };
                    return View("EditDetail", attribute);
                }
                else
                {
                    return new NotAllowedViewResult();

                }
            }
            else
            {
                return new NotFoundViewResult();
            }
        }


        [Authorize]
        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        public async Task<IActionResult> Section(int id)
        {
            if (id != 0)
            {
                var section = sectionRepository.GetWithChilds(id);
                if (section != null)
                {
                    ViewBag.AttributeType = AttributeType.Section;
                    var subjectId = section.Chapter.SubjectId;
                    var loggedUser = await userManager.GetUserAsync(User);
                    var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                    var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == subjectId && a.UserId == loggedUser.Id && a.IsActive);
                    var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == section.Chapter.Id && a.UserId == loggedUser.Id && a.IsActive);
                    if (isAttributeAllocated || isAdmin || isChapterAllocated)
                    {
                        ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

                        //var Subject = subjectRepository.GetWithChilds(subjectId);
                        //ViewBag.Subject = Subject;

                        var childAttributes = section.SubSections;
                        var childAttributesModel = new List<AttributeViewModel>();
                        foreach (var child in childAttributes)
                        {
                            var childAttributeModel = new AttributeViewModel()
                            {
                                Id = child.Id,
                                Name = child.Name,
                                Type = AttributeType.SubSection,
                                TypeText = AttributeType.SubSectionText,
                            };
                            childAttributesModel.Add(childAttributeModel);
                        }

                        ViewBag.Attributes = childAttributesModel;

                        if (!childAttributesModel.Any())
                        {
                            var otherSections = sectionRepository.Get().Where(x => x.ChapterId == section.Chapter.Id && x.Id!=section.Id);
                            var otherSectionsModel = new List<AttributeViewModel>();
                            foreach (var child in otherSections)
                            {
                                var childAttributeModel = new AttributeViewModel()
                                {
                                    Id = child.Id,
                                    Name = child.Name,
                                    Type = AttributeType.Section,
                                    TypeText = AttributeType.SectionText,
                                };
                                otherSectionsModel.Add(childAttributeModel);
                            }
                            ViewBag.OtherSectionAttributes = otherSectionsModel;
                        }

                        ViewBag.QuestionTypes = questionTypeRepository.Get().ToList();
                        ViewBag.QuestionDifficultyLevels = questionDifficultyLevelRepository.Get().ToList();

                        var attribute = new AttributeViewModel()
                        {
                            Id = section.Id,
                            Name = section.Name,
                            Type = AttributeType.Section,
                            Detail = section.Detail,
                            TypeText = AttributeType.SectionText,
                            ParentId = section.ChapterId,
                            ParentType = AttributeType.Chapter

                        };
                        return View("Detail", attribute);
                    }
                    else
                    {
                        return new NotAllowedViewResult();

                    }
                }
                else
                {
                    return new NotFoundViewResult();
                }
            }
            else
            {
                return new NotFoundViewResult();
            }
        }

      

        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseViewModel> DeleteDetailForSubSection(int id)
        {
            var existingModel = subSectionRepository.GetWithChilds(id);
            var loggedUser = await userManager.GetUserAsync(User);
            var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
            var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.Section.Chapter.SubjectId && a.UserId == loggedUser.Id && a.IsActive);
            var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == existingModel.Section.Chapter.Id && a.UserId == loggedUser.Id && a.IsActive);
            if (isAttributeAllocated || isAdmin || isChapterAllocated)
            {
                existingModel.Detail = string.Empty;
                await subSectionRepository.UpdateAsync(existingModel);
                await unitOfWork.SaveChangesAsync();
                return new BaseViewModel() { Success = true, Message = "Removed Detail Successfully!!" };
            }
            else
                return new BaseViewModel() { Success = false, Message = "Access Denied!!" };
        }

        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseViewModel> LoadSubSectionDetail(int id)
        {
            var existingModel = subSectionRepository.GetWithChilds(id);
            var loggedUser = await userManager.GetUserAsync(User);
            var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
            var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.Section.Chapter.SubjectId && a.UserId == loggedUser.Id && a.IsActive);
            var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == existingModel.Section.Chapter.Id && a.UserId == loggedUser.Id && a.IsActive);
            if (isAttributeAllocated || isAdmin || isChapterAllocated)
            {
                return new BaseViewModel() { Data = existingModel };
            }
            else
                return new BaseViewModel() { Success = false, Message = "Access Denied!!" };


        }
        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        public async Task<PartialViewResult> LoadSubSectionDetailView(int id)
        {
            var existingModel = subSectionRepository.GetWithChilds(id);
            var loggedUser = await userManager.GetUserAsync(User);
            var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
            var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.Section.Chapter.SubjectId && a.UserId == loggedUser.Id && a.IsActive);
            var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == existingModel.Section.Chapter.Id && a.UserId == loggedUser.Id && a.IsActive);
            if (isAttributeAllocated || isAdmin || isChapterAllocated)
            {

                var attribute = new AttributeViewModel()
                {
                    Id = existingModel.Id,
                    Name = existingModel.Name,
                    Type = AttributeType.SubSection,
                    Detail = existingModel.Detail,
                    TypeText = AttributeType.SubSectionText
                };
                return PartialView("_DescriptionWrapper", attribute);
            }
            else
            {
                return new NotAllowedViewResult();

            }
        }
        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<IActionResult> EditSubSection(int id)
        {
            var subSection = subSectionRepository.GetWithChilds(id);
            if (subSection != null)
            {
                ViewBag.AttributeType = AttributeType.SubSection;
                ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

                var subjectId = subSection.Section.Chapter.SubjectId;
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == subjectId && a.UserId == loggedUser.Id && a.IsActive);

                var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == subSection.Section.Chapter.Id && a.UserId == loggedUser.Id && a.IsActive);
                if (isAttributeAllocated || isAdmin || isChapterAllocated)
                {
                    ViewBag.Attributes = new List<AttributeViewModel>();
                    //ViewBag.Subject = subjectRepository.GetWithChilds(subjectId);
                    ViewBag.QuestionTypes = questionTypeRepository.Get().ToList();
                    ViewBag.QuestionDifficultyLevels = questionDifficultyLevelRepository.Get().ToList();

                    var attribute = new AttributeViewModel()
                    {
                        Id = subSection.Id,
                        Name = subSection.Name,
                        Detail = subSection.Detail,
                        Type = AttributeType.SubSection,
                        TypeText = AttributeType.SubSectionText,
                        ParentId = subSection.SectionId,
                        ParentType = AttributeType.Section
                    };
                    return View("EditDetail", attribute);
                }
                else
                {
                    return new NotAllowedViewResult();

                }
            }
            else
            {
                return new NotFoundViewResult();
            }
        }
        [Authorize]
        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        public async Task<IActionResult> SubSection(int id)
        {
            if (id != 0)
            {
                var subSection = subSectionRepository.GetWithChilds(id);
                if (subSection != null)
                {

                    ViewBag.AttributeType = AttributeType.SubSection;
                    var subjectId = subSection.Section.Chapter.SubjectId;
                    var loggedUser = await userManager.GetUserAsync(User);
                    var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                    var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == subjectId && a.UserId == loggedUser.Id && a.IsActive);
                    var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == subSection.Section.Chapter.Id && a.UserId == loggedUser.Id && a.IsActive);
                    if (isAttributeAllocated || isAdmin || isChapterAllocated)
                    {
                        ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

                        //ViewBag.Subject = subjectRepository.GetWithChilds(subjectId);
                        ViewBag.Attributes = new List<AttributeViewModel>();

                        ViewBag.QuestionTypes = questionTypeRepository.Get().ToList();
                        ViewBag.QuestionDifficultyLevels = questionDifficultyLevelRepository.Get().ToList();

                        var attribute = new AttributeViewModel()
                        {
                            Id = subSection.Id,
                            Name = subSection.Name,
                            Type = AttributeType.SubSection,
                            Detail = subSection.Detail,
                            TypeText = AttributeType.SubSectionText,
                            ParentId = subSection.SectionId,
                            ParentType = AttributeType.Section
                        };


                        if (attribute.Childs==null)
                        {
                            var otherSections = subSectionRepository.Get().Where(x => x.SectionId == subSection.SectionId && x.Id != subSection.Id);
                            var otherSectionsModel = new List<AttributeViewModel>();
                            foreach (var child in otherSections)
                            {
                                var childAttributeModel = new AttributeViewModel()
                                {
                                    Id = child.Id,
                                    Name = child.Name,
                                    Type = AttributeType.SubSection,
                                    TypeText = AttributeType.SubSectionText,
                                };
                                otherSectionsModel.Add(childAttributeModel);
                            }
                            ViewBag.OtherSectionAttributes = otherSectionsModel;
                        }
                        return View("Detail", attribute);
                    }
                    else
                    {
                        return new NotAllowedViewResult();

                    }
                }
                else
                {
                    return new NotFoundViewResult();
                }
            }
            return View("Detail", null);
        }

        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        public async Task<PartialViewResult> SubjectAllocationRequests()
        {

            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                List<SubjectWithParents> userSubjectRequests;
                if (!isAdmin)
                {
                    userSubjectRequests = subjectRepository.ListOfSubjectsRequested(loggedUser.Id);
                }
                else
                {
                    userSubjectRequests = subjectRepository.ListOfSubjectsRequested(string.Empty);
                }

                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(userSubjectRequests);


                return PartialView("Common/_SubjectAllocationRequests", subjectsVModel);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        public async Task<PartialViewResult> ChapterAllocationRequests()
        {

            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                List<ChapterWithParents> userChapterRequests;
                if (!isAdmin)
                {
                    userChapterRequests = chapterRepository.ListOfChaptersRequested(loggedUser.Id);
                }
                else
                {
                    userChapterRequests = chapterRepository.ListOfChaptersRequested(string.Empty);
                }

                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(userChapterRequests);


                return PartialView("Common/_ChapterAllocationRequests", chaptersVModel);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }


        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseModel> SaveSubject(AttributeViewModel model)
        {
            try
            {
                var existingModel = subjectRepository.GetWithChilds(model.Id); ;
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.Id && a.UserId == loggedUser.Id && a.IsActive);
                if (isAttributeAllocated || isAdmin)
                {


                    existingModel.Detail = model.Detail;
                    await subjectRepository.UpdateAsync(existingModel);
                    await unitOfWork.SaveChangesAsync();

                    return BaseModel.Succeeded("Document Saved Successfully!");
                }
                else
                    return BaseModel.Failed("Access Denied!!");


            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Succeeded(ex.Message);

            }

        }

        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseModel> SaveChapter(AttributeViewModel model)
        {
            try
            {
                var existingModel = chapterRepository.GetWithChilds(model.Id);
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.SubjectId && a.UserId == loggedUser.Id && a.IsActive);
                var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == existingModel.Id && a.UserId == loggedUser.Id && a.IsActive);
                if (isAttributeAllocated || isAdmin || isChapterAllocated)
                {
                    existingModel.Detail = model.Detail;
                    await chapterRepository.UpdateAsync(existingModel);
                    await unitOfWork.SaveChangesAsync();

                    return BaseModel.Succeeded("Document Saved Successfully!");
                }
                else
                    return BaseModel.Failed("Access Denied!!");
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Succeeded(ex.Message);

            }
        }

        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseModel> SaveSection(AttributeViewModel model)
        {
            try
            {
                var existingModel = sectionRepository.GetWithChilds(model.Id);
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.Chapter.SubjectId && a.UserId == loggedUser.Id && a.IsActive);
                var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == existingModel.Chapter.Id && a.UserId == loggedUser.Id && a.IsActive);
                if (isAttributeAllocated || isAdmin || isChapterAllocated)
                {
                    existingModel.Detail = model.Detail;
                    await sectionRepository.UpdateAsync(existingModel);
                    await unitOfWork.SaveChangesAsync();

                    return BaseModel.Succeeded("Document Saved Successfully!");
                }
                else
                    return BaseModel.Failed("Access Denied!!");
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Succeeded(ex.Message);

            }
        }

        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseModel> SaveSubSection(AttributeViewModel model)
        {
            try
            {
                var existingModel = subSectionRepository.GetWithChilds(model.Id);
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var isAttributeAllocated = userSubjectRepository.Get().Any(a => a.SubjectId == existingModel.Section.Chapter.SubjectId && a.UserId == loggedUser.Id && a.IsActive);
                var isChapterAllocated = userChapterRepository.Get().Any(a => a.ChapterId == existingModel.Section.Chapter.Id && a.UserId == loggedUser.Id && a.IsActive);
                if (isAttributeAllocated || isAdmin || isChapterAllocated)
                {
                    existingModel.Detail = model.Detail;
                    await subSectionRepository.UpdateAsync(existingModel);
                    await unitOfWork.SaveChangesAsync();

                    return BaseModel.Succeeded("Document Saved Successfully!");
                }
                else
                    return BaseModel.Failed("Access Denied!!");
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Succeeded(ex.Message);

            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseViewModel> AllocateSubject(int subjectId, string userId)
        {

            try
            {
                if (!string.IsNullOrWhiteSpace(userId))
                {
                    var isUserSubjectExist = userSubjectRepository.Get().Where(a => a.UserId == userId && a.SubjectId == subjectId);

                    if (!string.IsNullOrEmpty(userId))
                    {


                        if (!isUserSubjectExist.Any())
                        {
                            await allocateSubjectAsync(userId, subjectId);
                            return new BaseViewModel() { Success = true, Message = "Subject Allocated Successfully!!" };
                        }
                        else
                        {
                            return new BaseViewModel() { Success = false, Message = "This subject is aleady allocated to this user or it is requested by this user, kindly allocate it from Subject Requests page." };
                        }
                    }
                    else
                    {
                        return new BaseViewModel() { Success = false, Message = "You can not allocate subject from here." };
                    }
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "User(Student/Researcher) not selected, go to (Student/Researcher) and allocate subject from there thanks." };
                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseViewModel> AllocateChapter(int chapterId, string userId)
        {

            try
            {
                if (!string.IsNullOrWhiteSpace(userId))
                {
                    var isUserChapterExist = userChapterRepository.Get().Where(a => a.UserId == userId && a.ChapterId == chapterId);


                    if (!isUserChapterExist.Any())
                    {
                        var userChapter = new UserChapter()
                        {
                            UserId = userId,
                            ChapterId = chapterId,
                            IsActive = true
                        };
                        await userChapterRepository.AddAsync(userChapter);
                        await unitOfWork.SaveChangesAsync();
                        return new BaseViewModel() { Success = true, Message = "Chapter Allocated Successfully!!" };
                    }
                    else
                    {
                        return new BaseViewModel() { Success = false, Message = "This chapter is aleady allocated to this user." };
                    }
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "User(Student/Researcher) not selected, go to (Student/Researcher) and allocate chapter from there thanks." };
                }

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }



        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseViewModel> AllocateSubjectRequest(int subjectId, string userId)
        {

            try
            {
                if (!string.IsNullOrWhiteSpace(userId))
                {


                    var userSubjectExist = userSubjectRepository.Get().FirstOrDefault(a => a.UserId == userId && a.SubjectId == subjectId);
                    var userSubjectRequestExist = userSubjectRequestRepository.Get().FirstOrDefault(a => a.UserId == userId && a.SubjectId == subjectId);


                    if (userSubjectExist != null && userSubjectRequestExist != null)
                    {
                        userSubjectExist.IsActive = true;
                        userSubjectExist.UserSubjectRequestId = userSubjectRequestExist.Id;
                        userSubjectRequestExist.StatusId = Convert.ToInt32(RequestsStatus.Processed);

                        await userSubjectRepository.UpdateAsync(userSubjectExist);
                        await userSubjectRequestRepository.UpdateAsync(userSubjectRequestExist);
                        await unitOfWork.SaveChangesAsync();
                        return new BaseViewModel() { Success = true, Message = "Subject Allocated Successfully!!" };
                    }
                    else if (userSubjectExist != null)
                    {
                        userSubjectExist.IsActive = true;

                        await userSubjectRepository.UpdateAsync(userSubjectExist);

                        await unitOfWork.SaveChangesAsync();
                        return new BaseViewModel() { Success = true, Message = "Subject Allocated Successfully!!" };
                    }
                    else
                    {
                        await allocateSubjectAsync(userId, subjectId);
                        return new BaseViewModel() { Success = true, Message = "Subject Allocated Successfully!!" };
                    }
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "User(Student/Researcher) not selected, go to (Student/Researcher) and allocate subject from there thanks." };
                }

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }
        [Authorize(Roles = "Admin")]
        private async Task allocateSubjectAsync(string userId, int subjectId)
        {
            var userSubject = new UserSubject()
            {
                UserId = userId,
                SubjectId = subjectId,
                IsActive = true
            };
            await userSubjectRepository.AddAsync(userSubject);
            await unitOfWork.SaveChangesAsync();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseViewModel> AllocateChapterRequest(int chapterId, string userId)
        {

            try
            {
                if (!string.IsNullOrWhiteSpace(userId))
                {

                    var userChapterExist = userChapterRepository.Get().FirstOrDefault(a => a.UserId == userId && a.ChapterId == chapterId);
                    var userChapterRequestExist = userChapterRequestRepository.Get().FirstOrDefault(a => a.UserId == userId && a.ChapterId == chapterId);


                    if (userChapterExist != null && userChapterRequestExist != null)
                    {
                        userChapterExist.IsActive = true;
                        userChapterExist.UserChapterRequestId = userChapterRequestExist.Id;
                        userChapterRequestExist.StatusId = Convert.ToInt32(RequestsStatus.Processed);

                        await userChapterRepository.UpdateAsync(userChapterExist);
                        await userChapterRequestRepository.UpdateAsync(userChapterRequestExist);
                        await unitOfWork.SaveChangesAsync();
                        return new BaseViewModel() { Success = true, Message = "Chapter Allocated Successfully!!" };
                    }
                    else if (userChapterExist != null)
                    {
                        userChapterExist.IsActive = true;

                        await userChapterRepository.UpdateAsync(userChapterExist);

                        await unitOfWork.SaveChangesAsync();
                        return new BaseViewModel() { Success = true, Message = "Chapter Allocated Successfully!!" };
                    }
                    else
                    {
                        await allocateChapterAsync(userId, chapterId);
                        return new BaseViewModel() { Success = true, Message = "Chapter Allocated Successfully!!" };
                    }
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "User(Student/Researcher) not selected, go to (Student/Researcher) and allocate chapter from there thanks." };
                }

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }
        [Authorize(Roles = "Admin")]
        private async Task allocateChapterAsync(string userId, int chapterId)
        {
            var userChapter = new UserChapter()
            {
                UserId = userId,
                ChapterId = chapterId,
                IsActive = true
            };
            await userChapterRepository.AddAsync(userChapter);
            await unitOfWork.SaveChangesAsync();
        }


        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        [HttpPost]
        public async Task<BaseViewModel> RequestAllocateSubject(int subjectId)
        {

            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var isUserSubjectExist = userSubjectRepository.Get().FirstOrDefault(a => a.UserId == loggedUser.Id && a.SubjectId == subjectId);


                if (isUserSubjectExist == null)
                {
                    var userSubjectRequest = new UserSubjectRequest()
                    {
                        StatusId = Convert.ToInt32(RequestsStatus.Pending),
                        UserId = loggedUser.Id,
                        SubjectId = subjectId,
                        RequestDate = DateTime.Now
                    };
                    await userSubjectRequestRepository.AddAsync(userSubjectRequest);
                    await unitOfWork.SaveChangesAsync();

                    var userSubject = new UserSubject()
                    {
                        UserId = loggedUser.Id,
                        SubjectId = subjectId,
                        UserSubjectRequestId = userSubjectRequest.Id
                    };
                    await userSubjectRepository.AddAsync(userSubject);
                    await unitOfWork.SaveChangesAsync();

                    return new BaseViewModel() { Success = true, Message = "Request Sent Successfully!!" };
                }
                else if (isUserSubjectExist != null && !isUserSubjectExist.IsActive)
                {
                    var isUserSubjectRequestExist = userSubjectRequestRepository.Get().FirstOrDefault(a => a.UserId == loggedUser.Id && a.SubjectId == subjectId);


                    if (isUserSubjectRequestExist != null)
                    {
                        isUserSubjectRequestExist.StatusId = Convert.ToInt32(RequestsStatus.Pending);
                        isUserSubjectRequestExist.UserId = loggedUser.Id;
                        isUserSubjectRequestExist.SubjectId = subjectId;
                        isUserSubjectRequestExist.RequestDate = DateTime.Now;

                        isUserSubjectExist.UserSubjectRequestId = isUserSubjectRequestExist.Id;

                        await userSubjectRequestRepository.UpdateAsync(isUserSubjectRequestExist);
                        await userSubjectRepository.UpdateAsync(isUserSubjectExist);
                        await unitOfWork.SaveChangesAsync();
                    }
                    else
                    {
                        var userSubjectRequest = new UserSubjectRequest()
                        {
                            StatusId = Convert.ToInt32(RequestsStatus.Pending),
                            UserId = loggedUser.Id,
                            SubjectId = subjectId,
                            RequestDate = DateTime.Now
                        };
                        await userSubjectRequestRepository.AddAsync(userSubjectRequest);
                        await unitOfWork.SaveChangesAsync();

                        isUserSubjectExist.UserSubjectRequestId = userSubjectRequest.Id;
                        await userSubjectRepository.AddAsync(isUserSubjectExist);
                        await unitOfWork.SaveChangesAsync();
                    }


                    return new BaseViewModel() { Success = true, Message = "Request Sent Successfully!!" };
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "This subject is aleady allocated to you." };
                }

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }

        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        [HttpPost]
        public async Task<BaseViewModel> RequestAllocateChapter(int chapterId)
        {

            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var isUserChapterExist = userChapterRepository.Get().FirstOrDefault(a => a.UserId == loggedUser.Id && a.ChapterId == chapterId);


                if (isUserChapterExist == null)
                {


                    var userChapterRequest = new UserChapterRequest()
                    {
                        StatusId = Convert.ToInt32(RequestsStatus.Pending),
                        UserId = loggedUser.Id,
                        ChapterId = chapterId,
                        RequestDate = DateTime.Now
                    };
                    await userChapterRequestRepository.AddAsync(userChapterRequest);
                    await unitOfWork.SaveChangesAsync();

                    var userChapter = new UserChapter()
                    {
                        UserId = loggedUser.Id,
                        ChapterId = chapterId,
                        UserChapterRequestId = userChapterRequest.Id
                    };
                    await userChapterRepository.AddAsync(userChapter);
                    await unitOfWork.SaveChangesAsync();

                    return new BaseViewModel() { Success = true, Message = "Request Sent Successfully!!" };
                }
                else if (isUserChapterExist != null && !isUserChapterExist.IsActive)
                {
                    var isUserChapterRequestExist = userChapterRequestRepository.Get().FirstOrDefault(a => a.UserId == loggedUser.Id && a.ChapterId == chapterId);

                    if (isUserChapterRequestExist != null)
                    {
                        isUserChapterRequestExist.StatusId = Convert.ToInt32(RequestsStatus.Pending);
                        isUserChapterRequestExist.UserId = loggedUser.Id;
                        isUserChapterRequestExist.ChapterId = chapterId;
                        isUserChapterRequestExist.RequestDate = DateTime.Now;

                        isUserChapterExist.UserChapterRequestId = isUserChapterRequestExist.Id;

                        await userChapterRepository.UpdateAsync(isUserChapterExist);
                        await userChapterRequestRepository.UpdateAsync(isUserChapterRequestExist);
                        await unitOfWork.SaveChangesAsync();
                    }
                    else
                    {
                        var userChapterRequest = new UserChapterRequest()
                        {
                            StatusId = Convert.ToInt32(RequestsStatus.Pending),
                            UserId = loggedUser.Id,
                            ChapterId = chapterId,
                            RequestDate = DateTime.Now
                        };
                        await userChapterRequestRepository.AddAsync(userChapterRequest);
                        await unitOfWork.SaveChangesAsync();

                        isUserChapterExist.UserChapterRequestId = isUserChapterExist.Id;
                        await userChapterRepository.AddAsync(isUserChapterExist);
                        await unitOfWork.SaveChangesAsync();
                    }


                    return new BaseViewModel() { Success = true, Message = "Request Sent Successfully!!" };
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "This chapter is aleady allocated to you." };
                }

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }


        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseViewModel> DeallocateSubject(int subjectId, string userId)
        {

            try
            {

                var isUserSubjectExist = userSubjectRepository.Get().FirstOrDefault(a => a.UserId == userId && a.SubjectId == subjectId);


                if (isUserSubjectExist != null)
                {
                    isUserSubjectExist.IsActive = false;
                    await userSubjectRepository.UpdateAsync(isUserSubjectExist);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseViewModel() { Success = true, Message = "Subject Deallocated Successfully!!" };
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "No such subject exist or may be it is already deallocated." };
                }

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };

            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseViewModel> DeallocateChapter(int chapterId, string userId)
        {

            try
            {

                var isUserChapterExist = userChapterRepository.Get().FirstOrDefault(a => a.UserId == userId && a.ChapterId == chapterId && a.IsActive);

                if (isUserChapterExist != null)
                {
                    isUserChapterExist.IsActive = false;
                    await userChapterRepository.UpdateAsync(isUserChapterExist);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseViewModel() { Success = true, Message = "Chapter Deallocated Successfully!!" };
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "No such chapter exist or may be it is already deallocated." };
                }

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };

            }
        }

        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteAllocatedSubjectRequest(int subjectId, string userId)
        {

            try
            {

                var isUserSubjectRequestExist = userSubjectRequestRepository.Get().FirstOrDefault(a => a.UserId == userId && a.SubjectId == subjectId);
                var isUserSubjectExist = userSubjectRepository.Get().FirstOrDefault(a => a.UserId == userId && a.SubjectId == subjectId);

                if (isUserSubjectRequestExist != null && isUserSubjectExist != null)
                {
                    await userSubjectRepository.DeleteAsync(isUserSubjectExist);
                    await userSubjectRequestRepository.DeleteAsync(isUserSubjectRequestExist);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseViewModel() { Success = true, Message = "Request Deleted Successfully!!" };
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "No such request deleted exist or may be it is already deleted." };
                }

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };

            }
        }
        [Authorize(Roles = "Admin,Researcher,Teacher,Student")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteAllocatedChapterRequest(int chapterId, string userId)
        {

            try
            {

                var isUserChapterRequestExist = userChapterRequestRepository.Get().FirstOrDefault(a => a.UserId == userId && a.ChapterId == chapterId);
                var isUserChapterExist = userChapterRepository.Get().FirstOrDefault(a => a.UserId == userId && a.ChapterId == chapterId);

                if (isUserChapterRequestExist != null && isUserChapterExist != null)
                {
                    await userChapterRepository.DeleteAsync(isUserChapterExist);
                    await userChapterRequestRepository.DeleteAsync(isUserChapterRequestExist);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseViewModel() { Success = true, Message = "Request Deleted Successfully!!" };
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "No such request deleted exist or may be it is already deleted." };
                }

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };

            }
        }
        [Authorize(Roles = "Student,Researcher,Teacher,Teacher")]
        public async Task<PartialViewResult> MyAllocatedSubjects()
        {

            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                //var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(loggedUser.Id, true);

                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);
                //var student = new UserViewModel();
                //student.AllocatedSubjects = subjectsVModel;
                //ViewBag.Subjects = subjectsVModel;
                return PartialView("Common/AllocatedSubjects", subjectsVModel);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }
        [Authorize(Roles = "Student,Researcher,Teacher,Teacher")]
        public async Task<PartialViewResult> MyAllocatedChapters()
        {

            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                //var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(loggedUser.Id, true);

                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);
                //var student = new UserViewModel();
                //student.AllocatedChapters = chaptersVModel;
                //ViewBag.Chapters = chaptersVModel;
                return PartialView("Common/AllocatedChapters", chaptersVModel);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Student,Researcher,Teacher,Teacher")]
        public async Task<IActionResult> AllocatedSubjects()
        {

            try
            {

                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(loggedUser.Id, isAdmin);

                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);
                var student = new UserViewModel();
                student.AllocatedSubjects = subjectsVModel;
                ViewBag.Subjects = subjectsVModel;
                ViewBag.Student = student;
                return View(subjectsVModel);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Student,Researcher,Teacher,Teacher")]
        public async Task<IActionResult> AllocatedChapters()
        {

            try
            {
                ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(loggedUser.Id, isAdmin);

                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);
                var student = new UserViewModel();
                student.AllocatedChapters = chaptersVModel;
                ViewBag.Chapters = chaptersVModel;
                ViewBag.Student = student;
                return View(chaptersVModel);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Student,Researcher,Teacher,Teacher")]
        public async Task<IActionResult> AllocatedSubjectsByUserId(string id)
        {

            try
            {

                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(id, isAdmin);

                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);

                return View("~/Views/Shared/Common/AllocatedSubjects.cshtml", subjectsVModel);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Student,Researcher,Teacher,Teacher")]
        public async Task<IActionResult> AllocatedChaptersByUserId(string id)
        {

            try
            {

                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(id, isAdmin);

                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);

                return View("~/Views/Shared/Common/AllocatedChapters.cshtml", chaptersVModel);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }



        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<Word> AddWord(string wordText)
        {
            try
            {

                var word = new Word()
                {
                    Detail = wordText

                };
                await wordRepository.AddAsync(word);
                await unitOfWork.SaveChangesAsync();


                return word;
            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        //[Authorize(Roles = "Admin,Researcher,Teacher")]
        //public string ConvertToHtml(string fileName)
        //{

        //    SautinSoft.UseOffice u = new SautinSoft.UseOffice();
        //    var webRoot = env.WebRootPath + "\\UploadedArtifacts";
        //    var filePath = System.IO.Path.Combine(webRoot, fileName);
        //    var fileExtension = System.IO.Path.GetExtension(fileName);

        //    //Path to any local file
        //    string inputFilePath = filePath;
        //    //Path to output resulted file
        //    var outputFileName = DateTime.Now.ToString("MM_dd_yyyy_hh__mm_ss") + "_" + DateTime.Now.Millisecond + ".html";

        //    if (fileExtension == FileExtension.Pdf)
        //    {
        //        outputFileName = DateTime.Now.ToString("MM_dd_yyyy_hh__mm_ss") + "_" + DateTime.Now.Millisecond;
        //    }


        //    string outputFilePath = env.WebRootPath + "\\ConvertedHtmls\\" + outputFileName;

        //    //Prepare UseOffice .Net, loads MS Word in memory
        //    int ret = u.InitWord();

        //    //Return values:
        //    //0 - Loading successfully
        //    //1 - Can't load MS Word® library in memory 

        //    if (ret == 1)
        //        return string.Empty;

        //    //Converting
        //    if (fileExtension == FileExtension.Pdf)
        //    {
        //        ret = u.ConvertFile(inputFilePath, outputFilePath + ".docx", SautinSoft.UseOffice.eDirection.PDF_to_DOCX);

        //        ret = u.ConvertFile(outputFilePath + ".docx", outputFilePath + ".html", SautinSoft.UseOffice.eDirection.DOCX_to_HTML);
        //        outputFilePath = outputFilePath + ".html";
        //    }
        //    else if (fileExtension == FileExtension.Ppt)
        //    {
        //        ret = u.ConvertFile(inputFilePath, outputFilePath, SautinSoft.UseOffice.eDirection.PPT_to_HTML);
        //    }
        //    else if (fileExtension == FileExtension.Pptx)
        //    {
        //        ret = u.ConvertFile(inputFilePath, outputFilePath, SautinSoft.UseOffice.eDirection.PPTX_to_HTML);
        //    }
        //    else if (fileExtension == FileExtension.Doc)
        //    {
        //        ret = u.ConvertFile(inputFilePath, outputFilePath, SautinSoft.UseOffice.eDirection.DOC_to_HTML);
        //    }
        //    else if (fileExtension == FileExtension.Docx)
        //    {
        //        ret = u.ConvertFile(inputFilePath, outputFilePath, SautinSoft.UseOffice.eDirection.DOCX_to_HTML);
        //    }
        //    else if (fileExtension == FileExtension.Xls)
        //    {
        //        ret = u.ConvertFile(inputFilePath, outputFilePath, SautinSoft.UseOffice.eDirection.XLS_to_HTML);
        //    }
        //    else if (fileExtension == FileExtension.Xlsx)
        //    {
        //        ret = u.ConvertFile(inputFilePath, outputFilePath, SautinSoft.UseOffice.eDirection.XLSX_to_HTML);
        //    }
        //    if (ret != 0)
        //    {
        //        var text = "";
        //        switch (ret)
        //        {
        //            case 1:
        //                text = "Can't open input file. Check that you are using full local path to input file, URL and relative path are not supported";
        //                emailService.SendException("The Doc To Html not working here is a return text<br> " + text);
        //                break;
        //            case 2:
        //                text = "Can't create output file. Please check that you have permissions to write by this path or probably this path already used by another application";
        //                emailService.SendException("The Doc To Html not working here is a return text<br> " + text);
        //                break;
        //            case 3:
        //                text = "Converting failed, please contact with our Support Team";
        //                emailService.SendException("The Doc To Html not working here is a return text<br> " + text);
        //                break;
        //            case 4:
        //                text = "MS Office isn't installed. The component requires that any of these versions of MS Office should be installed: 2000, XP, 2003, 2007 or 2010";
        //                emailService.SendException("The Doc To Html not working here is a return text<br> " + text);
        //                break;

        //            default:
        //                break;
        //        }

        //    }

        //    string html = System.IO.File.ReadAllText(outputFilePath, Encoding.UTF8);


        //    //Release MS Word from memory
        //    u.CloseWord();

        //    //0 - Converting successfully
        //    //1 - Can't open input file. Check that you are using full local path to input file, URL and relative path are not supported
        //    //2 - Can't create output file. Please check that you have permissions to write by this path or probably this path already used by another application
        //    //3 - Converting failed, please contact with our Support Team
        //    //4 - MS Office isn't installed. The component requires that any of these versions of MS Office should be installed: 2000, XP, 2003, 2007 or 2010



        //    return html;
        //}

        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseModel> SaveQuestion(AttributeViewModel model)
        {
            try
            {

                var question = questionRepository.Get().FirstOrDefault(a => a.Id == Convert.ToInt32(model.Id));
                question.Detail = model.Detail;
                question.LastUpdated = DateTime.Now;

                await questionRepository.UpdateAsync(question);
                await unitOfWork.SaveChangesAsync();

                return new BaseModel() { Success = true, Message = "Question Updated Successfully!" };

            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseModel> SaveAnswer(AttributeViewModel model)
        {
            try
            {

                var answer = correctAnswerRepository.Get().FirstOrDefault(a => a.QuestionId == Convert.ToInt32(model.Id));
                answer.Answer = model.Detail;

                await correctAnswerRepository.UpdateAsync(answer);
                await unitOfWork.SaveChangesAsync();


                return new BaseModel() { Success = true, Message = "Answer Updated Successfully!" };

            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher,Teacher")]
        public async Task<BaseModel> SaveCommentary(AttributeViewModel model)
        {
            try
            {
                var commentary = commentaryRepository.Get().FirstOrDefault(a => a.Id == Convert.ToInt32(model.Id));

                commentary.Detail = model.Detail;
                commentary.LastUpdated = DateTime.Now;

                await commentaryRepository.UpdateAsync(commentary);
                await unitOfWork.SaveChangesAsync();

                return new BaseModel() { Success = true, Message = "Commentary Updated Successfully!" };

            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin")]
        public async Task<BaseModel> EmailAllocatedMember(int attributeId,string attributeType)
        {
            try
            {
                var users=new List<User>();
                AttributeModel attribute;
                if (attributeType== AttributeType.Subject)
                {
                    var attr = subjectRepository.Get().FirstOrDefault(x => x.Id==attributeId);
                    attribute = new AttributeModel()
                    {
                        TypeText= AttributeType.SubjectText,
                        Type= AttributeType.Subject,
                        Name = attr.Name
                    };
                    var allocatedUsers= userSubjectRepository.Get().Where(a => a.Id == attributeId).Select(x => x.User).ToList();
                    users.AddRange(allocatedUsers);
                }
                else if (attributeType == AttributeType.Chapter)
                {
                    var attr = chapterRepository.GetWithChilds(attributeId);
                    attribute = new AttributeModel()
                    {
                        TypeText = AttributeType.ChapterText,
                        Type= AttributeType.Chapter,
                        Name = attr.Name
                    };
                    var chapterAllocatedUsers = userChapterRepository.Get().Where(a => a.Id == attributeId).Select(x => x.User).ToList();
                    var subjectAllocatedUsers = userSubjectRepository.Get().Where(a => a.Id == attr.SubjectId).Select(x => x.User).ToList();
                    users.AddRange(chapterAllocatedUsers);
                    users.AddRange(subjectAllocatedUsers);

                }
                else if (attributeType == AttributeType.Section)
                {
                    var attr = sectionRepository.GetWithChilds(attributeId);
                    attribute = new AttributeModel()
                    {
                        TypeText = AttributeType.SectionText,
                        Type= AttributeType.Section,
                        Name = attr.Name
                    };
                    var chapterAllocatedUsers = userChapterRepository.Get().Where(a => a.Id == attr.ChapterId).Select(x => x.User).ToList();
                    var subjectAllocatedUsers = userSubjectRepository.Get().Where(a => a.Id == attr.Chapter.SubjectId).Select(x => x.User).ToList();
                    users.AddRange(chapterAllocatedUsers);
                    users.AddRange(subjectAllocatedUsers);

                }
                else
                {
                    var attr = subSectionRepository.GetWithChilds(attributeId);
                    attribute = new AttributeModel()
                    {
                        TypeText = AttributeType.SubSectionText,
                        Type= AttributeType.SubSection,
                        Name = attr.Name
                    };
                    var chapterAllocatedUsers = userChapterRepository.Get().Where(a => a.Id == attr.Section.ChapterId).Select(x => x.User).ToList();
                    var subjectAllocatedUsers = userSubjectRepository.Get().Where(a => a.Id == attr.Section.Chapter.SubjectId).Select(x => x.User).ToList();
                    users.AddRange(chapterAllocatedUsers);
                    users.AddRange(subjectAllocatedUsers);

                }
               
                var informAllocatedMemberModel = new InformAllocatedMemberModel();
                informAllocatedMemberModel.Users = users;
                informAllocatedMemberModel.Attribute = attribute;

               var isEmailSent= emailService.InformAllocatedMember(informAllocatedMemberModel);
                if (isEmailSent)
                {
                    return new BaseModel() { Success = true, Message = "All users are infromed regarding "+ attribute.TypeText + " content update!" };

                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Email not sent due to unknown reason!" };

                }

            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }

    }
}