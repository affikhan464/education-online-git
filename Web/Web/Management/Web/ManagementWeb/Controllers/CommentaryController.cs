﻿using AutoMapper;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Infrastructure.Communication;
using Management.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class CommentaryController : Controller
    {
        private readonly ISubjectRepository subjectRepository;
        private readonly IChapterRepository chapterRepository;
        private readonly IUnitOfWork unitOfWork;
        private IHostingEnvironment env;
        private readonly ISectionRepository sectionRepository;
        private readonly ISubSectionRepository subSectionRepository;
        private readonly ICommentaryRepository commentaryRepository;
        private readonly ICorrectAnswerRepository correctAnswerRepository;
        private readonly IWordRepository wordRepository;
        private readonly IWordCommentaryRepository wordCommentaryRepository;
        private readonly IEmailService emailService;

        public CommentaryController(IWordRepository _wordRepository, IWordCommentaryRepository _wordCommentaryRepository, ICorrectAnswerRepository _correctAnswerRepository, ICommentaryRepository _commentaryRepository, IEmailService _emailService, IHostingEnvironment _env, ISubjectRepository _subjectRepository, IChapterRepository _chapterRepository, ISectionRepository _sectionRepository, ISubSectionRepository _subSectionRepository, IUnitOfWork _unitOfWork)
        {
            subjectRepository = _subjectRepository;
            chapterRepository = _chapterRepository;
            unitOfWork = _unitOfWork;
            sectionRepository = _sectionRepository;
            emailService = _emailService;
            subSectionRepository = _subSectionRepository;
            correctAnswerRepository = _correctAnswerRepository;
            commentaryRepository = _commentaryRepository;
            wordRepository = _wordRepository;
            wordCommentaryRepository = _wordCommentaryRepository;

            env = _env;
        }
        public IActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = "Admin,Researcher,Student")]
        public PartialViewResult GetCommentaries(int wordId)
        {

            ViewBag.WordId = wordId;
            var model = LoadCommentariesList( wordId);
            return PartialView("_ExistingCommentariesList", model);
        }

        [Authorize(Roles = "Admin,Researcher")]
        public PartialViewResult Edit(int id)
        {
            var commentary = commentaryRepository.GetCommentary(id);
            var model = Mapper.Map<EditCommentaryViewModel>(commentary);

            return PartialView("_EditCommentary", model);
        }
        [Authorize(Roles = "Admin,Researcher")]
        public PartialViewResult GetAvailableCommentaries(int attributeId, string attributeType, int wordId)
        {
            var model = LoadAvailableCommentariesList(attributeId, attributeType, wordId);
            return PartialView("_AvailableCommentariesList", model);
        }
        [Authorize(Roles = "Admin,Researcher,Student")]
        private List<CommentaryViewModel> LoadCommentariesList(int wordId)
        {
            if (wordId != 0)
            {
                var commentary = commentaryRepository.GetManyCommentaries(wordId);
                var model = Mapper.Map<List<CommentaryViewModel>>(commentary);
                return model;
            }
            else
            {
                var model = new List<CommentaryViewModel>();
                return model;

            }

        }
        [Authorize(Roles = "Admin,Researcher,Student")]
        private List<CommentaryViewModel> LoadAvailableCommentariesList(int attributeId, string attributeType, int wordId)
        {

            var commentary = commentaryRepository.GetManyAvailableCommentaries(attributeId.ToString(), attributeType, 0);

            if (wordId != 0)
            {
                var listWordCommentaryIds = wordCommentaryRepository.Get().Where(a => a.WordId == wordId).Select(s => s.CommentaryId).ToList();
                commentary = commentary.Where(a => !listWordCommentaryIds.Contains(a.Id)).ToList();
            }



            var model = Mapper.Map<List<CommentaryViewModel>>(commentary);
            return model;
        }
        [Authorize(Roles = "Admin,Researcher,Student")]
        public PartialViewResult Get(string id)
        {
            var commentary = commentaryRepository.GetCommentary(Convert.ToInt32(id));
            var model = Mapper.Map<CommentaryViewModel>(commentary);

            return PartialView("_Detail", model);
        }
        [Authorize(Roles = "Admin,Researcher,Student")]
        public CommentaryViewModel GetCommentary(string id)
        {
            var commentary = commentaryRepository.GetCommentary(Convert.ToInt32(id));
            var model = Mapper.Map<CommentaryViewModel>(commentary);

            return model;
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> AddAvailableCommentary(CommentaryViewModel model)
        {
            try
            {
                if (model.Ids != string.Empty)
                {
                    var commentary = await AddExistingCommentaryAsync(model);

                    if (commentary != null)
                        return new BaseModel() { Success = true, Message = "Commentary Added Successfully!", Data = new { CommentaryIds = commentary.Ids, AttributeType = commentary.AttributeType, WordId = commentary.WordId } };
                    else
                        return BaseModel.Failed("There is an error ");
                }
                else
                {


                    return BaseModel.Failed("There is an error or you may not entered any commentary or commentary title.");

                }
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }

        }

        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> AddSubjectCommentary(CommentaryViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var commentary = await AddCommentaryAsync(AttributeType.Subject, model);

                    if (commentary != null)
                        return new BaseModel() { Success = true, Message = "Commentary Added Successfully!", Data = new { CommentaryIds = commentary.Ids, AttributeType = AttributeType.Subject, WordId = commentary.WordId } };
                    else
                        return BaseModel.Failed("There is an error ");
                }
                else
                {


                    return BaseModel.Failed("There is an error or you may not entered any commentary or commentary title.");

                }
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }

        }

        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> AddChapterCommentary(CommentaryViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var commentary = await AddCommentaryAsync(AttributeType.Chapter, model);
                    if (commentary != null)
                        return new BaseModel() { Success = true, Message = "Commentary Added Successfully!", Data = new { CommentaryIds = commentary.Ids, AttributeType = AttributeType.Chapter, WordId = commentary.WordId } };
                    else
                        return BaseModel.Failed("There is an error ");
                }
                else
                {


                    return BaseModel.Failed("There is an error or you may not entered any commentary or commentary title.");

                }
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }

        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> AddSectionCommentary(CommentaryViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var commentary = await AddCommentaryAsync(AttributeType.Section, model);
                    if (commentary != null)
                        return new BaseModel() { Success = true, Message = "Commentary Added Successfully!", Data = new { CommentaryIds = commentary.Ids, AttributeType = AttributeType.Section, WordId = commentary.WordId } };
                    else
                        return BaseModel.Failed("There is an error ");
                }
                else
                {


                    return BaseModel.Failed("There is an error or you may not entered any commentary or commentary title.");

                }
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }

        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> AddSubSectionCommentary(CommentaryViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var commentary = await AddCommentaryAsync(AttributeType.SubSection, model);
                    if (commentary != null)
                        return new BaseModel() { Success = true, Message = "Commentary Added Successfully!", Data = new { CommentaryIds = commentary.Ids, AttributeType = AttributeType.SubSection, WordId = commentary.WordId } };
                    else
                        return BaseModel.Failed("There is an error ");
                }
                else
                {


                    return BaseModel.Failed("There is an error or you may not entered any commentary or commentary title.");

                }
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }

        }

        [Authorize(Roles = "Admin,Researcher")]
        private async Task<CommentaryViewModel> AddExistingCommentaryAsync(CommentaryViewModel model)
        {
            try
            {
                var listIds = model.Ids.Replace(",,", ",").Trim(',').Split(",");

                var attributeType = "";
                foreach (var id in listIds)
                {
                    if (id != string.Empty)
                    {


                        var commentary = commentaryRepository.Get().FirstOrDefault(a => a.Id == Convert.ToInt32(id));
                        attributeType = commentary.AttributeType;
                        if (commentary != null)
                        {


                            if (model.WordId == 0)
                            {
                                var word = new Word()
                                {
                                    Detail = model.SelectedWord

                                };
                                await wordRepository.AddAsync(word);
                                await unitOfWork.SaveChangesAsync();
                                model.WordId = word.Id;
                            }





                            var wordCommentary = new WordCommentary()
                            {
                                WordId = model.WordId,
                                CommentaryId = commentary.Id
                            };

                            await wordCommentaryRepository.AddAsync(wordCommentary);
                            await unitOfWork.SaveChangesAsync();


                        }

                    }
                }
                var idsAgainstThisWord = wordCommentaryRepository.Get().Where(a => a.WordId == model.WordId).DistinctBy(s => s.CommentaryId).Select(a => a.CommentaryId).ToList();
                var idsString = string.Join(",", idsAgainstThisWord);

                var qModel = new CommentaryViewModel()
                {
                    Ids = idsString,
                    WordId = model.WordId,
                    AttributeType = attributeType
                };
                return qModel;
            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Researcher")]
        private async Task<CommentaryViewModel> AddCommentaryAsync(string attributeType, CommentaryViewModel model)
        {
            try
            {


                var commentary = new Commentary()
                {
                    Date = DateTime.Now,
                    LastUpdated = DateTime.Now,
                    CommentaryTitle = model.CommentaryTitle,
                    Detail = model.CommentaryDetail,
                    AttributeType = attributeType,
                    AttributeId = model.AttributeId

                };
                await commentaryRepository.AddAsync(commentary);
                await unitOfWork.SaveChangesAsync();

                Word word;
                if (model.WordId == 0)
                {
                    word = new Word()
                    {
                        Detail = model.SelectedWord

                    };
                    await wordRepository.AddAsync(word);
                    await unitOfWork.SaveChangesAsync();
                    model.WordId = word.Id;
                }
                else
                {
                    word = new Word()
                    {
                        Id = model.WordId
                    };
                }



                var wordCommentary = new WordCommentary()
                {
                    WordId = word.Id,
                    CommentaryId = commentary.Id
                };


                await wordCommentaryRepository.AddAsync(wordCommentary);
                await unitOfWork.SaveChangesAsync();

                var idsAgainstThisWord = wordCommentaryRepository.Get().Where(a => a.WordId == word.Id).Select(a => a.CommentaryId).ToList();
                var idsString = string.Join(",", idsAgainstThisWord);

                var commentaryModel = new CommentaryViewModel()
                {
                    Ids = idsString,
                    WordId = word.Id,
                    AttributeType = attributeType
                };

                return commentaryModel;
            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> UpdateCommentaryAsync(EditCommentaryViewModel model)
        {
            try
            {

                var commentary = commentaryRepository.Get().FirstOrDefault(a => a.Id == Convert.ToInt32(model.Id));
                commentary.CommentaryTitle = model.EditCommentaryTitle;
                commentary.Detail = model.EditCommentaryDetail;
                commentary.LastUpdated = DateTime.Now;


                await commentaryRepository.UpdateAsync(commentary);
                await unitOfWork.SaveChangesAsync();

                return new BaseModel() { Success = true, Message = "Commentary Updated Successfully!" };

            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> Remove(int id, int wordId)
        {
            try
            {

                var wordCommentary = wordCommentaryRepository.Get().FirstOrDefault(a => a.CommentaryId == id && a.WordId == wordId);
                if (wordCommentary != null)
                {
                    await wordCommentaryRepository.DeleteAsync(wordCommentary);
                    await unitOfWork.SaveChangesAsync();

                    var idsAgainstThisWord = wordCommentaryRepository.Get().Where(a => a.WordId == wordId).Select(a => a.CommentaryId).ToList();
                    var idsString = string.Empty;
                    if (idsAgainstThisWord.Count > 0)
                    {
                        idsString = string.Join(",", idsAgainstThisWord);

                    }
                    var qModel = new CommentaryViewModel()
                    {
                        Ids = idsString,
                        WordId = wordId
                    };


                    return new BaseModel() { Success = true, Message = "Commentary Removed Successfully!", Data = qModel };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "No Commentary against this word/sentence or it is Removed, refresh your page please!!" };
                }




            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> Delete(int id)
        {
            try
            {
                var wordQuestion = wordCommentaryRepository.Get().FirstOrDefault(a => a.CommentaryId == id);
                var question = commentaryRepository.Get().FirstOrDefault(a => a.Id == id);
                if (wordQuestion == null)
                {
                    if (question!=null)
                    {
                        await commentaryRepository.DeleteAsync(question);
                        await unitOfWork.SaveChangesAsync();
                        return new BaseModel() { Success = true, Message = "Commentary Deleted Successfully!!" };
                    }
                    else
                    {
                        return new BaseModel() { Success = false, Message = "No such commentary exist or it has been deleted already." };

                    }
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "You can't delete this commentary as it is assigned to some word/sentence." };
                }
            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
    }
}