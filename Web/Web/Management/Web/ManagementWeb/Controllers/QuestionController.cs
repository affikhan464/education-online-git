﻿using AutoMapper;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Infrastructure.Communication;
using Management.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class QuestionController : Controller
    {
        private readonly ISubjectRepository subjectRepository;
        private readonly IChapterRepository chapterRepository;
        private readonly IUnitOfWork unitOfWork;
        private IHostingEnvironment env;
        private readonly ISectionRepository sectionRepository;
        private readonly ISubSectionRepository subSectionRepository;
        private readonly IQuestionRepository questionRepository;
        private readonly ICorrectAnswerRepository correctAnswerRepository;
        private readonly IWordRepository wordRepository;
        private readonly IWordQuestionRepository wordQuestionRepository;
        private readonly IEmailService emailService;
        private readonly IQuestionTypeRepository questionTypeRepository;
        private readonly IQuestionDifficultyLevelRepository questionDifficultyLevelRepository;
        private readonly IQuestionChoiceRepository questionChoiceRepository;
        private readonly IAnswerRepository answerRepository;
        private readonly IAdviceRepository adviceRepository;
        private readonly IMTQAnsweredChoiceRepository mTQAnsweredChoiceRepository;
        public QuestionController(IMTQAnsweredChoiceRepository _mTQAnsweredChoiceRepository, IAdviceRepository _adviceRepository, IAnswerRepository _answerRepository, IQuestionChoiceRepository _questionChoiceRepository, IQuestionDifficultyLevelRepository _questionDifficultyLevelRepository, IQuestionTypeRepository _questionTypeRepository, IWordRepository _wordRepository, IWordQuestionRepository _wordQuestionRepository, ICorrectAnswerRepository _correctAnswerRepository, IQuestionRepository _questionRepository, IEmailService _emailService, IHostingEnvironment _env, ISubjectRepository _subjectRepository, IChapterRepository _chapterRepository, ISectionRepository _sectionRepository, ISubSectionRepository _subSectionRepository, IUnitOfWork _unitOfWork)
        {
            subjectRepository = _subjectRepository;
            chapterRepository = _chapterRepository;
            unitOfWork = _unitOfWork;
            sectionRepository = _sectionRepository;
            emailService = _emailService;
            subSectionRepository = _subSectionRepository;
            correctAnswerRepository = _correctAnswerRepository;
            questionRepository = _questionRepository;
            wordRepository = _wordRepository;
            wordQuestionRepository = _wordQuestionRepository;
            questionTypeRepository = _questionTypeRepository;
            questionDifficultyLevelRepository = _questionDifficultyLevelRepository;
            answerRepository = _answerRepository;
            questionChoiceRepository = _questionChoiceRepository;
            adviceRepository = _adviceRepository;
            env = _env;
            mTQAnsweredChoiceRepository = _mTQAnsweredChoiceRepository;
        }
        public IActionResult Index()
        {
            return View();
        }
        [Authorize(Roles = "Admin,Researcher,Student")]
        public PartialViewResult GetQuestions(string ids, int wordId)
        {

            ViewBag.WordId = wordId;
            var model = LoadQuestionsList(wordId);
            return PartialView("_ExistingQuestionsList", model);
        }

        [Authorize(Roles = "Admin,Researcher,Student")]
        public PartialViewResult GetExamQuestions(string attributeType, int attributeId)
        {
            var model = LoadExamQuestionsList(attributeType, attributeId);
            return PartialView("_AllExamQuestions", model);
        }

        [Authorize(Roles = "Admin,Researcher")]
        public PartialViewResult GetAvailableQuestions(int attributeId, string attributeType, int wordId)
        {
            var model = LoadAvailableQuestionsList(attributeId, attributeType, wordId);
            return PartialView("_AvailableQuestionsList", model);
        }
        [Authorize(Roles = "Admin,Researcher,Student")]
        private List<QuestionViewModel> LoadQuestionsList(int wordId)
        {
            if (wordId != 0)
            {
                var question = questionRepository.GetManyQuestions(wordId);
                var model = Mapper.Map<List<QuestionViewModel>>(question);
                return model;
            }
            else
            {
                var model = new List<QuestionViewModel>();
                return model;
            }
        }

        [Authorize(Roles = "Admin,Researcher,Student")]
        private List<QuestionViewModel> LoadAvailableQuestionsList(int attributeId, string attributeType, int wordId)
        {

            var question = questionRepository.GetManyAvailableQuestions(attributeId.ToString(), attributeType, 0);

            if (wordId != 0)
            {
                var listWordQuestionIds = wordQuestionRepository.Get().Where(a => a.WordId == wordId).Select(s => s.QuestionId).ToList();
                question = question.Where(a => !listWordQuestionIds.Contains(a.Id)).ToList();
            }



            var model = Mapper.Map<List<QuestionViewModel>>(question);
            return model;
        }
        [Authorize(Roles = "Admin,Researcher,Student")]
        public PartialViewResult Get(string id)
        {
            var question = questionRepository.GetWithAnswer(Convert.ToInt32(id));
            var model = Mapper.Map<QuestionViewModel>(question);

            return PartialView("_Detail", model);
        }

        [Authorize(Roles = "Admin,Researcher,Student")]
        public PartialViewResult GetSetQuestion(string id)
        {
            var question = questionRepository.GetWithAnswer(Convert.ToInt32(id));

            var model = Mapper.Map<QuestionViewModel>(question);
            model.IsSetQuestion = true;
            return PartialView("_Detail", model);
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> AddAvailableQuestion(QuestionViewModel model)
        {
            try
            {
                if (model.Ids != string.Empty)
                {
                    var question = await AddExistingQuestionAsync(model);

                    if (question != null)
                    {
                        return new BaseModel() { Success = true, Message = "Question Added Successfully!", Data = new { QuestionIds = question.Ids, AttributeType = question.AttributeType, WordId = question.WordId } };
                    }
                    else
                    {
                        return BaseModel.Failed("There is an error ");
                    }
                }
                else
                {


                    return BaseModel.Failed("There is an error or you may not entered any question or question title.");

                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }

        }

        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> AddSubjectQuestion(QuestionViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.QuestionCategory == QuestionCategories.ExamQuestion)
                    {
                        var isQuestionAdded = await AddExamQuestionAsync(AttributeType.Subject, model);

                        if (isQuestionAdded)
                        {
                            return new BaseModel() { Success = true, Message = "Question Added Successfully!" };
                        }
                        else
                        {
                            return BaseModel.Failed("There is an error ");
                        }
                    }
                    else
                    {
                        var question = await AddQuestionAsync(AttributeType.Subject, model);

                        if (question != null)
                        {
                            return new BaseModel() { Success = true, Message = "Question Added Successfully!", Data = new { QuestionIds = question.Ids, AttributeType = AttributeType.Subject, WordId = question.WordId } };
                        }
                        else
                        {
                            return BaseModel.Failed("There is an error ");
                        }
                    }

                }
                else
                {


                    return BaseModel.Failed("There is an error or you may not entered any question or question title.");

                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }

        }

        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> AddChapterQuestion(QuestionViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.QuestionCategory == QuestionCategories.ExamQuestion)
                    {
                        var isQuestionAdded = await AddExamQuestionAsync(AttributeType.Chapter, model);

                        if (isQuestionAdded)
                        {
                            return new BaseModel() { Success = true, Message = "Question Added Successfully!" };
                        }
                        else
                        {
                            return BaseModel.Failed("There is an error ");
                        }
                    }
                    else
                    {
                        var question = await AddQuestionAsync(AttributeType.Chapter, model);
                        if (question != null)
                        {
                            return new BaseModel() { Success = true, Message = "Question Added Successfully!", Data = new { QuestionIds = question.Ids, AttributeType = AttributeType.Chapter, WordId = question.WordId } };
                        }
                        else
                        {
                            return BaseModel.Failed("There is an error ");
                        }
                    }
                }
                else
                {


                    return BaseModel.Failed("There is an error or you may not entered any question or question title.");

                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }

        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> AddSectionQuestion(QuestionViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (model.QuestionCategory == QuestionCategories.ExamQuestion)
                    {
                        var isQuestionAdded = await AddExamQuestionAsync(AttributeType.Section, model);

                        if (isQuestionAdded)
                        {
                            return new BaseModel() { Success = true, Message = "Question Added Successfully!" };
                        }
                        else
                        {
                            return BaseModel.Failed("There is an error ");
                        }
                    }
                    else
                    {
                        var question = await AddQuestionAsync(AttributeType.Section, model);
                        if (question != null)
                        {
                            return new BaseModel() { Success = true, Message = "Question Added Successfully!", Data = new { QuestionIds = question.Ids, AttributeType = AttributeType.Section, WordId = question.WordId } };
                        }
                        else
                        {
                            return BaseModel.Failed("There is an error ");
                        }
                    }
                }
                else
                {


                    return BaseModel.Failed("There is an error or you may not entered any question or question title.");

                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }

        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> AddSubSectionQuestion(QuestionViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (model.QuestionCategory == QuestionCategories.ExamQuestion)
                    {
                        var isQuestionAdded = await AddExamQuestionAsync(AttributeType.SubSection, model);

                        if (isQuestionAdded)
                        {
                            return new BaseModel() { Success = true, Message = "Question Added Successfully!" };
                        }
                        else
                        {
                            return BaseModel.Failed("There is an error ");
                        }
                    }
                    else
                    {
                        var question = await AddQuestionAsync(AttributeType.SubSection, model);
                        if (question != null)
                        {
                            return new BaseModel() { Success = true, Message = "Question Added Successfully!", Data = new { QuestionIds = question.Ids, AttributeType = AttributeType.SubSection, WordId = question.WordId } };
                        }
                        else
                        {
                            return BaseModel.Failed("There is an error ");
                        }
                    }
                }
                else
                {


                    return BaseModel.Failed("There is an error or you may not entered any question or question title.");

                }
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }

        }

        [Authorize(Roles = "Admin,Researcher")]
        private async Task<QuestionViewModel> AddExistingQuestionAsync(QuestionViewModel model)
        {
            try
            {
                var listIds = model.Ids.Replace(",,", ",").Trim(',').Split(",");

                var attributeType = "";
                foreach (var id in listIds)
                {
                    if (id != string.Empty)
                    {


                        var question = questionRepository.Get().FirstOrDefault(a => a.Id == Convert.ToInt32(id));
                        attributeType = question.AttributeType;
                        if (question != null)
                        {


                            if (model.WordId == 0)
                            {
                                var word = new Word()
                                {
                                    Detail = model.SelectedWord

                                };
                                await wordRepository.AddAsync(word);
                                await unitOfWork.SaveChangesAsync();
                                model.WordId = word.Id;
                            }





                            var wordQuestion = new WordQuestion()
                            {
                                WordId = model.WordId,
                                QuestionId = question.Id
                            };

                            await wordQuestionRepository.AddAsync(wordQuestion);
                            await unitOfWork.SaveChangesAsync();


                        }

                    }
                }
                var idsAgainstThisWord = wordQuestionRepository.Get().Where(a => a.WordId == model.WordId).DistinctBy(s => s.QuestionId).Select(a => a.QuestionId).ToList();
                var idsString = string.Join(",", idsAgainstThisWord);

                var qModel = new QuestionViewModel()
                {
                    Ids = idsString,
                    WordId = model.WordId,
                    AttributeType = attributeType
                };
                return qModel;
            }
            catch (Exception ex)
            {

                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Researcher")]
        private async Task<QuestionViewModel> AddQuestionAsync(string attributeType, QuestionViewModel model)
        {
            try
            {


                var question = new Question()
                {
                    Date = DateTime.Now,
                    Detail = model.QuestionDetail,
                    AttributeType = attributeType,
                    AttributeId = model.AttributeId,
                    QuestionDifficultyLevelId = model.QuestionDifficultyLevelId,
                    QustionTypeId = model.QustionTypeId,
                    QuestionTitle = model.QuestionTitle,
                    QuestionCategoryId = Convert.ToInt32(QuestionCategories.NormalQuestion),
                    Source = model.Source,
                    Author = model.Author,
                    Year = model.Year
                };
                await questionRepository.AddAsync(question);
                await unitOfWork.SaveChangesAsync();
                var answer = new CorrectAnswer()
                {
                    Answer = model.AnswerDetail,
                    QuestionId = question.Id
                };
                Word word;
                if (model.WordId == 0)
                {
                    word = new Word()
                    {
                        Detail = model.SelectedWord

                    };
                    await wordRepository.AddAsync(word);
                    await unitOfWork.SaveChangesAsync();
                    model.WordId = word.Id;
                }
                else
                {
                    word = new Word()
                    {
                        Id = model.WordId
                    };
                }



                var wordQuestion = new WordQuestion()
                {
                    WordId = word.Id,
                    QuestionId = question.Id
                };

                await correctAnswerRepository.AddAsync(answer);
                await unitOfWork.SaveChangesAsync();
                await wordQuestionRepository.AddAsync(wordQuestion);
                await unitOfWork.SaveChangesAsync();

                var idsAgainstThisWord = wordQuestionRepository.Get().Where(a => a.WordId == word.Id).Select(a => a.QuestionId).ToList();
                var idsString = string.Join(",", idsAgainstThisWord);

                var qModel = new QuestionViewModel()
                {
                    Ids = idsString,
                    WordId = word.Id,
                    AttributeType = attributeType
                };

                return qModel;
            }
            catch (Exception ex)
            {

                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Researcher")]
        public PartialViewResult Edit(int id)
        {
            var question = questionRepository.GetQuestion(id);
            var model = Mapper.Map<EditQuestionViewModel>(question);
            ViewBag.QuestionTypes = questionTypeRepository.Get().ToList();
            ViewBag.QuestionDifficultyLevels = questionDifficultyLevelRepository.Get().ToList();
            return PartialView("_EditWrittenQuestionAnswer", model);
        }

        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> UpdateQuestionAsync(EditQuestionViewModel model)
        {
            try
            {

                var question = questionRepository.Get().FirstOrDefault(a => a.Id == Convert.ToInt32(model.Id));
                question.Detail = model.EditQuestionDetail;
                question.LastUpdated = DateTime.Now;
                question.QuestionDifficultyLevelId = model.QuestionDifficultyLevelId;
                question.QustionTypeId = model.QustionTypeId;
                question.QuestionTitle = model.EditQuestionTitle;
                question.Source = model.Source;
                question.Author = model.Author;
                question.Year = model.Year;
                await questionRepository.UpdateAsync(question);
                await unitOfWork.SaveChangesAsync();

                var answer = correctAnswerRepository.Get().FirstOrDefault(a => a.QuestionId == Convert.ToInt32(model.Id));
                answer.Answer = model.EditAnswerDetail;

                await correctAnswerRepository.UpdateAsync(answer);
                await unitOfWork.SaveChangesAsync();


                return new BaseModel() { Success = true, Message = "Question Updated Successfully!" };

            }
            catch (Exception ex)
            {

                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }

        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> Remove(int id, int wordId)
        {
            try
            {

                var wordQuestion = wordQuestionRepository.Get().FirstOrDefault(a => a.QuestionId == id && a.Question.IsDeleted == false && a.WordId == wordId);
                if (wordQuestion != null)
                {
                    await wordQuestionRepository.DeleteAsync(wordQuestion);
                    await unitOfWork.SaveChangesAsync();
                    var idsAgainstThisWord = wordQuestionRepository.Get().Where(a => a.WordId == wordId && a.Question.IsDeleted == false).Select(a => a.QuestionId).ToList();
                    var idsString = string.Empty;
                    if (idsAgainstThisWord.Count > 0)
                    {
                        idsString = string.Join(",", idsAgainstThisWord);

                    }
                    var qModel = new QuestionViewModel()
                    {
                        Ids = idsString,
                        WordId = wordId
                    };

                    return new BaseModel() { Success = true, Message = "Question Removed Successfully!", Data = qModel };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "No Question against this word/sentence or it is Removed, refresh your page please!!" };
                }




            }
            catch (Exception ex)
            {

                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }



        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> Delete(int id)
        {
            try
            {


                var question = questionRepository.Get().FirstOrDefault(a => a.Id == id);
                if (question != null)
                {
                    if (question.QuestionCategoryId == Convert.ToInt32(QuestionCategories.ExamQuestion))
                    {
                        question.IsDeleted = true;
                        await questionRepository.UpdateAsync(question);
                        await unitOfWork.SaveChangesAsync();
                        return new BaseModel() { Success = true, Message = "Question Deleted Successfully!!" };
                    }
                    else
                    {
                        var wordQuestion = wordQuestionRepository.Get().FirstOrDefault(a => a.QuestionId == id);
                        if (wordQuestion == null)
                        {
                            question.IsDeleted = true;
                            await questionRepository.UpdateAsync(question);
                            await unitOfWork.SaveChangesAsync();
                            return new BaseModel() { Success = true, Message = "Question Deleted Successfully!!" };
                        }
                        else
                        {
                            return new BaseModel() { Success = false, Message = "You can't delete this question as it is assigned to some word/sentence." };
                        }
                    }

                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Sorry, No such question exist in system, or it may be deleted. Kindly reload the page again." };
                }



            }
            catch (Exception ex)
            {

                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }


        [Authorize(Roles = "Admin,Researcher,Student")]
        private List<QuestionViewModel> LoadExamQuestionsList(string attributeType, int attributeId)
        {
            if (attributeId != 0)
            {
                var question = questionRepository.GetAttributeQuestions(attributeId.ToString(), attributeType);
                var model = Mapper.Map<List<QuestionViewModel>>(question);
                return model;
            }
            else
            {
                var model = new List<QuestionViewModel>();
                return model;
            }
        }

        [Authorize(Roles = "Admin,Researcher")]
        private async Task<bool> AddExamQuestionAsync(string attributeType, QuestionViewModel model)
        {
            try
            {
                var question = new Question()
                {
                    Date = DateTime.Now,
                    Detail = model.QuestionDetail,
                    AttributeType = attributeType,
                    AttributeId = model.AttributeId,
                    QuestionDifficultyLevelId = model.QuestionDifficultyLevelId,
                    QustionTypeId = model.QustionTypeId,
                    QuestionTitle = model.QuestionTitle,
                    QuestionCategoryId = Convert.ToInt32(QuestionCategories.ExamQuestion),
                    Source = model.Source,
                    Author = model.Author,
                    Year = model.Year,
                    AverageSolveMinutes = model.AverageSolveMinutes,
                    Marks = Convert.ToInt32(model.Marks).ToString(),
                };

                await questionRepository.AddAsync(question);


                //All the choices added against Question
                #region All the choices added against Question

                var questionChoices = new List<QuestionChoice>();
                var choiceAdvices = new List<Advice>();
                if (model.QuestionChoices != null)
                {
                    foreach (var choice in model.QuestionChoices)
                    {
                        var questionChoice = new QuestionChoice()
                        {
                            Detail = choice.Detail,
                            Qustion = question
                        };
                        questionChoices.Add(questionChoice);
                        if (choice.Advice != null && !string.IsNullOrEmpty(choice.Advice.Detail))
                        {
                            var advice = new Advice()
                            {
                                Detail = choice.Advice.Detail == null ? "" : choice.Advice.Detail,
                                QuestionChoice = questionChoice,
                                AdviceTypeId = Convert.ToInt32(AdviceTypes.Question)

                            };
                            choiceAdvices.Add(advice);
                        }
                        else
                        {
                            var advice = new Advice()
                            {
                                Detail = string.Empty,
                                QuestionChoice = questionChoice,
                                AdviceTypeId = Convert.ToInt32(AdviceTypes.Question)

                            };
                            choiceAdvices.Add(advice);
                        }
                    }
                    await questionChoiceRepository.AddManyAsync(questionChoices);
                    await adviceRepository.AddManyAsync(choiceAdvices);
                }



                #endregion

                //All the choices added against Questions End

                //Selected Correct choices added against Question
                #region Selected choices added against Question

                var correctAnswer = new CorrectAnswer();
                if (model.QustionTypeId == Convert.ToInt32(QustionTypesEnum.LongQuestion))
                {

                    correctAnswer.Answer = model.AnswerDetail;
                    correctAnswer.Qustion = question;

                    await correctAnswerRepository.AddAsync(correctAnswer);

                }
                else if (model.QustionTypeId == Convert.ToInt32(QustionTypesEnum.ShortQuestion))
                {

                    correctAnswer.Answer = model.ShortAnswerDetail;
                    correctAnswer.Qustion = question;

                    await correctAnswerRepository.AddAsync(correctAnswer);

                }
                else if (model.QustionTypeId == Convert.ToInt32(QustionTypesEnum.FillBlank))
                {
                    correctAnswer.Qustion = question;
                    correctAnswer.Answer = model.CorrectQuestionChoice;

                    await correctAnswerRepository.AddAsync(correctAnswer);


                    var questionChoice = new QuestionChoice()
                    {
                        Detail = model.CorrectQuestionChoice,
                        Qustion = question

                    };
                    await questionChoiceRepository.AddAsync(questionChoice);


                    var advice = new Advice()
                    {
                        Detail = model.SingleQuestionChoiceAdvice == null ? "" : model.SingleQuestionChoiceAdvice,
                        QuestionChoice = questionChoice,
                        AdviceTypeId = Convert.ToInt32(AdviceTypes.Question)

                    };
                    await adviceRepository.AddAsync(advice);
                }
                else if (model.QustionTypeId == Convert.ToInt32(QustionTypesEnum.MTQ) || model.QustionTypeId == Convert.ToInt32(QustionTypesEnum.MCQ))
                {

                    if (model.MTQCorrectQuestionChoices != null)
                    {
                        var mTQCorrectAnswers = new List<CorrectAnswer>();
                        foreach (var CorrectQuestionChoice in model.MTQCorrectQuestionChoices)
                        {
                            var mTQCorrectAnswer = new CorrectAnswer()
                            {
                                Qustion = question,
                                QuestionChoice = questionChoices.FirstOrDefault(x => x.Detail == CorrectQuestionChoice)
                            };

                            mTQCorrectAnswers.Add(mTQCorrectAnswer);
                        }
                        await correctAnswerRepository.AddManyAsync(mTQCorrectAnswers);
                    }


                }
                #endregion

                await unitOfWork.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {

                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return false;

            }
        }

        [Authorize(Roles = "Admin,Researcher")]
        public PartialViewResult EditExamQuestion(int id)
        {
            var question = questionRepository.GetExamQuestion(id);
            var model = Mapper.Map<QuestionViewModel>(question);
            ViewBag.QuestionTypes = questionTypeRepository.Get().ToList();
            ViewBag.QuestionDifficultyLevels = questionDifficultyLevelRepository.Get().ToList();
            return PartialView("_EditExamQuestionAnswer", model);
        }
        [Authorize(Roles = "Admin,Researcher")]
        public PartialViewResult GetExamQuestion(int id)
        {
            var question = questionRepository.GetExamQuestion(id);
            var model = Mapper.Map<QuestionViewModel>(question);
            return PartialView("_ViewExamQuestion", model);
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> UpdateExamQuestionAsync(QuestionViewModel model)
        {
            try
            {
                var question = questionRepository.Get().FirstOrDefault(a => a.Id == Convert.ToInt32(model.Id));
                var correctAnswersExisting = correctAnswerRepository.Get().Where(a => a.QuestionId == model.Id);

                question.Detail = model.QuestionDetail;
                question.LastUpdated = DateTime.Now;
                question.QuestionDifficultyLevelId = model.QuestionDifficultyLevelId;
                question.QustionTypeId = model.QustionTypeId;
                question.QuestionTitle = model.QuestionTitle;
                question.Source = model.Source;
                question.Author = model.Author;
                question.Year = model.Year;
                question.Marks = model.Marks;
                question.AverageSolveMinutes = model.AverageSolveMinutes;
                await questionRepository.UpdateAsync(question);
                await unitOfWork.SaveChangesAsync();


                //Update all existing choices do not need to delete it


                //Remove all initially added choices and correct answers by teacher/admin/researcher
                // var deleteableAdvices = adviceRepository.Get().Where(a => a.QuestionChoice.QuestionId == Convert.ToInt32(model.Id)
                // && !correctAnswersExisting.Select(x => x.QuestionChoiceId).Contains(a.QuestionChoiceId));
                // await adviceRepository.DeleteManyAsync(deleteableAdvices);

                var existingAdvices = adviceRepository.Get().Where(a => a.QuestionChoice.QuestionId == Convert.ToInt32(model.Id));

                // var deleteableQChoices = questionChoiceRepository.Get().Where(a => a.QuestionId == Convert.ToInt32(model.Id)
                // && !correctAnswersExisting.Select(x => x.QuestionChoiceId).Contains(a.Id));
                // await questionChoiceRepository.DeleteManyAsync(deleteableQChoices);

                var existingQChoices = questionChoiceRepository.Get().Where(a => a.QuestionId == Convert.ToInt32(model.Id));

                //  var deleteableAnswers = correctAnswerRepository.Get().Where(a => a.QuestionId == Convert.ToInt32(model.Id)
                // && !correctAnswers.Select(x => x.QuestionChoiceId).Contains(a.QuestionChoiceId));
                // await correctAnswerRepository.DeleteManyAsync(deleteableAnswers);

                //All the choices added against Question
                #region All the choices added against Question

                var choiceAdvices = new List<Advice>();
                var updateableQuestionChoicesAdvices = new List<Advice>();
                var questionChoices = new List<QuestionChoice>();
                var updateableQuestionChoices = new List<QuestionChoice>();
                if (model.QuestionChoices != null)
                {
                    var newAddingChoices = model.QuestionChoices.Where(a => !existingQChoices.Select(x => x.Id).Contains(a.Id));
                    foreach (var choice in newAddingChoices)
                    {
                        var questionChoice = new QuestionChoice()
                        {
                            Detail = choice.Detail,
                            QuestionId = question.Id
                        };
                        questionChoices.Add(questionChoice);


                        var advice = new Advice()
                        {
                            Detail = choice.Advice == null ? "" : choice.Advice.Detail,
                            QuestionChoice = questionChoice,
                            AdviceTypeId = Convert.ToInt32(AdviceTypes.Question)

                        };
                        choiceAdvices.Add(advice);
                    }

                    var updatableChoices = model.QuestionChoices.Where(a => existingQChoices.Select(x => x.Id).Contains(a.Id));
                    foreach (var choice in updatableChoices)
                    {
                        var existingChoice = existingQChoices.FirstOrDefault(a => a.Id == choice.Id);
                        existingChoice.Detail = choice.Detail;
                        updateableQuestionChoices.Add(existingChoice);

                        var existingChoiceadvice = existingAdvices.FirstOrDefault(a => a.QuestionChoiceId == choice.Id);
                        existingChoiceadvice.Detail = choice.Advice == null ? "" : choice.Advice.Detail == null ? string.Empty : choice.Advice.Detail;
                        updateableQuestionChoicesAdvices.Add(existingChoiceadvice);
                    }
                }

                if (questionChoices.Any())
                {
                    await questionChoiceRepository.AddManyAsync(questionChoices);
                }
                if (choiceAdvices.Any())
                {
                    await adviceRepository.AddManyAsync(choiceAdvices);
                }


                await questionChoiceRepository.UpdateManyAsync(updateableQuestionChoices);
                await adviceRepository.UpdateManyAsync(updateableQuestionChoicesAdvices);
                questionChoices.AddRange(updateableQuestionChoices);
                #endregion

                //Selected choices added against Question
                #region Selected choices added against Question

                //var correctAnswer = new CorrectAnswer();

                if (model.QustionTypeId == Convert.ToInt32(QustionTypesEnum.LongQuestion))
                {
                    foreach (var correctAnswer in correctAnswersExisting)
                    {
                        correctAnswer.Answer = model.AnswerDetail;
                        await correctAnswerRepository.UpdateAsync(correctAnswer);
                    }
                }
                else if (model.QustionTypeId == Convert.ToInt32(QustionTypesEnum.ShortQuestion))
                {
                    foreach (var correctAnswer in correctAnswersExisting)
                    {
                        correctAnswer.Answer = model.ShortAnswerDetail;
                        await correctAnswerRepository.UpdateAsync(correctAnswer);
                    }
                }
                else if (model.QustionTypeId == Convert.ToInt32(QustionTypesEnum.FillBlank))
                {
                    //foreach (var correctAnswer in correctAnswersExisting)
                    //{
                    //    correctAnswer.Answer = model.CorrectQuestionChoice;

                    //    await correctAnswerRepository.UpdateAsync(correctAnswer);

                    //    var questionChoiceExisting = questionChoiceRepository.Get().FirstOrDefault(a => a.QuestionId == model.Id);

                    //    questionChoiceExisting.Detail = model.CorrectQuestionChoice;
                    //    questionChoiceExisting.Qustion = question;

                    //    await questionChoiceRepository.UpdateAsync(questionChoiceExisting);

                    //    var questionAdviceExisting = adviceRepository.Get().FirstOrDefault(a => a.QuestionChoiceId == questionChoiceExisting.Id);

                    //    questionAdviceExisting.Detail = model.SingleQuestionChoiceAdvice == null ? "" : model.SingleQuestionChoiceAdvice;
                    //    questionAdviceExisting.AdviceTypeId = Convert.ToInt32(AdviceTypes.Question);

                    //    await adviceRepository.UpdateAsync(questionAdviceExisting);

                    //}
                }
                else if (model.QustionTypeId == Convert.ToInt32(QustionTypesEnum.MTQ) || model.QustionTypeId == Convert.ToInt32(QustionTypesEnum.MCQ))
                {
                    ////var updatedMTQsAlreadyAdded = new List<string>();

                    //foreach (var correctAnswer in correctAnswersExisting)
                    //{
                    //    if (model.MTQCorrectQuestionChoices != null)
                    //    {
                    //        var choiceId = questionChoices
                    //            .FirstOrDefault(x => x.Detail == model.MTQCorrectQuestionChoices.ToList().ElementAtOrDefault(correctAnswersExisting.ToList().IndexOf(correctAnswer))).Id;
                    //        correctAnswer.QuestionChoiceId = choiceId;

                    //        await correctAnswerRepository.UpdateAsync(correctAnswer);
                    //    }
                    //    //updated one
                    //    //var updatedMTQ = model.MTQCorrectQuestionChoices.ToList().ElementAtOrDefault(correctAnswersExisting.ToList().IndexOf(correctAnswer));
                    //    //updatedMTQsAlreadyAdded.Add(updatedMTQ);

                    //}

                    //var mTQCorrectAnswers = new List<CorrectAnswer>();
                    //if (model.MTQCorrectQuestionChoices.Count > correctAnswersExisting.Count())
                    //{
                    //    foreach (var CorrectQuestionChoice in model.MTQCorrectQuestionChoices.Where(a => !updatedMTQsAlreadyAdded.Contains(a)))
                    //    {

                    //        var mTQCorrectAnswer = new CorrectAnswer()
                    //        {
                    //            QuestionId = question.Id,
                    //            QuestionChoice = questionChoices.FirstOrDefault(x => x.Detail == CorrectQuestionChoice)
                    //        };

                    //        mTQCorrectAnswers.Add(mTQCorrectAnswer);

                    //    }
                    //    await correctAnswerRepository.AddManyAsync(mTQCorrectAnswers);
                    //}
                    //if (model.MTQCorrectQuestionChoices.Count < correctAnswersExisting.Count())
                    //{
                    //    var deletableChoices = correctAnswersExisting.Where(x=>x.Answer)
                    //    foreach (var CorrectQuestionChoice in model.MTQCorrectQuestionChoices.Where(a => !updatedMTQsAlreadyAdded.Contains(a)))
                    //    {
                    //        var mTQCorrectAnswer = new CorrectAnswer()
                    //        {
                    //            QuestionId = question.Id,
                    //            QuestionChoice = questionChoices.FirstOrDefault(x => x.Detail == CorrectQuestionChoice)
                    //        };

                    //        mTQCorrectAnswers.Add(mTQCorrectAnswer);
                    //    }
                    //    await correctAnswerRepository.AddManyAsync(mTQCorrectAnswers);
                    //}
                }



                #endregion

                await unitOfWork.SaveChangesAsync();

                return new BaseModel() { Success = true, Message = "Question Updated Successfully!" };

            }
            catch (Exception ex)
            {

                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }

    }


}