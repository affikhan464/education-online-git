﻿using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Management.Infrastructure.Communication;
using Management.Infrastructure.Security;
using Management.Infrastructure.Security.Identity;
using Management.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager userManager;
        private readonly SignInManager _signInManager;
        private readonly IEncryptionDecryption security;
        private readonly IUnitOfWork unitOfWork;
        private readonly IEmailService emailService;
        public AccountController(IEncryptionDecryption _security, IEmailService _emailService, UserManager _userManager,SignInManager signInManager, IUnitOfWork _unitOfWork)
        {
            userManager = _userManager;
            _signInManager = signInManager;
            unitOfWork = _unitOfWork;
            emailService = _emailService;
            security = _security;
        }
        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if(User.Identity.IsAuthenticated)
            {
                var user = await userManager.GetUserAsync(User);
                var userRoles = await userManager.GetRolesAsync(user);

              

                if (userRoles.Contains(UserRoles.Admin.ToString()))
                {
                    return RedirectToAction("Index", "Home");
                }
                else if (userRoles.Contains(UserRoles.Student.ToString()))
                {
                    return RedirectToAction("Index", "Student");
                }
                else if (userRoles.Contains(UserRoles.Researcher.ToString()))
                {
                    return RedirectToAction("Index", "Researcher");
                }
                else if (userRoles.Contains(UserRoles.Teacher.ToString()))
                {
                    return RedirectToAction("Index", "Teacher");
                }
            }

            //var admins = _userManager.GetUsersInRoleAsync("admin");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {

            var user = await userManager.FindByEmailAsync(model.Email);
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, false, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    var userRoles = await userManager.GetRolesAsync(user);

                   
                    if (userRoles.Any())
                    {
                        ViewData["ReturnUrl"] = returnUrl;
                        if (!string.IsNullOrEmpty( returnUrl))
                        {
                            return LocalRedirect(returnUrl);
                        }
                       else if (userRoles.Contains(UserRoles.Admin.ToString()))
                        {
                            return RedirectToAction( "Index","Home");
                        }else if(userRoles.Contains(UserRoles.Student.ToString()))
                        {
                            return RedirectToAction("Index", "Student");
                        }
                        else if (userRoles.Contains(UserRoles.Researcher.ToString()))
                        {
                            return RedirectToAction("Index", "Researcher");
                        }
                        else if (userRoles.Contains(UserRoles.Teacher.ToString()))
                        {
                            return RedirectToAction("Index", "Teacher");
                        }
                    }
                }
                else if (result.IsNotAllowed)
                {
                    ModelState.AddModelError(string.Empty, "Contact administrator to give you rights of login.");

                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid credentials");
                }

                
            }

            
            return View(model);
        }

     
        public async Task<IActionResult> LogOut()
        {
                 await _signInManager.SignOutAsync();
                 return RedirectToAction("Login", "Account");
        }
      
        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public async Task<BaseModel> ForgotPassword(ForgotPasswordViewModel model)
        {

            var user = await userManager.FindByEmailAsync(model.Email);
            if (user!=null)
            {
                var mailModel = new Mail()
                {
                    ToEMailAddress = user.Email,
                    UserName = user.FirstName
                };
                var resetPasswordToken = await userManager.GeneratePasswordResetTokenAsync(user);
                emailService.SendResetPasswordLink(mailModel, resetPasswordToken);
            }
                else
                {
                return BaseModel.Failed("No user exist with this email address.");
            }

            return BaseModel.Succeeded("Password reset email has been sent to your email address.");
        }
      

        [HttpGet]
        public IActionResult ResetPassword()
        {
            var hashCode = HttpContext.Request.QueryString.ToString();
           
            if (string.IsNullOrEmpty(hashCode.Trim()))
            {
                return RedirectToAction("Login");
            }
            return View();
        }
        [HttpPost]
        public async Task<BaseModel> ResetPassword(ResetPasswordViewModel model)
        {
            try
            {
                var forr = new ForgotPassword();
                var obj = security.DecryptKey(model.ResetToken, true);
                var forgotPassword = Newtonsoft.Json.JsonConvert.DeserializeObject(obj,typeof(ForgotPassword))as ForgotPassword;

                var user = userManager.Users.FirstOrDefault(u => u.Email == forgotPassword.Email);

                var result=await userManager.ResetPasswordAsync(user, forgotPassword.ResetToken, model.Password);
                if (!result.Succeeded)
                {
                    return BaseModel.Failed("The token is expired, Reset Your Password Again!!");

                }
                //await userManager.UpdateAsync(user);
                //await unitOfWork.SaveChangesAsync();

                return BaseModel.Succeeded("Password Reset Successfully!!");




            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return BaseModel.Failed(ex.Message);
            }
        }
    }
}
