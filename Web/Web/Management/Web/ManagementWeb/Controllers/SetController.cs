﻿using AutoMapper;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Infrastructure.Communication;
using Management.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class SetController : Controller
    {

        private readonly IUnitOfWork unitOfWork;
        private IHostingEnvironment env;

        private readonly IQuestionRepository questionRepository;
        private readonly ICorrectAnswerRepository correctAnswerRepository;
        private readonly IEmailService emailService;
        private readonly IQuestionTypeRepository questionTypeRepository;
        private readonly ISetRepository setRepository;
        private readonly ISetQuestionRepository setQuestionRepository;
        private readonly IQuestionDifficultyLevelRepository questionDifficultyLevelRepository;
        public SetController(ISetQuestionRepository _setQuestionRepository, ISetRepository _setRepository, IQuestionDifficultyLevelRepository _questionDifficultyLevelRepository, IQuestionTypeRepository _questionTypeRepository, ICorrectAnswerRepository _correctAnswerRepository, IQuestionRepository _questionRepository, IEmailService _emailService, IHostingEnvironment _env, IUnitOfWork _unitOfWork)
        {

            unitOfWork = _unitOfWork;
            emailService = _emailService;
            correctAnswerRepository = _correctAnswerRepository;
            questionRepository = _questionRepository;
            setRepository = _setRepository;
            setQuestionRepository = _setQuestionRepository;

            questionTypeRepository = _questionTypeRepository;
            questionDifficultyLevelRepository = _questionDifficultyLevelRepository;
            env = _env;
        }
        [Authorize(Roles = "Admin,Researcher")]

        public PartialViewResult Get(SetViewModel model)
        {
            try
            {
                var listOfSets = setRepository.Get().Where(x => x.AttributeId == model.AttributeId && x.AttributeType == model.AttributeType).ToList();

                return PartialView("_ListOfSets", listOfSets);
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        [HttpPost]
        public async Task<BaseModel> Add(SetViewModel model)
        {
            try
            {
                var set = new Set()
                {
                    Title = model.Title,
                    AttributeType = model.AttributeType,
                    AttributeId = model.AttributeId,
                    LastUpdated = DateTime.Now,
                    AddedDate = DateTime.Now
                };

                await setRepository.AddAsync(set);
                await unitOfWork.SaveChangesAsync();
                return BaseModel.Succeeded("Set Added Successfully!");
                
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> Delete(int id)
        {
            try
            {
                var set = setRepository.Get().FirstOrDefault(a => a.Id == id);

                if (set != null)
                {

                    await setQuestionRepository.DeleteManyAsync(setQuestionRepository.Get().Where(a => a.SetId == id));
                    await setRepository.DeleteAsync(set);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseModel() { Success = true, Message = "Set Removed Successfully!" };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "No Set Exist or it is Removed, refresh your page please!!" };
                }
            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> Update(SetViewModel model)
        {
            try
            {
                var set = setRepository.Get().FirstOrDefault(a => a.Id == Convert.ToInt32(model.Id));

                if (set != null)
                {
                    set.Title = model.Title;
                    set.LastUpdated = DateTime.Now;


                    await setRepository.UpdateAsync(set);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseModel() { Success = true, Message = "Set Updated Successfully!" };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "This set is not exist or may be deleted, kindly refesh your page!" };

                }

            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher,Student")]
        public PartialViewResult GetQuestions(int setId)
        {
            var questions = setQuestionRepository.GetBySetId(setId);
            var model = Mapper.Map<List<QuestionViewModel>>(questions);
            return PartialView("_AllQuestions", model);
        }

        [Authorize(Roles = "Admin,Researcher")]
        public PartialViewResult EditSetQuestion(int id)
        {
            var question = questionRepository.GetSetsQuestion(id);
            var model = Mapper.Map<QuestionViewModel>(question);

            return PartialView("_EditWrittenQuestionAnswer", model);
        }



        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> AddQuestion(QuestionViewModel model)
        {
            try
            {

                var question = new Question()
                {
                    Date = DateTime.Now,
                    Detail = model.QuestionDetail,
                    QuestionDifficultyLevelId = model.QuestionDifficultyLevelId,
                    QustionTypeId = model.QustionTypeId,
                    QuestionTitle = model.QuestionTitle,
                    QuestionCategoryId = Convert.ToInt32(QuestionCategories.SetQuestion),
                    Source = model.Source,
                    Author = model.Author,
                    Year = model.Year
                };

                await questionRepository.AddAsync(question);


                var answer = new CorrectAnswer()
                {
                    Answer = model.AnswerDetail,
                    Qustion = question
                };
                await correctAnswerRepository.AddAsync(answer);


                var setQuestion = new SetQuestion()
                {
                    SetId = model.SetId,
                    Question = question
                };

                await setQuestionRepository.AddAsync(setQuestion);
                await unitOfWork.SaveChangesAsync();
                return BaseModel.Succeeded("Question Added Successfully!");
            }
            catch (Exception ex)
            {
                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> UpdateQuestion(QuestionViewModel model)
        {
            try
            {

                var question = questionRepository.Get().FirstOrDefault(a => a.Id == Convert.ToInt32(model.Id));
                question.Detail = model.QuestionDetail;
                question.LastUpdated = DateTime.Now;
                question.QuestionDifficultyLevelId = model.QuestionDifficultyLevelId;
                question.QustionTypeId = model.QustionTypeId;
                question.QuestionTitle = model.QuestionTitle;
                question.Source = model.Source;
                question.Author = model.Author;
                question.Year = model.Year;

                await questionRepository.UpdateAsync(question);

                var answer = correctAnswerRepository.Get().FirstOrDefault(a => a.QuestionId == Convert.ToInt32(model.Id));
                answer.Answer = model.AnswerDetail;

                await correctAnswerRepository.UpdateAsync(answer);

                await unitOfWork.SaveChangesAsync();

                return new BaseModel() { Success = true, Message = "Question Updated Successfully!" };

            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [Authorize(Roles = "Admin,Researcher")]
        public async Task<BaseModel> RemoveQuestion(int id)
        {
            try
            {
                var question = questionRepository.Get().Where(a => a.Id == id);

                if (question != null)
                {

                    var setQuestion = setQuestionRepository.Get().FirstOrDefault(a => a.QuestionId == id);
                    if (setQuestion != null)
                    {
                        await setQuestionRepository.DeleteAsync(setQuestion);
                    }

                    await setQuestionRepository.DeleteAsync(setQuestion);
                    await unitOfWork.SaveChangesAsync();

                    return new BaseModel() { Success = true, Message = "Question Removed Successfully!" };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "No Question exist or it is Removed, refresh your page please!!" };
                }
            }
            catch (Exception ex)
            {

                 emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }


    }
}