﻿using AutoMapper;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Filters;
using Management.Infrastructure.Communication;
using Management.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class GeoController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private IHostingEnvironment env;
        private readonly ICountryRepository countryRepository;
        private readonly ICityRepository cityRepository;
        private readonly IRegionRepository regionRepository;
        private readonly IEmailService emailService;

        public GeoController(IRegionRepository _regionRepository, ICityRepository _cityRepository, ICountryRepository _countryRepository, IEmailService _emailService, IHostingEnvironment _env, IUnitOfWork _unitOfWork)
        {
            unitOfWork = _unitOfWork;
            regionRepository = _regionRepository;
            countryRepository = _countryRepository;
            cityRepository = _cityRepository;
            env = _env;
            emailService = _emailService;
        }


        [HttpPost]
        public async Task<BaseModel> AddCountry(List<CountryViewModel> model)
        {
            try
            {
                var countries = Mapper.Map<List<Country>>(model);

                //var countries = new List<Country>();
                //foreach (var item in model)
                //{
                //    var country = new Country();
                //    country.alpha2Code = item.alpha2Code;
                //    country.callingCodes = item.callingCodes[0] == null ? string.Empty : item.callingCodes[0];
                //    country.lat = item.latlng[0];
                //    country.lng = item.latlng[1];
                //    country.name = item.name;
                //    country.Region = item.Region;
                //    country.alpha3Code = item.alpha3Code;
                //    country.Flag = item.Flag;
                //    countries.Add(country);
                //}
                await countryRepository.AddManyAsync(countries);
                await unitOfWork.SaveChangesAsync();
                return new BaseModel()
                {
                    Success = true,
                    Message = "Batch Created Successfully!"
                };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [HttpPost]
        public async Task<BaseModel> AddRegion(List<RegionViewModel> model)
        { 
            var newRegions = new List<RegionViewModel>();
            try
            {
                foreach (var region in model)
                {
                    var isExisting = regionRepository.Get().Any(a => a.countryCode == region.countryCode && a.name == region.name && a.fipsCode == region.fipsCode);
                    if (!isExisting) {
                        var countries = countryRepository.Get().FirstOrDefault(a => a.alpha2Code == region.countryCode);

                        region.CountryId = countries.Id;
                        region.Id = 0;

                        newRegions.Add(region);
                    }

                }
                if (newRegions.Any())
                {
                    var regions = Mapper.Map<List<Region>>(newRegions);

                    await regionRepository.AddManyAsync(regions);
                    await unitOfWork.SaveChangesAsync();
                }
          
               
               
                return new BaseModel()
                {
                    Success = true,
                    Message = "Batch Created Successfully!"
                };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [AllowAnonymous]
        public async Task<BaseModel> AddOtherRegion()
        {


            
            try
            {
                var countries = countryRepository.Get().Distinct();
                foreach (var country in countries)
                {
                    var otherRegion = new Region()
                    {
                        name = "Other",
                        CountryId = country.Id,
                        countryCode = country.alpha2Code,
                    };
                    await regionRepository.AddAsync(otherRegion);

                }


                await unitOfWork.SaveChangesAsync();
                return new BaseModel()
                {
                    Success = true
                };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [HttpPost]
        public async Task<BaseModel> AddCities(List<CityViewModel> model)
        {
            var citiesTobeAdded = new List<CityViewModel>();
            try
            {
                
                foreach (var city in model)
                {
                    var isExisting = cityRepository.Get().Any(a => a.countryCode == city.countryCode && a.name == city.name && a.latitude == city.latitude && a.longitude == city.longitude);
                    if (!isExisting)
                    {
                        var region = regionRepository.Get().Where(a => a.countryCode == city.countryCode).ToList();
                        if (string.IsNullOrEmpty(city.regionCode))
                        {
                            city.RegionId = region.FirstOrDefault(a => a.name == "Other").Id;
                        }
                        else
                        {
                            city.RegionId = region.FirstOrDefault(x => x.isoCode == city.regionCode).Id;
                        }
                        city.Id = 0;
                        citiesTobeAdded.Add(city);
                    }

                }

                if (citiesTobeAdded.Any())
                {
                    var newcity = Mapper.Map<List<City>>(citiesTobeAdded);

                    await cityRepository.AddManyAsync(newcity);
                    await unitOfWork.SaveChangesAsync();
                }

                return new BaseModel()
                {
                    Success = true,
                    Message = "Batch Created Successfully!"
                };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [HttpGet]
        public async Task<BaseModel> GetCountries()
        {
            try
            {
                var regions = regionRepository.Get().Select(a => a.CountryId).Distinct().ToList();
                var countries = countryRepository.Get().Where(a => regions.Contains(a.Id)).ToList();

                var model = Mapper.Map<List<CountryViewModel>>(countries);

                return new BaseModel()
                {
                    Success = true,
                    Message = "Batch Created Successfully!",
                    Data = model
                };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        [HttpGet]
        public List<RegionViewModel> GetRegions(int countryCode)
        {
            try
            {
                var regions = regionRepository.Get().Where(a => a.CountryId== countryCode).ToList();

                var model = Mapper.Map<List<RegionViewModel>>(regions);

                return model;
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [HttpGet]
        public List<CityViewModel> GetCities(int regionCode)
        {
            try
            {
                var Cities = cityRepository.Get().Where(a => a.RegionId== regionCode).ToList();

                var model = Mapper.Map<List<CityViewModel>>(Cities);

                return model;
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return null;
            }
        }
        [HttpGet]
        public async Task<BaseModel> GetCountriesCitiesData(string countryCode)
        {
            try
            {
                var cities = cityRepository.Get().Where(a =>a.countryCode== countryCode).Count();
                return new BaseModel()
                {
                    Data = cities
                };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
                return BaseModel.Failed(ex.Message);
            }
        }
        //[Authorize(Roles = "Admin")]
        //public PartialViewResult Get(int id)
        //{
        //    try
        //    {
        //        var listOfBatches = batchRepository.Get().Where(x => x.Id == id).ToList();
        //        var model = Mapper.Map<List<BatchViewModel>>(listOfBatches);
        //        return PartialView("_AllBatches", model);
        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
        //        return null;
        //    }
        //}

        //[Authorize(Roles = "Admin")]
        //public async Task<BaseModel> Delete(int id)
        //{
        //    try
        //    {
        //        var batch = batchRepository.Get().FirstOrDefault(a => a.Id == id);

        //        if (batch != null)
        //        {

        //            await batchRepository.DeleteAsync(batch);
        //            await unitOfWork.SaveChangesAsync();
        //            return new BaseModel() { Success = true, Message = "Batch Removed Successfully!" };
        //        }
        //        else
        //        {
        //            return new BaseModel() { Success = false, Message = "No Batch Exist or it is Removed, refresh your page please!!" };
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
        //        return BaseModel.Failed(ex.Message);
        //    }
        //}
        //[Authorize(Roles = "Admin")]
        //public async Task<BaseModel> Update(BatchViewModel model)
        //{
        //    try
        //    {
        //        var batch = batchRepository.Get().FirstOrDefault(x => x.Id == model.Id);

        //        batch.Title = model.Title;

        //        await batchRepository.UpdateAsync(batch);
        //        await unitOfWork.SaveChangesAsync();
        //        return BaseModel.Succeeded("Batch Updated Successfully!");


        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "</br></br>" + ex.StackTrace);
        //        return BaseModel.Failed(ex.Message);
        //    }
        //}

        //[Authorize(Roles = "Admin")]
        //public async Task<PartialViewResult> Edit(int id)
        //{
        //    var batch = await batchRepository.Get().FirstOrDefaultAsync(a=>a.Id==id);
        //    var model = Mapper.Map<BatchViewModel>(batch);
        //    return PartialView("_EditBatch", model);
        //}


    }
}