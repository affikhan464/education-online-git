﻿using AutoMapper;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Management.Infrastructure.Communication;
using Management.Infrastructure.Security;
using Management.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class StudentController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IEncryptionDecryption security;
        private readonly IEmailService emailService;
        private readonly RoleManager<Role> roleManager;
        private readonly IUserRoleRepository userRoleManager;
        private IHostingEnvironment env;
        private readonly IUserArtifactRepository userArtifactRepository;
        private readonly IEducationLevelRepository educationLevelRepository;
        private readonly ISubjectRepository subjectRepository;
        private readonly IChapterRepository chapterRepository;

        private readonly IUserChapterRequestRepository userChapterRequestRepository;
        private readonly IUserChapterRepository userChapterRepository;

        private readonly IUserSubjectRequestRepository userSubjectRequestRepository;
        private readonly IUserSubjectRepository userSubjectRepository;
        private readonly IArtifactRepository artifactRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IBatchRepository batchRepository;
        private readonly IBatchTestRepository batchTestRepository;
        private readonly ITestRepository testRepository;
        private readonly IUserBatchRepository userBatchRepository;
        private readonly IUserTestAttemptRepository userTestAttemptRepository;
        private readonly IStudentStudyTypeRepository studentStudyTypeRepository;
        private readonly IScheduleRepository scheduleRepository;
        private readonly IUserBatchReferralRepository userBatchReferralRepository;

        public StudentController(IUserBatchReferralRepository _userBatchReferralRepository, IScheduleRepository _scheduleRepository, IStudentStudyTypeRepository _studentStudyTypeRepository, IUserRoleRepository _userRoleManager, RoleManager<Role> _roleManager, IUserTestAttemptRepository _userTestAttemptRepository, IBatchRepository _batchRepository, IUserBatchRepository _userBatchRepository, ITestRepository _testRepository, IBatchTestRepository _batchTestRepository, IUserChapterRepository _userChapterRepository, IUserChapterRequestRepository _userChapterRequestRepository, IChapterRepository _chapterRepository, IUserSubjectRequestRepository _userSubjectRequestRepository, IEncryptionDecryption _security, IEmailService _emailService, ISubjectRepository _subjectRepository, IEducationLevelRepository _educationLevelRepository, IUserSubjectRepository _userSubjectRepository, IUserArtifactRepository _userArtifactRepository, IHostingEnvironment _env, IArtifactRepository _artifactRepository, UserManager<User> _userManager, IUnitOfWork _unitOfWork)
        {
            educationLevelRepository = _educationLevelRepository;
            userManager = _userManager;
            userRoleManager = _userRoleManager;
            roleManager = _roleManager;
            unitOfWork = _unitOfWork;
            artifactRepository = _artifactRepository;
            env = _env;
            userArtifactRepository = _userArtifactRepository;
            userChapterRequestRepository = _userChapterRequestRepository;
            userChapterRepository = _userChapterRepository;
            userSubjectRequestRepository = _userSubjectRequestRepository;
            userSubjectRepository = _userSubjectRepository;
            subjectRepository = _subjectRepository;
            security = _security;
            emailService = _emailService;
            chapterRepository = _chapterRepository;
            batchTestRepository = _batchTestRepository;
            batchRepository = _batchRepository;
            testRepository = _testRepository;
            userBatchRepository = _userBatchRepository;
            userTestAttemptRepository = _userTestAttemptRepository;
            scheduleRepository = _scheduleRepository;
            studentStudyTypeRepository = _studentStudyTypeRepository;
            userBatchReferralRepository = _userBatchReferralRepository;
        }
        [Authorize(Roles = "Student")]
        public async Task<IActionResult> Index()
        {
            var loggedUser = await userManager.GetUserAsync(User);

            var batch = userBatchRepository.Get().Include(c => c.Batch).Where(x => x.UserId == loggedUser.Id).Select(a=>a.Batch);

            if (batch.Any())
            {
                ViewBag.IsBatchAssigned = true;
                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(loggedUser.Id, false);

                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);
                ViewBag.AllocatedSubjectCount = subjectsVModel.Count;
                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(loggedUser.Id, false);

                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);
                ViewBag.AllocatedChapterCount = chaptersVModel.Count;

                var schedules= scheduleRepository.Get().Include("Batch").Include("Batch.Subject").Where(schdl=> batch.Select(a=>a.Id).Contains(schdl.BatchId) && schdl.ScheduleType!=Convert.ToInt32(ScheduleTypes.Test) && DateTime.Now < schdl.EndDateTime);

                var scheduleVModel = Mapper.Map<List<ScheduleViewModel>>(schedules);
                scheduleVModel.ForEach(x => x.IsTestOnlyUser = studentStudyTypeRepository.Get().FirstOrDefault(a => a.StudentId == loggedUser.Id && a.BatchId == x.BatchId).StudyTypeId == Convert.ToInt32(StudyTypes.TestOnly));
                ViewBag.UserSchedules = scheduleVModel;
          
                ViewBag.SubjectAllocationRequestCount = userSubjectRequestRepository.Get().Where(a => a.UserId == loggedUser.Id && a.StatusId == Convert.ToInt32(RequestsStatus.Pending)).Count();
                ViewBag.ChapterAllocationRequestCount = userChapterRequestRepository.Get().Where(a => a.UserId == loggedUser.Id && a.StatusId == Convert.ToInt32(RequestsStatus.Pending)).Count();
                
                var attemptedTests = userTestAttemptRepository.Get().Include(x => x.Test).Include(x => x.User).Where(c => c.UserId == loggedUser.Id).ToList();
                ViewBag.AttemptedTests = attemptedTests;

                var batchTest = batchTestRepository.Get().Include(x => x.Test).Include(x => x.Batch).Include("Test.UserTestAttempts").Where(c => batch.Select(x => x.Id).Contains(c.BatchId)).ToList();
                var batchTestVModel = Mapper.Map<List<BatchTestViewModel>>(batchTest);
                ViewBag.BatchTests = batchTestVModel;
                ViewBag.VoucherCount = 0;
                //ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();
                //ViewBag.Subjects = subjectsVModel;
                //ViewBag.Chapters = subjectsVModel;

            }
            else
            {
                ViewBag.IsBatchAssigned = false;

            }

            return View();
        }

        [Route("MyVouchers")]
        [Authorize(Roles = "Student")]
        public IActionResult MyVouchers()
        {
            return View();
        }

        //[Authorize]
        //[HttpGet]
        //public async Task<PartialViewResult> LoadDisciplineMenu()
        //{
        // //   ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();
        //    var loggedUser = await userManager.GetUserAsync(User);
        //    var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
        //    var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(loggedUser.Id, isAdmin);
        //    var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);

        //    var chapters = chapterRepository.ListOfChaptersAllocatedToUser(loggedUser.Id, isAdmin);
        //    var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);

        //    ViewBag.Subjects = subjectsVModel;
        //    ViewBag.Chapters = chaptersVModel;
        //    return PartialView("Common/_DisciplineMenu");
        //}
        public IActionResult Register()
        {
            var hashCode = HttpContext.Request.QueryString.ToString();
            hashCode = hashCode.Replace("?u=", "");
            if (!string.IsNullOrEmpty(hashCode))
            {
                var userId = security.DecryptKey(hashCode, true);

                var user = userManager.Users.FirstOrDefault(u => u.Id == userId);
                var student = Mapper.Map<UserViewModel>(user);
                if (student != null)
                {
                    student.IsEditModel = true;

                }

                return PartialView(student);
            }
            else
                return View();

        }
        [Authorize(Roles = "Student")]
        public IActionResult Enroll()
        {
            return View();
        }
        [Authorize(Roles = "Student")]
        [Route("/Student/Enroll/DistanceLearning")]
        public IActionResult UpgradeToDistanceLearning(int id)
        {
            ViewBag.UserVouchers = 0;
            return View();
        }
        [Authorize(Roles = "Student")]
        [HttpPost]
        public async Task<BaseViewModel> Enroll(EnrollViewModel model)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var isStudentBatchExist = userBatchRepository.Get().Any(a => a.BatchId == model.BatchId && a.UserId == loggedUser.Id);
                var isStudentStudyTypeExist = studentStudyTypeRepository.Get().Where(a => a.BatchId == model.BatchId && a.StudyTypeId == model.StudyType && a.StudentId == loggedUser.Id);
                if (!isStudentStudyTypeExist.Any() && !isStudentBatchExist)
                {
                    var studenStudyType = new StudentStudyType()
                    {
                        BatchId = model.BatchId,
                        StudyTypeId = model.StudyType,
                        StudentId = loggedUser.Id
                    };
                    await studentStudyTypeRepository.AddAsync(studenStudyType);
                }
                else if (!isStudentStudyTypeExist.Any() && isStudentBatchExist)
                {
                    var existingStudenStudyType = isStudentStudyTypeExist.FirstOrDefault();
                    existingStudenStudyType.StudyTypeId = model.StudyType;
                    await studentStudyTypeRepository.UpdateAsync(existingStudenStudyType);
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "You are already enrolled in this batch!!" };
                }
                var isUserSubjectExist = userSubjectRepository.Get().Any(a => a.SubjectId == model.SubjectId && a.UserId == loggedUser.Id);
                if (!isUserSubjectExist && model.StudyType!=Convert.ToInt32(StudyTypes.TestOnly))
                {
                    var userSubject = new UserSubject()
                    {
                        SubjectId = model.SubjectId,
                        UserId = loggedUser.Id,
                        IsActive = true
                    };
                    await userSubjectRepository.AddAsync(userSubject);
                }
                if (!isStudentBatchExist)
                {
                    var userBatch = new UserBatch()
                    {
                        BatchId = model.BatchId,
                        UserId = loggedUser.Id,
                        IsActive = true
                    };
                    var Teachers = await userManager.GetUsersInRoleAsync("Teacher");
                    var batchTeacher = userBatchRepository.Get().Include("User").Include("Batch").FirstOrDefault(a => a.BatchId == model.BatchId && Teachers.Select(x => x.Id).Contains(a.UserId));
                    await userBatchRepository.AddAsync(userBatch);
                    await unitOfWork.SaveChangesAsync();
                    var mailModel = new Mail()
                    {
                        Teacher= batchTeacher.User,
                        Student= loggedUser,
                        Batch= batchTeacher.Batch
                    };
                    emailService.StudentEnrolledSuccessfully(mailModel);
                    
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "You are already enrolled in this batch!!" };
                }
                return new BaseViewModel() { Success = true, Message = "Enrolled Successfully!!" };

            }
            catch (Exception ex)
            {
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }
        [HttpPost]
        public async Task<BaseViewModel> Register(UserViewModel model)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var mailModel = new Mail()
                {
                    ToEMailAddress = model.Email,
                    UserName = model.FirstName
                };


                var isUserExist = userManager.Users.Any(u => u.Email == model.Email);
                if (!isUserExist)
                {
                    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                    var random = new Random();
                    var referralCode = new string(
                        Enumerable.Repeat(chars, 8)
                                  .Select(s => s[random.Next(s.Length)])
                                  .ToArray());

                    var user = new User()
                    {
                        Email = model.Email,
                        UserName = model.Email,
                        RegistrationDate = DateTime.Now,
                        IsActive = false,
                        AddedByUser = loggedUser,
                        MobileNumber = model.MobileNumber,
                        Address = model.Address,
                        NationalTaxNumber = model.NationalTaxNumber,
                        CNIC = model.CNIC,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Gender = model.Gender,
                        PhoneNumber = model.PhoneNumber,
                        SubjectExpertise = model.SubjectExpertise,
                        UserExperienceSummary = model.UserExperienceSummary,
                        YearOfExperience = model.YearOfExperience,
                        SchoolName = model.SchoolName,
                        CityId = model.CityId,
                        ReferralCode= referralCode,

                    };
                    
                    if (string.IsNullOrEmpty(model.Password))
                        await userManager.CreateAsync(user, "Student@123");
                    else
                        await userManager.CreateAsync(user, model.Password);

                    var roleId = roleManager.Roles.FirstOrDefault(a => a.Name == UserRoles.Student.ToString()).Id;

                    var userRole = new UserRole()
                    {
                        RoleId = roleId,
                        AppRoleId = roleId,
                        UserId = user.Id,
                        AppUserId = user.Id,
                    };
                    await userRoleManager.AddAsync(userRole);

                    //await userManager.AddToRoleAsync(user, UserRoles.Student.ToString());
                    await unitOfWork.SaveChangesAsync();
                    if (User != null)
                    {
                        var isAdminLogedin = User.IsInRole(UserRoles.Admin.ToString());

                        if (isAdminLogedin)
                        {
                            mailModel.UserId = user.Id;
                            emailService.StudentRegisteredByAdminMail(mailModel);
                        }
                        else
                        {

                            emailService.StudentRegisteredMail(mailModel);
                        }
                    }
                    else
                    {

                        emailService.StudentRegisteredMail(mailModel);
                    }
                    return new BaseViewModel() { Success = true, Message = "Student Added Successfully!!" };

                }
                else
                    return new BaseViewModel() { Success = false, Message = "Email Already Exist!!" };



            }
            catch (Exception ex)
            {
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }
        
        public async Task<BaseViewModel> UpdateStudent(UserViewModel model)
        {
            try
            {

                var user = userManager.Users.FirstOrDefault(u => u.Id == model.Id);

                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.MobileNumber = model.MobileNumber;
                user.NationalTaxNumber = model.NationalTaxNumber;
                user.PhoneNumber = model.PhoneNumber;
                user.SubjectExpertise = model.SubjectExpertise;
                user.UserExperienceSummary = model.UserExperienceSummary;
                user.Address = model.Address;
                user.CNIC = model.CNIC;
                user.DateOfBirth = model.DateOfBirth;
                user.Gender = model.Gender;
                user.Email = model.Email;
                user.UserName = model.Email;
                user.DateOfBirth = model.DateOfBirth;
                user.YearOfExperience = model.YearOfExperience;
                user.SchoolName = model.SchoolName;
                user.CityId = model.CityId;
                if (User != null)
                {
                    var isAdminLogedin = User.IsInRole(UserRoles.Admin.ToString());
                    if (!isAdminLogedin)
                    {
                        var resetPasswordToken = await userManager.GeneratePasswordResetTokenAsync(user);
                        await userManager.ResetPasswordAsync(user, resetPasswordToken, model.Password);

                    }
                }
                await userManager.UpdateAsync(user);
                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Information Updated Successfully!!" };




            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseViewModel> Delete(string userId)
        {
            try
            {
                var user = userManager.Users.FirstOrDefault(u => u.Id == userId);
                user.IsDeleted = true;
                user.Email = user.Email + "DELETED";
                user.UserName = user.UserName + "DELETED";
                user.NormalizedEmail = user.NormalizedEmail + "DELETED";
                user.NormalizedUserName = user.NormalizedUserName + "DELETED";

                var isdeletedAlready = userManager.Users.FirstOrDefault(u => u.Email == user.Email);
                if (isdeletedAlready != null)
                {
                    await userManager.DeleteAsync(isdeletedAlready);
                }

                await userManager.UpdateAsync(user);

                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Student Deleted Successfully!!" };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<PartialViewResult> Edit(string userId)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var user = userManager.Users.Include(x => x.City).Include("City.Region").Include("City.Region.Country").FirstOrDefault(u => u.Id == userId);
                UserViewModel student = new UserViewModel();
                if (isAdmin || loggedUser.Id == user.Id)
                {
                    student = Mapper.Map<UserViewModel>(user);

                }
                student.IsEditModel = true;


                return PartialView("_StudentRegistraionForm", student);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        [HttpGet]
        public async Task<PartialViewResult> Get(string userId)
        {
            try
            {
                var user = await userManager.FindByIdAsync(userId);
                var role = await userManager.GetRolesAsync(user);
                var userArtifact = userArtifactRepository.Get().FirstOrDefault(x => x.UserId == userId);
                var profilePic = userArtifact != null ? artifactRepository.Get().FirstOrDefault(a => a.Id == userArtifact.ArtifactId).FileName : "";

                var student = Mapper.Map<UserViewModel>(user);
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(userId, isAdmin);
                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);

                student.AllocatedSubjects = subjectsVModel;

                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(user.Id, isAdmin);
                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);

                student.AllocatedChapters = chaptersVModel;

                student.ProfilePictureUrl = profilePic;
                student.Role = role.Contains(UserRoles.Admin.ToString()) ? UserRoles.Admin : UserRoles.Student;
                return PartialView("_StudentDetail", student);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        [HttpGet]
        public async Task<PartialViewResult> GetByEmail(string email)
        {
            try
            {
                var user = await userManager.FindByEmailAsync(email);
                var role = await userManager.GetRolesAsync(user);
                var userArtifact = userArtifactRepository.Get().FirstOrDefault(x => x.UserId == user.Id);
                var profilePic = userArtifact != null ? artifactRepository.Get().FirstOrDefault(a => a.Id == userArtifact.ArtifactId).FileName : "";

                var student = Mapper.Map<UserViewModel>(user);
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(loggedUser.Id, isAdmin);
                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(loggedUser.Id, isAdmin);
                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);
                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);

                student.AllocatedSubjects = subjectsVModel;
                student.AllocatedChapters = chaptersVModel;
                student.ProfilePictureUrl = profilePic;
                student.Role = role.Contains(UserRoles.Admin.ToString()) ? UserRoles.Admin : UserRoles.Student;
                return PartialView("_StudentDetail", student);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseViewModel> Block(string userId)
        {
            try
            {
                var user = userManager.Users.FirstOrDefault(u => u.Id == userId);
                var state = string.Empty;
                if (user.IsActive)
                {
                    user.IsActive = false;
                    user.EmailConfirmed = false;
                    state = UserState.Blocked;
                }
                else
                {
                    state = UserState.Reinstated;
                    user.EmailConfirmed = true;
                    user.IsActive = true;
                }
                await userManager.UpdateAsync(user);

                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Student " + state + " Successfully!!", State = state };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }


        [Authorize]
        [HttpPost]
        public async Task<BaseViewModel> AddProfile(UserViewModel model)
        {

            try
            {

                var webRoot = env.WebRootPath + "\\UploadedArtifacts";
                var filePath = System.IO.Path.Combine(webRoot, model.FileName);


                var user = userManager.Users.FirstOrDefault(x => x.Id == model.Id);
                var isUserArtifactExist = userArtifactRepository.Get().Where(a => a.UserId == user.Id);
                Artifact artifact;

                if (isUserArtifactExist.Any())
                {
                    var artifactId = isUserArtifactExist.FirstOrDefault().ArtifactId;
                    artifact = artifactRepository.Get().FirstOrDefault(a => a.Id == artifactId);
                    if (!string.IsNullOrEmpty(artifact.URL))
                    {
                        var isFileExist = System.IO.File.Exists(artifact.URL);
                        if (isFileExist)
                        {
                            System.IO.File.Delete(artifact.URL);

                        }
                    }

                    artifact.URL = filePath;
                    artifact.FileName = model.FileName;



                    await artifactRepository.UpdateAsync(artifact);
                }
                else
                {
                    artifact = new Artifact()
                    {
                        URL = filePath,
                        FileName = model.FileName
                    };
                    await artifactRepository.AddAsync(artifact);

                }


                var userArtifact = new UserArtifact()
                {
                    UserId = user.Id,
                    ArtifactId = artifact.Id
                };


                //var artifactNewId =  artifactRepository.AddAsync(artifact);
                await userArtifactRepository.AddAsync(userArtifact);
                await unitOfWork.SaveChangesAsync();
                return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                throw;
            }
        }


        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseViewModel> DeallocateChapter(int chapterId, string userId)
        {

            try
            {

                var isUserChapterExist = userChapterRepository.Get().FirstOrDefault(a => a.UserId == userId && a.ChapterId == chapterId && a.IsActive);


                if (isUserChapterExist != null)
                {
                    isUserChapterExist.IsActive = false;
                    await userChapterRepository.UpdateAsync(isUserChapterExist);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseViewModel() { Success = true, Message = "Chapter Deallocated Successfully!!" };
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "No such chapter exist or may be it is already deallocated." };
                }

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };

            }
        }





        [Authorize(Roles = "Admin,Student")]
        public async Task<IActionResult> AllocatedChapters()
        {

            try
            {
                //   ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(loggedUser.Id, isAdmin);

                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);
                var student = new UserViewModel();
                student.AllocatedChapters = chaptersVModel;
                ViewBag.Chapters = chaptersVModel;
                ViewBag.Student = student;
                return View(chaptersVModel);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        [Authorize(Roles = "Admin")]
        public async Task<PartialViewResult> AllocatedSubjectsByUserId(string id)
        {

            try
            {

                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(id, isAdmin);

                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);
                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(id, isAdmin);

                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);
                var subjectAndChaptersAllocated = new SubjectsAndChaptersViewModel();

                subjectAndChaptersAllocated.AllocatedSubjects = subjectsVModel;
                subjectAndChaptersAllocated.AllocatedChapters = chaptersVModel;
                return PartialView("~/Views/Shared/Common/_AllocatedSubjectsAndChapters.cshtml", subjectAndChaptersAllocated);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        //[Authorize(Roles = "Admin")]
        //public async Task<IActionResult> AllocatedChaptersByUserId(string id)
        //{

        //    try
        //    {

        //        var loggedUser = await userManager.GetUserAsync(User);
        //        var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
        //        var chapters = chapterRepository.ListOfChaptersAllocatedToUser(id, isAdmin);

        //        var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);

        //        return View("~/Views/Shared/Common/AllocatedChapters.cshtml", chaptersVModel);

        //    }
        //    catch (Exception ex)
        //    {
        //        emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
        //        return null;
        //    }
        //}
        [Authorize]
        [HttpPost]
        public async Task<BaseViewModel> DeleteProfile(string Id)
        {

            try
            {
                var user = userManager.Users.FirstOrDefault(x => x.Id == Id);

                var isUserArtifactExist = userArtifactRepository.Get().Where(a => a.UserId == user.Id);

                if (isUserArtifactExist.Any())
                {
                    var artifactId = isUserArtifactExist.FirstOrDefault().ArtifactId;
                    var artifact = artifactRepository.Get().FirstOrDefault(a => a.Id == artifactId);
                    if (!string.IsNullOrEmpty(artifact.URL))
                    {
                        var isFileExist = System.IO.File.Exists(artifact.URL);
                        if (isFileExist)
                        {
                            System.IO.File.Delete(artifact.URL);

                        }
                    }
                    await userArtifactRepository.DeleteAsync(isUserArtifactExist.FirstOrDefault());
                    await unitOfWork.SaveChangesAsync();
                    await artifactRepository.DeleteAsync(artifact);
                }

                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }

            // the anonymous object in the result below will be convert to json and set back to the browser

        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<PartialViewResult> Students()
        {

            var students = await userManager.GetUsersInRoleAsync("Student");
            var model = Mapper.Map<List<UserViewModel>>(students.Where(a => a.IsDeleted == false));
            var userArtifacts = userArtifactRepository.Get().Where(x => model.Select(a => a.Id).Contains(x.UserId)).Include(a => a.Artifact);
            foreach (var user in model)
            {
                var userArtifact = userArtifacts.FirstOrDefault(x => x.UserId == user.Id);
                var profilePic = userArtifact != null ? userArtifact.Artifact.FileName : "";
                user.ProfilePictureUrl = profilePic;
            }
            return PartialView("_Students", model);
        }
        [AllowAnonymous]
        public IActionResult ThankYou()
        {
            return View();
        }
    }
}