﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Infrastructure.Security.Identity;
using Management.Data.Models;
using AutoMapper;
using Management.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Management.Models;
using Management.Data.Models.CustomModels;
using Management.Infrastructure.Security;
using Management.Infrastructure.Communication;
using Microsoft.EntityFrameworkCore;
using MoreLinq;

namespace Management.Controllers
{

    public class TeacherController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IEncryptionDecryption security;
        private readonly IEmailService emailService;
        private readonly RoleManager<Role> roleManager;
        private readonly IUserRoleRepository userRoleManager;
        private IHostingEnvironment env;
        private readonly IUserArtifactRepository userArtifactRepository;
        private readonly IEducationLevelRepository educationLevelRepository;
        private readonly ISubjectRepository subjectRepository;
        private readonly IChapterRepository chapterRepository;

        private readonly IUserChapterRequestRepository userChapterRequestRepository;
        private readonly IUserChapterRepository userChapterRepository;

        private readonly IUserSubjectRequestRepository userSubjectRequestRepository;
        private readonly IUserSubjectRepository userSubjectRepository;
        private readonly IArtifactRepository artifactRepository;
        private readonly IUserBatchRepository userBatchRepository;
        private readonly IUnitOfWork unitOfWork;


        private readonly IScheduleRepository scheduleRepository;
        public TeacherController(IScheduleRepository _scheduleRepository, IUserRoleRepository _userRoleManager, RoleManager<Role> _roleManager, IUserBatchRepository _userBatchRepository, IUserChapterRepository _userChapterRepository, IUserChapterRequestRepository _userChapterRequestRepository, IChapterRepository _chapterRepository, IUserSubjectRequestRepository _userSubjectRequestRepository, IEncryptionDecryption _security, IEmailService _emailService, ISubjectRepository _subjectRepository, IEducationLevelRepository _educationLevelRepository, IUserSubjectRepository _userSubjectRepository, IUserArtifactRepository _userArtifactRepository, IHostingEnvironment _env, IArtifactRepository _artifactRepository, UserManager<User> _userManager, IUnitOfWork _unitOfWork)
        {
            educationLevelRepository = _educationLevelRepository;

            userManager = _userManager;
            userRoleManager = _userRoleManager;
            roleManager = _roleManager;
            unitOfWork = _unitOfWork;
            artifactRepository = _artifactRepository;
            env = _env;
            userArtifactRepository = _userArtifactRepository;
            userChapterRequestRepository = _userChapterRequestRepository;
            userChapterRepository = _userChapterRepository;
            userSubjectRequestRepository = _userSubjectRequestRepository;
            userSubjectRepository = _userSubjectRepository;
            subjectRepository = _subjectRepository;
            security = _security;
            emailService = _emailService;
            chapterRepository = _chapterRepository;
            userBatchRepository = _userBatchRepository;
            scheduleRepository = _scheduleRepository;
        }
        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> Index()
        {
            var loggedUser = await userManager.GetUserAsync(User);
            //var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
            var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(loggedUser.Id, false);

            var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);
            ViewBag.AllocatedSubjectCount = subjectsVModel.Count;

            var chapters = chapterRepository.ListOfChaptersAllocatedToUser(loggedUser.Id, false);

            var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);
            ViewBag.AllocatedChapterCount = chaptersVModel.Count;

            ViewBag.SubjectAllocationRequestCount = userSubjectRequestRepository.Get().Where(a => a.UserId == loggedUser.Id && a.StatusId == Convert.ToInt32(RequestsStatus.Pending)).Count();
            ViewBag.ChapterAllocationRequestCount = userChapterRequestRepository.Get().Where(a => a.UserId == loggedUser.Id && a.StatusId == Convert.ToInt32(RequestsStatus.Pending)).Count();


            var batch = userBatchRepository.Get().Include(c => c.Batch).Where(x => x.UserId == loggedUser.Id).Select(a => a.Batch);
            var schedules = scheduleRepository.Get().Include("Batch").Include("Batch.Subject").Where(schdl => batch.Select(a => a.Id).Contains(schdl.BatchId) && schdl.ScheduleType != Convert.ToInt32(ScheduleTypes.Test) && DateTime.Now < schdl.EndDateTime);

            var scheduleVModel = Mapper.Map<List<ScheduleViewModel>>(schedules);
            ViewBag.UserSchedules = scheduleVModel;
            //ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();
            //ViewBag.Subjects = subjectsVModel;
            //ViewBag.Chapters = subjectsVModel;
            return View();
        }

        [AllowAnonymous]
        public IActionResult ThankYou()
        {
            return View();
        }
        [Authorize(Roles = "Teacher")]

        public async Task<IActionResult> OnlineClass(int id)
        {

            var schedules = scheduleRepository.Get().Include("Batch").Include("Batch.Subject").FirstOrDefault(schdl => schdl.Id == id);

            var Teachers = await userManager.GetUsersInRoleAsync("Teacher");
            var batchTeacher = userBatchRepository.Get().Include("User").Include("Batch").FirstOrDefault(a => a.BatchId == schedules.BatchId && Teachers.Select(x => x.Id).Contains(a.UserId)).User;

            var scheduleVModel = Mapper.Map<ScheduleViewModel>(schedules);
            scheduleVModel.BatchTeacher = batchTeacher;
            ViewBag.UserSchedules = scheduleVModel;

            return View();
        }
        //[Authorize(Roles = "Admin,Teacher")]
        //[HttpGet]
        //public async Task<PartialViewResult> LoadDisciplineMenu()
        //{
        //    ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();
        //    var loggedUser = await userManager.GetUserAsync(User);
        //    var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
        //    var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(loggedUser.Id,isAdmin);
        //    var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);

        //    var chapters = chapterRepository.ListOfChaptersAllocatedToUser(loggedUser.Id, isAdmin);
        //    var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);

        //    ViewBag.Subjects = subjectsVModel;
        //    ViewBag.Chapters = chaptersVModel;
        //    return PartialView("Common/_DisciplineMenu");
        //}
        public IActionResult Register()
        {
            var hashCode = HttpContext.Request.QueryString.ToString();
            hashCode = hashCode.Replace("?u=", "");
            if (!string.IsNullOrEmpty(hashCode))
            {
                var decryptedHash = security.DecryptKey(hashCode, true);
                var userId = decryptedHash.Split("###")[0];
                var passwordResetToken = decryptedHash.Split("###")[1];
                var user = userManager.Users.FirstOrDefault(u => u.Id == userId);
                var Teacher = Mapper.Map<UserViewModel>(user);
                if (Teacher != null)
                {
                    Teacher.IsEditModel = true;
                    Teacher.PasswordResetToken = passwordResetToken;
                }

                return PartialView(Teacher);
            }
            else
                return View();

        }
        [HttpPost]
        public async Task<BaseViewModel> Register(UserViewModel model)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var mailModel = new Mail()
                {
                    ToEMailAddress = model.Email,
                    UserName = model.FirstName
                };


                var isUserExist = userManager.Users.Any(u => u.Email == model.Email);
                if (!isUserExist)
                {

                    var user = new User()
                    {
                        Email = model.Email,
                        UserName = model.Email,
                        RegistrationDate = DateTime.Now,
                        IsActive = false,
                        AddedByUser = loggedUser,
                        MobileNumber = model.MobileNumber,
                        Address = model.Address,
                        NationalTaxNumber = model.NationalTaxNumber,
                        CNIC = model.CNIC,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Gender = model.Gender,
                        PhoneNumber = model.PhoneNumber,
                        SubjectExpertise = model.SubjectExpertise,
                        UserExperienceSummary = model.UserExperienceSummary,
                        YearOfExperience = model.YearOfExperience,

                    };
                    if (string.IsNullOrEmpty(model.Password))
                        await userManager.CreateAsync(user, "Teacher@123");
                    else
                        await userManager.CreateAsync(user, model.Password);

                    await unitOfWork.SaveChangesAsync();
                    var r = new Role()
                    {
                        Name = UserRoles.Teacher.ToString()
                    };
                    var roleId = roleManager.Roles.FirstOrDefault(a => a.Name == UserRoles.Teacher.ToString()).Id;

                    var userRole = new UserRole()
                    {
                        RoleId = roleId,
                        AppRoleId = roleId,
                        UserId = user.Id,
                        AppUserId = user.Id,
                    };
                    await userRoleManager.AddAsync(userRole);
                    //await userManager.AddToRoleAsync(user, UserRoles.Teacher.ToString());

                    await unitOfWork.SaveChangesAsync();
                    if (User != null)
                    {
                        var isAdminLogedin = User.IsInRole(UserRoles.Admin.ToString());

                        if (isAdminLogedin)
                        {
                            mailModel.UserId = user.Id;
                            var resetPasswordToken = await userManager.GeneratePasswordResetTokenAsync(user);
                            mailModel.ResetPasswordToken = resetPasswordToken;
                            emailService.TeacherRegisteredByAdminMail(mailModel);
                        }
                        else
                        {

                            emailService.TeacherRegisteredMail(mailModel);
                        }
                    }
                    else
                    {

                        emailService.TeacherRegisteredMail(mailModel);
                    }
                    return new BaseViewModel() { Success = true, Message = "Teacher Added Successfully!!" };

                }
                else
                    return new BaseViewModel() { Success = false, Message = "Email Already Exist!!" };



            }
            catch (Exception ex)
            {
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }

        [AllowAnonymous]
        public async Task<BaseViewModel> UpdateTeacher(UserViewModel model)
        {
            try
            {

                var user = userManager.Users.FirstOrDefault(u => u.Id == model.Id);

                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.MobileNumber = model.MobileNumber;
                user.NationalTaxNumber = model.NationalTaxNumber;
                user.PhoneNumber = model.PhoneNumber;
                user.SubjectExpertise = model.SubjectExpertise;
                user.UserExperienceSummary = model.UserExperienceSummary;
                user.Address = model.Address;
                user.CNIC = model.CNIC;
                user.DateOfBirth = model.DateOfBirth;
                user.Gender = model.Gender;
                user.Email = model.Email;
                user.UserName = model.Email;
                user.DateOfBirth = model.DateOfBirth;
                user.YearOfExperience = model.YearOfExperience;
                var isAuthorizedToUpdate = false;
                var loggedUser = await userManager.GetUserAsync(User);

                if (loggedUser != null)
                {

                    var isAdminLogedin = User.IsInRole(UserRoles.Admin.ToString());
                    if (!isAdminLogedin )
                    {
                        var resetPasswordToken = await userManager.GeneratePasswordResetTokenAsync(user);
                        await userManager.ResetPasswordAsync(user, resetPasswordToken, model.Password);
                        isAuthorizedToUpdate = true;
                    }
                    else
                    {
                        isAuthorizedToUpdate = true;
                    }
                }
                else
                {
                    var resetPasswordToken = model.PasswordResetToken;
                    var status = await userManager.ResetPasswordAsync(user, resetPasswordToken, model.Password);
                    if (status.Succeeded)
                    {
                        isAuthorizedToUpdate = true;
                    }
                    else
                    {
                        return new BaseViewModel() { Success = false, Message = "Token expired you need to reset your password again by clicking on 'Forgot Password' !!" };

                    }
                }
                if (isAuthorizedToUpdate)
                {

                    await userManager.UpdateAsync(user);
                    await unitOfWork.SaveChangesAsync();
                    return new BaseViewModel() { Success = true, Message = "Information Updated Successfully!!" };
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "You are not authorized to update user secured informations !!" };

                }





            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }

        [Authorize(Roles = "Admin")]
        public BaseModel TeacherInvite(Mail model)
        {
            try
            {
                emailService.TeacherInvite(model);

                return BaseModel.Succeeded("Invitation Sent Successfully!!");
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return BaseModel.Failed(ex.Message);
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseViewModel> Delete(string userId)
        {
            try
            {
                var user = userManager.Users.FirstOrDefault(u => u.Id == userId);
                user.IsDeleted = true;
                user.Email = user.Email + "DELETED";
                user.UserName = user.UserName + "DELETED";
                user.NormalizedEmail = user.NormalizedEmail + "DELETED";
                user.NormalizedUserName = user.NormalizedUserName + "DELETED";
                var isdeletedAlready = userManager.Users.FirstOrDefault(u => u.Email == user.Email);
                if (isdeletedAlready != null)
                {
                    await userManager.DeleteAsync(isdeletedAlready);
                }
                await userManager.UpdateAsync(user);

                await unitOfWork.SaveChangesAsync();
                return new BaseViewModel() { Success = true, Message = "Teacher Deleted Successfully!!" };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpPost]
        public async Task<PartialViewResult> Edit(string userId)
        {
            try
            {

                var user = userManager.Users.FirstOrDefault(u => u.Id == userId);
                var loggedUser = await userManager.GetUserAsync(User);

                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                UserViewModel Teacher = new UserViewModel();
                if (isAdmin || loggedUser.Id == user.Id)
                {
                    Teacher = Mapper.Map<UserViewModel>(user);

                }
                Teacher.IsEditModel = true;



                return PartialView("_TeacherRegistraionForm", Teacher);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpGet]
        public async Task<PartialViewResult> Get(string userId)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var user = await userManager.FindByIdAsync(userId);
                var role = await userManager.GetRolesAsync(user);
                var userArtifact = userArtifactRepository.Get().FirstOrDefault(x => x.UserId == userId);
                var profilePic = userArtifact != null ? artifactRepository.Get().FirstOrDefault(a => a.Id == userArtifact.ArtifactId).FileName : "";

                var Teacher = Mapper.Map<UserViewModel>(user);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(user.Id, isAdmin);
                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);

                Teacher.AllocatedSubjects = subjectsVModel;

                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(userId, isAdmin);

                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);

                Teacher.AllocatedChapters = chaptersVModel;

                Teacher.ProfilePictureUrl = profilePic;
                Teacher.Role = role.Contains(UserRoles.Admin.ToString()) ? UserRoles.Admin : UserRoles.Teacher;
                return PartialView("_TeacherDetail", Teacher);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpGet]
        public async Task<PartialViewResult> GetByEmail(string email)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var user = await userManager.FindByEmailAsync(email);
                var role = await userManager.GetRolesAsync(user);
                var userArtifact = userArtifactRepository.Get().FirstOrDefault(x => x.UserId == user.Id);
                var profilePic = userArtifact != null ? artifactRepository.Get().FirstOrDefault(a => a.Id == userArtifact.ArtifactId).FileName : "";

                var Teacher = Mapper.Map<UserViewModel>(user);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(loggedUser.Id, isAdmin);
                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(loggedUser.Id, isAdmin);
                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);
                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);

                Teacher.AllocatedSubjects = subjectsVModel;
                Teacher.AllocatedChapters = chaptersVModel;

                Teacher.ProfilePictureUrl = profilePic;
                Teacher.Role = role.Contains(UserRoles.Admin.ToString()) ? UserRoles.Admin : UserRoles.Teacher;
                return PartialView("_TeacherDetail", Teacher);
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<BaseViewModel> Block(string userId)
        {
            try
            {
                var user = userManager.Users.FirstOrDefault(u => u.Id == userId);
                var state = string.Empty;
                if (user.IsActive)
                {
                    user.IsActive = false;
                    user.EmailConfirmed = false;
                    state = UserState.Blocked;
                }
                else
                {
                    state = UserState.Reinstated;
                    user.EmailConfirmed = true;
                    user.IsActive = true;
                }
                await userManager.UpdateAsync(user);

                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Teacher " + state + " Successfully!!", State = state };
            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }


        [Authorize]
        [HttpPost]
        public async Task<BaseViewModel> AddProfile(UserViewModel model)
        {

            try
            {

                var webRoot = env.WebRootPath + "\\UploadedArtifacts";
                var filePath = System.IO.Path.Combine(webRoot, model.FileName);


                var user = userManager.Users.FirstOrDefault(x => x.Id == model.Id);
                var isUserArtifactExist = userArtifactRepository.Get().Where(a => a.UserId == user.Id);
                Artifact artifact;

                if (isUserArtifactExist.Any())
                {
                    var artifactId = isUserArtifactExist.FirstOrDefault().ArtifactId;
                    artifact = artifactRepository.Get().FirstOrDefault(a => a.Id == artifactId);
                    if (!string.IsNullOrEmpty(artifact.URL))
                    {
                        var isFileExist = System.IO.File.Exists(artifact.URL);
                        if (isFileExist)
                        {
                            System.IO.File.Delete(artifact.URL);

                        }
                    }

                    artifact.URL = filePath;
                    artifact.FileName = model.FileName;



                    await artifactRepository.UpdateAsync(artifact);
                }
                else
                {
                    artifact = new Artifact()
                    {
                        URL = filePath,
                        FileName = model.FileName
                    };
                    await artifactRepository.AddAsync(artifact);

                }


                var userArtifact = new UserArtifact()
                {
                    UserId = user.Id,
                    ArtifactId = artifact.Id
                };


                //var artifactNewId =  artifactRepository.AddAsync(artifact);
                await userArtifactRepository.AddAsync(userArtifact);
                await unitOfWork.SaveChangesAsync();
                return new BaseViewModel() { Success = true, Message = "Saved Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                throw;
            }
        }





        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> AllocatedChapters()
        {

            try
            {
                //ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var chapters = chapterRepository.ListOfChaptersAllocatedToUser(loggedUser.Id, isAdmin);

                var chaptersVModel = Mapper.Map<List<ChapterViewModel>>(chapters);
                var Teacher = new UserViewModel();
                Teacher.AllocatedChapters = chaptersVModel;
                ViewBag.Chapters = chaptersVModel;
                ViewBag.Teacher = Teacher;
                return View(chaptersVModel);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Teacher")]
        public async Task<IActionResult> AllocatedSubjects()
        {

            try
            {
                //ViewBag.EducationLevel = educationLevelRepository.GetEducationLevelWithBoardLevels();

                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var subjects = subjectRepository.ListOfSubjectsAllocatedToUser(loggedUser.Id, isAdmin);

                var subjectsVModel = Mapper.Map<List<SubjectViewModel>>(subjects);
                var Teacher = new UserViewModel();
                Teacher.AllocatedSubjects = subjectsVModel;
                ViewBag.Subjects = subjectsVModel;
                ViewBag.Teacher = Teacher;
                return View(subjectsVModel);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }

        [Authorize(Roles = "Admin,Teacher")]
        [HttpPost]
        public async Task<BaseViewModel> DeleteProfile(string Id)
        {

            try
            {
                var user = userManager.Users.FirstOrDefault(x => x.Id == Id);

                var isUserArtifactExist = userArtifactRepository.Get().Where(a => a.UserId == user.Id);

                if (isUserArtifactExist.Any())
                {
                    var artifactId = isUserArtifactExist.FirstOrDefault().ArtifactId;
                    var artifact = artifactRepository.Get().FirstOrDefault(a => a.Id == artifactId);
                    if (!string.IsNullOrEmpty(artifact.URL))
                    {
                        var isFileExist = System.IO.File.Exists(artifact.URL);
                        if (isFileExist)
                        {
                            System.IO.File.Delete(artifact.URL);

                        }
                    }
                    await userArtifactRepository.DeleteAsync(isUserArtifactExist.FirstOrDefault());
                    await unitOfWork.SaveChangesAsync();
                    await artifactRepository.DeleteAsync(artifact);
                }

                await unitOfWork.SaveChangesAsync();

                return new BaseViewModel() { Success = true, Message = "Deleted Successfully!!" };

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }

            // the anonymous object in the result below will be convert to json and set back to the browser

        }


        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<PartialViewResult> Teachers()
        {
            var Teachers = await userManager.GetUsersInRoleAsync("Teacher");
            var model = Mapper.Map<List<UserViewModel>>(Teachers.Where(a => a.IsDeleted == false));
            var userArtifacts = userArtifactRepository.Get().Where(x => model.Select(a => a.Id).Contains(x.UserId)).Include(a => a.Artifact);
            foreach (var user in model)
            {
                var userArtifact = userArtifacts.FirstOrDefault(x => x.UserId == user.Id);
                var profilePic = userArtifact != null ? userArtifact.Artifact.FileName : "";
                user.ProfilePictureUrl = profilePic;
            }
            return PartialView("_Teachers", model);
        }

        [Authorize(Roles = "Admin,Student")]
        [HttpGet]
        public async Task<PartialViewResult> GetTeachersBySubjectId(int subjectId)
        {

            //&& a.User.UserRole.Role.Name == UserRoles.Teacher.ToString()
            var teachers = userSubjectRepository.Get().Include(x => x.User).Include("User.UserRoles").Include("User.UserRoles.Role").Where(a => a.SubjectId == subjectId && !a.User.IsDeleted && a.User.UserRoles.FirstOrDefault().Role.Name == UserRoles.Teacher.ToString()).DistinctBy(a => a.UserId).Select(a => a.User).ToList();
            var model = Mapper.Map<List<UserViewModel>>(teachers);
            //var userArtifacts = userArtifactRepository.Get().Where(x => model.Select(a => a.Id).Contains(x.UserId)).Include(a=>a.Artifact);
            //foreach (var user in model)
            //{
            //    var userArtifact = userArtifacts.FirstOrDefault(x => x.UserId == user.Id);
            //    var profilePic = userArtifact != null ? userArtifact.Artifact.FileName : "";
            //    user.ProfilePictureUrl = profilePic;
            //}
            return PartialView("_TeachersCardList", model);
        }
        [Authorize(Roles = "Admin")]
        public async Task<PartialViewResult> AllocatedBatchesByUserId(string id)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var isAdmin = await userManager.IsInRoleAsync(loggedUser, UserRoles.Admin.ToString());
                var batches = new List<UserBatch>();
                if (isAdmin)
                {
                    batches = userBatchRepository.Get().Where(a => a.UserId == id).Include(x => x.Batch).Include("Batch.UserBatchRequests").Include(x => x.User).ToList();
                }
                else
                {
                    batches = userBatchRepository.Get().Where(a => a.UserId == id && a.IsActive == true).Include(x => x.Batch).Include("Batch.UserBatchRequests").Include(x => x.User).ToList();
                }
                var BatchesVModel = Mapper.Map<List<UserBatchViewModel>>(batches);
                return PartialView("~/Views/Shared/Common/_AllocatedBatches.cshtml", BatchesVModel);

            }
            catch (Exception ex)
            {
                emailService.SendException(ex.Message + "<br/><br/> Inner Exception<br/><br/> " + ex.InnerException);
                return null;
            }
        }
    }
}