﻿using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data.Models;
using Management.Infrastructure.Communication;
using Management.Infrastructure.Security;
using Management.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Controllers
{
    public class ReferralController : Controller
    {
        private readonly UserManager<User> userManager;
        private readonly IEncryptionDecryption security;
        private readonly IEmailService emailService;
        private readonly RoleManager<Role> roleManager;
        private readonly IUserRoleRepository userRoleManager;
        private IHostingEnvironment env;
        private readonly IUserArtifactRepository userArtifactRepository;
        private readonly IEducationLevelRepository educationLevelRepository;
        private readonly ISubjectRepository subjectRepository;
        private readonly IChapterRepository chapterRepository;

        private readonly IUserChapterRequestRepository userChapterRequestRepository;
        private readonly IUserChapterRepository userChapterRepository;

        private readonly IUserSubjectRequestRepository userSubjectRequestRepository;
        private readonly IUserSubjectRepository userSubjectRepository;
        private readonly IArtifactRepository artifactRepository;
        private readonly IUnitOfWork unitOfWork;
        private readonly IBatchRepository batchRepository;
        private readonly IBatchTestRepository batchTestRepository;
        private readonly ITestRepository testRepository;
        private readonly IUserBatchRepository userBatchRepository;
        private readonly IUserTestAttemptRepository userTestAttemptRepository;
        private readonly IStudentStudyTypeRepository studentStudyTypeRepository;
        private readonly IScheduleRepository scheduleRepository;
        private readonly IUserBatchReferralRepository userBatchReferralRepository;

        public ReferralController(IUserBatchReferralRepository _userBatchReferralRepository, IScheduleRepository _scheduleRepository, IStudentStudyTypeRepository _studentStudyTypeRepository, IUserRoleRepository _userRoleManager, RoleManager<Role> _roleManager, IUserTestAttemptRepository _userTestAttemptRepository, IBatchRepository _batchRepository, IUserBatchRepository _userBatchRepository, ITestRepository _testRepository, IBatchTestRepository _batchTestRepository, IUserChapterRepository _userChapterRepository, IUserChapterRequestRepository _userChapterRequestRepository, IChapterRepository _chapterRepository, IUserSubjectRequestRepository _userSubjectRequestRepository, IEncryptionDecryption _security, IEmailService _emailService, ISubjectRepository _subjectRepository, IEducationLevelRepository _educationLevelRepository, IUserSubjectRepository _userSubjectRepository, IUserArtifactRepository _userArtifactRepository, IHostingEnvironment _env, IArtifactRepository _artifactRepository, UserManager<User> _userManager, IUnitOfWork _unitOfWork)
        {
            educationLevelRepository = _educationLevelRepository;
            userManager = _userManager;
            userRoleManager = _userRoleManager;
            roleManager = _roleManager;
            unitOfWork = _unitOfWork;
            artifactRepository = _artifactRepository;
            env = _env;
            userArtifactRepository = _userArtifactRepository;
            userChapterRequestRepository = _userChapterRequestRepository;
            userChapterRepository = _userChapterRepository;
            userSubjectRequestRepository = _userSubjectRequestRepository;
            userSubjectRepository = _userSubjectRepository;
            subjectRepository = _subjectRepository;
            security = _security;
            emailService = _emailService;
            chapterRepository = _chapterRepository;
            batchTestRepository = _batchTestRepository;
            batchRepository = _batchRepository;
            testRepository = _testRepository;
            userBatchRepository = _userBatchRepository;
            userTestAttemptRepository = _userTestAttemptRepository;
            scheduleRepository = _scheduleRepository;
            studentStudyTypeRepository = _studentStudyTypeRepository;
            userBatchReferralRepository = _userBatchReferralRepository;
        }

        [Authorize(Roles = "Student")]
        [HttpPost]
        public async Task<BaseViewModel> ReferFriend(UserBatchReferralViewModel model)
        {
            try
            {
                var loggedUser = await userManager.GetUserAsync(User);
                var isUserBatchRefExist = userBatchReferralRepository.Get().Where(a => a.BatchId == model.BatchId && a.ReferringUserEmail == model.ReferringUserEmail && a.ReferralUserId == loggedUser.Id);

                if (!isUserBatchRefExist.Any())
                {
                    var referralCode = loggedUser.ReferralCode;
                    if (referralCode == null)
                    {
                        var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                        var random = new Random();
                        referralCode = new string(
                           Enumerable.Repeat(chars, 8)
                                     .Select(s => s[random.Next(s.Length)])
                                     .ToArray());

                        loggedUser.ReferralCode = referralCode;
                        await userManager.UpdateAsync(loggedUser);
                    }

                    var userBatchReferral = new UserBatchReferral()
                    {
                        BatchId = model.BatchId,
                        ReferringUserEmail = model.ReferringUserEmail,
                        ReferralUserId = loggedUser.Id,
                        ReferralCode = referralCode
                    };
                    await userBatchReferralRepository.AddAsync(userBatchReferral);
                    await unitOfWork.SaveChangesAsync();
                }
                else
                {
                    return new BaseViewModel() { Success = false, Message = "You have already referred this student!!" };
                }

                return new BaseViewModel() { Success = true, Message = "Shared Successfully!!" };

            }
            catch (Exception ex)
            {
                return new BaseViewModel() { Success = false, Message = ex.Message };
            }
        }

    }
}