﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class About
    {
        public int UserID { get; set; }
        //[AllowHtml]
        [UIHint("tinymce_full")]
        [Display(Name = "About")]
        public string AboutMe { get; set; }
    }
}
