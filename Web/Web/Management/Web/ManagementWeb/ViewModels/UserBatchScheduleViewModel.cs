﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class ScheduleViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }   
        public string Notes { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string LabelCategory { get; set; }
        public string UserId { get; set; }
        public  User User { get; set; }
        public  User BatchTeacher { get; set; }
        public int BatchId { get; set; }
        public virtual Batch Batch { get; set; }

        public int? TestId { get; set; }
        public virtual Test Test { get; set; }
        public string EventUrl { get;  set; }
        public string Password { get;  set; }
        public string UserName { get;  set; }
        public int ScheduleType { get;  set; }
        public bool IsTestOnlyUser { get;  set; }
        public bool IsBatchUser { get;  set; }
        public bool IsTest { get;  set; }
}
}
