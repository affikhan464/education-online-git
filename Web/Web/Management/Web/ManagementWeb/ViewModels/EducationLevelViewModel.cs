﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Management.Controllers
{
    public class EducationLevelViewModel
    {
        public string Name { get; set; }
        public int ParentId { get; set; }
    }
}