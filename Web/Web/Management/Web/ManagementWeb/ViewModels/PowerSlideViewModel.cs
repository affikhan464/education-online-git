﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class PowerSlideViewModel
    {
        public int Id { get;  set; }
        public string Title { get;  set; }
        public string Detail { get;  set; }
        public string AttributeType { get; set; }
        public int AttributeId { get; set; }
        public DateTime AddedDate { get;  set; }
    }
}
