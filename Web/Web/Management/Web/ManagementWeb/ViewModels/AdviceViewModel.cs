﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class AdviceViewModel
    {
        public int Id { get; set; }
        [MaxLength]
        public string Detail { get; set; }
        public int OldAdviceId { get; set; }
        public int AdviceTypeId { get; set; }
        public virtual AdviceType AdviceType { get; set; }
        
        public int? QuestionChoiceId { get; set; }
        public virtual QuestionChoice QuestionChoice { get; set; }
    }
}
