﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class AnswerViewModel
    {
        public int Id { get; set; }
        /// <summary>
        /// Any Single way answer details, Fill in the black, Long and short answer
        /// </summary>
        public string Detail { get; set; }
        public string ImageFileName { get; set; }
        public int WrittenAnswerType { get; set; }
        public int MCQChoiceId { get; set; }
        public List<int> MTQChoicesIds { get; set; }

    }
}
