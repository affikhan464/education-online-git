﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Management.ViewModels
{
    public class CommentaryViewModel
    {
        public int Id { get; set; }
        public string Ids { get; set; }

        public string AttributeType { get; set; }
        public string AttributeId { get; set; }
        [UIHint("tinymce_full")]
        [Display(Name = "CommentaryDetail")]
        [Required]
        public string CommentaryDetail { get; set; }
        [Required]
        public string CommentaryTitle { get; set; }

        public DateTime Date { get; set; }
        public DateTime LastUpdated { get; set; }
        public string SelectedWord { get; set; }
        public int WordId { get; set; }
        public bool isEditMode { get; set; }
    }
}