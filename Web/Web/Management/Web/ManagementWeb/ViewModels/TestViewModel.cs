﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.ViewModels
{
    public class TestViewModel
    {
        public int Id { get; set; }
        public string Detail { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public string AttributeType { get; set; }
        public string AttributeId { get; set; }
        public string TestTitle { get; set; }
        public int TestTypeId { get; set; }
        public TestType TestType { get; set; }
        public string QuestionIds { get; set; }
        public  List<UserQuestionAttempt> UserQuestionAttempts { get; set; }
        public  List<UserTestAttempt> UserTestAttempts { get; set; }
        public List<SetWithQuestionsViewModel> SetWithQuestionsViewModel { get; set; }
        public List<AttributeViewModel> Attributes { get; set; }
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public int BatchId { get; set; }
        public Batch Batch { get; set; }
        public string IncludeQuestion { get; set; }
        public bool WithAdvice { get; set; }
        public int? QuestionDifficultyLevelId { get; set; }

        public QuestionDifficultyLevel QuestionDifficultyLevel { get; set; }

        public List<TestQuestionModel> TestQuestions { get; set; }


    }
}
