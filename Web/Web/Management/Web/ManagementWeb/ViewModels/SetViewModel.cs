﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class SetViewModel
    {
    
        public int Id { get;  set; }
        public string Title { get;  set; }
        public string AttributeType { get; set; }
        public int AttributeId { get; set; }
    }
}
