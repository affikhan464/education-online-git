﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.ViewModels
{
    public class BatchTestViewModel
    {
        public int Id { get; set; }
       
        public int BatchId { get; set; }
        public  Batch Batch { get; set; }

        public int TestId { get; set; }
        public Test Test { get; set; }
    }
}
