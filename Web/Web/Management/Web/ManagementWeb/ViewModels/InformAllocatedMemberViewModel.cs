﻿using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class InformAllocatedMemberViewModel
    {
        public AttributeViewModel Attribute { get; set; }
        public Mail Mail { get; set; }
    }
}
