﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.ViewModels
{
    public class UserBatchViewModel
    {
        public int Id { get; set; }
        public string BatchTeacherId { get; set; }
        public string UserId { get; set; }
        public UserViewModel User { get; set; }
        public int BatchId { get; set; }
        public bool IsActive { get; set; }
        public bool IsTeacher { get; set; }
        public bool IsTestOnlyUser { get; set; }
        public Batch Batch { get; set; }
    }
}
