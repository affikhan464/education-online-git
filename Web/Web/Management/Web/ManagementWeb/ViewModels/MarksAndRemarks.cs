﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class MarksAndRemarks
    {
        public int QuestionId { get; set; }
        public int TestId { get; set; }
        public int Marks { get; set; }
        public string Remarks { get; set; }
        public string UserId { get; set; }
    }
}
