﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class BaseModel
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
        public string Error { get; set; }

        public static BaseModel Failed(string error)
        {
            return new BaseModel
            {
                Success = false,
                Message = error
            };
        }

        public static BaseModel Succeeded(string msg)
        {
            return new BaseModel
            {
                Success = true,
                Message = msg
            };
        }
    }
}
