﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class QuestionChoiceViewModel
    {
        public int Id { get; set; }
        public string Detail { get; set; }
        public int OldChoiceId { get; set; }
        public int QuestionId { get; set; }
        public Question Qustion { get; set; }
        public AdviceViewModel Advice { get; set; }
    }
}
