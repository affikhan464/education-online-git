﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.ViewModels
{
    public class BatchViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string BatchTeacherId { get; set; }
        public int SubjectId { get; set; }
        public decimal BatchFee { get; set; }
        public decimal BatchReferralAmount { get; set; }
        public Subject Subject { get; set; }
        public List<UserViewModel> ListBatchUsers { get; set; }
        public ICollection<UserBatchViewModel> BatchUsers { get; set; }
        public ICollection<UserBatchRequest> UserBatchRequests { get; set; }
        public ICollection<BatchTestViewModel> BatchTests { get; set; }
    }
}
