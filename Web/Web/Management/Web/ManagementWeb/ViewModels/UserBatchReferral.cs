﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.ViewModels
{
    public class UserBatchReferralViewModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime ReferralDate { get; set; }
        public string ReferringUserEmail { get; set; }
        
        public string ReferralUserId { get; set; }
        public virtual UserViewModel ReferralUser { get; set; }
        
        public int BatchId { get; set; }
        public virtual BatchViewModel Batch { get; set; }
    }
}
