﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class EnrollViewModel
    {
        public int StudyType { get; set; }
        public int SubjectId { get; set; }
        public int BatchId { get; set; }
        public string BatchTeacherId { get; set; }
    }
}
