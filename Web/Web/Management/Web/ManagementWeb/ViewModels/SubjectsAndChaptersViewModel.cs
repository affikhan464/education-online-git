﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class SubjectsAndChaptersViewModel
    {
        public List<SubjectViewModel> AllocatedSubjects { get; set; }
        public List<ChapterViewModel> AllocatedChapters { get; set; }
    }
}
