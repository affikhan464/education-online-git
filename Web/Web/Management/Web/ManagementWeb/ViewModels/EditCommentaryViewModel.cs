﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class EditCommentaryViewModel
    {
        public int Id { get; set; }

        public string AttributeType { get; set; }
        public string AttributeId { get; set; }
        [Required]
        public string EditCommentaryDetail { get; set; }
        [Required]
        public string EditCommentaryTitle { get; set; }
        public string EditAnswerDetail { get; set; }
        public DateTime Date { get; set; }
        public DateTime LastUpdated { get; set; }
        public string SelectedWord { get; set; }
        public int WordId { get;  set; }
    }
}
