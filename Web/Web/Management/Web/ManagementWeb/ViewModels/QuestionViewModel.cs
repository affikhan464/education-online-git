﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class QuestionViewModel
    {
        public int Id { get; set; }
        public string Ids { get; set; }

        public string AttributeType { get; set; }
        public string AttributeId { get; set; }
        [UIHint("tinymce_full")]
        [Display(Name = "QuestionDetail")]
        [Required]
        [DataType(DataType.Text, ErrorMessage = "Please write some question")]
        public string QuestionDetail { get; set; }
        [Required]
        public string QuestionTitle { get; set; }

        [UIHint("tinymce_full")]
        [Display(Name = "AnswerDetail")]
        public string AnswerDetail { get; set; }
        [UIHint("tinymce_full")]
        [Display(Name = "ShortAnswerDetail")]
        public string ShortAnswerDetail { get; set; }
        public string Source { get; set; }
        public string Author { get; set; }
        public string Year { get; set; }
        public DateTime Date { get; set; }
        public DateTime LastUpdated { get; set; }
        public QuestionDifficultyLevel QuestionDifficultyLevel { get; set; }
        public int QuestionDifficultyLevelId { get; set; }
        public int QustionTypeId { get; set; }
        public QustionType QustionType { get; set; }
        public string SelectedWord { get; set; }
        public int WordId { get;  set; }
        public bool isEditMode { get; set; }
        public bool IsSetQuestion { get; set; }
        public int SetId { get; set; }
        public List<AdviceViewModel> QuestionChoiceAdvices { get; set; }
        public List<QuestionChoiceViewModel> QuestionChoices { get; set; }
        public string CorrectQuestionChoice { get; set; }
        public string SingleQuestionChoiceAdvice { get; set; }
        public int CorrectQuestionChoiceId { get; set; }
        public List<string> MTQCorrectQuestionChoices { get;  set; }
        public QuestionCategories QuestionCategory { get; set; }
        public bool IsDeleted { get; set; }
        public string Marks { get; set; }
        public decimal AverageSolveMinutes{ get; set; }

    }
}
