﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class UserChapterRequestViewModel
    {
        public int Id { get; set; }
        public DateTime RequestDate { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public int ChapterId { get; set; }
        public ChapterViewModel Chapters { get; set; }
        public int StatusId { get; set; }
        public RequestStatus RequestStatus { get; set; }
    }
}
