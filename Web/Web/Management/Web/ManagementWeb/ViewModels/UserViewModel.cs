﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Management.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Country { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string CNIC { get; set; }
        public string MobileNumber { get; set; }
        public string NationalTaxNumber { get; set; }
        public string UserExperienceSummary { get; set; }
        public string SubjectExpertise { get; set; }
        public string Gender { get; set; }
        public string ProfilePictureUrl { get; set; }
        public string YearOfExperience { get; set; }
        public string FileName { get; set; }
        public string Password { get; set; }
        public bool IsEditModel { get; set; }
        public UserRoles Role { get; set; }
        public List<SubjectViewModel> AllocatedSubjects { get; set; }
        public List<ChapterViewModel> AllocatedChapters { get; set; }
        public int? CityId { get; set; }
        public virtual City City { get; set; }
        public string SchoolName { get; set; }
        public int BatchId { get; set; }

        public string ZoomUserName { get; set; }
        public string ZoomPassword { get; set; }
        public string ZoomUrl { get; set; }
        public string PasswordResetToken { get; set; }
        public string FriendReferralCode { get; set; }
        public int StudentStudyTypeId { get; set; }
    }
}