﻿using Management.Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class SubjectViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortNumber { get; set; }
        public int ClassId { get; set; }
        public string UserId { get; set; }
        public int? IconId { get; set; }
        public EducationLevel EducationLevel { get; set; }
        public BoardLevel BoardLevel { get; set; }
        public User User { get; set; }
        public IdentityRole<string> UserRole { get; set; }
        public Class Class { get; set; }
        public int RequestStatusId { get; set; }
        public bool IsAllocated { get; set; }
        public UserSubjectRequest UserSubjectRequest { get; set; }

    }
}
