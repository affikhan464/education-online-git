﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Management.ViewModels
{
    public class AttributeViewModel
    {
        public string Name { get; set; }
        public int ParentId { get; set; }
        public string ParentType { get; set; }
        public string ParentName { get; set; }
        public bool IsParentAllocated { get; set; }

        [UIHint("tinymce_full")]
        [Display(Name = "New Arrival Text")]
        public string NewArrivalText { get;  set; }

        public int Id { get; set; }
        public string Type { get;  set; }
        public string ChildText { get;  set; }
        public string ChildType { get;  set; }
        public List<AttributeViewModel> Childs { get; set; }
        public int SortNumber { get; set; }
        public string FileName { get;  set; }
        public string IconUrl { get;  set; }
        
        [UIHint("tinymce_full")]
        [Display(Name = "Detail")]
        public string Detail { get; set; }
        public string TypeText { get; set; }
        public bool IsAllocated { get; set; }
        public bool IsDemoChapter { get; set; }
        public List<ChapterViewModel> OtherNonChapters { get; set; }

    }
}