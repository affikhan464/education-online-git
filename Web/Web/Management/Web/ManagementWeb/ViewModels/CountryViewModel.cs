﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class CountryViewModel
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string alpha2Code { get; set; }
        public string alpha3Code { get; set; }
        public List<string> callingCodes { get; set; }
        public List<decimal> latlng { get; set; }
        public string Region { get; set; }
        public string Flag { get; set; }
        public ICollection<RegionViewModel> Regions { get; set; }

    }

}
