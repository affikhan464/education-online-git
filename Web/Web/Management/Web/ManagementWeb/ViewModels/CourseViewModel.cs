﻿using System.Collections.Generic;
using Management.Data.Models;
using Newtonsoft.Json;

namespace Management.Models
{
    public class CourseViewModel
    {
      
        public List<object> Courses { get; set; }
        public string CoursesType { get;  set; }
    }
}