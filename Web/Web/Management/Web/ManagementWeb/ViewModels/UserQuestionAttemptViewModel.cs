﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class UserQuestionAttemptViewModel
    {
        public DateTime Date { get; set; }
        public decimal ViewSeconds { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public int QuestionId { get; set; }
        public int QuestionTypeId { get; set; }
        public QuestionViewModel Question { get; set; }
        public int AnswerId { get; set; }
        public  AnswerViewModel Answer { get; set; }
        public  Test Test { get; set; }
        public  int TestId { get; set; }
    }
}
