﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class EditQuestionViewModel
    {
        public int Id { get; set; }

        public string AttributeType { get; set; }
        public string AttributeId { get; set; }
        [Required]
        public string EditQuestionDetail { get; set; }
        [Required]
        public string EditQuestionTitle { get; set; }
        public string EditAnswerDetail { get; set; }
        public DateTime Date { get; set; }
        public DateTime LastUpdated { get; set; }
        public QuestionDifficultyLevel QuestionDifficultyLevel { get; set; }
        public int QuestionDifficultyLevelId { get; set; }
        public int QustionTypeId { get; set; }
        public QustionType QustionType { get; set; }
        public string SelectedWord { get; set; }
        public int WordId { get;  set; }
        public string Source { get; set; }
        public string Author { get; set; }
        public string Year { get; set; }

        public bool isEditMode { get; set; }
        public bool IsSetQuestion { get; set; }
        public int SetId { get; set; }
        public List<QuestionChoiceViewModel> QuestionChoices { get; set; }
        public string CorrectQuestionChoice { get; set; }
        public int CorrectQuestionChoiceId { get; set; }
        public List<string> MTQCorrectQuestionChoices { get; internal set; }
        public QuestionCategories QuestionCategory { get; set; }
    }
}
