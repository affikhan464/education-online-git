﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.ViewModels
{
    public class TestQuestionViewModel
    {
       
        public int TestId { get; set; }
        public Test Test { get; set; }
        public int QuestionId { get; set; }
        public QuestionViewModel Question { get; set; }
    }
}
