﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class CityViewModel
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string countryCode { get; set; }
        public string regionfipsCode { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
        public string regionCode { get; set; }
        public int? RegionId { get; set; }
        public RegionViewModel Region { get; set; }

    }
}
