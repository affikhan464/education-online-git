﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.ViewModels
{
    public class SetWithQuestionsViewModel
    {
        public SetViewModel Set { get; set; }
        public List<QuestionViewModel> Questions { get; set; }
    }
}
