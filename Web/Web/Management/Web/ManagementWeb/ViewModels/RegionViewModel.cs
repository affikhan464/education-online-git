﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class RegionViewModel
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string countryCode { get; set; }
        public string fipsCode { get; set; }
        public string isoCode { get; set; }
        public int? CountryId { get; set; }
        public CountryViewModel Country { get; set; }
        public ICollection<CityViewModel> Cities { get; set; }
        

    }
}
