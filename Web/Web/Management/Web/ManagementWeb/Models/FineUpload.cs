﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Models
{
    [ModelBinder(typeof(ModelBinder))]
    public class FineUpload
    {
        public string Filename { get; set; }
        public Stream InputStream { get; set; }

        public void SaveAs(string destination, bool overwrite = false, bool autoCreateDirectory = true)
        {
            if (autoCreateDirectory)
            {
                var directory = new FileInfo(destination).Directory;
                if (directory != null) directory.Create();
            }

            using (var file = new FileStream(destination, overwrite ? FileMode.Create : FileMode.CreateNew))
                InputStream.CopyTo(file);
        }

        public class ModelBinder : IModelBinder
        {
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                

                var request = controllerContext.HttpContext.Request;
                var formUpload = request.Form.Files.Count > 0;

                // find filename
                var xFileName = request.Headers["X-File-Name"];
                var qqFile = "qqfile";
                var formFilename = formUpload ? request.Form.Files[0].FileName : null;

                var upload = new FineUpload
                {
                    Filename = formFilename,
                    InputStream = formUpload ? request.Form.Files.First().OpenReadStream() : null
                };

                return upload;
            }

            public Task BindModelAsync(ModelBindingContext bindingContext)
            {
                if (bindingContext == null) throw new ArgumentNullException(nameof(bindingContext));

                HttpRequest request = bindingContext.HttpContext.Request;
               
                bindingContext.Result = ModelBindingResult.Success(new FineUpload { Filename = request.Form.Files.First().FileName, InputStream = request.Form.Files.First().OpenReadStream() });
                return Task.CompletedTask;
            }
        }

    }
}
