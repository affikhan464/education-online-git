﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Filters
{
    public class AllocationAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly string _AttributeType;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="AttributeType">Attribute Types i.e Subject, Chapter, Section, SubSection</param>
        public AllocationAuthorizeAttribute(string AttributeType)
        {
            _AttributeType = AttributeType;
        }
        //public override void OnAuthorization(AuthorizationContext filterContext)
        //{
        //    base.OnAuthorization(filterContext);

        //    if (!_isAuthorized)
        //    {
        //        filterContext.Result = new HttpUnauthorizedResult();
        //    }
        //    else if (filterContext.HttpContext.User.IsInRole("Administrator") || filterContext.HttpContext.User.IsInRole("User") || filterContext.HttpContext.User.IsInRole("Manager"))
        //    {
        //        // is authenticated and is in one of the roles 
        //        SetCachePolicy(filterContext);
        //    }
        //    else
        //    {
        //        filterContext.Controller.TempData.Add("RedirectReason", "You are not authorized to access this page.");
        //        filterContext.Result = new RedirectResult("~/Error");
        //    }
        //}

    }
}
