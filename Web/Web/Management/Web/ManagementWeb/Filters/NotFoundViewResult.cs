﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Management.Filters
{
    public class NotFoundViewResult : ViewResult
    {
        public NotFoundViewResult()
        {
            ViewName = "Views/Error/NotFound.cshtml";
            StatusCode = (int)HttpStatusCode.NotFound;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewName"></param>
        public NotFoundViewResult(string viewName)
        {
            ViewName = viewName;
            StatusCode = (int)HttpStatusCode.NotFound;
        }
    }
    public class NotFoundPartialViewResult : PartialViewResult
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewName"></param>
        public NotFoundPartialViewResult(string viewName)
        {
            ViewName = viewName;
            StatusCode = (int)HttpStatusCode.NotFound;
        }

    }
    public class NotAllowedViewResult : PartialViewResult
    {
        public NotAllowedViewResult()
        {
            ViewName = "Views/Error/Error403.cshtml";
            StatusCode = (int)HttpStatusCode.Forbidden;

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewName"></param>
        public NotAllowedViewResult(string viewName)
        {
            ViewName = viewName;
            StatusCode = (int)HttpStatusCode.Forbidden;
            
        }

    }
}
