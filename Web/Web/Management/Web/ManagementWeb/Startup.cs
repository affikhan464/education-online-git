﻿using AutoMapper;
using Management.Data.DataContext;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Management.Infrastructure.DependencyResolution;
using Management.Infrastructure.Security.Identity;
using Management.Microsoft.AspNetCore.Mvc.Razor.Compilation;
using Management.ViewModels;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Razor.Compilation;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace Management
{
    public class Startup
    {


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

        }
        public IConfiguration Configuration { get; }
        public void ConfigureServices(IServiceCollection services)
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<UserViewModel, User>();
                cfg.CreateMap<User, UserViewModel>()
                .ForMember(dest => dest.ProfilePictureUrl, source => source.MapFrom(x => x.ProfilePicture.Artifact.FileName));

                cfg.CreateMap<SubjectViewModel, Subject>();
                cfg.CreateMap<Subject, SubjectViewModel>();

                cfg.CreateMap<SubjectViewModel, SubjectWithParents>();
                cfg.CreateMap<SubjectWithParents, SubjectViewModel>();

                cfg.CreateMap<ChapterViewModel, ChapterWithParents>();
                cfg.CreateMap<ChapterWithParents, ChapterViewModel>();

                cfg.CreateMap<ChapterViewModel, Chapter>();
                cfg.CreateMap<Chapter, ChapterViewModel>();

                cfg.CreateMap<CountryViewModel, Country>()
                .ForMember(dest => dest.callingCodes, source => source.MapFrom(x => x.callingCodes[0] == null ? string.Empty : x.callingCodes[0]))
                    .ForMember(dest => dest.lat, source => source.MapFrom(x => x.latlng[0]))
                    .ForMember(dest => dest.lng, source => source.MapFrom(x => x.latlng[1]));

                cfg.CreateMap<Country, CountryViewModel>();

                cfg.CreateMap<CityViewModel, City>();
                cfg.CreateMap<City, CityViewModel>();

                cfg.CreateMap<RegionViewModel, Region>();
                cfg.CreateMap<Region, RegionViewModel>();

                cfg.CreateMap<QuestionChoice, QuestionChoiceViewModel>();
                cfg.CreateMap<QuestionChoiceViewModel, QuestionChoice>();
                
                cfg.CreateMap<Advice, AdviceViewModel>();
                cfg.CreateMap<AdviceViewModel, Advice>();

                cfg.CreateMap<QuestionModel, QuestionViewModel>();
                cfg.CreateMap<QuestionViewModel, QuestionModel>();
                cfg.CreateMap<CommentaryModel, CommentaryViewModel>();
                cfg.CreateMap<CommentaryViewModel, CommentaryModel>();

                cfg.CreateMap<UserSubjectRequest, UserSubjectRequestViewModel>();
                cfg.CreateMap<UserSubjectRequestViewModel, UserSubjectRequest>();

                cfg.CreateMap<UserChapterRequest, UserChapterRequestViewModel>();
                cfg.CreateMap<UserChapterRequestViewModel, UserChapterRequest>();

                cfg.CreateMap<TestViewModel, Test>();

                cfg.CreateMap<Test, TestViewModel>();


                cfg.CreateMap<TestModel, TestViewModel>();
                cfg.CreateMap<TestViewModel, TestModel>();


                cfg.CreateMap<TestQuestionViewModel, TestQuestionModel>();
                cfg.CreateMap<TestQuestionModel, TestQuestionViewModel>();

                cfg.CreateMap<UserBatchReferralViewModel, UserBatchReferral>();
                cfg.CreateMap<UserBatchReferral, UserBatchReferralViewModel>();

                cfg.CreateMap<TestQuestionViewModel, TestQuestion>();
                cfg.CreateMap<TestQuestion, TestQuestionViewModel>();

                cfg.CreateMap<TestQuestion, TestQuestionModel>();
                cfg.CreateMap<TestQuestionModel, TestQuestion>();

                cfg.CreateMap<PowerSlideViewModel, PowerSlide>();
                cfg.CreateMap<PowerSlide, PowerSlideViewModel>();

                cfg.CreateMap<Set, SetViewModel>();
                cfg.CreateMap<SetViewModel, Set>();
                cfg.CreateMap<Question, QuestionViewModel>();
                cfg.CreateMap<QuestionViewModel, Question>();

                cfg.CreateMap<QuestionModel, Question>();
                cfg.CreateMap<Question, QuestionModel>();

                cfg.CreateMap<BatchViewModel, Batch>();
                cfg.CreateMap<Batch, BatchViewModel>();

                cfg.CreateMap<BatchTestViewModel, BatchTest>();
                cfg.CreateMap<BatchTest, BatchTestViewModel>();

                cfg.CreateMap<UserBatchViewModel, UserBatch>();
                cfg.CreateMap<UserBatch, UserBatchViewModel>();

                cfg.CreateMap<ScheduleViewModel, Schedule>();
                cfg.CreateMap<Schedule, ScheduleViewModel>();

                cfg.CreateMap<SetWithQuestionsViewModel, SetWithQuestions>();
                cfg.CreateMap<SetWithQuestions, SetWithQuestionsViewModel>();

                cfg.CreateMap<QuestionModel, EditQuestionViewModel>()
                .ForMember(dest => dest.EditAnswerDetail, source => source.MapFrom(x => x.AnswerDetail))
                .ForMember(dest => dest.EditQuestionTitle, source => source.MapFrom(x => x.QuestionTitle))
                .ForMember(dest => dest.EditQuestionDetail, source => source.MapFrom(x => x.QuestionDetail));

                cfg.CreateMap<Chapter, AttributeViewModel>()
                .ForMember(dest => dest.ParentId, source => source.MapFrom(x => x.SubjectId))
                .ForMember(dest => dest.ParentName, source => source.MapFrom(x => x.Subject.Name))
                .ForMember(dest => dest.ParentType, source => source.MapFrom(x => AttributeType.Subject))
                .ForMember(dest => dest.Type, source => source.MapFrom(x => AttributeType.Chapter))
                .ForMember(dest => dest.ChildText, source => source.MapFrom(x => AttributeType.SectionText))
                .ForMember(dest => dest.ChildType, source => source.MapFrom(x => AttributeType.Section))
                .ForMember(dest => dest.IconUrl, source => source.MapFrom(x => x.Artifact == null ? string.Empty : "/UploadedArtifacts/" + x.Artifact.FileName));

                cfg.CreateMap<Section, AttributeViewModel>()
               .ForMember(dest => dest.ParentId, source => source.MapFrom(x => x.ChapterId))
               .ForMember(dest => dest.ParentType, source => source.MapFrom(x => AttributeType.Chapter))
                .ForMember(dest => dest.Type, source => source.MapFrom(x => AttributeType.Section))
                .ForMember(dest => dest.ChildText, source => source.MapFrom(x => AttributeType.SubSectionText))
                .ForMember(dest => dest.ChildType, source => source.MapFrom(x => AttributeType.SubSection))
               .ForMember(dest => dest.IconUrl, source => source.MapFrom(x => x.Artifact == null ? string.Empty : "/UploadedArtifacts/" + x.Artifact.FileName));

                cfg.CreateMap<SubSection, AttributeViewModel>()
               .ForMember(dest => dest.ParentId, source => source.MapFrom(x => x.SectionId))
               .ForMember(dest => dest.ParentType, source => source.MapFrom(x => AttributeType.Section))
                .ForMember(dest => dest.Type, source => source.MapFrom(x => AttributeType.SubSection))
               .ForMember(dest => dest.IconUrl, source => source.MapFrom(x => x.Artifact == null ? string.Empty : "/UploadedArtifacts/" + x.Artifact.FileName));



                cfg.CreateMap<EditQuestionViewModel, QuestionModel>()
                .ForMember(dest => dest.AnswerDetail, source => source.MapFrom(x => x.EditAnswerDetail))
                .ForMember(dest => dest.QuestionTitle, source => source.MapFrom(x => x.EditQuestionTitle))
                .ForMember(dest => dest.QuestionDetail, source => source.MapFrom(x => x.EditQuestionDetail));

                cfg.CreateMap<CommentaryModel, EditCommentaryViewModel>()
                .ForMember(dest => dest.EditCommentaryTitle, source => source.MapFrom(x => x.CommentaryTitle))
                .ForMember(dest => dest.EditCommentaryDetail, source => source.MapFrom(x => x.CommentaryDetail));
                cfg.CreateMap<EditCommentaryViewModel, CommentaryModel>()
                .ForMember(dest => dest.CommentaryTitle, source => source.MapFrom(x => x.EditCommentaryTitle))
                .ForMember(dest => dest.CommentaryDetail, source => source.MapFrom(x => x.EditCommentaryDetail));
            });
            //var config = ModelMapper.Configure();
            //IMapper mapper = config.CreateMapper();
            //services.ad(mapper);
            services.ConfigureModules(Configuration);
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 5;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            });

            services.ConfigureApplicationCookie(options =>
             {
                 options.SlidingExpiration = true;
                 options.ExpireTimeSpan = TimeSpan.FromDays(365);
             });
            services.AddCors(setup => setup.AddPolicy("CorsPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials();
            }));
            //services.AddCors(options => options.AddPolicy("AllowSpecificOrigins", builder =>
            //{
            //    builder.WithOrigins("http://182.191.92.72:8080",
            //              "http://182.191.92.72:8085",
            //              "http://localhost:90",
            //              "http://localhost:8090",
            //              "http://localhost:9090",
            //              "http://localhost:8085",
            //              "http://localhost:8080",
            //              "http://localhost:61310",
            //              "http://theilum.com",
            //              "http://www.theilum.com",
            //              "https://www.theilum.com",
            //              "https://www.theilum.com/",
            //              "https://theilum.com",
            //              "https://theilum.com/",
            //              "https://app.theilum.com",
            //              "http://app.theilum.com").AllowAnyMethod().AllowAnyHeader();
            //}));
            services.AddMvc()
            .ConfigureApplicationPartManager(manager =>
            {
                var oldMetadataReferenceFeatureProvider = manager.FeatureProviders.First(f => f is MetadataReferenceFeatureProvider);
                manager.FeatureProviders.Remove(oldMetadataReferenceFeatureProvider);
                manager.FeatureProviders.Add(new ReferencesMetadataReferenceFeatureProvider());
            });



        }


        public void Configure(IApplicationBuilder app, IServiceProvider services, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetService<DatabaseContext>().EnsureSeeded(services);
            }
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
           
            app.UseStaticFiles();
            app.UseManagementIdentity();
            app.UseStatusCodePagesWithReExecute("/error/{0}");
            app.UseCors("CorsPolicy");
            //app.UseCors("AllowSpecificOrigins");
            app.UseMvc(routes =>
           {
               routes.MapRoute(
                   name: "Default",
                   template: "{controller=Account}/{action=Login}/{id?}"
                   );

           });


        }
    }
}

