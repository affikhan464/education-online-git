﻿using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Management.Infrastructure.Communication
{
   public interface IEmailService
    {
        bool SendMail1(Mail model);
        bool SendMail(Mail model);
        bool SubjectExpertContactUs(Mail model);

        bool ContactUs(Mail model);
        bool ResearcherInvite(Mail model);
        bool ResearcherRegisteredByAdminMail(Mail model);
        bool ResearcherRegisteredMail(Mail model);
        bool TeacherInvite(Mail model);
        bool TeacherRegisteredByAdminMail(Mail model);
        bool TeacherRegisteredMail(Mail model);
        bool StudentInvite(Mail model);
        bool StudentRegisteredByAdminMail(Mail model);
        bool StudentRegisteredMail(Mail model);
        bool SendResetPasswordLink(Mail model, string ResetToken);
        bool SendException(string exceptionMessage);
        bool InformAllocatedMember(InformAllocatedMemberModel model);
        bool TestEvaluationCompletedMail(ExamTestMailModel model);
        bool TestCompletionMailToTeacher(ExamTestMailModel model);
        bool TestEvaluationRequestMailToTeacher(ExamTestMailModel model);
        bool TestCreatedMail(ExamTestMailModel model);
        bool OnlineClassScheduled(InformAllocatedMemberModel model);
        bool TestScheduled(InformAllocatedMemberModel model);
        bool StudentEnrolledSuccessfully(Mail model);

    }
}
