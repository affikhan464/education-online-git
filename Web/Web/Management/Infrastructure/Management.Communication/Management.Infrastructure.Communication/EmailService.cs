﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Management.Infrastructure.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;
using System.IO;

namespace Management.Infrastructure.Communication
{
    public class EmailService : IEmailService
    {
        private readonly IConfiguration configuration;
        private readonly IEncryptionDecryption security;
        private IHostingEnvironment env;
        public EmailService(IHostingEnvironment _env, IConfiguration _configuration, IEncryptionDecryption _security)
        {
            configuration = _configuration;
            security = _security; env = _env;
        }
        public bool SendMail(Mail model)
        {
            var mail = new MailMessage();
            var SmtpServer = new SmtpClient("smtp.gmail.com");

            var FromEmail = configuration["EmailService:FromEmail"];
            var password = configuration["EmailService:Password"];


            mail.From = new MailAddress(FromEmail);
            mail.To.Add(model.ToEMailAddress);
            mail.Subject = model.Subject;
            mail.IsBodyHtml = true;
            if (model.AlternativeView != null)
                mail.AlternateViews.Add(model.AlternativeView);
            else
                mail.Body = model.Body;


            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, password);
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);

            return true;


        }

        public bool SendMail1(Mail model)
        {

            Environment.SetEnvironmentVariable("SENDGRID_KEY", "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");
            var apiKey = Environment.GetEnvironmentVariable("SENDGRID_KEY");
            var client = new SendGridClient(apiKey);
            var FromEmail = configuration["EmailService:FromEmail"];

            var from = new EmailAddress(FromEmail, "Class Online");
            var subject = model.Subject;
            var to = new EmailAddress(model.ToEMailAddress);
            var stream = model.AlternativeView.ContentStream;
            var htmlBody = string.Empty;
            using (var reader = new StreamReader(stream))
            {
                htmlBody = reader.ReadToEnd();
            }
            var msg = MailHelper.CreateSingleEmail(from, to, subject, "", htmlBody);
            var response = client.SendEmailAsync(msg);
            return true;


        }
        public bool ContactUs(Mail model)
        {
            var mail = new MailMessage();
            var SmtpServer = new SmtpClient("smtp.gmail.com");
            var LoginLink = configuration["AppSettings:LoginLink"];
            var FromEmail = configuration["EmailService:FromEmail"];
            var ToAdminEmail = configuration["EmailService:ToAdminEmail"];
            var PasswordHash = configuration["EmailService:Password"];
            var weblink = configuration["AppSettings:LiveLink"];

            var password = security.DecryptKey(PasswordHash, true);


            string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/ContactUs.html", Encoding.UTF8);
            html = html.Replace("##Body##", model.Body);
            html = html.Replace("##Name##", model.UserName);
            html = html.Replace("##OtherDetails##", string.Empty);
            html = html.Replace("##UserType##", "");
            html = html.Replace("##UserEmail##", model.UserEmail);
            html = html.Replace("##EnvirontmentUrl##", weblink);
            html = html.Replace("##LoginLink##", LoginLink);

            var htmlWithLogo = GenerateBody(html);




            mail.From = new MailAddress(FromEmail);
            mail.To.Add(ToAdminEmail);
            mail.Subject = model.UserName + " Just contact us from website";
            mail.IsBodyHtml = true;
            mail.AlternateViews.Add(htmlWithLogo);

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, password);
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);

            return true;


        }
        public bool SubjectExpertContactUs(Mail model)
        {
            var mail = new MailMessage();
            var SmtpServer = new SmtpClient("smtp.gmail.com");
            var LoginLink = configuration["AppSettings:LoginLink"];
            var FromEmail = configuration["EmailService:FromEmail"];
            var ToAdminEmail = configuration["EmailService:ToAdminEmail"];
            var PasswordHash = configuration["EmailService:Password"];
            var weblink = configuration["AppSettings:LiveLink"];

            var password = security.DecryptKey(PasswordHash, true);


            string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/ContactUs.html", Encoding.UTF8);
            html = html.Replace("##Body##", model.Body);
            html = html.Replace("##OtherDetails##", "His Current School is: '" + model.School + "', Mobile Number is: " + model.Mobile + " and interested in Subject:" + model.Subject);
            html = html.Replace("##Name##", model.UserName);
            html = html.Replace("##UserType##", "Subject Expert ");
            html = html.Replace("##UserEmail##", model.UserEmail);
            html = html.Replace("##EnvirontmentUrl##", weblink);
            html = html.Replace("##LoginLink##", LoginLink);

            var htmlWithLogo = GenerateBody(html);

            mail.From = new MailAddress(FromEmail);
            mail.To.Add(ToAdminEmail);
            mail.Subject = model.Subject + " Subject Expert " + model.UserName + " just shown interest in Ilum.";
            mail.IsBodyHtml = true;
            mail.AlternateViews.Add(htmlWithLogo);

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, password);
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);

            return true;


        }
        public bool SendException(string exceptionMessage)
        {
            var mail = new MailMessage();
            var SmtpServer = new SmtpClient("smtp.gmail.com");
            var LoginLink = configuration["AppSettings:LoginLink"];
            var FromEmail = configuration["EmailService:FromEmail"];
            var ToDeveloper = configuration["EmailService:ToDeveloper"];
            var PasswordHash = configuration["EmailService:Password"];
            var weblink = configuration["AppSettings:LiveLink"];

            var password = security.DecryptKey(PasswordHash, true);


            string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/Exception.html", Encoding.UTF8);
            html = html.Replace("##Body##", exceptionMessage);
            html = html.Replace("##EnvirontmentUrl##", weblink);
            html = html.Replace("##LoginLink##", LoginLink);

            var htmlWithLogo = GenerateBody(html);




            mail.From = new MailAddress(FromEmail);
            mail.To.Add(ToDeveloper);
            mail.Subject = "Exception from Ilum";
            mail.IsBodyHtml = true;
            mail.AlternateViews.Add(htmlWithLogo);

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(FromEmail, password);
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);

            return true;


        }

        private AlternateView GenerateBody(string html)
        {
            var weblink = configuration["AppSettings:LiveLink"];
            var filePath = env.WebRootPath + "/images/logo.png";
            LinkedResource res = new LinkedResource(filePath);
            res.ContentId = Guid.NewGuid().ToString();
            var logo = @"<img width='240' src='https://app.theilum.com/images/logo.png'/>";
            html = html.Replace("##LogoUrl##", logo);

            var avHtml = AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html);
            avHtml.LinkedResources.Add(res);
            return avHtml;
        }
        public bool SendResetPasswordLink(Mail model, string ResetToken)
        {


            try
            {
                var user = new ForgotPassword()
                {
                    Email = model.ToEMailAddress,
                    ResetToken = ResetToken
                };
                var userString = Newtonsoft.Json.JsonConvert.SerializeObject(user);
                var key = security.EncryptKey(userString, true);
                var LoginLink = configuration["AppSettings:LoginLink"];
                var weblink = configuration["AppSettings:LiveLink"];
                var link = LoginLink + "/Account/ResetPassword?t=" + key;

                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/ResetPassword.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##ResetLink##", link);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);
                var alternativeView = GenerateBody(html);
                model.Subject = "Account Recovery - Class Online";
                model.AlternativeView = alternativeView;

                var isSent = SendMail(model);
                return isSent;

            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool ResearcherInvite(Mail model)
        {


            try
            {
                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Researcher/Register";
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/ResearcherInvite.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##InviteLink##", link);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);
                var alternativeView = GenerateBody(html);
                model.Subject = "Invitation For Researcher - Class Online";
                model.AlternativeView = alternativeView;

                var isSent = SendMail(model);
                return isSent;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool ResearcherRegisteredByAdminMail(Mail model)
        {
            try
            {
                var LoginLink = configuration["AppSettings:LoginLink"];
                var weblink = configuration["AppSettings:LiveLink"];
                var hashCode = security.EncryptKey(model.UserId, true);
                var link = LoginLink + "/Researcher/Register";
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/ResearcherInviteByAdmin.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##InviteLink##", link + "?u=" + hashCode);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);
                var alternativeView = GenerateBody(html);
                model.Subject = "Successfully Registered as a Researcher - Class Online";
                model.AlternativeView = alternativeView;

                var isSent = SendMail(model);
                return isSent;

            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool ResearcherRegisteredMail(Mail model)
        {
            try
            {

                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Researcher/Register";
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/ResearcherRegistered.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);

                var alternativeView = GenerateBody(html);
                model.Subject = "Application for researcher job - Class Online";
                model.AlternativeView = alternativeView;

                var isSent = SendMail(model);
                return isSent;


            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool TeacherInvite(Mail model)
        {


            try
            {
                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Teacher/Register";
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/TeacherInvite.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##InviteLink##", link);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);
                var alternativeView = GenerateBody(html);
                model.Subject = "Invitation For Teacher - Class Online";
                model.AlternativeView = alternativeView;

                var isSent = SendMail(model);
                return isSent;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool TeacherRegisteredByAdminMail(Mail model)
        {
            try
            {
                var LoginLink = configuration["AppSettings:LoginLink"];
                var weblink = configuration["AppSettings:LiveLink"];
                var hashCode = security.EncryptKey(model.UserId+"###"+ model.ResetPasswordToken, true);
                var link = LoginLink + "/Teacher/Register";
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/TeacherInviteByAdmin.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##InviteLink##", link + "?u=" + hashCode);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);
                var alternativeView = GenerateBody(html);
                model.Subject = "Successfully Registered as a Teacher - Class Online";
                model.AlternativeView = alternativeView;

                var isSent = SendMail(model);
                return isSent;

            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool TeacherRegisteredMail(Mail model)
        {
            try
            {

                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Teacher/Register";
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/TeacherRegistered.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);

                var alternativeView = GenerateBody(html);
                model.Subject = "Application for Teacher job - Class Online";
                model.AlternativeView = alternativeView;

                var isSent = SendMail(model);
                return isSent;


            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool StudentRegisteredByAdminMail(Mail model)
        {
            try
            {
                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Student/Register";
                var hashCode = security.EncryptKey(model.UserId, true);
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/StudentRegisteredByAdmin.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##InviteLink##", link + "?u=" + hashCode);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);

                var alternativeView = GenerateBody(html);
                model.Subject = "Successfully Registered - Class Online";
                model.AlternativeView = alternativeView;

                var isSent = SendMail(model);

                return isSent;



            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool StudentRegisteredMail(Mail model)
        {
            try
            {
                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/StudentRegistered.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);

                var alternativeView = GenerateBody(html);
                model.Subject = "Successfully Registered - Class Online";
                model.AlternativeView = alternativeView;

                var isSent = SendMail(model);

                return isSent;

            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool StudentInvite(Mail model)
        {


            try
            {

                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Student/Register";

                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/StudentInvite.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.UserName);
                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##InviteLink##", link);
                html = html.Replace("##LoginLink##", LoginLink);

                var alternativeView = GenerateBody(html);
                model.Subject = "Successfully Registered - Class Online";
                model.AlternativeView = alternativeView;

                var isSent = SendMail(model);

                return isSent;



            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool InformAllocatedMember(InformAllocatedMemberModel model)
        {
            try
            {
                bool isSent = false;
                foreach (var user in model.Users)
                {
                    var weblink = configuration["AppSettings:LiveLink"];
                    var LoginLink = configuration["AppSettings:LoginLink"];
                    var link = LoginLink + "/Book/" + model.Attribute.Type + "?id=" + model.Attribute.Id;

                    string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/ContentUpdatedInfoToAllocatedMember.html", Encoding.UTF8);
                    html = html.Replace("##UserName##", user.FirstName);
                    html = html.Replace("##EnvirontmentUrl##", weblink);
                    html = html.Replace("##InviteLink##", link);
                    html = html.Replace("##LoginLink##", LoginLink);
                    html = html.Replace("##AttributeText##", model.Attribute.TypeText);
                    html = html.Replace("##AttributeName##", model.Attribute.Name);

                    var alternativeView = GenerateBody(html);
                    var mail = new Mail();
                    mail.Subject = model.Attribute.TypeText + " Updated - Class Online";
                    mail.AlternativeView = alternativeView;
                    mail.ToEMailAddress = user.Email;
                    isSent = SendMail(mail);
                }


                return isSent;



            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool TestEvaluationRequestMailToTeacher(ExamTestMailModel model)
        {
            try
            {
                bool isSent = false;

                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Batch/Test?id=" + model.Test.Id + "&uid=" + model.Student.Id;


                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/TestEvaluationRequest.html", Encoding.UTF8);
                html = html.Replace("##StudentName##", model.Student.FirstName + " " + model.Student.LastName);
                html = html.Replace("##TeacherName##", model.Teacher.FirstName + " " + model.Teacher.LastName);
                var title = model.Student.Gender == Gender.Female ? "Ms." : "Mr.";
                html = html.Replace("##Title##", title);
                html = html.Replace("##SubjectName##", model.Subject.Name);
                html = html.Replace("##LoginLink##", LoginLink);
                html = html.Replace("##TestTitle##", model.Test.TestTitle);
                html = html.Replace("##BatchName##", model.Batch.Title);
                html = html.Replace("##InviteLink##", link);

                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);

                var alternativeView = GenerateBody(html);
                var mail = new Mail();
                mail.Subject = model.Student.FirstName + " " + model.Student.LastName + " has requested to evaluate his test - Class Online";
                mail.AlternativeView = alternativeView;
                mail.ToEMailAddress = model.Teacher.Email;
                isSent = SendMail(mail);
                return isSent;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool TestCompletionMailToTeacher(ExamTestMailModel model)
        {
            try
            {
                bool isSent = false;

                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Batch/Test?id=" + model.Test.Id + "&uid=" + model.Student.Id;

                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/TestCompletionRequest.html", Encoding.UTF8);
                html = html.Replace("##StudentName##", model.Student.FirstName + " " + model.Student.LastName);
                html = html.Replace("##TeacherName##", model.Teacher.FirstName + " " + model.Teacher.LastName);
                var title = model.Student.Gender == Gender.Female ? "Ms." : "Mr.";
                html = html.Replace("##Title##", title);
                html = html.Replace("##SubjectName##", model.Subject.Name);
                html = html.Replace("##LoginLink##", LoginLink);
                html = html.Replace("##TestTitle##", model.Test.TestTitle);
                html = html.Replace("##BatchName##", model.Batch.Title);
                html = html.Replace("##InviteLink##", link);

                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);

                var alternativeView = GenerateBody(html);
                var mail = new Mail();
                mail.Subject = model.Student.FirstName + " " + model.Student.LastName + " has attempted his test - Class Online";
                mail.AlternativeView = alternativeView;
                mail.ToEMailAddress = model.Teacher.Email;
                isSent = SendMail(mail);



                return isSent;



            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool TestEvaluationCompletedMail(ExamTestMailModel model)
        {
            try
            {
                bool isSent = false;

                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Test/Result/" + model.Test.Id;

                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/TestEvaluationCompleted.html", Encoding.UTF8);
                html = html.Replace("##StudentName##", model.Student.FirstName + " " + model.Student.LastName);
                html = html.Replace("##TeacherName##", "Teacher");
                html = html.Replace("##SubjectName##", model.Subject.Name);
                html = html.Replace("##LoginLink##", LoginLink);
                html = html.Replace("##TestTitle##", model.Test.TestTitle);
                html = html.Replace("##BatchName##", model.Batch.Title);
                html = html.Replace("##EvaluationDate##", model.UserTestAttempt.EvaluationDate.ToString("MMM dd, yyyy hh:mm tt"));
                html = html.Replace("##UnattemptedQuestions##", model.UnattemptedQuestions.ToString());
                html = html.Replace("##TotalMarks##", model.TotalMarks.ToString());
                html = html.Replace("##ObtainedMarks##", model.UserTestAttempt.Marks.ToString());
                html = html.Replace("##InviteLink##", link);

                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);

                var alternativeView = GenerateBody(html);
                var mail = new Mail();
                mail.Subject = model.Student.FirstName + " Your Test => " + model.Test.TestTitle + " Evaluation Completed - Class Online";
                mail.AlternativeView = alternativeView;
                mail.ToEMailAddress = model.Student.Email;
                isSent = SendMail(mail);



                return isSent;



            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool TestCreatedMail(ExamTestMailModel model)
        {
            try
            {
                bool isSent = false;

                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink + "/Test/Inprogress/" + model.Test.Id;

                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/TestCreatedMailTemplate.html", Encoding.UTF8);
                html = html.Replace("##StudentName##", model.Student.FirstName + " " + model.Student.LastName);
                html = html.Replace("##TeacherName##", model.Teacher.FirstName + " " + model.Teacher.LastName);
                html = html.Replace("##SubjectName##", model.Subject.Name);
                html = html.Replace("##LoginLink##", LoginLink);
                html = html.Replace("##TestTitle##", model.Test.TestTitle);
                html = html.Replace("##BatchName##", model.Batch.Title);
                html = html.Replace("##ExpiryDate##", model.Test.ExpiryDate.ToString("MMM dd, yyyy hh:mm tt"));
                html = html.Replace("##TestDuration##", model.Test.Hours != 0 ? model.Test.Hours + "hrs and " + model.Test.Minutes + "mins" : model.Test.Minutes + "mins");
                html = html.Replace("##InviteLink##", link);

                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);

                var alternativeView = GenerateBody(html);
                var mail = new Mail();
                mail.Subject = "New Test has been created for your batch - Class Online";
                mail.AlternativeView = alternativeView;
                mail.ToEMailAddress = model.Student.Email;
                isSent = SendMail(mail);



                return isSent;



            }
            catch (Exception ex)
            {
                return false;
            }

        }


        public bool OnlineClassScheduled(InformAllocatedMemberModel model)
        {
            try
            {
                bool isSent = false;

                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink;
                foreach (var user in model.Users)
                {

                    string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/OnlineClassScheduled.html", Encoding.UTF8);

                    html = html.Replace("##Title##", model.Schedule.Title);
                    html = html.Replace("##SubjectName##", model.Schedule.Batch.Subject.Name);
                    html = html.Replace("##LoginLink##", LoginLink);
                    html = html.Replace("##BatchName##", model.Schedule.Batch.Title);
                    html = html.Replace("##InviteLink##", link);
                    html = html.Replace("##StartTime##", model.Schedule.StartDateTime.ToString("dd/MM/yyyy hh:mm tt"));
                    html = html.Replace("##EndTime##", model.Schedule.EndDateTime.ToString("dd/MM/yyyy hh:mm tt"));

                    html = html.Replace("##EnvirontmentUrl##", weblink);
                    html = html.Replace("##LoginLink##", LoginLink);

                    var alternativeView = GenerateBody(html);
                    var mail = new Mail();
                    mail.Subject = "An Online Class has been Scheduled for you - Class Online";
                    mail.AlternativeView = alternativeView;
                    mail.ToEMailAddress = user.Email;
                    isSent = SendMail(mail);
                }



                return isSent;



            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool TestScheduled(InformAllocatedMemberModel model)
        {
            try
            {
                bool isSent = false;

                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink;
                foreach (var user in model.Users)
                {

                    string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/OnlineClassScheduled.html", Encoding.UTF8);

                    html = html.Replace("##Title##", model.Schedule.Title);
                    html = html.Replace("##SubjectName##", model.Schedule.Batch.Subject.Name);
                    html = html.Replace("##LoginLink##", LoginLink);
                    html = html.Replace("##BatchName##", model.Schedule.Batch.Title);
                    html = html.Replace("##InviteLink##", link);
                    html = html.Replace("##StartTime##", model.Schedule.StartDateTime.ToString("dd/MM/yyyy hh:mm tt"));
                    html = html.Replace("##EndTime##", model.Schedule.EndDateTime.ToString("dd/MM/yyyy hh:mm tt"));

                    html = html.Replace("##EnvirontmentUrl##", weblink);
                    html = html.Replace("##LoginLink##", LoginLink);

                    var alternativeView = GenerateBody(html);
                    var mail = new Mail();
                    mail.Subject = "An Online Class has been Scheduled for you - Class Online";
                    mail.AlternativeView = alternativeView;
                    mail.ToEMailAddress = user.Email;
                    isSent = SendMail(mail);
                }



                return isSent;



            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public bool StudentEnrolledSuccessfully(Mail model)
        {
            try
            {
                bool isSent = false;

                var weblink = configuration["AppSettings:LiveLink"];
                var LoginLink = configuration["AppSettings:LoginLink"];
                var link = LoginLink;

                string html = System.IO.File.ReadAllText(env.WebRootPath + "/EmailTemplates/StudentEnrolledSuccess.html", Encoding.UTF8);
                html = html.Replace("##UserName##", model.Teacher.FirstName+" "+ model.Teacher.LastName);
                html = html.Replace("##StudentName##", model.Student.FirstName + " " + model.Student.LastName);
                html = html.Replace("##BatchName##", model.Batch.Title);
                html = html.Replace("##InviteLink##", link);

                html = html.Replace("##EnvirontmentUrl##", weblink);
                html = html.Replace("##LoginLink##", LoginLink);

                var alternativeView = GenerateBody(html);
                var mail = new Mail();
                mail.Subject = "New Student has been enrolled in your batch - Class Online";
                mail.AlternativeView = alternativeView;
                mail.ToEMailAddress = model.Teacher.Email;
                isSent = SendMail(mail);

                return isSent;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
