﻿using Management.Data.DataContext;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Management.Infrastructure.Security.Identity;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using Management.Data.Models;

namespace Management.Infrastructure.DependencyResolution
{
    public static class DbContextExtensions
    {
        public static bool AllMigrationsApplied(this DatabaseContext context)
        {
            var applied = context.GetService<IHistoryRepository>()
                            .GetAppliedMigrations()
                            .Select(m => m.MigrationId);

            var total = context.GetService<IMigrationsAssembly>()
                        .Migrations
                        .Select(m => m.Key);

            return !total.Except(applied).Any();

        }

        public static void EnsureSeeded(this DatabaseContext context, IServiceProvider serviceProvider)
        {

            var passwordHasher = serviceProvider.GetService(typeof(IPasswordHasher<User>)) as IPasswordHasher<User>;
            var _userManager = serviceProvider.GetService(typeof(UserManager<User>)) as UserManager<User>;

            if (!context.Roles.Any())
            {
                context.Roles.Add(new Role { Name = UserRoles.Admin.ToString(), NormalizedName = "ADMIN" });
                context.Roles.Add(new Role { Name = UserRoles.Student.ToString(), NormalizedName = "STUDENT" });
                context.Roles.Add(new Role { Name = UserRoles.Researcher.ToString(), NormalizedName = "RESEARCHER" });
                context.Roles.Add(new Role { Name = UserRoles.Teacher.ToString(), NormalizedName = "TEACHER" });
                context.SaveChanges();
            }
            if (!context.Roles.Any(a => a.Name == UserRoles.Teacher.ToString()))
            {
                context.Roles.Add(new Role { Name = UserRoles.Teacher.ToString(), NormalizedName = "TEACHER" });
                context.SaveChanges();

            }
            if (!context.Users.Any())
            {
                var admin = new User
                {
                    Email = "admin@mgmt.com",
                    UserName = "admin@mgmt.com",
                    IsActive = true

                };

                var student = new User
                {
                    Email = "student@mgmt.com",
                    UserName = "student@mgmt.com"
                };

                var researcher = new User
                {
                    Email = "researcher@mgmt.com",
                    UserName = "researcher@mgmt.com",
                    IsActive = true
                };

                _userManager.CreateAsync(admin, "Admin@123");
                _userManager.CreateAsync(student, "Student@123");
                _userManager.CreateAsync(researcher, "Researcher@123");
            }
            if (!context.UserRoles.Any())
            {

                var adminUser = context.Users.FirstOrDefault(x => x.Email == "admin@mgmt.com");
                if (adminUser != null)
                {
                    var adminRole = context.Roles.FirstOrDefault(x => x.Name.ToLower() == "admin");
                    if (adminRole != null)
                    {
                        context.UserRoles.Add(new UserRole { UserId = adminUser.Id, RoleId = adminRole.Id });
                        context.SaveChanges();
                    }
                }

                var studentUser = context.Users.FirstOrDefault(x => x.Email == "student@mgmt.com");
                if (studentUser != null)
                {
                    var studentRole = context.Roles.FirstOrDefault(x => x.Name.ToLower() == "student");
                    if (studentRole != null)
                    {
                        context.UserRoles.Add(new UserRole { UserId = studentUser.Id, RoleId = studentRole.Id });
                        context.SaveChanges();
                    }
                }

                var researcherUser = context.Users.FirstOrDefault(x => x.Email == "researcher@mgmt.com");

                if (researcherUser != null)
                {
                    var researcherRole = context.Roles.FirstOrDefault(x => x.Name.ToLower() == "researcher");
                    if (researcherRole != null)
                    {
                        context.UserRoles.Add(new UserRole { UserId = researcherUser.Id, RoleId = researcherRole.Id });
                        context.SaveChanges();
                    }
                }

            }


            if (!context.AdviceTypes.Any())
            {
                var adviceList = new List<AdviceType>()
               {
                   new AdviceType(){ Type=AdviceTypes.Question.ToString() },
                   new AdviceType(){ Type=AdviceTypes.Word.ToString() }
               };
                context.AdviceTypes.AddRange(adviceList);
                context.SaveChanges();
            }


            if (!context.ArtifactTypes.Any())
            {
                var artifactTypesList = new List<ArtifactType>()
               {
                   new ArtifactType(){ Name=ArtifactTypes.Audio },
                   new ArtifactType(){ Name=ArtifactTypes.Video },
                   new ArtifactType(){ Name=ArtifactTypes.Image },
                   new ArtifactType(){ Name=ArtifactTypes.pdf },
                   new ArtifactType(){ Name=ArtifactTypes.xlxs },
                   new ArtifactType(){ Name=ArtifactTypes.docx }
               };
                context.ArtifactTypes.AddRange(artifactTypesList);
                context.SaveChanges();
            }

            if (!context.EducationLevels.Any())
            {
                var educationLevelsList = new List<EducationLevel>()
               {
                   new EducationLevel(){ Name=EducationLevelType.Board },
                   new EducationLevel(){ Name=EducationLevelType.Body }
               };
                context.EducationLevels.AddRange(educationLevelsList);
                context.SaveChanges();
            }

            if (!context.QustionTypes.Any())
            {
                var qustionTypeList = new List<QustionType>()
               {
                   new QustionType(){ Type = QustionTypes.LongQuestion },
                   new QustionType(){ Type = QustionTypes.MCQ },
                   new QustionType(){ Type = QustionTypes.FillBlank },
                   new QustionType(){ Type = QustionTypes.MatchingColumns },
                   new QustionType(){ Type = QustionTypes.MTQ },
                   new QustionType(){ Type = QustionTypes.ShortQuestion }

               };
                context.QustionTypes.AddRange(qustionTypeList);
                context.SaveChanges();
            }
            if (!context.RequestStatuses.Any())
            {
                var requestStatusesList = new List<RequestStatus>()
               {
                   new RequestStatus(){ Status = RequestsStatus.Processed.ToString() },
                   new RequestStatus(){ Status = RequestsStatus.Pending.ToString() },

               };
                context.RequestStatuses.AddRange(requestStatusesList);
                context.SaveChanges();
            }
            if (!context.QuestionDifficultyLevels.Any())
            {
                var qustionTypeList = new List<QuestionDifficultyLevel>()
               {
                   new QuestionDifficultyLevel(){ Level=QuestionDifficultyLevels.Easy },
                   new QuestionDifficultyLevel(){ Level=QuestionDifficultyLevels.Moderate },
                   new QuestionDifficultyLevel(){ Level=QuestionDifficultyLevels.Difficult }
               };
                context.QuestionDifficultyLevels.AddRange(qustionTypeList);
                context.SaveChanges();
            }

            if (!context.QuestionCategories.Any())
            {
                var qustionCategoryList = new List<QuestionCategory>()
               {
                   new QuestionCategory(){ Name =QuestionCategoriesName.NormalQuestions },
                   new QuestionCategory(){ Name=QuestionCategoriesName.SetsQuestions },
                   new QuestionCategory(){ Name =QuestionCategoriesName.ExamQuestions }
               };
                context.QuestionCategories.AddRange(qustionCategoryList);
                context.SaveChanges();
            }
            if (!context.TestTypes.Any())
            {
                var testTypeList = new List<TestType>()
               {
                   new TestType(){ Name =TestTypes.Test.ToString() },
                   new TestType(){ Name =TestTypes.Mock.ToString() },
                   new TestType(){ Name =TestTypes.StudentTest.ToString() }
               };
                context.TestTypes.AddRange(testTypeList);
                context.SaveChanges();
            }
            if (!context.StudyTypes.Any())
            {
                var StudyTypeList = new List<StudyType>()
               {
                   new StudyType(){ Name =StudyTypes.Book.ToString() },
                   new StudyType(){ Name =StudyTypes.DistanceLearning.ToString() },
                   new StudyType(){ Name =StudyTypes.TestOnly.ToString() },

               };
                context.StudyTypes.AddRange(StudyTypeList);
                context.SaveChanges();
            }
            if (context.StudyTypes.FirstOrDefault(a => a.Name == StudyTypes.TestOnly.ToString()) == null)
            {
                var StudyType = new StudyType() { Name = StudyTypes.TestOnly.ToString() };
                context.StudyTypes.Add(StudyType);
                context.SaveChanges();
            }
            if (!context.VoucherTypes.Any())
            {
                var VoucherTypesList = new List<VoucherType>()
               {
                   new VoucherType(){ Name =VoucherTypes.CRV.ToString() },
                   new VoucherType(){ Name =VoucherTypes.CPV.ToString() },
                   new VoucherType(){ Name =VoucherTypes.BRV.ToString() },
                   new VoucherType(){ Name =VoucherTypes.BPV.ToString() },
                   new VoucherType(){ Name =VoucherTypes.JV.ToString() },

               };
                context.VoucherTypes.AddRange(VoucherTypesList);
                context.SaveChanges();
            }
            if (!context.VoucherTypes.Any())
            {
                var VoucherTypesList = new List<VoucherType>()
               {
                   new VoucherType(){ Name =VoucherTypes.CRV.ToString() },
                   new VoucherType(){ Name =VoucherTypes.CPV.ToString() },
                   new VoucherType(){ Name =VoucherTypes.BRV.ToString() },
                   new VoucherType(){ Name =VoucherTypes.BPV.ToString() },
                   new VoucherType(){ Name =VoucherTypes.JV.ToString() },

               };
                context.VoucherTypes.AddRange(VoucherTypesList);
                context.SaveChanges();
            }
            if (!context.BillTypes.Any())
            {
                var list = new List<BillType>()
               {
                   new BillType(){ Name =BillTypes.Fee.ToString() },
                   new BillType(){ Name =BillTypes.Expense.ToString() },
                   new BillType(){ Name =BillTypes.ReferralAmount.ToString() },

               };
                context.BillTypes.AddRange(list);
                context.SaveChanges();
            }
            if (!context.GLLevels.Any())
            {
                var list = new List<GLLevel>()
               {
                   new GLLevel(){ Name =GLLevels.Level1.ToString() },
                   new GLLevel(){ Name =GLLevels.Level2.ToString() },
                   new GLLevel(){ Name =GLLevels.Level3.ToString() },
                   new GLLevel(){ Name =GLLevels.Level4.ToString() },
                   new GLLevel(){ Name =GLLevels.Level5.ToString() },

               };
                context.GLLevels.AddRange(list);
                context.SaveChanges();
            }
        }
    }
}
