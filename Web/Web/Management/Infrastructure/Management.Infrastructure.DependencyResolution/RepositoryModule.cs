﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Management.Data.DataContext;
using Management.Data.Interfaces;
using Management.Data.Interfaces.UnitOfWork;
using Management.Data;


using Management.Infrastructure.Security.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Management.Data.Interfaces.RepositoryInterfaces;
using Microsoft.AspNetCore.Identity;
using Management.Data.Repositories;
using Management.Data.Models;
using System;
using Management.Infrastructure.Communication;
using Management.Infrastructure.Security;

namespace Management.Infrastructure.DependencyResolution
{
    public static class RepositoryModule
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DatabaseContext>(opts =>
                    opts.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
                    options => options.MigrationsAssembly("Management.Data"))
            );

            services.AddIdentity<User, Role>(options =>
            {
                options.SignIn.RequireConfirmedEmail = true;
            })
                .AddEntityFrameworkStores<DatabaseContext>()
                .AddDefaultTokenProviders()
                .AddUserManager<UserManager>();


            services.AddScoped<IDatabaseContext, DatabaseContext>();
            services.AddScoped<IUnitOfWork>(options => new UnitOfWork(options.GetService<IDatabaseContext>()));
            services.AddScoped<UserManager>();
            services.AddScoped<SignInManager>();
            services.AddScoped<IEmailService, EmailService>();
            services.AddScoped<IEncryptionDecryption, EncryptionDecryption>();
            // services.AddScoped<RoleManager<IdentityRole>>();
            services.AddScoped<IEducationLevelRepository, EducationLevelRepository>();
            services.AddScoped<IBoardLevelRepository, BoardLevelRepository>();
            services.AddScoped<ISubSectionRepository, SubSectionRepository>();
            services.AddScoped<ISectionRepository, SectionRepository>();
            services.AddScoped<IClassRepository, ClassRepository>();
            services.AddScoped<ISubjectRepository, SubjectRepository>();
            services.AddScoped<IChapterRepository, ChapterRepository>();
            services.AddScoped<IArtifactRepository, ArtifactRepository>();
            services.AddScoped<IUserArtifactRepository, UserArtifactRepository>();
            services.AddScoped<IUserSubjectRepository, UserSubjectRepository>();
            services.AddScoped<IQuestionRepository, QuestionRepository>();
            services.AddScoped<IQuestionTypeRepository, QuestionTypeRepository>();
            services.AddScoped<IQuestionDifficultyLevelRepository, QuestionDifficultyLevelRepository>();
            services.AddScoped<ICorrectAnswerRepository, CorrectAnswerRepository>();
            services.AddScoped<IWordRepository, WordRepository>();
            services.AddScoped<IWordQuestionRepository, WordQuestionRepository>();
            services.AddScoped<IUserSubjectRequestRepository, UserSubjectRequestRepository>();
            //services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ICommentaryRepository, CommentaryRepository>();
            services.AddScoped<ISetRepository, SetRepository>();
            services.AddScoped<ISetQuestionRepository, SetQuestionRepository>();
            services.AddScoped<ITestRepository, TestRepository>();
            services.AddScoped<ITestQuestionRepository, TestQuestionRepository>();
            services.AddScoped<IWordCommentaryRepository, WordCommentaryRepository>();
            services.AddScoped<IUserChapterRepository, UserChapterRepository>();
            services.AddScoped<IUserChapterRequestRepository, UserChapterRequestRepository>();
            services.AddScoped<ITourRepository, TourRepository>();
            services.AddScoped<ISubjectDemoChapterRepository, SubjectDemoChapterRepository>();
            services.AddScoped<IPowerSlideRepository, PowerSlideRepository>();
            services.AddScoped<IAdviceRepository, AdviceRepository>();
            services.AddScoped<IAnswerRepository, AnswerRepository>();
            services.AddScoped<IQuestionChoiceRepository, QuestionChoiceRepository>();
            services.AddScoped<ITestCreatedFromRepository, TestCreatedFromRepository>();

            services.AddScoped<IUserTestAttemptRepository, UserTestAttemptRepository>();
            services.AddScoped<IUserQuestionAttemptRepository, UserQuestionAttemptRepository>();

            services.AddScoped<IMTQAnsweredChoiceRepository, MTQAnsweredChoiceRepository>();

            services.AddScoped<IUserBatchRepository, UserBatchRepository>();
            services.AddScoped<IBatchRepository, BatchRepository>();
            services.AddScoped<IScheduleRepository, ScheduleRepository>();
            
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IRegionRepository, RegionRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IBatchTestRepository, BatchTestRepository>();
            services.AddScoped<IUserRoleRepository, UserRoleRepository>();
            services.AddScoped<IStudentStudyTypeRepository, StudentStudyTypeRepository>();
            services.AddScoped<IUserBatchReferralRepository, UserBatchReferralRepository>();

        }
    }
}

