﻿using Management.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Infrastructure.Security.Identity
{

    public abstract class ManagementIdentityDbContext : ManagementIdentityDbContext<User,Role,string, IdentityUserClaim<string>,
    UserRole, IdentityUserLogin<string>,
    IdentityRoleClaim<string>, IdentityUserToken<string>>
    {
        public ManagementIdentityDbContext(DbContextOptions options) : base(options)
        {
        }
    }
    public abstract class ManagementIdentityDbContext<TUser,TRole,TKey, TUserClaim, TUserRole, TUserLogin, TRoleClaim, TUserToken> 
        : IdentityDbContext<TUser,TRole,string,TUserClaim,TUserRole,TUserLogin,TRoleClaim,TUserToken>

        where TUser : IdentityUser
        where TRole : IdentityRole
        where TUserClaim: IdentityUserClaim<string>
        where TUserRole: IdentityUserRole<string>
        where TUserLogin: IdentityUserLogin<string>
        where TRoleClaim : IdentityRoleClaim<string>
        where TUserToken : IdentityUserToken<string>

    {


        public ManagementIdentityDbContext(DbContextOptions options) : base(options)
        {
        }

        public ManagementIdentityDbContext()
        {
        }


        public new DbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }
    }
}
