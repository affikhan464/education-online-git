﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class NewArrivalText_InAllAttributes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NewArrivalText",
                table: "SubSections",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NewArrivalText",
                table: "Subjects",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NewArrivalText",
                table: "Sections",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NewArrivalText",
                table: "EducationLevels",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NewArrivalText",
                table: "Classes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NewArrivalText",
                table: "Chapters",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NewArrivalText",
                table: "BoardLevels",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NewArrivalText",
                table: "SubSections");

            migrationBuilder.DropColumn(
                name: "NewArrivalText",
                table: "Subjects");

            migrationBuilder.DropColumn(
                name: "NewArrivalText",
                table: "Sections");

            migrationBuilder.DropColumn(
                name: "NewArrivalText",
                table: "EducationLevels");

            migrationBuilder.DropColumn(
                name: "NewArrivalText",
                table: "Classes");

            migrationBuilder.DropColumn(
                name: "NewArrivalText",
                table: "Chapters");

            migrationBuilder.DropColumn(
                name: "NewArrivalText",
                table: "BoardLevels");
        }
    }
}
