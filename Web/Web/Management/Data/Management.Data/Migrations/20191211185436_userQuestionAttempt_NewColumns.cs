﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class userQuestionAttempt_NewColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsEvaluated",
                table: "UserTestAttempts",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsMarksAwarded",
                table: "UserQuestionAttempts",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Marks",
                table: "UserQuestionAttempts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Remarks",
                table: "UserQuestionAttempts",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsEvaluated",
                table: "UserTestAttempts");

            migrationBuilder.DropColumn(
                name: "IsMarksAwarded",
                table: "UserQuestionAttempts");

            migrationBuilder.DropColumn(
                name: "Marks",
                table: "UserQuestionAttempts");

            migrationBuilder.DropColumn(
                name: "Remarks",
                table: "UserQuestionAttempts");
        }
    }
}
