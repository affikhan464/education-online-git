﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class artifactId_AddedTo_Attributes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IconId",
                table: "SubSections",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IconId",
                table: "Subjects",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IconId",
                table: "Sections",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IconId",
                table: "EducationLevels",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IconId",
                table: "Classes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IconId",
                table: "Chapters",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IconId",
                table: "BoardLevels",
                type: "int",
                nullable: true);

          

            migrationBuilder.CreateIndex(
                name: "IX_SubSections_IconId",
                table: "SubSections",
                column: "IconId");

            migrationBuilder.CreateIndex(
                name: "IX_Subjects_IconId",
                table: "Subjects",
                column: "IconId");

            migrationBuilder.CreateIndex(
                name: "IX_Sections_IconId",
                table: "Sections",
                column: "IconId");

            migrationBuilder.CreateIndex(
                name: "IX_EducationLevels_IconId",
                table: "EducationLevels",
                column: "IconId");

            migrationBuilder.CreateIndex(
                name: "IX_Classes_IconId",
                table: "Classes",
                column: "IconId");

            migrationBuilder.CreateIndex(
                name: "IX_Chapters_IconId",
                table: "Chapters",
                column: "IconId");

            migrationBuilder.CreateIndex(
                name: "IX_BoardLevels_IconId",
                table: "BoardLevels",
                column: "IconId");

            migrationBuilder.AddForeignKey(
                name: "FK_BoardLevels_Artifacts_IconId",
                table: "BoardLevels",
                column: "IconId",
                principalTable: "Artifacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Chapters_Artifacts_IconId",
                table: "Chapters",
                column: "IconId",
                principalTable: "Artifacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Classes_Artifacts_IconId",
                table: "Classes",
                column: "IconId",
                principalTable: "Artifacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EducationLevels_Artifacts_IconId",
                table: "EducationLevels",
                column: "IconId",
                principalTable: "Artifacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sections_Artifacts_IconId",
                table: "Sections",
                column: "IconId",
                principalTable: "Artifacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Subjects_Artifacts_IconId",
                table: "Subjects",
                column: "IconId",
                principalTable: "Artifacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubSections_Artifacts_IconId",
                table: "SubSections",
                column: "IconId",
                principalTable: "Artifacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BoardLevels_Artifacts_IconId",
                table: "BoardLevels");

            migrationBuilder.DropForeignKey(
                name: "FK_Chapters_Artifacts_IconId",
                table: "Chapters");

            migrationBuilder.DropForeignKey(
                name: "FK_Classes_Artifacts_IconId",
                table: "Classes");

            migrationBuilder.DropForeignKey(
                name: "FK_EducationLevels_Artifacts_IconId",
                table: "EducationLevels");

            migrationBuilder.DropForeignKey(
                name: "FK_Sections_Artifacts_IconId",
                table: "Sections");

            migrationBuilder.DropForeignKey(
                name: "FK_Subjects_Artifacts_IconId",
                table: "Subjects");

            migrationBuilder.DropForeignKey(
                name: "FK_SubSections_Artifacts_IconId",
                table: "SubSections");

            migrationBuilder.DropIndex(
                name: "IX_SubSections_IconId",
                table: "SubSections");

            migrationBuilder.DropIndex(
                name: "IX_Subjects_IconId",
                table: "Subjects");

            migrationBuilder.DropIndex(
                name: "IX_Sections_IconId",
                table: "Sections");

            migrationBuilder.DropIndex(
                name: "IX_EducationLevels_IconId",
                table: "EducationLevels");

            migrationBuilder.DropIndex(
                name: "IX_Classes_IconId",
                table: "Classes");

            migrationBuilder.DropIndex(
                name: "IX_Chapters_IconId",
                table: "Chapters");

            migrationBuilder.DropIndex(
                name: "IX_BoardLevels_IconId",
                table: "BoardLevels");

            migrationBuilder.DropColumn(
                name: "IconId",
                table: "SubSections");

            migrationBuilder.DropColumn(
                name: "IconId",
                table: "Subjects");

            migrationBuilder.DropColumn(
                name: "IconId",
                table: "Sections");

            migrationBuilder.DropColumn(
                name: "IconId",
                table: "EducationLevels");

            migrationBuilder.DropColumn(
                name: "IconId",
                table: "Classes");

            migrationBuilder.DropColumn(
                name: "IconId",
                table: "Chapters");

            migrationBuilder.DropColumn(
                name: "IconId",
                table: "BoardLevels");
        }
    }
}
