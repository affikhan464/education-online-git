﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class BatchID_StudentStudyType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BatchId",
                table: "StudentStudyTypes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_StudentStudyTypes_BatchId",
                table: "StudentStudyTypes",
                column: "BatchId");

            migrationBuilder.AddForeignKey(
                name: "FK_StudentStudyTypes_Batches_BatchId",
                table: "StudentStudyTypes",
                column: "BatchId",
                principalTable: "Batches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudentStudyTypes_Batches_BatchId",
                table: "StudentStudyTypes");

            migrationBuilder.DropIndex(
                name: "IX_StudentStudyTypes_BatchId",
                table: "StudentStudyTypes");

            migrationBuilder.DropColumn(
                name: "BatchId",
                table: "StudentStudyTypes");
        }
    }
}
