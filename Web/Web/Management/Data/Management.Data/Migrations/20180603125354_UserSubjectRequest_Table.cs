﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class UserSubjectRequest_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserSubjectRequests_UserSubjects_UserSubjectId",
                table: "UserSubjectRequests");
            
            migrationBuilder.DropColumn(
                name: "UserSubjectId",
                table: "UserSubjectRequests");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserSubjectId",
                table: "UserSubjectRequests",
                nullable: true);

          

            migrationBuilder.AddForeignKey(
                name: "FK_UserSubjectRequests_UserSubjects_UserSubjectId",
                table: "UserSubjectRequests",
                column: "UserSubjectId",
                principalTable: "UserSubjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
