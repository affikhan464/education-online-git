﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class New_column_inAdviceTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Questions",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Marks",
                table: "Questions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "QuestionChoiceId",
                table: "Advices",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Advices_QuestionChoiceId",
                table: "Advices",
                column: "QuestionChoiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Advices_QuestionChoices_QuestionChoiceId",
                table: "Advices",
                column: "QuestionChoiceId",
                principalTable: "QuestionChoices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Advices_QuestionChoices_QuestionChoiceId",
                table: "Advices");

            migrationBuilder.DropIndex(
                name: "IX_Advices_QuestionChoiceId",
                table: "Advices");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Questions");

            migrationBuilder.DropColumn(
                name: "Marks",
                table: "Questions");

            migrationBuilder.DropColumn(
                name: "QuestionChoiceId",
                table: "Advices");
        }
    }
}
