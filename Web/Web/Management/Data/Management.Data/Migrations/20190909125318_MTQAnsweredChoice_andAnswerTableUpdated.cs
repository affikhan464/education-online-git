﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class MTQAnsweredChoice_andAnswerTableUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answers_QuestionChoices_QuestionChoiceId",
                table: "Answers");

            migrationBuilder.DropIndex(
                name: "IX_Answers_QuestionChoiceId",
                table: "Answers");

            migrationBuilder.DropColumn(
                name: "QuestionChoiceId",
                table: "Answers");

            migrationBuilder.AddColumn<string>(
                name: "Detail",
                table: "Answers",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MCQChoiceId",
                table: "Answers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MTQAnsweredChoice",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AnswerId = table.Column<int>(type: "int", nullable: true),
                    MTQChoiceId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MTQAnsweredChoice", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MTQAnsweredChoice_Answers_AnswerId",
                        column: x => x.AnswerId,
                        principalTable: "Answers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MTQAnsweredChoice_QuestionChoices_MTQChoiceId",
                        column: x => x.MTQChoiceId,
                        principalTable: "QuestionChoices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Answers_MCQChoiceId",
                table: "Answers",
                column: "MCQChoiceId");

            migrationBuilder.CreateIndex(
                name: "IX_MTQAnsweredChoice_AnswerId",
                table: "MTQAnsweredChoice",
                column: "AnswerId");

            migrationBuilder.CreateIndex(
                name: "IX_MTQAnsweredChoice_MTQChoiceId",
                table: "MTQAnsweredChoice",
                column: "MTQChoiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Answers_QuestionChoices_MCQChoiceId",
                table: "Answers",
                column: "MCQChoiceId",
                principalTable: "QuestionChoices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Answers_QuestionChoices_MCQChoiceId",
                table: "Answers");

            migrationBuilder.DropTable(
                name: "MTQAnsweredChoice");

            migrationBuilder.DropIndex(
                name: "IX_Answers_MCQChoiceId",
                table: "Answers");

            migrationBuilder.DropColumn(
                name: "Detail",
                table: "Answers");

            migrationBuilder.DropColumn(
                name: "MCQChoiceId",
                table: "Answers");

            migrationBuilder.AddColumn<int>(
                name: "QuestionChoiceId",
                table: "Answers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Answers_QuestionChoiceId",
                table: "Answers",
                column: "QuestionChoiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Answers_QuestionChoices_QuestionChoiceId",
                table: "Answers",
                column: "QuestionChoiceId",
                principalTable: "QuestionChoices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
