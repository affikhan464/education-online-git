﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class UserBatchRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "UserBatches",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "UserBatchRequestId",
                table: "UserBatches",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UserBatchRequests",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BatchId = table.Column<int>(type: "int", nullable: false),
                    RequestDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StatusId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserBatchRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserBatchRequests_Batches_BatchId",
                        column: x => x.BatchId,
                        principalTable: "Batches",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserBatchRequests_RequestStatuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "RequestStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserBatchRequests_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserBatches_UserBatchRequestId",
                table: "UserBatches",
                column: "UserBatchRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBatchRequests_BatchId",
                table: "UserBatchRequests",
                column: "BatchId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBatchRequests_StatusId",
                table: "UserBatchRequests",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_UserBatchRequests_UserId",
                table: "UserBatchRequests",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserBatches_UserBatchRequests_UserBatchRequestId",
                table: "UserBatches",
                column: "UserBatchRequestId",
                principalTable: "UserBatchRequests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserBatches_UserBatchRequests_UserBatchRequestId",
                table: "UserBatches");

            migrationBuilder.DropTable(
                name: "UserBatchRequests");

            migrationBuilder.DropIndex(
                name: "IX_UserBatches_UserBatchRequestId",
                table: "UserBatches");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "UserBatches");

            migrationBuilder.DropColumn(
                name: "UserBatchRequestId",
                table: "UserBatches");
        }
    }
}
