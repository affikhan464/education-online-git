﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class QuestionId_ColumnNameChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserQuestionAttempts_Questions_QustionId",
                table: "UserQuestionAttempts");

            migrationBuilder.DropIndex(
                name: "IX_UserQuestionAttempts_QustionId",
                table: "UserQuestionAttempts");

            migrationBuilder.DropColumn(
                name: "Question",
                table: "UserQuestionAttempts");

            migrationBuilder.DropColumn(
                name: "QustionId",
                table: "UserQuestionAttempts");

            migrationBuilder.AddColumn<int>(
                name: "QuestionId",
                table: "UserQuestionAttempts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TestId",
                table: "UserQuestionAttempts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserQuestionAttempts_QuestionId",
                table: "UserQuestionAttempts",
                column: "QuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_UserQuestionAttempts_TestId",
                table: "UserQuestionAttempts",
                column: "TestId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserQuestionAttempts_Questions_QuestionId",
                table: "UserQuestionAttempts",
                column: "QuestionId",
                principalTable: "Questions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserQuestionAttempts_Tests_TestId",
                table: "UserQuestionAttempts",
                column: "TestId",
                principalTable: "Tests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserQuestionAttempts_Questions_QuestionId",
                table: "UserQuestionAttempts");

            migrationBuilder.DropForeignKey(
                name: "FK_UserQuestionAttempts_Tests_TestId",
                table: "UserQuestionAttempts");

            migrationBuilder.DropIndex(
                name: "IX_UserQuestionAttempts_QuestionId",
                table: "UserQuestionAttempts");

            migrationBuilder.DropIndex(
                name: "IX_UserQuestionAttempts_TestId",
                table: "UserQuestionAttempts");

            migrationBuilder.DropColumn(
                name: "QuestionId",
                table: "UserQuestionAttempts");

            migrationBuilder.DropColumn(
                name: "TestId",
                table: "UserQuestionAttempts");

            migrationBuilder.AddColumn<int>(
                name: "Question",
                table: "UserQuestionAttempts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "QustionId",
                table: "UserQuestionAttempts",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserQuestionAttempts_QustionId",
                table: "UserQuestionAttempts",
                column: "QustionId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserQuestionAttempts_Questions_QustionId",
                table: "UserQuestionAttempts",
                column: "QustionId",
                principalTable: "Questions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
