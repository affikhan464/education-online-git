﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class sort_number_added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SortNumber",
                table: "SubSections",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SubSectionNumber",
                table: "SubSections",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SortNumber",
                table: "Subjects",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SectionNumber",
                table: "Sections",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SortNumber",
                table: "Sections",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SortNumber",
                table: "EducationLevels",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SortNumber",
                table: "Classes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SortNumber",
                table: "Chapters",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SortNumber",
                table: "BoardLevels",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SortNumber",
                table: "SubSections");

            migrationBuilder.DropColumn(
                name: "SubSectionNumber",
                table: "SubSections");

            migrationBuilder.DropColumn(
                name: "SortNumber",
                table: "Subjects");

            migrationBuilder.DropColumn(
                name: "SectionNumber",
                table: "Sections");

            migrationBuilder.DropColumn(
                name: "SortNumber",
                table: "Sections");

            migrationBuilder.DropColumn(
                name: "SortNumber",
                table: "EducationLevels");

            migrationBuilder.DropColumn(
                name: "SortNumber",
                table: "Classes");

            migrationBuilder.DropColumn(
                name: "SortNumber",
                table: "Chapters");

            migrationBuilder.DropColumn(
                name: "SortNumber",
                table: "BoardLevels");
        }
    }
}
