﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class userSubject_AndChapter_added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserSubjectRequestId",
                table: "UserSubjects",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "UserChapterRequests",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChapterId = table.Column<int>(type: "int", nullable: false),
                    RequestDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StatusId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserChapterRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserChapterRequests_Chapters_ChapterId",
                        column: x => x.ChapterId,
                        principalTable: "Chapters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserChapterRequests_RequestStatuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "RequestStatuses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserChapterRequests_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserChapters",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ChapterId = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    UserChapterRequestId = table.Column<int>(type: "int", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserChapters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserChapters_Chapters_ChapterId",
                        column: x => x.ChapterId,
                        principalTable: "Chapters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserChapters_UserChapterRequests_UserChapterRequestId",
                        column: x => x.UserChapterRequestId,
                        principalTable: "UserChapterRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserChapters_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserSubjects_UserSubjectRequestId",
                table: "UserSubjects",
                column: "UserSubjectRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_UserChapterRequests_ChapterId",
                table: "UserChapterRequests",
                column: "ChapterId");

            migrationBuilder.CreateIndex(
                name: "IX_UserChapterRequests_StatusId",
                table: "UserChapterRequests",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_UserChapterRequests_UserId",
                table: "UserChapterRequests",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserChapters_ChapterId",
                table: "UserChapters",
                column: "ChapterId");

            migrationBuilder.CreateIndex(
                name: "IX_UserChapters_UserChapterRequestId",
                table: "UserChapters",
                column: "UserChapterRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_UserChapters_UserId",
                table: "UserChapters",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserSubjects_UserSubjectRequests_UserSubjectRequestId",
                table: "UserSubjects",
                column: "UserSubjectRequestId",
                principalTable: "UserSubjectRequests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserSubjects_UserSubjectRequests_UserSubjectRequestId",
                table: "UserSubjects");

            migrationBuilder.DropTable(
                name: "UserChapters");

            migrationBuilder.DropTable(
                name: "UserChapterRequests");

            migrationBuilder.DropIndex(
                name: "IX_UserSubjects_UserSubjectRequestId",
                table: "UserSubjects");

            migrationBuilder.DropColumn(
                name: "UserSubjectRequestId",
                table: "UserSubjects");
        }
    }
}
