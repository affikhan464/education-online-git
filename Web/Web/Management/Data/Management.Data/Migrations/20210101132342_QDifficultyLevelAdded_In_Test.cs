﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class QDifficultyLevelAdded_In_Test : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "QuestionDifficultyLevelId",
                table: "Tests",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Tests_QuestionDifficultyLevelId",
                table: "Tests",
                column: "QuestionDifficultyLevelId");

            migrationBuilder.AddForeignKey(
                name: "FK_Tests_QuestionDifficultyLevels_QuestionDifficultyLevelId",
                table: "Tests",
                column: "QuestionDifficultyLevelId",
                principalTable: "QuestionDifficultyLevels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Tests_QuestionDifficultyLevels_QuestionDifficultyLevelId",
                table: "Tests");

            migrationBuilder.DropIndex(
                name: "IX_Tests_QuestionDifficultyLevelId",
                table: "Tests");

            migrationBuilder.DropColumn(
                name: "QuestionDifficultyLevelId",
                table: "Tests");
        }
    }
}
