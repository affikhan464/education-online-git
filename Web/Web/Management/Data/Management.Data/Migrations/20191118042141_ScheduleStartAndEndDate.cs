﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Management.Data.Migrations
{
    public partial class ScheduleStartAndEndDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ScheduleDateTime",
                table: "UserBatchSchedules");

            migrationBuilder.AddColumn<DateTime>(
                name: "EndDateTime",
                table: "UserBatchSchedules",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "LabelCategory",
                table: "UserBatchSchedules",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartDateTime",
                table: "UserBatchSchedules",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EndDateTime",
                table: "UserBatchSchedules");

            migrationBuilder.DropColumn(
                name: "LabelCategory",
                table: "UserBatchSchedules");

            migrationBuilder.DropColumn(
                name: "StartDateTime",
                table: "UserBatchSchedules");

            migrationBuilder.AddColumn<DateTime>(
                name: "ScheduleDateTime",
                table: "UserBatchSchedules",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
