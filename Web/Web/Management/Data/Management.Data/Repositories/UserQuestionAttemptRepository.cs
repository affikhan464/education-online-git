﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class UserQuestionAttemptRepository : GenericRepository<UserQuestionAttempt>, IUserQuestionAttemptRepository
    {
        public UserQuestionAttemptRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
       
    }
}
