﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Management.Data.Repositories
{
   public class UserSubjectRequestRepository : GenericRepository<UserSubjectRequest>, IUserSubjectRequestRepository
    {
        public UserSubjectRequestRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
        List<UserSubjectRequest> IUserSubjectRequestRepository.GetRequests(string userId)
        {
            IQueryable<UserSubjectRequest> userRequests;
            if (userId!=string.Empty)
            {
                 userRequests = _dbContext.UserSubjectRequests.Include(a => a.Subject).Include(x => x.User).Where(z => z.UserId == userId);
            }
            else
            {
                userRequests = _dbContext.UserSubjectRequests.Include(a => a.Subject).Include(x => x.User);

            }


            return userRequests.ToList();
        }
    }
}
