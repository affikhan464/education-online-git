﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Management.Data.Repositories
{
    public class ClassRepository : GenericRepository<Class>, IClassRepository
    {
        public ClassRepository(IDatabaseContext dbContext) : base(dbContext)
        {
            
        }
        Class IClassRepository.GetWithChilds(int id)
        {

            var classes = _dbContext.Classes.Include(a => a.Artifact).Include(a => a.Subjects).FirstOrDefault(x => x.Id == id);
            return classes;
        }
    }
}
