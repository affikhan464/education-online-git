﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class TourRepository : GenericRepository<Tour>, ITourRepository
    {
        public TourRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
       
    }
}
