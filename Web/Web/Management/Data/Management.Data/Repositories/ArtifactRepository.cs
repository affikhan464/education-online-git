﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class ArtifactRepository : GenericRepository<Artifact>, IArtifactRepository
    {
        public ArtifactRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
        public void addArtifactSpecial(Artifact artifact)
        {
            
            _dbContext.Artifacts.Add(artifact);
            _dbContext.SaveChangesAsync();
        }

    }
}
