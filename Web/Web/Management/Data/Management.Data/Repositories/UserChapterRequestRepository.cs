﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Management.Data.Repositories
{
   public class UserChapterRequestRepository : GenericRepository<UserChapterRequest>, IUserChapterRequestRepository
    {
        public UserChapterRequestRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
        List<UserChapterRequest> IUserChapterRequestRepository.GetRequests(string userId)
        {
            IQueryable<UserChapterRequest> userRequests;
            if (userId!=string.Empty)
            {
                 userRequests = _dbContext.UserChapterRequests.Include(a => a.Chapter).Include(x => x.User).Where(z => z.UserId == userId);
            }
            else
            {
                userRequests = _dbContext.UserChapterRequests.Include(a => a.Chapter).Include(x => x.User);

            }


            return userRequests.ToList();
        }
    }
}
