﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Repositories
{
   public class QuestionDifficultyLevelRepository : GenericRepository<QuestionDifficultyLevel>, IQuestionDifficultyLevelRepository
    {
        public QuestionDifficultyLevelRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }

    }
}