﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using MoreLinq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Management.Data.Repositories
{
    public class QuestionRepository : GenericRepository<Question>, IQuestionRepository
    {
        public QuestionRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
        QuestionModel IQuestionRepository.GetSetsQuestion(int id)
        {

            var sections = (from question in _dbContext.Questions
                            join correctAnswer in _dbContext.CorrectAnswers on question.Id equals correctAnswer.QuestionId
                            join questionDifficultyLevel in _dbContext.QuestionDifficultyLevels on question.QuestionDifficultyLevelId equals questionDifficultyLevel.Id
                            where question.Id == id && question.IsDeleted == false
                            select new QuestionModel()
                            {
                                Id = question.Id,
                                AnswerDetail = correctAnswer.Answer,
                                QuestionDifficultyLevel = questionDifficultyLevel,
                                QuestionDifficultyLevelId = question.QuestionDifficultyLevelId,
                                QuestionDetail = question.Detail,
                                QustionTypeId = question.QustionTypeId,
                                QuestionTitle = question.QuestionTitle,
                                Source = question.Source,
                                Author = question.Author,
                                Year = question.Year

                            }).FirstOrDefault();
            return sections;
        }
        QuestionModel IQuestionRepository.GetQuestion(int id)
        {

            var sections = (from question in _dbContext.Questions
                            join wordQuestion in _dbContext.WordQuestions on question.Id equals wordQuestion.QuestionId
                            join correctAnswer in _dbContext.CorrectAnswers on question.Id equals correctAnswer.QuestionId
                            join questionDifficultyLevel in _dbContext.QuestionDifficultyLevels on question.QuestionDifficultyLevelId equals questionDifficultyLevel.Id
                            where question.Id == id && question.IsDeleted == false
                            select new QuestionModel()
                            {
                                Id = question.Id,
                                AnswerDetail = correctAnswer.Answer,
                                QuestionDifficultyLevel = questionDifficultyLevel,
                                QuestionDifficultyLevelId = question.QuestionDifficultyLevelId,
                                WordId = wordQuestion.WordId,
                                QuestionDetail = question.Detail,
                                QustionTypeId = question.QustionTypeId,
                                QuestionTitle = question.QuestionTitle,
                                Source = question.Source,
                                Author = question.Author,
                                Year = question.Year

                            }).FirstOrDefault();
            return sections;
        }

        QuestionModel IQuestionRepository.GetWithAnswer(int id)
        {

            var sections = (from question in _dbContext.Questions
                            join correctAnswer in _dbContext.CorrectAnswers on question.Id equals correctAnswer.QuestionId
                            join questionDifficultyLevel in _dbContext.QuestionDifficultyLevels on question.QuestionDifficultyLevelId equals questionDifficultyLevel.Id
                            where question.Id == id && question.IsDeleted == false
                            select new QuestionModel()
                            {
                                Id = question.Id,
                                AnswerDetail = correctAnswer.Answer,
                                QuestionDifficultyLevel = questionDifficultyLevel,
                                QuestionDifficultyLevelId = question.QuestionDifficultyLevelId,
                                WordId = 0,
                                QuestionDetail = question.Detail,
                                QustionTypeId = question.QustionTypeId,
                                QuestionTitle = question.QuestionTitle,
                                Source = question.Source,
                                Author = question.Author,
                                Year = question.Year

                            }).FirstOrDefault();
            return sections;
        }

        List<QuestionModel> IQuestionRepository.GetManyQuestions(int wordId)
        {

            var data = (from question in _dbContext.Questions
                        join wordQuestion in _dbContext.WordQuestions on question.Id equals wordQuestion.QuestionId
                        join correctAnswer in _dbContext.CorrectAnswers on question.Id equals correctAnswer.QuestionId
                        join questionDifficultyLevel in _dbContext.QuestionDifficultyLevels on question.QuestionDifficultyLevelId equals questionDifficultyLevel.Id
                        where wordQuestion.WordId == wordId && question.IsDeleted == false
                        select new QuestionModel()
                        {
                            Id = question.Id,
                            AnswerDetail = correctAnswer.Answer,
                            QuestionDifficultyLevel = questionDifficultyLevel,
                            QuestionDifficultyLevelId = question.QuestionDifficultyLevelId,
                            WordId = wordQuestion.WordId,
                            QuestionDetail = question.Detail,
                            QustionTypeId = question.QustionTypeId,
                            QuestionTitle = question.QuestionTitle,
                            Source = question.Source,
                            Author = question.Author,
                            Year = question.Year

                        }).ToList();
            if (wordId != 0 && data != null)
            {
                data = data.Where(s => s.WordId == wordId).DistinctBy(a => a.Id).ToList();
            }
            else
            {
                data = data.DistinctBy(a => a.Id).ToList();
            }

            return data;
        }
        List<QuestionModel> IQuestionRepository.GetManyAvailableQuestions(string attributeId, string attributeType, int wordId)
        {

            var data = (from question in _dbContext.Questions
                        join questionDifficultyLevel in _dbContext.QuestionDifficultyLevels on question.QuestionDifficultyLevelId equals questionDifficultyLevel.Id
                        where question.AttributeId == attributeId && question.AttributeType == attributeType && question.IsDeleted == false
                        select new QuestionModel()
                        {
                            Id = question.Id,
                            QuestionDifficultyLevel = questionDifficultyLevel,
                            QuestionDifficultyLevelId = question.QuestionDifficultyLevelId,
                            QuestionDetail = question.Detail,
                            QustionTypeId = question.QustionTypeId,
                            QuestionTitle = question.QuestionTitle,
                            Source = question.Source,
                            Author = question.Author,
                            Year = question.Year

                        }).ToList();
            if (wordId != 0 && data != null)
            {
                data = data.Where(s => s.WordId == wordId).DistinctBy(a => a.Id).ToList();
            }
            else
            {
                data = data.DistinctBy(a => a.Id).ToList();
            }

            return data;
        }
        List<QuestionModel> IQuestionRepository.GetAttributeQuestions(string attributeId, string attributeType)
        {

            var data = (from question in _dbContext.Questions
                        join questionDifficultyLevel in _dbContext.QuestionDifficultyLevels on question.QuestionDifficultyLevelId equals questionDifficultyLevel.Id
                        where question.AttributeId == attributeId && question.AttributeType == attributeType && question.IsDeleted == false
                        && question.QuestionCategoryId==Convert.ToInt32(QuestionCategories.ExamQuestion)
                        select new QuestionModel()
                        {
                            Id = question.Id,
                            QuestionDifficultyLevel = questionDifficultyLevel,
                            QuestionDifficultyLevelId = question.QuestionDifficultyLevelId,
                            QuestionDetail = question.Detail,
                            QustionTypeId = question.QustionTypeId,
                            QuestionTitle = question.QuestionTitle,
                            Source = question.Source,
                            Author = question.Author,
                            Year = question.Year

                        }).ToList();

            data = data.DistinctBy(a => a.Id).ToList();


            return data;
        }


        QuestionModel IQuestionRepository.GetExamQuestion(int id)
        {

            var question = _dbContext.Questions.Include(a => a.QuestionDifficultyLevel).Include(a => a.QuestionChoices).Include("QuestionChoices.Advice").Include(a => a.CorrectAnswers).FirstOrDefault(a => a.Id == id);
            QuestionModel model;
            if (question.QustionTypeId != Convert.ToInt32(QustionTypesEnum.FillBlank) && question.QustionTypeId != Convert.ToInt32(QustionTypesEnum.LongQuestion) && question.QustionTypeId != Convert.ToInt32(QustionTypesEnum.ShortQuestion))
            {
                model = new QuestionModel()
                {
                    Id = question.Id,
                    AnswerDetail = question.CorrectAnswers.FirstOrDefault().Answer,
                    QuestionDifficultyLevelId = question.QuestionDifficultyLevelId,
                    QuestionDifficultyLevel = question.QuestionDifficultyLevel,
                    QuestionDetail = question.Detail,
                    QustionTypeId = question.QustionTypeId,
                    QuestionTitle = question.QuestionTitle,
                    Source = question.Source,
                    Author = question.Author,
                    Year = question.Year,
                    Marks = question.Marks,
                    QuestionCategory = (QuestionCategories)question.QuestionCategoryId,

                    QuestionChoiceAdvices = question.QuestionChoices.Any()  ? question.QuestionChoices.Select(c => c.Advice).ToList() : new List<Advice>() { new Advice() { Detail = question.QuestionChoices.FirstOrDefault().Detail } },
                    QuestionChoices = question.QuestionChoices.Any() ? question.QuestionChoices.ToList() : new List<QuestionChoice>(),
                    SingleQuestionChoiceAdvice = question.QuestionChoices.Any()  ? question.QuestionChoices.Select(a => a.Advice).FirstOrDefault().Detail : question.QuestionChoices.FirstOrDefault().Advice.Detail,
                    MTQCorrectQuestionChoices = question.CorrectAnswers.Any()  ? question.CorrectAnswers.Select(a => a.QuestionChoice.Detail).ToList() : new List<string>() { question.CorrectAnswers.FirstOrDefault().Answer },
                    CorrectQuestionChoice = question.CorrectAnswers.Any()  ? question.CorrectAnswers.FirstOrDefault().QuestionChoice.Detail : question.CorrectAnswers.FirstOrDefault().Answer
                };

            }
            else
            {
                model = new QuestionModel()
                {
                    Id = question.Id,
                    AnswerDetail =question.QustionTypeId == Convert.ToInt32(QustionTypesEnum.LongQuestion)? question.CorrectAnswers.FirstOrDefault().Answer :string.Empty ,
                    ShortAnswerDetail= question.QustionTypeId == Convert.ToInt32(QustionTypesEnum.ShortQuestion) ? question.CorrectAnswers.FirstOrDefault().Answer : string.Empty,
                    QuestionDifficultyLevelId = question.QuestionDifficultyLevelId,
                    QuestionDifficultyLevel = question.QuestionDifficultyLevel,
                    QuestionDetail = question.Detail,
                    QustionTypeId = question.QustionTypeId,
                    QuestionTitle = question.QuestionTitle,
                    Source = question.Source,
                    Author = question.Author,
                    Year = question.Year,
                    Marks = question.Marks,
                    QuestionCategory = (QuestionCategories)question.QuestionCategoryId,

                    SingleQuestionChoiceAdvice = question.QuestionChoices.FirstOrDefault() == null ? string.Empty : question.QuestionChoices.FirstOrDefault().Advice == null ? string.Empty : question.QuestionChoices.FirstOrDefault().Advice.Detail,

                    CorrectQuestionChoice =  question.CorrectAnswers.FirstOrDefault().Answer
                };
            }

            return model;
        }

    }

}

