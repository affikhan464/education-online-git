﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Management.Data.Repositories
{
    public class SetQuestionRepository : GenericRepository<SetQuestion>, ISetQuestionRepository
    {
        public SetQuestionRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }

        public List<QuestionModel> GetBySetId(int setId)
        {
            var model = (from setQuestion in _dbContext.SetQuestions
                         join question in _dbContext.Questions on setQuestion.QuestionId equals question.Id
                         where setQuestion.SetId==setId
                         select new QuestionModel()
                         {
                             Id=question.Id,
                             Date=question.Date,
                             QuestionTitle=question.QuestionTitle,
                             QuestionDifficultyLevelId=question.QuestionDifficultyLevelId
                             
                         }).ToList();
            return model;
        }
    }
}
