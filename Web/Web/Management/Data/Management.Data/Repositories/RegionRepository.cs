﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class RegionRepository : GenericRepository<Region>, IRegionRepository
    {
        public RegionRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
       
    }
}
