﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Management.Data.Repositories
{
    public class EducationLevelRepository:GenericRepository<EducationLevel>, IEducationLevelRepository
    {
        public  EducationLevelRepository(IDatabaseContext dbContext) : base(dbContext)
        {
            
        }
        List<EducationLevel> IEducationLevelRepository.GetEducationLevelWithBoardLevels()
        {

            var educationLevels= _dbContext.EducationLevels.Include(a =>a.BoardLevels).Include("BoardLevels.Classes").Include("BoardLevels.Classes.Subjects").Include("BoardLevels.Classes.Subjects.Chapters").Include("BoardLevels.Classes.Subjects.Chapters.Sections").Include("BoardLevels.Classes.Subjects.Chapters.Sections.SubSections").ToList();
         



            return educationLevels;
        }
        EducationLevel IEducationLevelRepository.GetWithChilds(int id)
        {

            var educationLevel = _dbContext.EducationLevels.Include(a => a.Artifact).Include(a => a.BoardLevels).FirstOrDefault(x => x.Id == id);
            return educationLevel;
        }
    }
}
