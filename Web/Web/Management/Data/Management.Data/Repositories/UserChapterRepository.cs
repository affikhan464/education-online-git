﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Repositories
{
   public class UserChapterRepository : GenericRepository<UserChapter>, IUserChapterRepository
    {
        public UserChapterRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
    }
}
