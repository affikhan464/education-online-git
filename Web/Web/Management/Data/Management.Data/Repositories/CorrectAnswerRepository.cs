﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class CorrectAnswerRepository : GenericRepository<CorrectAnswer>, ICorrectAnswerRepository
    {
        public CorrectAnswerRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
       
    }
}
