﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class MTQAnsweredChoiceRepository : GenericRepository<MTQAnsweredChoice>, IMTQAnsweredChoiceRepository
    {
        public MTQAnsweredChoiceRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }

    }
}
