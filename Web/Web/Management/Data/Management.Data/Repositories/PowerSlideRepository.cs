﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class PowerSlideRepository : GenericRepository<PowerSlide>, IPowerSlideRepository
    {
        public PowerSlideRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
       
    }
}
