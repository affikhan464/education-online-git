﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Data.Repositories
{
    public class TestRepository : GenericRepository<Test>, ITestRepository
    {
        public TestRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
        async Task<Test> ITestRepository.GetTestWithQuestions(int id)
        {

            var test = await _dbContext.Tests.Include(a => a.TestQuestions).FirstOrDefaultAsync(a => a.Id == id);
            return test;
        }

        async Task<TestModel> ITestRepository.GetInProgressTest(int id, string userId)
        {
            //var test = await _dbContext.Tests.Where(c => c.Id == id&& c.UserId == userId)
            //    .Include(x => x.TestQuestions).Include(x => x.UserTestAttempts).Include(x => x.UserQuestionAttempts).Include("UserQuestionAttempts.Answer").Include("UserQuestionAttempts.Answer.MTQAnsweredChoices").FirstOrDefaultAsync();

            var userTestAttempts = await _dbContext.UserTestAttempts.Where(c => c.TestId == id && c.UserId == userId )
                .Include(x => x.Test).Include(x => x.User).Include("User.ProfilePicture").Include("User.ProfilePicture.Artifact").Include("Test.TestQuestions").FirstOrDefaultAsync();
            var userQAttempts =  _dbContext.UserQuestionAttempts.Where(a => a.TestId == id && a.UserId == userId).Include("Test.UserQuestionAttempts").Include("Test.UserQuestionAttempts.Answer").Include("Test.UserQuestionAttempts.Answer.MTQAnsweredChoices");
           
            var test = new Test();
            var qids = new List<int>();
            if (userTestAttempts != null)
            {
                test = userTestAttempts.Test;
                qids = userTestAttempts.Test.TestQuestions.Select(x => x.QuestionId).ToList();
            }
            else
            {

                 test = await _dbContext.Tests.Where(c => c.Id == id)
                    .Include(x => x.TestQuestions).Include(x => x.UserTestAttempts).FirstOrDefaultAsync();

            }



            var questions = _dbContext.Questions.Where(a => qids.Contains(a.Id))
                .Include(a => a.QuestionChoices)
                .Include("QuestionChoices.Advice").Include(a => a.CorrectAnswers);

            var model = new TestModel()
            {
                Id = test.Id,
                AddedDate = test.AddedDate,
                ExpiryDate = test.ExpiryDate,
                Hours = test.Hours,
                IncludeQuestion = test.IncludeQuestion,
                LastUpdated = test.LastUpdated,
                Minutes = test.Minutes,
                TestTitle = test.TestTitle,
                TestTypeId = test.TestTypeId,
                User=test.User,
                UserId = test.UserId,
                WithAdvice = test.WithAdvice,
                UserTestAttempts = test.UserTestAttempts.ToList(),
                UserQuestionAttempts = userQAttempts.ToList(),
            };

            var tqs = new List<TestQuestionModel>();
            foreach (var question in questions)
            {
                var tq = new TestQuestionModel();
                QuestionModel questionModel;
                if (question.QustionTypeId != Convert.ToInt32(QustionTypesEnum.FillBlank) && question.QustionTypeId != Convert.ToInt32(QustionTypesEnum.LongQuestion) && question.QustionTypeId != Convert.ToInt32(QustionTypesEnum.ShortQuestion))
                {
                    questionModel = new QuestionModel()
                    {
                        Id = question.Id,
                        AnswerDetail = question.CorrectAnswers.FirstOrDefault().Answer,
                        QuestionDifficultyLevelId = question.QuestionDifficultyLevelId,
                        QuestionDetail = question.Detail,
                        QustionTypeId = question.QustionTypeId,
                        QuestionTitle = question.QuestionTitle,
                        Source = question.Source,
                        Author = question.Author,
                        Year = question.Year,
                        Marks = question.Marks,
                        QuestionCategory = (QuestionCategories)question.QuestionCategoryId,

                        QuestionChoiceAdvices = question.QuestionChoices.Any() ? question.QuestionChoices.Select(c => c.Advice).ToList() : new List<Advice>() { new Advice() { Detail = question.QuestionChoices.FirstOrDefault().Detail } },
                        QuestionChoices = question.QuestionChoices.Any() ? question.QuestionChoices.ToList() : new List<QuestionChoice>(),
                        SingleQuestionChoiceAdvice = question.QuestionChoices.Any() ? question.QuestionChoices.Select(a => a.Advice).FirstOrDefault().Detail : question.QuestionChoices.FirstOrDefault().Advice.Detail,
                        MTQCorrectQuestionChoices = question.CorrectAnswers.Any() ? question.CorrectAnswers.Select(a => a.QuestionChoice.Detail).ToList() : new List<string>() { question.CorrectAnswers.FirstOrDefault().Answer },
                    };

                }
                else
                {
                    questionModel = new QuestionModel()
                    {
                        Id = question.Id,
                        AnswerDetail = question.QustionTypeId == Convert.ToInt32(QustionTypesEnum.LongQuestion) || question.QustionTypeId == Convert.ToInt32(QustionTypesEnum.FillBlank) ? question.CorrectAnswers.FirstOrDefault().Answer : string.Empty,
                        ShortAnswerDetail = question.QustionTypeId == Convert.ToInt32(QustionTypesEnum.ShortQuestion) ? question.CorrectAnswers.FirstOrDefault().Answer : string.Empty,
                        QuestionDifficultyLevelId = question.QuestionDifficultyLevelId,
                        QuestionDetail = question.Detail,
                        QustionTypeId = question.QustionTypeId,
                        QuestionTitle = question.QuestionTitle,
                        Source = question.Source,
                        Author = question.Author,
                        Year = question.Year,
                        Marks = question.Marks,
                        QuestionCategory = (QuestionCategories)question.QuestionCategoryId,

                        SingleQuestionChoiceAdvice = question.QuestionChoices.FirstOrDefault() == null ? string.Empty : question.QuestionChoices.FirstOrDefault().Advice == null ? string.Empty : question.QuestionChoices.FirstOrDefault().Advice.Detail

                    };
                }
                tq.Question = questionModel;
                tqs.Add(tq);
            }
            model.TestQuestions = tqs;
            return model;

        }
    }
}
