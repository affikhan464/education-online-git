﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Management.Data.Repositories
{
   public class TestCreatedFromRepository : GenericRepository<TestCreatedFrom>, ITestCreatedFromRepository
    {
        public TestCreatedFromRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
    }
}
