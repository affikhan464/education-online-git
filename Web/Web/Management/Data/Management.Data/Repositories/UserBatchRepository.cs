﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
    public class UserBatchRepository : GenericRepository<UserBatch>, IUserBatchRepository
    {
        public UserBatchRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
     
    }
}