﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Microsoft.EntityFrameworkCore;
using MoreLinq;
using System.Collections.Generic;
using System.Linq;

namespace Management.Data.Repositories
{
    public class SetRepository : GenericRepository<Set>, ISetRepository
    {
        public SetRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }

        public List<SetWithQuestions> GetSetWithQuestions(int attributeId, string attributeType)
        {
            var setList = _dbContext.Sets.Where(m => m.AttributeId == attributeId && m.AttributeType == attributeType);
            var setQuestions = setList.Include(a => a.SetQuestions).Include("SetQuestions.Question").Select(a => a.SetQuestions).ToList();

            var model = new List<SetWithQuestions>();
            foreach (var set in setList)
            {
                var newmodel = new SetWithQuestions()
                {
                    Set = set
                };
                var questions = new List<Question>();
                foreach (var setQuestion in setList.Select(a => a.SetQuestions.Where(b => b.SetId == set.Id)))
                {
                    questions.AddRange(setQuestion.Select(a => a.Question));
                }

                newmodel.Questions = questions;
                model.Add(newmodel);
            }

            return model;
        }
        public List<SetWithQuestions> GetSetWithQuestionsByTestId(int attributeId, string attributeType, int testId)
        {
            var testModel = _dbContext.Tests.Include(x => x.TestQuestions).Include("TestQuestions.Question").Include("TestQuestions.Question.SetQuestion").Include("TestQuestions.Question.SetQuestion.Set").FirstOrDefault(m => m.Id == testId);


            var model = new List<SetWithQuestions>();
            var testQuestions = testModel.TestQuestions;
            var sets = new List<Set>();
            foreach (var testQuestion in testQuestions)
            {
                if (testQuestion.Question.SetQuestion.Any())
                {
                var set = testQuestion.Question.SetQuestion.FirstOrDefault().Set;
                if (!sets.Contains(set))
                {
                    sets.Add(set);
                }
                }
               
            }
            foreach (var set in sets)
            {
                var newmodel = new SetWithQuestions()
                {
                    Set = set
                };
                var questions = new List<Question>();
                foreach (var setQuestion in sets.Select(a => a.SetQuestions.Where(b => b.SetId == set.Id)))
                {
                    questions.AddRange(setQuestion.Select(a => a.Question));
                }
                newmodel.Questions = questions;
                model.Add(newmodel);
            }

            return model;
        }


    }
}
