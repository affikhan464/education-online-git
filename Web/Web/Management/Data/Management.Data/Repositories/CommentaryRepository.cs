﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System.Collections.Generic;
using System.Linq;
using MoreLinq;
namespace Management.Data.Repositories
{
   public class CommentaryRepository : GenericRepository<Commentary>, ICommentaryRepository
    {
        public CommentaryRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
        CommentaryModel ICommentaryRepository.GetCommentary(int id)
        {

            var sections = (from commentary in _dbContext.Commentaries
                            join wordCommentary in _dbContext.WordCommentaries on commentary.Id equals wordCommentary.CommentaryId
                          
                            where commentary.Id == id
                            select new CommentaryModel()
                            {
                                Id = commentary.Id,
                                WordId = wordCommentary.WordId,
                                CommentaryDetail = commentary.Detail,
                                CommentaryTitle = commentary.CommentaryTitle

                            }).FirstOrDefault();
            return sections;
        }
        List<CommentaryModel> ICommentaryRepository.GetManyCommentaries(int wordId)
        {

            var data = (from commentary in _dbContext.Commentaries
                        join wordCommentary in _dbContext.WordCommentaries on commentary.Id equals wordCommentary.CommentaryId
                        where wordCommentary.WordId == wordId
                        select new CommentaryModel()
                        {
                            Id = commentary.Id,
                            WordId = wordCommentary.WordId,
                            CommentaryDetail = commentary.Detail,
                           
                            CommentaryTitle = commentary.CommentaryTitle

                        }).ToList();
            if (wordId != 0 && data != null)
            {
                data = data.Where(s => s.WordId == wordId).DistinctBy(a => a.Id).ToList();
            }
            else
            {
                data = data.DistinctBy(a => a.Id).ToList();
            }

            return data;
        }
        List<CommentaryModel> ICommentaryRepository.GetManyAvailableCommentaries(string attributeId, string attributeType, int wordId)
        {

            var data = (from commentary in _dbContext.Commentaries
                        where commentary.AttributeId == attributeId && commentary.AttributeType == attributeType
                        select new CommentaryModel()
                        {
                            Id = commentary.Id,
                           CommentaryDetail = commentary.Detail,
                           CommentaryTitle = commentary.CommentaryTitle

                        }).ToList();
            if (wordId != 0 && data != null)
            {
                data = data.Where(s => s.WordId == wordId).DistinctBy(a => a.Id).ToList();
            }
            else
            {
                data = data.DistinctBy(a => a.Id).ToList();
            }

            return data;
        }
    }
}
