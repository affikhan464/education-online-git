﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class WordQuestionRepository : GenericRepository<WordQuestion>, IWordQuestionRepository
    {
        public WordQuestionRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
       
    }
}
