﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Management.Data.Repositories
{
    public class SubjectRepository : GenericRepository<Subject>, ISubjectRepository
    {
        public SubjectRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
        Subject ISubjectRepository.GetWithChilds(int id)
        {

            var subject = _dbContext.Subjects.Include(a => a.Artifact).Include(a => a.Class).Include("Class.BoardLevel").Include("Class.BoardLevel.EducationLevel").Include(a => a.Chapters).Include("Chapters.Sections").Include("Chapters.Sections.SubSections").FirstOrDefault(x => x.Id == id);
            return subject;
        }
        Subject ISubjectRepository.GetWithChapters(int id)
        {

            var subject = _dbContext.Subjects.Include(a => a.Chapters).FirstOrDefault(x => x.Id == id);
            return subject;
        }

        List<SubjectWithParents> ISubjectRepository.ListOfSubjectsAllocatedToUser(string userId, bool showDeAllocatedToo)
        {

            List<SubjectWithParents> subjects;

            if (showDeAllocatedToo)
            {
                subjects = (from userSubjects in _dbContext.UserSubjects

                            join subject in _dbContext.Subjects on userSubjects.SubjectId equals subject.Id
                            join Class in _dbContext.Classes on subject.ClassId equals Class.Id
                            join boardLevel in _dbContext.BoardLevels on Class.BoardLevelId equals boardLevel.Id
                            join educationLevel in _dbContext.EducationLevels on boardLevel.EducationLevelId equals educationLevel.Id
                            join user in _dbContext.Users on userSubjects.UserId equals user.Id
                            join tblUserSubjectRequest in _dbContext.UserSubjectRequests on userSubjects.UserSubjectRequestId equals tblUserSubjectRequest.Id into TempUserSubjectRequest
                            from userSubjectRequest in TempUserSubjectRequest.DefaultIfEmpty()

                            where user.Id.Contains(userId)

                            select new SubjectWithParents
                            {
                                Id = subject.Id,
                                Class = Class,
                                Name = subject.Name,
                                BoardLevel = boardLevel,
                                EducationLevel = educationLevel,
                                IsAllocated = userSubjects.IsActive,
                                UserId = userId,
                                UserSubjectRequest = userSubjectRequest

                            }).ToList();
            }
            else
            {
                subjects = (from userSubjects in _dbContext.UserSubjects

                            join subject in _dbContext.Subjects on userSubjects.SubjectId equals subject.Id
                            join Class in _dbContext.Classes on subject.ClassId equals Class.Id
                            join boardLevel in _dbContext.BoardLevels on Class.BoardLevelId equals boardLevel.Id
                            join educationLevel in _dbContext.EducationLevels on boardLevel.EducationLevelId equals educationLevel.Id
                            join user in _dbContext.Users on userSubjects.UserId equals user.Id
                            join tblUserSubjectRequest in _dbContext.UserSubjectRequests on userSubjects.UserSubjectRequestId equals tblUserSubjectRequest.Id into TempUserSubjectRequest
                            from userSubjectRequest in TempUserSubjectRequest.DefaultIfEmpty()
                            where user.Id.Contains(userId) && userSubjects.IsActive==true

                            select new SubjectWithParents
                            {
                                Id = subject.Id,
                                Class = Class,
                                Name = subject.Name,
                                BoardLevel = boardLevel,
                                EducationLevel = educationLevel,
                                IsAllocated = userSubjects.IsActive,
                                UserId = userId,
                                UserSubjectRequest = userSubjectRequest
                            }).ToList();
            }
             
            return subjects;
        }

        List<Subject> ISubjectRepository.GetWithDemoChapter(int parentId)
        {

            List<Subject> subjects;

           
                subjects = (from subject in _dbContext.Subjects
                            join Class in _dbContext.Classes on subject.ClassId equals Class.Id
                            join boardLevel in _dbContext.BoardLevels on Class.BoardLevelId equals boardLevel.Id
                          
                            join tblSubDemoChapter in _dbContext.SubjectDemoChapters on subject.Id equals tblSubDemoChapter.SubjectId into TempSubDemoChapter
                            from subjectDemoChapter in TempSubDemoChapter.DefaultIfEmpty()

                            where boardLevel.Id == parentId

                            select new Subject
                            {
                                Id = subject.Id,
                                Name = subject.Name,
                                NewArrivalText=subject.NewArrivalText,
                                SubjectDemoChapter = TempSubDemoChapter.ToList()
                            }).ToList();
            
             
            return subjects;
        }
        List<SubjectWithParents> ISubjectRepository.ListOfSubjectsRequested(string userId)
        {
            //var educationLevels = _dbContext.EducationLevels.Include(a => a.BoardLevels).Select(x=>x.BoardLevels.Select(s=>s.Classes)).ToList();

            //var subjects = _dbContext.Subjects.Include(a => a.Artifact).Include(a => a.Chapters).FirstOrDefault(x => x.Id == id);

            var subjects = (from userSubjectRequests in _dbContext.UserSubjectRequests
                            
                            join subject in _dbContext.Subjects on userSubjectRequests.SubjectId equals subject.Id
                            join Class in _dbContext.Classes on subject.ClassId equals Class.Id
                            join boardLevel in _dbContext.BoardLevels on Class.BoardLevelId equals boardLevel.Id
                            join educationLevel in _dbContext.EducationLevels on boardLevel.EducationLevelId equals educationLevel.Id
                            join user in _dbContext.Users on userSubjectRequests.UserId equals user.Id
                            where user.Id.Contains(userId) && userSubjectRequests.StatusId == Convert.ToInt32(RequestsStatus.Pending)



                            select new SubjectWithParents
                            {
                                Id = subject.Id,
                                Class = Class,
                                Name = subject.Name,
                                BoardLevel = boardLevel,
                                EducationLevel = educationLevel,
                                User=user,
                                UserId=user.Id,
                                RequestStatusId=userSubjectRequests.StatusId

                            }).ToList();
            return subjects;
        }
      
    }
    
}
