﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class CountryRepository : GenericRepository<Country>, ICountryRepository
    {
        public CountryRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
       
    }
}
