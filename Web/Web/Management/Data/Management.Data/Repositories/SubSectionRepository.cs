﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Management.Data.Repositories
{
    public class SubSectionRepository : GenericRepository<SubSection>, ISubSectionRepository
    {
        public SubSectionRepository(IDatabaseContext dbContext) : base(dbContext)
        {
            
        }
        SubSection ISubSectionRepository.GetWithChilds(int id)
        {

            var subSection = _dbContext.SubSections.Include(a => a.Section).Include(a => a.Section.Chapter).FirstOrDefault(a=>a.Id==id);
            return subSection;
        }
    }
}
