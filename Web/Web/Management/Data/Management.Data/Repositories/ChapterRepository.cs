﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Management.Data.Models.CustomModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Management.Data.Repositories
{
    public class ChapterRepository : GenericRepository<Chapter>, IChapterRepository
    {
        public ChapterRepository(IDatabaseContext dbContext) : base(dbContext)
        {
            
        }
       Chapter IChapterRepository.GetWithChilds(int id)
        {

            var chapters = _dbContext.Chapters.Include(a => a.Artifact).Include(a => a.Subject).Include(a => a.Sections).Include("Sections.SubSections").FirstOrDefault(x => x.Id == id);
            return chapters;
        }
        Chapter IChapterRepository.GetWithSection(int id)
        {

            var chapters = _dbContext.Chapters.Include(a => a.Sections).FirstOrDefault(x => x.Id == id);
            return chapters;
        }
        List<ChapterWithParents> IChapterRepository.ListOfChaptersAllocatedToUser(string userId, bool showDeAllocatedToo)
        {

            List<ChapterWithParents> chapters;

            if (showDeAllocatedToo)
            {
                chapters = (from userChapters in _dbContext.UserChapters

                            join chapter in _dbContext.Chapters on userChapters.ChapterId equals chapter.Id
                            join subject in _dbContext.Subjects on chapter.SubjectId equals subject.Id
                            join Class in _dbContext.Classes on subject.ClassId equals Class.Id
                            join boardLevel in _dbContext.BoardLevels on Class.BoardLevelId equals boardLevel.Id
                            join educationLevel in _dbContext.EducationLevels on boardLevel.EducationLevelId equals educationLevel.Id
                            join user in _dbContext.Users on userChapters.UserId equals user.Id
                            join tblUserChapterRequest in _dbContext.UserChapterRequests on userChapters.UserChapterRequestId equals tblUserChapterRequest.Id into TempUserChapterRequest
                            from userChapterRequest in TempUserChapterRequest.DefaultIfEmpty()

                            where user.Id.Contains(userId)

                            select new ChapterWithParents
                            {
                                Id = chapter.Id,
                                Subject = subject,
                                Name = chapter.Name,
                                BoardLevel = boardLevel,
                                Class=Class,
                                EducationLevel = educationLevel,
                                IsAllocated = userChapters.IsActive,
                                UserId = userId,
                                UserChapterRequest = userChapterRequest
                                
                            }).ToList();
            }
            else
            {
                chapters = (from userChapters in _dbContext.UserChapters
                            join user in _dbContext.Users on userChapters.UserId equals user.Id
                            join chapter in _dbContext.Chapters on userChapters.ChapterId equals chapter.Id
                            join subject in _dbContext.Subjects on chapter.SubjectId equals subject.Id
                            join Class in _dbContext.Classes on subject.ClassId equals Class.Id
                            join boardLevel in _dbContext.BoardLevels on Class.BoardLevelId equals boardLevel.Id
                            join educationLevel in _dbContext.EducationLevels on boardLevel.EducationLevelId equals educationLevel.Id
                           
                            join tblUserChapterRequest in _dbContext.UserChapterRequests on userChapters.UserChapterRequestId equals tblUserChapterRequest.Id into TempUserChapterRequest
                            from userChapterRequest in TempUserChapterRequest.DefaultIfEmpty()

                            where user.Id.Contains(userId) && userChapters.IsActive == true

                            select new ChapterWithParents
                            {
                                Id = chapter.Id,
                                Subject = subject,
                                Class = Class,
                                Name = chapter.Name,
                                BoardLevel = boardLevel,
                                EducationLevel = educationLevel,
                                IsAllocated = userChapters.IsActive,
                                UserId = userId,
                                UserChapterRequest = userChapterRequest

                            }).ToList();
            }

            return chapters;
        }
        List<ChapterWithParents> IChapterRepository.ListOfChaptersRequested(string userId)
        {
           var chapters = (from userChapterRequests in _dbContext.UserChapterRequests
                           join chapter in _dbContext.Chapters on userChapterRequests.ChapterId equals chapter.Id

                           join subject in _dbContext.Subjects on chapter.SubjectId equals subject.Id
                            join Class in _dbContext.Classes on subject.ClassId equals Class.Id
                            join boardLevel in _dbContext.BoardLevels on Class.BoardLevelId equals boardLevel.Id
                            join educationLevel in _dbContext.EducationLevels on boardLevel.EducationLevelId equals educationLevel.Id
                            join user in _dbContext.Users on userChapterRequests.UserId equals user.Id
                            where user.Id.Contains(userId) && userChapterRequests.StatusId == Convert.ToInt32(RequestsStatus.Pending)



                            select new ChapterWithParents
                            {
                                Id = chapter.Id,
                                Class = Class,
                                Name = chapter.Name,
                                BoardLevel = boardLevel,
                                Subject = subject,
                                EducationLevel = educationLevel,
                                User = user,
                                UserId = user.Id,
                                RequestStatusId = userChapterRequests.StatusId

                            }).ToList();
            return chapters;
        }
        List<Chapter> IChapterRepository.GetListWithChilds(List<int> ids)
        {

            var chapters = _dbContext.Chapters.Include(a => a.Sections).Include("Sections.SubSections").Where(x => ids.Contains( x.Id )).ToList();
            return chapters;
        }
    }
}
