﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Management.Data.Repositories
{
    public class SubjectDemoChapterRepository : GenericRepository<SubjectDemoChapter>, ISubjectDemoChapterRepository
    {
        public SubjectDemoChapterRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
        SubjectDemoChapter ISubjectDemoChapterRepository.GetWithChilds(int id)
        {
            var subjectDemoChapter = _dbContext.SubjectDemoChapters.Include(a => a.Chapter).FirstOrDefault(x => x.ChapterId == id);
            return subjectDemoChapter;
        }
    }
}
