﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class CityRepository : GenericRepository<City>, ICityRepository
    {
        public CityRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
       
    }
}
