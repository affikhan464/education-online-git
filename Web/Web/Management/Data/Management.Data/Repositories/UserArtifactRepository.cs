﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Repositories
{
    public class UserArtifactRepository : GenericRepository<UserArtifact>, IUserArtifactRepository
    {
        public UserArtifactRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
    }
}
