﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Management.Data.Repositories
{
    public class SectionRepository : GenericRepository<Section>, ISectionRepository
    {
        public SectionRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
        Section ISectionRepository.GetWithChilds(int id)
        {

            var sections = _dbContext.Sections.Include(a => a.Artifact).Include(a => a.Chapter).Include("Chapter.Subject").Include(a => a.SubSections).FirstOrDefault(x => x.Id == id);
            return sections;
        }
    }
}
