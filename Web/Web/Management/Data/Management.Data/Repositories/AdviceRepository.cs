﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class AdviceRepository : GenericRepository<Advice>, IAdviceRepository
    {
        public AdviceRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }

    }
}
