﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class WordRepository : GenericRepository<Word>, IWordRepository
    {
        public WordRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
       
    }
}
