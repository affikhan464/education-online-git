﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Management.Data.Repositories
{
    public class BoardLevelRepository : GenericRepository<BoardLevel>, IBoardLevelRepository
    {
        public BoardLevelRepository(IDatabaseContext dbContext) : base(dbContext)
        {
            
        }
        BoardLevel IBoardLevelRepository.GetWithChilds(int id)
        {

            var boardLevel = _dbContext.BoardLevels.Include(a => a.Artifact).Include(a => a.Classes).FirstOrDefault(x => x.Id == id);
            return boardLevel;
        }
    }
}
