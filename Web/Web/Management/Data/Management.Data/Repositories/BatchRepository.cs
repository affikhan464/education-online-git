﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using System.Collections.Generic;

namespace Management.Data.Repositories
{
   public class BatchRepository : GenericRepository<Batch>, IBatchRepository
    {
        public BatchRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }

        
    }
}
