﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;


namespace Management.Data.Repositories
{
   public class AnswerRepository : GenericRepository<Answer>, IAnswerRepository
    {
        public AnswerRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }

    }
}
