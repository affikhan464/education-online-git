﻿using Management.Data.Interfaces;
using Management.Data.Interfaces.RepositoryInterfaces;
using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Repositories
{
   public class UserSubjectRepository : GenericRepository<UserSubject>, IUserSubjectRepository
    {
        public UserSubjectRepository(IDatabaseContext dbContext) : base(dbContext)
        {

        }
    }
}
