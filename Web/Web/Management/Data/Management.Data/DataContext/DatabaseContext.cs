﻿using Management.Data.Interfaces;
using Management.Data.Models;
using Management.Infrastructure.Security.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;


namespace Management.Data.DataContext
{
    public class DatabaseContext : ManagementIdentityDbContext, IDatabaseContext
    {
        public DatabaseContext(DbContextOptions options) : base(options) { }

        public DbSet<Advice> Advices { get; set; }
        public DbSet<AdviceArtifact> AdviceArtifacts { get; set; }
        public DbSet<AdviceType> AdviceTypes { get; set; }
        public DbSet<Answer> Answers { get; set; }
        public DbSet<Artifact> Artifacts { get; set; }
        public DbSet<ArtifactType> ArtifactTypes { get; set; }
        public DbSet<BoardLevel> BoardLevels { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<Chapter> Chapters { get; set; }
        public DbSet<CorrectAnswer> CorrectAnswers { get; set; }
        public DbSet<Commentary> Commentaries { get; set; }
        public DbSet<DegreeLevel> DegreeLevels { get; set; }
        public DbSet<DegreeType> DegreeTypes { get; set; }
        public DbSet<EducationLevel> EducationLevels { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<Experience> Experiences { get; set; }
        public DbSet<PowerSlide> PowerSlides { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<QuestionAdvice> QuestionAdvices { get; set; }
        public DbSet<QuestionCategory> QuestionCategories { get; set; }
        public DbSet<QuestionChoice> QuestionChoices { get; set; }
        public DbSet<QuestionDifficultyLevel> QuestionDifficultyLevels { get; set; }
        public DbSet<QustionType> QustionTypes { get; set; }
        public DbSet<RequestStatus> RequestStatuses { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<SectionQuestion> SectionQuestions { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<SubSection> SubSections { get; set; }
        public DbSet<SubSectionQuestion> SubSectionQuestions { get; set; }
        public DbSet<Tour> Tours { get; set; }
        public DbSet<UserArtifact> UserArtifacts { get; set; }
        public DbSet<UserEducation> UserEducations { get; set; }
        public DbSet<UserExperience> UserExperiences { get; set; }
        public DbSet<UserChapterRequest> UserChapterRequests { get; set; }
        public DbSet<UserSubjectRequest> UserSubjectRequests { get; set; }
        public DbSet<UserQuestionAttempt> UserQuestionAttempts { get; set; }
        public DbSet<Word> Words { get; set; }
        public DbSet<UserChapter> UserChapters { get; set; }
        public DbSet<UserSubject> UserSubjects { get; set; }
        public DbSet<Batch> Batches { get; set; }
        public DbSet<UserBatch> UserBatches { get; set; }
        public DbSet<UserBatchRequest> UserBatchRequests { get; set; }
        public DbSet<WordAdvice> WordAdvices { get; set; }
        public DbSet<WordQuestion> WordQuestions { get; set; }
        public DbSet<WordCommentary> WordCommentaries { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<SetQuestion> SetQuestions { get; set; }
        public DbSet<Test> Tests { get; set; }
        public DbSet<TestType> TestTypes { get; set; }
        public DbSet<TestQuestion> TestQuestions { get; set; }
        public DbSet<UserTestAttempt> UserTestAttempts { get; set; }
        public DbSet<SubjectDemoChapter> SubjectDemoChapters { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<BatchTest> BatchTests { get; set; }
        public DbSet<StudyType> StudyTypes { get; set; }
        public DbSet<StudentStudyType> StudentStudyTypes { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<VoucherType> VoucherTypes { get; set; }
        public DbSet<GLAccount> GLAccounts { get; set; }
        public DbSet<GeneralLedger> GeneralLedgers { get; set; }
        public DbSet<GLLevel> GLLevels { get; set; }
        public DbSet<BillType> BillTypes { get; set; }
        public DbSet<UserBatchReferral> UserBatchReferrals { get; set; }
        public DbSet<PartyLedger> PartiesLedgers { get; set; }
        public DbSet<PrimeBook> PrimeBooks { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<EducationLevel>()
            .HasMany(g => g.BoardLevels);


            builder.Entity<UserRole>()
                 .HasOne(e => e.User)
        .WithMany(e => e.UserRoles)
        .HasForeignKey(e => e.UserId);

            builder.Entity<UserRole>()
                .HasOne(e => e.Role)
        .WithMany(e => e.UserRoles)
        .HasForeignKey(e => e.RoleId);

            base.OnModelCreating(builder);

        }
    }
}
