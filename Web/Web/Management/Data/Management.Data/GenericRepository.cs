﻿using Management.Data.Interfaces.RepositoryInterfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Management.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Management.Data
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        internal readonly IDatabaseContext _dbContext;
        private readonly DbSet<T> _dbSet;
       
        public GenericRepository(IDatabaseContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<T>();
        }
        public async Task AddAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public async Task AddManyAsync(IEnumerable<T> entities)
        {
            await _dbSet.AddRangeAsync(entities);                   
        }

        public async Task DeleteAsync(T entity)
        {
            await Task.FromResult(_dbSet.Remove(entity));
        }

        public async Task DeleteManyAsync(IEnumerable<T> entities)
        {
            await Task.Run(() => _dbSet.RemoveRange(entities));
        }

        public IQueryable<T> Get()
        {
            return _dbSet.AsQueryable();
        }

        public async Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbSet.Where(predicate).ToListAsync();
        }

        public  Task UpdateAsync(T entity)
        {
            return Task.Run(() =>
            {
                _dbSet.Attach(entity);
                _dbContext.Entry(entity).State = EntityState.Modified;
            });
        }

        public Task UpdateManyAsync(IEnumerable<T> entities)
        {
            return Task.Run(() =>
            {
                foreach (var entity in entities)
                    UpdateAsync(entity);
            }); 
        }
    }
}
