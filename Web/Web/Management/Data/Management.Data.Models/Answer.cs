﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
   public class Answer
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Question")]
        public int? QuestionId { get; set; }
        public virtual Question Qustion { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("MCQChoice")]
        public int? MCQChoiceId { get; set; }
        public virtual QuestionChoice MCQChoice { get; set; }
        /// <summary>
        /// Any Single way answer details, Fill in the black, Long and short answer
        /// </summary>
        public string Detail { get; set; }
        public virtual ICollection<MTQAnsweredChoice> MTQAnsweredChoices { get; set; }
        public int WrittenAnswerType { get; set; }
        public string ImageFileName { get; set; }
        [MaxLength]
        public string ImageFileUrl { get; set; }
    }
}
