﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserBatch
    {
        [Key]
        public int Id { get; set; }
        public bool IsActive { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Batch")]
        public int BatchId { get; set; }
        public virtual Batch Batch { get; set; }

        [ForeignKey("UserBatchRequest")]
        public int? UserBatchRequestId { get; set; }
        public virtual UserBatchRequest UserBatchRequest { get; set; }
    }
}
