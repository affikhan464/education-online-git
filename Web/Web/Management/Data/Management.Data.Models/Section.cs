﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Section
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int SectionNumber { get; set; }
        public string NewArrivalText { get; set; }
        public int SortNumber { get; set; }
        [MaxLength]
        public string Detail { get; set; }
        [ForeignKey("Chapter")]
        public int ChapterId { get; set; }
        public virtual Chapter Chapter { get; set; }
        public virtual ICollection<SubSection> SubSections { get; set; }

        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }
        [ForeignKey("Artifact")]
        public int? IconId { get; set; }
        public virtual Artifact Artifact { get; set; }
    }
}
