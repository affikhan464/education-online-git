﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Management.Data.Models
{
    public class RequestStatus
    {
        [Key]
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
