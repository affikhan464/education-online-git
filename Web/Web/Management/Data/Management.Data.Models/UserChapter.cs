﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserChapter
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Chapter")]
        public int ChapterId { get; set; }
        public virtual Chapter Chapter { get; set; }
        public bool IsActive { get; set; }

        [ForeignKey("UserChapterRequest")]
        [DefaultValue(2)]
        public int? UserChapterRequestId { get; set; }
        public virtual UserChapterRequest UserChapterRequest { get; set; }
    }
}
