﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class City
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string name { get; set; }
        public string countryCode { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }

        [ForeignKey("Region")]
        public int? RegionId { get; set; }
        public virtual Region Region { get; set; }

    }
}
