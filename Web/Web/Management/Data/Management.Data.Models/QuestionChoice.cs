﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class QuestionChoice
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Detail { get; set; }

        [ForeignKey("Question")]
        public int QuestionId { get; set; }
        public virtual Question Qustion { get; set; }
        public virtual Advice Advice { get; set; }
    }
}
