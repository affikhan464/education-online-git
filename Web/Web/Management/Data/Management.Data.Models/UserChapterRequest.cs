﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserChapterRequest
    {
        [Key]
        public int Id { get; set; }
        public DateTime  RequestDate { get; set; }

        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Chapter")]
        public virtual int ChapterId { get; set; }
        public virtual Chapter Chapter { get; set; }

        [ForeignKey("RequestStatus")]
        public virtual int StatusId { get; set; }
        public virtual RequestStatus RequestStatus { get; set; }
    }
}
