﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserBatchReferral
    {
        [Key]
        public int Id { get; set; }
        public DateTime ReferralDate { get; set; }
        public string ReferringUserEmail { get; set; }
        public string ReferringUserCNIC { get; set; }
        public string ReferralCode { get; set; }
        public bool IsEnrolledByReferralCode{ get; set; }

        [ForeignKey("ReferralUser")]
        [MaxLength]
        public string ReferralUserId { get; set; }
        public virtual User ReferralUser { get; set; }

        [ForeignKey("Batch")]
        public int BatchId { get; set; }
        public virtual Batch Batch { get; set; }
    }
}
