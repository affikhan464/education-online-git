﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class GLAccount
    {
        [Key]
        public int Id { get; set; }
        public string Code { get; set; }
        [MaxLength]
        public string AccountTitle { get; set; }
        public string NormalBalance { get; set; }//Client or Supplier
        [ForeignKey("GLAcount")]
        public int GLLevelId { get; set; }
        public virtual GLLevel GLLevel { get; set; }
    }
}
