﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Question
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Detail { get; set; }
        public DateTime Date { get; set; }
        public DateTime LastUpdated { get; set; }
        public string AttributeType { get; set; }
        public string AttributeId { get; set; }
        public string QuestionTitle { get; set; }

        [ForeignKey("QuestionDifficultyLevel")]
        public int QuestionDifficultyLevelId { get; set; }
        public virtual QuestionDifficultyLevel QuestionDifficultyLevel { get; set; }

        [ForeignKey("QustionType")]
        public int QustionTypeId { get; set; }
        public virtual QustionType QustionType { get; set; }

        [ForeignKey("QuestionCategory")]
        public int? QuestionCategoryId { get; set; }
        public virtual QuestionCategory QuestionCategory { get; set; }
        public virtual ICollection<SetQuestion> SetQuestion { get; set; }
        public string Source { get; set; }
        public string Author { get; set; }
        public string Year { get; set; }

        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
        public string Marks { get; set; }
        [DefaultValue(0)]
        public decimal AverageSolveMinutes{ get; set; }
        public virtual ICollection<QuestionChoice> QuestionChoices { get; set; }
        public virtual ICollection<CorrectAnswer> CorrectAnswers { get; set; }

    }
}
