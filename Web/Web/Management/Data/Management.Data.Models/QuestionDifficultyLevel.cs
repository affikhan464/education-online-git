﻿using System.ComponentModel.DataAnnotations;

namespace Management.Data.Models
{
    public class QuestionDifficultyLevel
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Level { get; set; }
    }
}