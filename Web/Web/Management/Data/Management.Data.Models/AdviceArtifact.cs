﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class AdviceArtifact
    {
        [Key]
        public int Id { get; set; }
        
        [ForeignKey("Advice")]
        public int AdviceId { get; set; }
        public virtual Advice Advice { get; set; }

        [ForeignKey("Artifact")]
        public int ArtifactId { get; set; }
        public virtual Artifact Artifact { get; set; }
    }
}
