﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Management.Data.Models
{
    public class Commentary
    {
        [Key]
        public int Id { get; set; }
        public string Detail { get; set; }
        public DateTime LastUpdated { get; set; }
        public string CommentaryTitle { get; set; }
        public DateTime Date { get; set; }
        public string AttributeType { get; set; }
        public string AttributeId { get; set; }
    }
}
