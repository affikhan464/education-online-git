﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class GeneralLedger
    {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string GLAccountCode { get; set; }
        [MaxLength]
        public string Description { get; set; }
        [MaxLength]
        public string Narration { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
    }
}
