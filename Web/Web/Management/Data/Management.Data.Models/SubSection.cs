﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
   public class SubSection
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int SubSectionNumber { get; set; }
        public string NewArrivalText { get; set; }
        public int SortNumber { get; set; }
        [MaxLength]
        public string Detail { get; set; }
        [ForeignKey("Section")]
        public int SectionId { get; set; }
        public virtual Section Section { get; set; }
        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Artifact")]
        public int? IconId { get; set; }
        public virtual Artifact Artifact { get; set; }
    }
}
