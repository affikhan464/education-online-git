﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
   public class WordAdvice
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Advice")]
        public int AdviceId { get; set; }
        public virtual Advice Advice { get; set; }

        [ForeignKey("Word")]
        public int WordId { get; set; }
        public virtual Word Word { get; set; }
    }
}
