﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class PrimeBook
    {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public string GLAccountCode { get; set; }
        [MaxLength]
        public string Description { get; set; }
        [MaxLength]
        public string Particular { get; set; }  
        [MaxLength]
        public string Narration { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }

        [ForeignKey("VoucherType")]
        public int? VoucherTypeId { get; set; }
        public virtual VoucherType VoucherType { get; set; }

        [ForeignKey("BillType")]
        public int BillTypeId { get; set; }
        public virtual VoucherType BillType { get; set; }
        [ForeignKey("GLAcount")]
        public int GLAcountId { get; set; }
        public virtual GLAccount GLAcount { get; set; }
    }
}
