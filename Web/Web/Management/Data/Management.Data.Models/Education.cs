﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Management.Data.Models
{
    public class Education
    {
        [Key]
        public int Id { get; set; }
        public DegreeLevel DegreeLevel { get; set; }
        public DegreeType DegreeType { get; set; }
        public string DegreeTitle { get; set; }
        public string Location { get; set; }
        public string Institution { get; set; }
        public DateTime CompletionYear { get; set; }
    }
}
