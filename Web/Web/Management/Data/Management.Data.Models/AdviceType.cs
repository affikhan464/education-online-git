﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Management.Data.Models
{
    public class AdviceType
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Type { get; set; }
    }
}
