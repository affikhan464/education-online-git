﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserQuestionAttempt
    {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public decimal ViewSeconds { get; set; }

        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Question")]
        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }

        [ForeignKey("Answer")]
        public int AnswerId { get; set; }
        public virtual Answer Answer { get; set; }

        [ForeignKey("Test")]
        public int? TestId { get; set; }
        public virtual Test Test { get; set; }
        public bool IsMarksAwarded { get; set; }
        [MaxLength]
        public string Remarks { get; set; }
        public int Marks { get; set; }
        public bool IsQuestionAttempted { get; set; }
    }
}
