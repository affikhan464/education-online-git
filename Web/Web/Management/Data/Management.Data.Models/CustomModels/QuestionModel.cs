﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Models.CustomModels
{
  public  class QuestionModel
    {
        public int Id { get; set; }

        public string AttributeType { get; set; }
        public string AttributeId { get; set; }
        public string QuestionDetail { get; set; }
        public string AnswerDetail { get; set; }
        public string ShortAnswerDetail { get; set; }
        public DateTime Date { get; set; }
        public DateTime LastUpdated { get; set; }
        public QuestionDifficultyLevel QuestionDifficultyLevel { get; set; }
        public int QuestionDifficultyLevelId { get; set; }
        public int QustionTypeId { get; set; }
        public QustionType QustionType { get; set; }
        public string SelectedWord { get; set; }
        public int WordId { get; set; }
        public string QuestionTitle { get; set; }
        public int SetId { get; set; }
        public string Source { get; set; }
        public string Author { get; set; }
        public string Year { get; set; }
        
        public bool isEditMode { get; set; }
        public bool IsSetQuestion { get; set; }
        
        public List<QuestionChoice> QuestionChoices { get; set; }
        public List<Advice> QuestionChoiceAdvices { get; set; }
        public string CorrectQuestionChoice { get; set; }
        public string SingleQuestionChoiceAdvice { get; set; }
        public int CorrectQuestionChoiceId { get; set; }
        public List<string> MTQCorrectQuestionChoices { get; set; }
        public QuestionCategories QuestionCategory { get; set; }
        public bool IsDeleted { get; set; }
        public string Marks { get; set; }
    }
}
