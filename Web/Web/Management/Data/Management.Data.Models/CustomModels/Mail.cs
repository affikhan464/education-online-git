﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Management.Data.Models.CustomModels
{
    public class Mail
    {
        public string UserName { get; set; }
        public string ToEMailAddress { get; set; }
        public string FromEMailAddress { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public string UserId { get; set; }
        public AlternateView AlternativeView { get; set; }
        public string UserEmail { get; set; }
        public string Mobile { get; set; }
        public string School { get; set; }
        public Batch Batch { get; set; }
        public User Student { get; set; }
        public User Teacher { get; set; }
        public bool IsTeacher { get; set; }
        public string ResetPasswordToken { get; set; }
    }
}
