﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Models.CustomModels
{
  public  class CommentaryModel
    {
        public int Id { get; set; }

        public string AttributeType { get; set; }
        public string AttributeId { get; set; }
        public string CommentaryDetail { get; set; }
        public DateTime Date { get; set; }
        public string SelectedWord { get; set; }
        public int WordId { get; set; }
        public string CommentaryTitle { get; set; }
    }
}
