﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Models.CustomModels
{
    public class ForgotPassword
    {
        public string Email { get; set; }
        public string ResetToken { get; set; }
    }
}
