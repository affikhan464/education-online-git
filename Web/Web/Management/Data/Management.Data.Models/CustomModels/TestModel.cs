﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Models.CustomModels
{
    public class TestModel
    {
        public int Id { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public string TestTitle { get; set; }
        
        public int TestTypeId { get; set; }
        public TestType TestType { get; set; }
        
        public string UserId { get; set; }
        public User User { get; set; }

        public List<UserQuestionAttempt> UserQuestionAttempts { get; set; }
        public  List<UserTestAttempt> UserTestAttempts { get; set; }
        public List<TestCreatedFrom> TestCreatedFroms { get; set; }
        public int Minutes { get; set; }
        public int Hours { get; set; }
        public bool WithAdvice { get; set; }
        public string IncludeQuestion { get; set; }
        public List<TestQuestionModel> TestQuestions { get; set; }
    }

    public class TestQuestionModel
    {
        public QuestionModel Question { get; set; }
    }
}
