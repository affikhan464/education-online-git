﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Models.CustomModels
{
   public class TestWithQuestions
    {
        public Test Test { get; set; }
        public TestQuestion TestQuestion { get; set; }
    }
}
