﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Management.Data.Models.CustomModels
{
    public class SetWithQuestions
    {
        public Set Set { get; set; }
        public List<Question> Questions { get; set; }
    }
}
