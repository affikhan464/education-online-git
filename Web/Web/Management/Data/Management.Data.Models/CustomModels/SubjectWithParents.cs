﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Models.CustomModels
{
  public  class SubjectWithParents
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public EducationLevel EducationLevel { get; set; }
        public BoardLevel BoardLevel { get; set; }
        public Class Class { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public int RequestStatusId { get; set; }
        public bool IsAllocated { get; set; }
        public IdentityRole<string> UserRole { get; set; }
        public UserSubjectRequest UserSubjectRequest { get; set; }

    }
}
