﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Models.CustomModels
{
    public class AttributeModel
    {
        public string Name { get; set; }
        public int ParentId { get; set; }
        public string ParentType { get; set; }
        public int Id { get; set; }
        public string Type { get;  set; }
        public string ChildText { get; set; }
        public string ChildType { get; set; }
        public int SortNumber { get; set; }
        public string FileName { get; set; }
        public string IconUrl { get; set; }
        public string TypeText { get; set; }
        public bool IsAllocated { get; set; }
    }
}
