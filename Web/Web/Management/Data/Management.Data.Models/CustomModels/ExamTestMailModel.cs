﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Models.CustomModels
{
   public class ExamTestMailModel
    {
        public Test Test { get; set; }
        public TestQuestion TestQuestion { get; set; }
        public User Student { get; set; }
        public User Teacher { get; set; }
        public Subject Subject { get; set; }
        public Batch Batch { get; set; }
        public UserTestAttempt UserTestAttempt { get; set; }
        public int UnattemptedQuestions { get; set; }
        public int TotalMarks { get; set; }
    }
}
