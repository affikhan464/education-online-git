﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Models.CustomModels
{
    public class InformAllocatedMemberModel
    {
        public AttributeModel Attribute { get; set; }
        public Mail Mail { get; set; }
        public Schedule Schedule { get; set; }
        public List<User> Users { get; set; }
    }
}
