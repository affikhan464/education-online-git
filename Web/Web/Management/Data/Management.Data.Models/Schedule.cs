﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Schedule
    {
        [Key]
        public int Id { get; set; }
        public string Title { get; set; }
        [MaxLength]
        public string Notes { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string LabelCategory { get; set; }
        [MaxLength]
        public string EventUrl { get; set; }
       
        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Batch")]
        public int BatchId { get; set; }
        public virtual Batch Batch { get; set; }

        [ForeignKey("Test")]
        public int? TestId { get; set; }
        public virtual Test Test { get; set; }

        public int ScheduleType { get;  set; }
    }
}
