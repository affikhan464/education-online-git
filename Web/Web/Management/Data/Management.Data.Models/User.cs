﻿using Management.Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        public string MobileNumber { get; set; }
        public bool IsActive { get; set; }
        [DefaultValue(false)]
        public bool IsDeleted { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string CNIC { get; set; }
        public string NationalTaxNumber { get; set; }
        public string UserExperienceSummary { get; set; }
        public string SubjectExpertise { get; set; }
        public string Gender { get; set; }
        public string YearOfExperience { get; set; }
        [ForeignKey("AddedBy")]
        [MaxLength]
        public string AddedBy { get; set; }
        public virtual User AddedByUser { get; set; }
        public virtual UserArtifact ProfilePicture { get; set; }

        [ForeignKey("City")]
        public int? CityId { get; set; }
        public virtual City City { get; set; }
        public string SchoolName { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
        public string ZoomUserName { get; set; }
        public string ZoomPassword { get; set; }
        public string ZoomUrl { get; set; }
        public string ReferralCode { get; set; }
    }


}
