﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Management.Data.Models
{
    public class Set
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Title { get; set; }
        public string AttributeType { get; set; }
        public int AttributeId { get; set; }
        public DateTime LastUpdated { get; set; }
        public DateTime AddedDate { get; set; }
        public virtual ICollection<SetQuestion> SetQuestions { get; set; }
    }
}
