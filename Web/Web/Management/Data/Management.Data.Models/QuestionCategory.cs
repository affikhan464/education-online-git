﻿using System.ComponentModel.DataAnnotations;

namespace Management.Data.Models
{
    public class QuestionCategory
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Name { get; set; }
    }
}