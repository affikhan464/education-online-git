﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class BoardLevel
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int SortNumber { get; set; }
        public string NewArrivalText { get; set; }

        [ForeignKey("EducationLevel")]
        public int EducationLevelId { get; set; }
        public virtual EducationLevel EducationLevel { get; set; }
        public virtual ICollection<Class> Classes { get; set; }
        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }
        [ForeignKey("Artifact")]
        public int? IconId { get; set; }
        public virtual Artifact Artifact { get; set; }
    }
}
