﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class SubjectDemoChapter
    {
        [Key]
        public int Id { get; set; }
      
        [ForeignKey("Subject")]
        public int? SubjectId { get; set; }
        public Subject Subject { get; set; }

        [ForeignKey("Chapter")]
        public int? ChapterId { get; set; }
        public virtual Chapter Chapter { get; set; }
    }
}
