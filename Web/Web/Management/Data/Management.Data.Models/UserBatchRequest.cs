﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserBatchRequest
    {
        [Key]
        public int Id { get; set; }
        public DateTime  RequestDate { get; set; }

        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Batch")]
        public int BatchId { get; set; }
        public virtual Batch Batch { get; set; }

        [ForeignKey("RequestStatus")]
        public int StatusId { get; set; }
        public virtual RequestStatus RequestStatus { get; set; }
    }
}
