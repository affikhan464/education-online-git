﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class TestCreatedFrom
    {
        [Key]
        public int Id { get; set; }
       
        public string AttributeType { get; set; }
        public int AttributeId { get; set; }

        [ForeignKey("Test")]
        public int TestId { get; set; }
        public virtual Test Test { get; set; }
    }
}
