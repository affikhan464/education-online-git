﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserEducation
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }
        [ForeignKey("Education")]

        public int EducationId { get; set; }
        public virtual Education Education { get; set; }
    }
}
