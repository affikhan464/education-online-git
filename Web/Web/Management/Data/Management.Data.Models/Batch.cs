﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Batch
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Title { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal BatchFee { get; set; }
        public decimal BatchReferralAmount { get; set; }

        [ForeignKey("Subject")]
        public int? SubjectId { get; set; }
        public virtual Subject Subject { get; set; }
        public ICollection<UserBatch> BatchUsers { get; set; }
        public ICollection<BatchTest> BatchTests { get; set; }
        public ICollection<UserBatchRequest> UserBatchRequests { get; set; }
    }
}
