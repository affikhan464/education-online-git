﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserTestAttempt
    {
        [Key]
        public int Id { get; set; }
        public DateTime StartedDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public bool IsFinished { get; set; }
        public bool IsEvaluated { get; set; }
        public DateTime EvaluationDate { get; set; }
        public int Marks { get; set; }

        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Test")]
        public int TestId { get; set; }
        public virtual Test Test { get; set; }
    }
}
