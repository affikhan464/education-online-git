﻿using System.ComponentModel.DataAnnotations;

namespace Management.Data.Models
{
    public class QustionType
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Type { get; set; }
    }
}