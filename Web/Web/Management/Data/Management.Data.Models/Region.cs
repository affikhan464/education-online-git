﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Region
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string name { get; set; }
        public string countryCode { get; set; }
        public string fipsCode { get; set; }
        public string isoCode { get; set; }

        [ForeignKey("Country")]
        public int? CountryId { get; set; }
        public virtual Country Country { get; set; }
        public ICollection<City> Cities { get; set; }
        

    }
}
