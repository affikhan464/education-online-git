﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Management.Data.Models
{
    public class DegreeType
    {
        [Key]
        public int Id { get; set; }
       
        public string Type { get; set; }
    }
}
