﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Country
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string name { get; set; }
        public string alpha2Code { get; set; }
        public string alpha3Code { get; set; }
        public string callingCodes { get; set; }
        public decimal lat { get; set; }
        public decimal lng { get; set; }
        public string Region { get; set; }
        [MaxLength]
        public string Flag { get; set; }
        public ICollection<Region> Regions { get; set; }

    }

}
