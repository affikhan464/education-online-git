﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Management.Data.Models
{
    public class Experience
    {
        [Key]
        public int Id { get; set; }
        public string JobTitle { get; set; }
        public string Company { get; set; }
        public string Location { get; set; }
        public DateTime TimePeriodFrom { get; set; }
        public DateTime TimePeriodTo { get; set; }
        public bool IsCurrentlyWorkingHere { get; set; }
        [MaxLength]
        public string Description { get; set; }

    }
  
}
