﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class StudentStudyType
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Student")]
        [MaxLength]
        public string StudentId { get; set; }
        public virtual User Student { get; set; }

        [ForeignKey("StudyType")]
        public int StudyTypeId { get; set; }
        public virtual StudyType StudyType { get; set; }

        [ForeignKey("Batch")]
        public int BatchId { get; set; }
        public virtual Batch Batch { get; set; }
    }
}
