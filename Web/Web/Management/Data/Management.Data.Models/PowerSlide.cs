﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Management.Data.Models
{
    public class PowerSlide
    {
        [Key]
        public int Id { get; set; }
        public string Detail { get; set; }
        public DateTime LastUpdated { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string AttributeType { get; set; }
        public int AttributeId { get; set; }
    }
}
