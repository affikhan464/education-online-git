﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class SetQuestion
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Set")]
        public int SetId { get; set; }
        public virtual Set Set { get; set; }

        [ForeignKey("Question")]
        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }
    }
}
