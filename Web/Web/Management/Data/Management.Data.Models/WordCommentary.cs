﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class WordCommentary
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Word")]
        public int WordId { get; set; }
        public virtual Word Word { get; set; }

        [ForeignKey("Commentary")]
        public int CommentaryId { get; set; }
        public virtual Commentary Commentary { get; set; }
    }
}
