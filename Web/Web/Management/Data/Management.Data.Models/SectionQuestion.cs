﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class SectionQuestion
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Section")]
        public int SectionId { get; set; }
        public virtual Section Section { get; set; }

        [ForeignKey("Question")]
        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }
    }
}
