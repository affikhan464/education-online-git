﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class SubSectionQuestion
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Section")]
        public int SubSectionId { get; set; }
        public virtual SubSection SubSection { get; set; }

        [ForeignKey("Question")]
        public int QuestionId { get; set; }
        public virtual Question Question { get; set; }
    }
}
