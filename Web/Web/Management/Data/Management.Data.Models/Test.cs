﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Test
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Detail { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime LastUpdated { get; set; }
        public string AttributeType { get; set; }
        public string AttributeId { get; set; }
        public string TestTitle { get; set; }

        [ForeignKey("TestType")]
        public int TestTypeId { get; set; }
        public virtual TestType TestType { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<BatchTest>  BatchTest { get; set; }
        public virtual ICollection<TestQuestion> TestQuestions { get; set; }
        public virtual ICollection<UserTestAttempt> UserTestAttempts { get; set; }
        public virtual ICollection<UserQuestionAttempt> UserQuestionAttempts { get; set; }
        public virtual ICollection<TestCreatedFrom> TestCreatedFroms { get; set; }
        public int Minutes { get; set; }
        public int Hours { get; set; }
        public bool WithAdvice { get; set; }
        public string IncludeQuestion { get; set; }


        [ForeignKey("QuestionDifficultyLevel")]
        public int? QuestionDifficultyLevelId { get; set; }
        public virtual QuestionDifficultyLevel QuestionDifficultyLevel { get; set; }
    }

}
