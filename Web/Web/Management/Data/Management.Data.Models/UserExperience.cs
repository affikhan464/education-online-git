﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserExperience
    {
        [Key]
        public int Id { get; set; }
      
        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }
        [ForeignKey("Experience")]
       
        public int ExperienceId { get; set; }
        public virtual Experience Experience { get; set; }
    }
    
}
