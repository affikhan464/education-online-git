﻿using Management.Data.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserRole : IdentityUserRole<string>
    {
        [ForeignKey("User")]
        public string AppUserId { get; set; }
        [ForeignKey("Role")]
        public string AppRoleId { get; set; }
        public virtual User User { get; set; }
        public virtual Role Role { get; set; }

    }

    public class Role : IdentityRole
    {
        public ICollection<UserRole> UserRoles { get; set; }
    }
}
