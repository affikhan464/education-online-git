﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class UserSubject
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("User")]
        [MaxLength]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        [ForeignKey("Subject")]
        public int SubjectId { get; set; }
        public virtual Subject Subject { get; set; }
        public bool IsActive { get; set; }

        [ForeignKey("UserSubjectRequest")]
        [DefaultValue(2)]
        public int? UserSubjectRequestId { get; set; }
        public virtual UserSubjectRequest UserSubjectRequest { get; set; }
    }
}
