﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class BatchTest
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Batch")]
        [MaxLength]
        public int BatchId { get; set; }
        public virtual Batch Batch { get; set; }

        [ForeignKey("Test")]
        public int TestId { get; set; }
        public virtual Test Test { get; set; }
    }
}
