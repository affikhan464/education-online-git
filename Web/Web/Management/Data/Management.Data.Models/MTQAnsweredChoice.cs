﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
   public class MTQAnsweredChoice
    {
        [Key]
        public int Id { get; set; }


        [ForeignKey("Answer")]
        public int? AnswerId { get; set; }
        public virtual Answer Answer { get; set; }

        [ForeignKey("MTQChoice")]
        public int? MTQChoiceId { get; set; }
        public virtual QuestionChoice MTQChoice { get; set; }

    }
}
