﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Models
{
   
    public enum UserRoles
    {
        Admin = 1,
        Student,
        Researcher,
        Teacher
    }
  public struct AttributeType
    {
        public const string BoardLevel = "BoardLevel";
        public const string EducationLevel = "EducationLevel";
        public const string ClassText = "Class";
        public const string Class = "Class";
        public const string SubjectText = "Subject";
        public const string Subject = "Subject";
        public const string ChapterText = "Chapter";
        public const string Chapter = "Chapter";
        public const string SectionText = "Section";
        public const string Section = "Section";
        public const string SubSectionText = "Sub Section";
        public const string SubSection = "SubSection";
    }

    public struct FileExtension
    {
        public const string Pdf = ".pdf";
        public const string Xlsx = ".xlsx";
        public const string Xls = ".xls";
        public const string Doc = ".doc";
        public const string Docx = ".docx";
        public const string Ppt = ".ppt";
        public const string Pptx = ".pptx";
    }
    public struct UserState
    {
        public const string Blocked = "Blocked";
        public const string Reinstated = "Reinstated";
      
    }
    public struct EducationLevelType
    {
        public const string Board = "Board";
        public const string Body = "Body";
      
    }
    public enum AdviceTypes
    {
         Question =1,
         Word

    }
    public enum ScheduleTypes
    {
        OnlineClass = 1,
        Test

    }
    public struct QustionTypes
    {
        public const string LongQuestion = "Long Question";
        public const string MCQ = "MCQ";
        public const string FillBlank = "Fill in the Blank";
        public const string MatchingColumns = "Matching Columns";
        public const string MTQ = "MTQ";
        public const string ShortQuestion = "Short Question";
    }
    public enum QustionTypesEnum
    {
        LongQuestion =1,
        MCQ ,
        FillBlank,
        MatchingColumns,
        MTQ,
        ShortQuestion,

    }
    public enum RequestsStatus
    {
         Processed = 1,
        Pending = 2
    }
    public enum WrittenAnswerType
    {
         Upload = 1,
        Editor = 2
    }
    public enum VoucherTypes
    {
        CRV = 1,
        CPV ,
        BRV ,
        BPV ,
        JV
    }
    public enum BillTypes
    {
        Fee = 1,
        Expense,
        ReferralAmount,
    }
    public enum GLLevels
    {
        Level1 = 1,
        Level2,
        Level3,
        Level4,
        Level5,
    }
    public struct QuestionDifficultyLevels
    {
        public const string Easy = "Easy";
        public const string Moderate = "Moderate";
        public const string Difficult = "Difficult";
      
    }
  
    public struct IncludeQuestionType
    {
        public const string all = "all";
        public const string notyetanswered = "notyetanswered";
        public const string previouslymarked = "previouslymarked";
        public const string notyetansweredcorrectly = "notyetansweredcorrectly";
        public const string byQuestionLevel = "byQuestionLevel";

    }
  public enum QuestionCategories
    {
        NormalQuestion = 1,
        SetQuestion ,     
        ExamQuestion 
    }
    public struct QuestionCategoriesName
    {
        public const string NormalQuestions = "NormalQuestions";
        public const string SetsQuestions = "SetsQuestions";
        public const string ExamQuestions = "ExamQuestions";
    }
    public enum TestTypes
    {
       Test = 1,
       Mock,
       StudentTest
    }
    public enum StudyTypes
    {
       Book = 1,
        DistanceLearning,
        TestOnly,
    }
    public struct ArtifactTypes
    {
        public const string Audio = "Audio";
        public const string Video = "Video";
        public const string Image = "Image";
       public const string pdf = "pdf";
        public const string xlxs = "xlxs";
        public const string docx = "docx";
    }
    public struct Gender
    {
        public const string Male = "Male";
        public const string Female = "Female";

    }
}
