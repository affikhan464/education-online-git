﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class Advice
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Detail { get; set; }

        [ForeignKey("AdviceType")]
        public int AdviceTypeId { get; set; }
        public virtual AdviceType AdviceType { get; set; }

        [ForeignKey("QuestionChoice")]
        public int? QuestionChoiceId { get; set; }
        public virtual QuestionChoice QuestionChoice { get; set; }
    }
}
