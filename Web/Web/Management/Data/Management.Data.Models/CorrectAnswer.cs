﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Management.Data.Models
{
    public class CorrectAnswer
    {
        [Key]
        public int Id { get; set; }
        [MaxLength]
        public string Answer { get; set; }

        [ForeignKey("Question")]
        public int QuestionId { get; set; }
        public virtual Question Qustion { get; set; }

        [ForeignKey("QuestionChoice")]
        public int? QuestionChoiceId { get; set; }
        public virtual QuestionChoice QuestionChoice { get; set; }
    }
}
