﻿using Management.Data.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Management.Data.Interfaces
{
    public interface IDatabaseContext
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbSet<Advice> Advices { get; set; }
        DbSet<AdviceArtifact> AdviceArtifacts { get; set; }
        DbSet<AdviceType> AdviceTypes { get; set; }
        DbSet<Answer> Answers { get; set; }
        DbSet<Artifact> Artifacts { get; set; }
        DbSet<ArtifactType> ArtifactTypes { get; set; }
        DbSet<BoardLevel> BoardLevels { get; set; }
        DbSet<Class> Classes { get; set; }
        DbSet<Chapter> Chapters { get; set; }
        DbSet<CorrectAnswer> CorrectAnswers { get; set; }
        DbSet<Commentary> Commentaries { get; set; }
        DbSet<DegreeLevel> DegreeLevels { get; set; }
        DbSet<DegreeType> DegreeTypes { get; set; }
        DbSet<EducationLevel> EducationLevels { get; set; }
        DbSet<Education> Educations { get; set; }
        DbSet<Experience> Experiences { get; set; }
        DbSet<PowerSlide> PowerSlides { get; set; }
        DbSet<Question> Questions { get; set; }
        DbSet<QuestionAdvice> QuestionAdvices { get; set; }
        DbSet<QuestionCategory> QuestionCategories { get; set; }
        DbSet<QuestionChoice> QuestionChoices { get; set; }
        DbSet<QuestionDifficultyLevel> QuestionDifficultyLevels { get; set; }
        DbSet<QustionType> QustionTypes { get; set; }
        DbSet<RequestStatus> RequestStatuses { get; set; }
        DbSet<Section> Sections { get; set; }
        DbSet<SectionQuestion> SectionQuestions { get; set; }
        DbSet<Subject> Subjects { get; set; }
        DbSet<SubSection> SubSections { get; set; }
        DbSet<SubSectionQuestion> SubSectionQuestions { get; set; }
        DbSet<Tour> Tours { get; set; }
        DbSet<UserArtifact> UserArtifacts { get; set; }
        DbSet<UserEducation> UserEducations { get; set; }
        DbSet<UserExperience> UserExperiences { get; set; }
        DbSet<UserChapterRequest> UserChapterRequests { get; set; }
        DbSet<UserSubjectRequest> UserSubjectRequests { get; set; }
        DbSet<UserQuestionAttempt> UserQuestionAttempts { get; set; }
        DbSet<Word> Words { get; set; }
        DbSet<User> Users { get; set; }
        DbSet<UserRole> UserRoles { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<UserChapter> UserChapters { get; set; }
        DbSet<UserSubject> UserSubjects { get; set; }
        DbSet<WordAdvice> WordAdvices { get; set; }
        DbSet<WordQuestion> WordQuestions { get; set; }
        DbSet<WordCommentary> WordCommentaries { get; set; }
        DbSet<Set> Sets { get; set; }
        DbSet<SetQuestion> SetQuestions { get; set; }
        DbSet<Test> Tests { get; set; }
        DbSet<TestType> TestTypes { get; set; }
        DbSet<TestQuestion> TestQuestions { get; set; }
        DbSet<UserTestAttempt> UserTestAttempts { get; set; }
        DbSet<SubjectDemoChapter> SubjectDemoChapters { get; set; }
        DbSet<Batch> Batches { get; set; }
        DbSet<UserBatch> UserBatches { get; set; }
        DbSet<UserBatchRequest> UserBatchRequests { get; set; }
        DbSet<City> Cities { get; set; }
        DbSet<Region> Regions { get; set; }
        DbSet<Country> Countries { get; set; }
        DbSet<BatchTest> BatchTests { get; set; }
        DbSet<StudyType> StudyTypes { get; set; }
        DbSet<StudentStudyType> StudentStudyTypes { get; set; }
        DbSet<Schedule> Schedules { get; set; }
        DbSet<VoucherType> VoucherTypes { get; set; }
        DbSet<GLAccount> GLAccounts { get; set; }
        DbSet<GeneralLedger> GeneralLedgers { get; set; }
        DbSet<GLLevel> GLLevels { get; set; }
        DbSet<BillType> BillTypes { get; set; }
        DbSet<UserBatchReferral> UserBatchReferrals { get; set; }
        DbSet<PartyLedger> PartiesLedgers { get; set; }
        DbSet<PrimeBook> PrimeBooks { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));

        EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}
