﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface ISetQuestionRepository : IGenericRepository<SetQuestion>
    {
        List<QuestionModel> GetBySetId(int setId);
    }
}
