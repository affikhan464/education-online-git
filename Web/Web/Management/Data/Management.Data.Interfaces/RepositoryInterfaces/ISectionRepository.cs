﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface ISectionRepository : IGenericRepository<Section>
    {
        Section GetWithChilds(int id);
    }
}
