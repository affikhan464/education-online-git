﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface ISubjectRepository : IGenericRepository<Subject>
    {
       
        Subject GetWithChilds(int id);
        List<SubjectWithParents> ListOfSubjectsRequested(string userId);
        List<SubjectWithParents> ListOfSubjectsAllocatedToUser(string userId, bool showDeAllocatedToo);
        Subject GetWithChapters(int id);
        List<Subject> GetWithDemoChapter(int parentId);
    }
}
