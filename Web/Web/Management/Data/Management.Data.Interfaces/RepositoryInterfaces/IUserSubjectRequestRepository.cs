﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface IUserSubjectRequestRepository : IGenericRepository<UserSubjectRequest>
    {
        List<UserSubjectRequest> GetRequests(string userId);
    }
}
