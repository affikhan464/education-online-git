﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface ISetRepository : IGenericRepository<Set>
    {
        List<SetWithQuestions> GetSetWithQuestions(int attributeId, string attributeType);
        List<SetWithQuestions> GetSetWithQuestionsByTestId(int attributeId, string attributeType,int testId);
    }
}
