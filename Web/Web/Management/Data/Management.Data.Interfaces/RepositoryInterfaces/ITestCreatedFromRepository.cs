﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface ITestCreatedFromRepository : IGenericRepository<TestCreatedFrom>
    {

    }
}
