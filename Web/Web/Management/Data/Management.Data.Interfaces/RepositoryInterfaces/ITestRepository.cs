﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface ITestRepository : IGenericRepository<Test>
    {
        Task<Test> GetTestWithQuestions(int id);
        Task<TestModel> GetInProgressTest(int id, string userId);
    }
}
