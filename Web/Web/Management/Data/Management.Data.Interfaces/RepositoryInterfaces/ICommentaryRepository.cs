﻿using Management.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Management.Data.Models.CustomModels;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface ICommentaryRepository : IGenericRepository<Commentary>
    {
        CommentaryModel GetCommentary(int id);
        List<CommentaryModel> GetManyAvailableCommentaries(string attributeId, string attributeType, int wordId);
        List<CommentaryModel> GetManyCommentaries(int wordId);
    }
}
