﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface IQuestionRepository : IGenericRepository<Question>
    {
        QuestionModel GetQuestion(int id);
        QuestionModel GetSetsQuestion(int id);
        List<QuestionModel> GetManyQuestions(int wordId);
        List<QuestionModel> GetManyAvailableQuestions(string attributeId, string attributeType, int wordId);
        QuestionModel GetWithAnswer(int id);
        List<QuestionModel> GetAttributeQuestions(string attributeId, string attributeType);
        QuestionModel GetExamQuestion(int id);
    }
}
