﻿using Management.Data.Models;
using Management.Data.Models.CustomModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Data.Interfaces.RepositoryInterfaces
{
    public interface IChapterRepository : IGenericRepository<Chapter>
    {
        Chapter GetWithChilds(int id);
        List<ChapterWithParents> ListOfChaptersAllocatedToUser(string userId, bool showDeAllocatedToo);
        List<ChapterWithParents> ListOfChaptersRequested(string userId);
        Chapter GetWithSection(int id);
        List<Chapter> GetListWithChilds(List<int> ids);
    }
}
