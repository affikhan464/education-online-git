﻿using Microsoft.Extensions.ProjectModel;
using Microsoft.VisualStudio.Web.CodeGeneration;
using Microsoft.VisualStudio.Web.CodeGeneration.CommandLine;
using Microsoft.VisualStudio.Web.CodeGeneration.DotNet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Management.Scaffolding.CodeGenerator
{
    [Alias("Managementgen")]
   public  class Generator : ICodeGenerator
    {
        private readonly ICodeGeneratorActionsService _codeGeneratorActionService;
        private readonly IProjectContext _projectContext;
        private readonly IApplicationInfo _applicationInfo;

        private static string solutionPath = "D:\\Projects\\StartUp\\Management\\";
        private static string interfacePath = solutionPath + "Data\\Management.Data.Interfaces\\RepositoryInterfaces\\";
        private static string repositoryPath = solutionPath + "Data\\Management.Data\\Repositories\\";
        private static string bindingFilePath = solutionPath + "Infrastructure\\Management.Infrastructure.DependencyResolution\\RepositoryModule.cs";
        private static string dbContextPath = solutionPath + "Data\\Management.Data\\DataContext\\DatabaseContext.cs";
        private static string pathToModel = solutionPath + "\\Data\\Management.Data.Models";


        private static string interfaceTemplate = "Interface.cshtml";
        private static string repositoryTemplate = "Repository.cshtml";
        private static string controllerTemplate = "Controller.cshtml";
        

        public Generator(ICodeGeneratorActionsService codeGeneratorActionService,IProjectContext projectContext,IApplicationInfo applicationInfo)
        {
            _codeGeneratorActionService = codeGeneratorActionService ?? throw new ArgumentNullException(nameof(codeGeneratorActionService));
            _projectContext = projectContext ?? throw new ArgumentNullException(nameof(projectContext)); 
            _applicationInfo = applicationInfo ?? throw new ArgumentNullException(nameof(applicationInfo)); 
        }

        public async Task GenerateCode(ControllerModel model)
        {

            if(model == null)
            {
                throw new ArgumentNullException(nameof(model));
            }

            if (string.IsNullOrWhiteSpace(model.ModelName))
            {
                throw new ArgumentNullException(nameof(model.ModelName));
            }

            if (!File.Exists($"{solutionPath}\\Data\\Management.Data.Models\\{model.ModelName}.cs"))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("the model file does not exist.");
                return;
            }

            var templateFolders = GetTemplateFolders();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Generating I{model.ModelName}Repository.....");

            //Generate Repository interface
            await _codeGeneratorActionService.AddFileFromTemplateAsync($"{interfacePath}I{model.ModelName}Repository.cs", interfaceTemplate, templateFolders, model);

            Console.WriteLine($"Generating {model.ModelName}Repository.....");
            //Generate Repository 
            await _codeGeneratorActionService.AddFileFromTemplateAsync($"{repositoryPath}{model.ModelName}Repository.cs", repositoryTemplate, templateFolders, model);

            Console.WriteLine($"Generating {model.ModelName}Controller.....");
            //Generate Controller
            await _codeGeneratorActionService.AddFileFromTemplateAsync($"{_applicationInfo.ApplicationBasePath}\\Controllers\\{model.ModelName}Controller.cs", controllerTemplate, templateFolders, model);


            Console.WriteLine("Adding DbSet....");
            //EditBindings
            AddDbSet(dbContextPath, model);


            Console.WriteLine("Adding Dependency Injection Bindings....");
            //EditBindings
            AddBindings(bindingFilePath, model);

            Console.ResetColor();

        }

        private List<string> GetTemplateFolders()
        {

             return TemplateFoldersUtilities.GetTemplateFolders(
              containingProject: this.GetType().GetTypeInfo().Assembly.GetName().Name,
              applicationBasePath: solutionPath,
              baseFolders: new[] { "Controller", "Interface", "Repository" },
              projectContext: _projectContext
              );
 
        }

        private void AddDbSet(string filePath , ControllerModel model)
        {
            var allText = File.ReadAllText(filePath);
            if(!string.IsNullOrWhiteSpace(allText))
            {
              
                int lastIndex = -1;

                if(allText.LastIndexOf("set") > -1)
                {
                    lastIndex = allText.LastIndexOf("set") + 2;
                }else
                {
                    lastIndex = allText.LastIndexOf('{') + 2;
                }



                var textBefore = allText.Substring(0, lastIndex + 1);
                var textAfter = allText.Substring(lastIndex+1);
                var newText = $"\n public DbSet<{model.ModelName}> {model.ModelName.ToLower()}s" + "{get;set;}";

                File.WriteAllText(filePath, $"{textBefore}{newText}{textAfter}");



            }
        }

        private void AddBindings(string filePath, ControllerModel model)
        {
            var allText = File.ReadAllText(filePath);

            if (!string.IsNullOrWhiteSpace(allText))
            {
                var lastIndex = allText.LastIndexOf(';');
                if (lastIndex > -1)
                {
                    var textBefore = allText.Substring(0, lastIndex + 1);
                    var textAfter = allText.Substring(lastIndex);
                    var newText = $"services.AddTransient<I{model.ModelName}Repository,{model.ModelName}Repository>()";

                    File.WriteAllText(filePath, textBefore + "\n" + newText + textAfter);
                }
            }
        }

        private void ScaffoldRollback(string modelName)
        {
            //remove repository interface
            var repositoryInterface = interfacePath + "\\" + "I" + modelName + "Repository.cs";
            if (File.Exists(repositoryInterface))
            {
                File.Delete(repositoryInterface);

            }
            //remove repository

            var repository = repositoryPath + "\\" + modelName + "Repository.cs";
            if(File.Exists(repository))
            {
                File.Delete(repository);
            }

            //remove controller

            var controller = $"{_applicationInfo.ApplicationBasePath}\\Controllers\\{modelName}Controller.cs";
            if(File.Exists(controller))
            {
                File.Delete(controller);
            }

            //remove DbSet

            var dbContextCode = File.ReadAllText(dbContextPath);
            if(!string.IsNullOrWhiteSpace(dbContextCode))
            {
               var replacedCode = dbContextCode.Replace($"public DbSet<{modelName}> {modelName.ToLower()}s {{get;set;}}", "");
                File.WriteAllText(dbContextPath, replacedCode);
            }

            //remove binding

            var repositoryModuleCode = File.ReadAllText(bindingFilePath);
            if (!string.IsNullOrWhiteSpace(repositoryPath))
            {
                var replacedBindings = repositoryModuleCode.Replace($"services.AddTransient<I{modelName}Repository,{modelName}Repository>();", "");
                File.WriteAllText(bindingFilePath, replacedBindings);
            }



        }
       
    }

    public class ControllerModel
    {
        [Option(Name = "ModelName", ShortName = "name", Description = "Name of the model")]
        public string ModelName { get; set; }
    }
}
